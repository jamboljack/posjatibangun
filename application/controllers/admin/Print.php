<?php
defined('BASEPATH') or exit('No direct script access allowed');

class print extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library("EscPos");
    }

    public function index()
    {
        try {
            $connector = new FilePrintConnector("/dev/usb/lp0");
            /* Print a "Hello world" receipt" */
            $printer = new Printer($connector);
            $printer->text("Hello World!\n");
            $printer->cut();
            /* Close printer */
            $printer->close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
    }
}
/* Location: ./application/controller/admin/Print.php */

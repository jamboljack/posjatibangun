<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bank extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/bank_m');
    }

    public function index()
    {
        $this->template->display('admin/master/bank_v');
    }

    public function data_list()
    {
        $List = $this->bank_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row     = array();
            $bank_id = $r->bank_id;
            $row[]   = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $bank_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a onclick="hapusData(' . $bank_id . ')" title="Delete Data">
                            <i class="icon-close"></i>
                        </a>';
            $row[]  = $no;
            $row[]  = $r->bank_nama;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->bank_m->count_all(),
            "recordsFiltered" => $this->bank_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function nama_exists($nama)
    {
        $this->db->where('bank_nama', $nama);
        $query = $this->db->get('ok_bank');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_nama_exists()
    {
        if (array_key_exists('nama', $_POST)) {
            if ($this->nama_exists($this->input->post('nama', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->bank_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->bank_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->bank_m->update_data();
    }

    public function deletedata($id)
    {
        $this->bank_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Bank.php */

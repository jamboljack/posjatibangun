<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_hutang_status extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_hutang_status_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $this->template->display('admin/reporthutang/reporthutangstatus_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_hutang_status_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->suplier_nama;
            $row[]  = $r->pembelian_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = number_format($r->pembelian_total, 0, '', ',');
            $row[]  = number_format($r->pembelian_dibayar, 2, '.', ',');
            $row[]  = number_format($r->pembelian_sisa_hutang, 0, '', ',');
            $row[]  = ($r->pembelian_sisa_hutang==0?'<span class="label label-success">LUNAS</span>':'<span class="label label-danger">BELUM LUNAS</span>');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_hutang_status_m->count_all(),
            "recordsFiltered" => $this->lap_hutang_status_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printstatus($suplier = 'all', $status = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($suplier != 'all') {
            $data['listData'] = $this->db->order_by('suplier_nama', 'asc')->get_where('ok_suplier', array('suplier_id' => $suplier))->result();
        } else {
            $data['listData'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        }

        $this->load->view('admin/reporthutang/printstatus_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_hutang_status.php */

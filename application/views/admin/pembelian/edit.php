<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>

<style type="text/css">
    .nominal{
        text-align: right;
    }
</style>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Pembelian Barang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/pembelian');?>">Pembelian Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Edit Pembelian Barang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-edit"></i> Data Pembelian
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                <input type="hidden" name="suplier_id" id="suplier_id" value="<?=$detail->suplier_id;?>">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">No. Faktur</label>
                                            <div class="col-md-9">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control" placeholder="Input No. Faktur" name="no_faktur" id="no_faktur" autocomplete="off" value="<?=$detail->pembelian_no_faktur;?>" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tanggal</label>
                                            <div class="col-md-5">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <input type="text" class="form-control date-picker" placeholder="DD-MM-YYYY" name="tanggal" id="tanggal" value="<?=date('d-m-Y', strtotime($detail->pembelian_tanggal));?>" autocomplete="off">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-truck"></i> Data Suplier
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formDataSuplier" name="formDataSuplier">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Kode</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="kode_suplier" value="<?=$detail->suplier_kode;?>"  autocomplete="off" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Nama</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="nama_suplier" id="nama_suplier" autocomplete="off" value="<?=$detail->suplier_nama;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" name="alamat" id="alamat" autocomplete="off" readonly rows="2"><?=$detail->suplier_alamat;?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-credit-card"></i> Total Transaksi
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                <input type="hidden" name="totalpembelian" id="totalpembelian">
                                <input type="hidden" name="dibayar" value="<?=$detail->pembelian_dibayar;?>">
                                <input type="hidden" name="tipe_set" value="<?=$detail->tipe_bayar_set;?>">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">Kasir :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$this->session->userdata('nama');?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalinvoice"></div>
                                        </div>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pembelian Barang 
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form method="post" id="formBarang" role="form" name="formBarang" class="form">
                        <input type="hidden" name="barang_id" id="barang_id">
                        <input type="hidden" name="unit_id" id="unit_id">
                        <input type="hidden" name="pembelian_detail_id" id="pembelian_detail_id">
                        <input type="hidden" name="qty_lama" id="qty_lama">
                        <input type="hidden" name="pembelian_id" value="<?=$detail->pembelian_id;?>">
                        <input type="hidden" name="harga_pokok" id="harga_pokok">
                        <input type="hidden" name="harga_beli" id="harga_beli">
                        <input type="hidden" name="disc1" id="disc1">
                        <input type="hidden" name="disc2" id="disc2">
                        <input type="hidden" name="disc3" id="disc3">
                        <input type="hidden" name="disc4" id="disc4">
                        <input type="hidden" name="ppn" id="ppn">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Kode Barang</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="kode_barang" id="kode_barang" placeholder="Cari Kode Barang" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <a data-toggle="modal" data-target="#formDataBarang" title="Cari Data Barang">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="-" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Jumlah</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control digit" name="qty" id="qty" autocomplete="off" placeholder="0" onkeyup="hitungSubTotal()" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Unit <a onclick="ubahUnit()" title="Ubah Unit"><i class="icon-pencil"></i></a></label>
                                            <input type="text" class="form-control" name="satuan" id="satuan" readonly>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Harga Beli</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control number" name="harga" id="harga" autocomplete="off" placeholder="0" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Sub Total</label>
                                            <input type="text" placeholder="0" class="form-control nominal" name="total" id="total" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <p>
                                            <strong id="labeldisc1"></strong>
                                            <strong id="labeldisc2"></strong>
                                            <strong id="labeldisc3"></strong>
                                            <strong id="labeldisc4"></strong>
                                            <strong id="labelppn"></strong>
                                        </p>
                                    </div>
                                    <div class="col-md-2">
                                        <p align="right">
                                            <strong id="labelstok"></strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions" align="center">
                                <button type="button" class="btn btn-primary" id="btn_item" name="btn_item" disabled><i class="fa fa-floppy-o"></i> Simpan</button>
                                <a onclick="resetForm()" class="btn btn-danger" id="btn_reset" name="btn_reset" disabled><i class="fa fa-refresh"></i> Reset</a>
                                <a class="btn btn-primary" id="btn_bayar" data-toggle="modal" data-target="#formModalBayar"><i class="fa fa-credit-card"></i> Bayar</a>
                                <a href="<?=site_url('admin/pembelian');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Batal</a>
                                <a href="<?=site_url('admin/pembelian/printfaktur/'.$detail->pembelian_id);?>" type="button" class="btn btn-danger" target="_blank"><i class="icon-printer"></i> Print</a>
                            </div>
                        </form>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="5%">No</th>
                                    <th width="10%">Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th width="5%">Jumlah</th>
                                    <th width="5%">Unit</th>
                                    <th width="10%">Harga Beli</th>
                                    <th width="5%">Disc1</th>
                                    <th width="5%">Disc2</th>
                                    <th width="5%">Disc3</th>
                                    <th width="5%">Disc4</th>
                                    <th width="5%">PPN</th>
                                    <th width="10%">Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
var statusinput;
statusinput = 'Tambah';

$(document).ready(function() {
    $('#formDataBarang').on('shown.bs.modal', function () {
       var table = $('#tableDataBarang').DataTable();
       table.columns.adjust();
    });

    $('#formDataUnit').on('shown.bs.modal', function () {
       var table = $('#tableUnit').DataTable();
       table.columns.adjust();
    });

    $('#labeldisc1').text('Disc 1 : 0.00 %');
    $('#labeldisc2').text('Disc 2 : 0.00 %');
    $('#labeldisc3').text('Disc 3 : 0.00 %');
    $('#labeldisc4').text('Disc 4 : 0.00 %');
    $('#labelppn').text('PPN : 0.00 %');
    $('#labelstok').text('Stok : 0');
});

$('#kode_barang').keydown(function (e) {
    if (e.which === 9 || e.which == 13){
        var kode_barang = $('#kode_barang').val();
        if (kode_barang === '') {
            swal({
                title:"Info",
                text: "Mohon Isi Kode Barang",
                timer: 2000,
                showConfirmButton: false,
                type: "info"
            });
        } else {
            $.ajax({
                url : "<?=site_url('admin/pembelian/get_data_barang_by_kode/'); ?>" + kode_barang,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    if (data === null) {
                        swal({
                            title:"Info",
                            text: "Kode Barang tidak ditemukan",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "info"
                        });
                    } else {
                        var locale          = 'en';
                        var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                        var formatter       = new Intl.NumberFormat(locale, options);
                        $('#barang_id').val(data.barang_id);
                        $('#kode_barang').val(data.barang_kode);
                        $('#nama_barang').val(data.barang_nama);
                        $('#unit_id').val(data.unit_id);
                        $('#satuan').val(data.unit_nama);
                        $('#harga_beli').val(data.unit_hrg_beli);
                        $('#harga').val(formatter.format(data.unit_hrg_beli));
                        $('#qty').val('');
                        $('#disc1').val(data.unit_disc1);
                        $('#disc2').val(data.unit_disc2);
                        $('#disc3').val(data.unit_disc3);
                        $('#disc4').val(data.unit_disc4);
                        $('#ppn').val(data.unit_ppn);
                        $('#harga_pokok').val(data.unit_hrg_pokok);
                        // $('#total').val(formatter.format(data.unit_hrg_pokok));
                        $('#total').val('');
                        $('#labeldisc1').text('Disc 1 : '+data.unit_disc1+' %');
                        $('#labeldisc2').text('Disc 2 : '+data.unit_disc2+' %');
                        $('#labeldisc3').text('Disc 3 : '+data.unit_disc3+' %');
                        $('#labeldisc4').text('Disc 4 : '+data.unit_disc4+' %');
                        $('#labelppn').text('PPN : '+data.unit_ppn+' %');
                        $('#labelstok').text('Stok : '+formatter.format(data.unit_qty));
                        document.formBarang.qty.disabled=false;
                        document.formBarang.harga.disabled=false;
                        document.formBarang.btn_item.disabled=false;
                        $("#btn_reset").attr("disabled", false);
                        $('#qty').focus();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data Barang from ajax');
                }
            });
        }
        e.preventDefault();
    }
});

// Cek Harga Beli jika Berubah atau Tidak
$('#harga').keydown(function (e) {
    if (e.which === 9 || e.which == 13){
        var harga = $('#harga').val();
        if (harga === '' || harga == 0) {
            swal({
                title:"Info",
                text: "Mohon Isi Harga Barang",
                timer: 2000,
                showConfirmButton: false,
                type: "info"
            });
        } else {
            var locale      = 'en';
            var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter   = new Intl.NumberFormat(locale, options);
            var options2    = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter2  = new Intl.NumberFormat(locale, options2);
            var myForm      = document.formBarang;
            var barang_id   = myForm.barang_id.value;
            var unit_id     = myForm.unit_id.value;
            var HargaLama   = myForm.harga_beli.value;
            var HargaBeli   = myForm.harga.value;
            HargaBeli       = HargaBeli.replace(/[,]/g, '');
            HargaBeli       = parseInt(HargaBeli);
            if (HargaBeli != HargaLama) {
                $('#formEditHargaJual')[0].reset();
                $.ajax({
                    url : "<?=site_url('admin/pembelian/get_data_unit/');?>"+unit_id,
                    type: "GET",
                    dataType: "JSON",
                    success: function(data) {
                        $('#barang_id_ubah').val(data.barang_id);
                        $('#unit_id_ubah').val(data.unit_id);
                        $('#unit_id_multiple').val(data.unit_multiple);
                        $('#unit_nama').val(data.unit_nama);
                        $('#unit_hrg_beli').val(formatter.format(HargaBeli));
                        $('#unit_hrg_beli_old').val(data.unit_hrg_beli);
                        $('#unit_disc1').val(formatter2.format(data.unit_disc1));
                        $('#unit_disc2').val(formatter2.format(data.unit_disc2));
                        $('#unit_disc3').val(formatter2.format(data.unit_disc3));
                        $('#unit_disc4').val(formatter2.format(data.unit_disc4));
                        $('#unit_ppn').val(formatter2.format(data.unit_ppn));
                        $('#unit_hrg_pokok').val(formatter.format(data.unit_hrg_pokok));
                        $('#unit_hrg_pokok_old').val(data.unit_hrg_pokok);
                        $('#unit_hrg_jual').val(formatter.format(data.unit_hrg_jual));
                        $('#unit_hrg_jual_old').val(data.unit_hrg_jual);
                        $('#unit_set').val(data.unit_set);
                        hitungPokok();
                        $('#formModalHargaJual').modal('show');
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Error get data from ajax');
                    }
                });
            } else {
                hitungSubTotal();
                btn_item.click();
            }
        }
        e.preventDefault();
    }
});

$(document).ready(function() {
    $('#formModalHargaJual').on('shown.bs.modal', function() {
        $('#unit_hrg_beli').focus();
    });

    $("body").on('keyup', "#unit_hrg_beli", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#unit_disc1", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#unit_disc2", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#unit_disc3", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#unit_disc4", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#unit_ppn", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#diskon", function(){
        hitungDiskon();
    });
});

function hitungPokok() {
    var locale      = 'en';
    var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formEditHargaJual;
    var HargaBeli   = myForm.edit_harga_beli.value;
    HargaBeli       = HargaBeli.replace(/[,]/g, '');
    HargaBeli       = parseInt(HargaBeli);
    var Disc1       = myForm.editdisc1.value;
    Disc1           = Disc1.replace(/[,]/g, '');
    Disc1           = parseFloat(Disc1);
    var Disc2       = myForm.editdisc2.value;
    Disc2           = Disc2.replace(/[,]/g, '');
    Disc2           = parseFloat(Disc2);
    var Disc3       = myForm.editdisc3.value;
    Disc3           = Disc3.replace(/[,]/g, '');
    Disc3           = parseFloat(Disc3);
    var Disc4       = myForm.editdisc4.value;
    Disc4           = Disc4.replace(/[,]/g, '');
    Disc4           = parseFloat(Disc4);
    var PPN         = myForm.editppn.value;
    PPN             = PPN.replace(/[,]/g, '');
    PPN             = parseFloat(PPN);
    if (isNaN(Disc1)) {
        Disc1 = 0;
    } else {
        Disc1 = Disc1;
    }
    if (isNaN(Disc2)) {
        Disc2 = 0;
    } else {
        Disc2 = Disc2;
    }
    if (isNaN(Disc3)) {
        Disc3 = 0;
    } else {
        Disc3 = Disc3;
    }
    if (isNaN(Disc4)) {
        Disc4 = 0;
    } else {
        Disc4 = Disc4;
    }
    if (isNaN(PPN)) {
        PPN = 0;
    } else {
        PPN = PPN;
    }
    
    var HargaPokok;
    if (HargaBeli == 0) {
        HargaPokok = 0;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc1)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc2)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc3)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon     = (HargaBeli*Disc4)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc3)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc4)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaPokok = HargaDisc2-Diskon3;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else {
        HargaPokok = HargaBeli;
    }

    if (PPN > 0) {
        Pajak      = (HargaPokok*PPN)/100;
        HargaPokok = (HargaPokok+Pajak);
    } else {
        HargaPokok = HargaPokok;
    }

    if (HargaPokok > 0) {
        myForm.edit_harga_pokok.value = formatter.format(HargaPokok);
    } else {
        myForm.edit_harga_pokok.value = 0;
    }
}

$(document).ready(function() {
    const myNumber = {
        digitGroupSeparator: ',',
        decimalCharacter: '.',
        maximumValue: '999999999999999',
        minimumValue: '0',
        decimalPlaces:'0',
    };
    $('.number').autoNumeric('init', myNumber);

    const myDigit = {
        digitGroupSeparator: ',',
        decimalCharacter: '.',
        maximumValue: '999999999999999.00',
        minimumValue: '0',
        decimalPlaces:'2',
    };
    $('.digit').autoNumeric('init', myDigit);
});

function hitungDiskon() {
    var locale              = 'en';
    var options             = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter           = new Intl.NumberFormat(locale, options);
    var myForm              = document.formBayar;
    var Subtotal            = myForm.bayar_subtotal.value;
    Subtotal                = Subtotal.replace(/[,]/g, '');
    Subtotal                = parseInt(Subtotal);
    var Diskon              = myForm.diskon.value;
    Diskon                  = Diskon.replace(/[,]/g, '');
    Diskon                  = parseInt(Diskon);

    if (Diskon === 0 || isNaN(Diskon)) {
        var TotalAkhir   = Subtotal;
    } else {
        var TotalAkhir   = (Subtotal-Diskon);
    }
    
    if (isNaN(TotalAkhir)) {        
        myForm.bayar_total.value = 0;
        $('#totalpembelian').val(0);
    } else {
        myForm.bayar_total.value = formatter.format(TotalAkhir);
        $('#totalpembelian').val(TotalAkhir);
    }
}

$(document).ready(function() {
    // $('.number').maskMoney({thousands:',', precision:0});
    // $('.digit').maskMoney({thousands:'', precision:2});
    hitungTotal();
    dataBarang();
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    var pembelian_id = '<?=$detail->pembelian_id;?>';
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "destoy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/pembelian/data_detail_list/')?>"+pembelian_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 5 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 6, 7, 8, 9, 10, 11, 12 ],
                "className": "text-right",
            }
        ],
    });    
});

function dataBarang() {
    var tableBarang;
    tableBarang = $('#tableDataBarang').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/pembelian/data_barang_list'); ?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 6 ],
                "className": "text-center",
            },
            {
                "targets": [ 5, 7 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihdata(id) {
    $.ajax({
        url : "<?=site_url('admin/pembelian/get_data_barang/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale          = 'en';
            var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter       = new Intl.NumberFormat(locale, options);
            $('#barang_id').val(data.barang_id);
            $('#kode_barang').val(data.barang_kode);
            $('#nama_barang').val(data.barang_nama);
            $('#unit_id').val(data.unit_id);
            $('#satuan').val(data.unit_nama);
            $('#harga_beli').val(data.unit_hrg_beli);
            $('#harga').val(formatter.format(data.unit_hrg_beli));
            $('#qty').val('');
            $('#disc1').val(data.unit_disc1);
            $('#disc2').val(data.unit_disc2);
            $('#disc3').val(data.unit_disc3);
            $('#disc4').val(data.unit_disc4);
            $('#ppn').val(data.unit_ppn);
            // $('#harga_pokok').val(data.unit_hrg_pokok);
            $('#harga_pokok').val(data.unit_hrg_pokok);
            // $('#total').val(formatter.format(data.unit_hrg_pokok));
            $('#total').val('');
            $('#labeldisc1').text('Disc 1 : '+data.unit_disc1+' %');
            $('#labeldisc2').text('Disc 2 : '+data.unit_disc2+' %');
            $('#labeldisc3').text('Disc 3 : '+data.unit_disc3+' %');
            $('#labeldisc4').text('Disc 4 : '+data.unit_disc4+' %');
            $('#labelppn').text('PPN : '+data.unit_ppn+' %');
            $('#labelstok').text('Stok : '+formatter.format(data.unit_qty));
            document.formBarang.qty.disabled=false;
            document.formBarang.harga.disabled=false;
            document.formBarang.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
            $('#formDataBarang').modal('hide');
            $('#qty').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hitungSubTotal() {
    var locale      = 'en';
    var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formBarang;
    var Qty         = myForm.qty.value;
    Qty             = Qty.replace(/[,]/g, ''); // Ini String
    Qty             = parseFloat(Qty); // Ini Integer
    var HargaBeli   = myForm.harga.value;
    HargaBeli       = HargaBeli.replace(/[,]/g, ''); // Ini String
    HargaBeli       = parseInt(HargaBeli); // Ini Integer
    var Disc1       = myForm.disc1.value;
    Disc1           = Disc1.replace(/[,]/g, '');
    Disc1           = parseFloat(Disc1);
    var Disc2       = myForm.disc2.value;
    Disc2           = Disc2.replace(/[,]/g, '');
    Disc2           = parseFloat(Disc2);
    var Disc3       = myForm.disc3.value;
    Disc3           = Disc3.replace(/[,]/g, '');
    Disc3           = parseFloat(Disc3);
    var Disc4       = myForm.disc4.value;
    Disc4           = Disc4.replace(/[,]/g, '');
    Disc4           = parseFloat(Disc4);
    var PPN         = myForm.ppn.value;
    PPN             = PPN.replace(/[,]/g, '');
    PPN             = parseFloat(PPN);
    // if (isNaN(Disc1)) {
    //     Disc1 = 0;
    // } else {
    //     Disc1 = Disc1;
    // }
    // if (isNaN(Disc2)) {
    //     Disc2 = 0;
    // } else {
    //     Disc2 = Disc2;
    // }
    // if (isNaN(Disc3)) {
    //     Disc3 = 0;
    // } else {
    //     Disc3 = Disc3;
    // }
    // if (isNaN(Disc4)) {
    //     Disc4 = 0;
    // } else {
    //     Disc4 = Disc4;
    // }
    // if (isNaN(PPN)) {
    //     PPN = 0;
    // } else {
    //     PPN = PPN;
    // }
    
    // var HargaPokok;
    // if (HargaBeli == 0) {
    //     HargaPokok = 0;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 == 0) {
    //     Diskon     = (HargaBeli*Disc1)/100;
    //     HargaPokok = HargaBeli-Diskon;
    // } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
    //     Diskon     = (HargaBeli*Disc2)/100;
    //     HargaPokok = HargaBeli-Diskon;
    // } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
    //     Diskon     = (HargaBeli*Disc3)/100;
    //     HargaPokok = HargaBeli-Diskon;
    // } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
    //     Diskon     = (HargaBeli*Disc4)/100;
    //     HargaPokok = HargaBeli-Diskon;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc2)/100;
    //     HargaPokok = HargaDisc1-Diskon2;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc3)/100;
    //     HargaPokok = HargaDisc1-Diskon2;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc4)/100;
    //     HargaPokok = HargaDisc1-Diskon2;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 == 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc2)/100;
    //     HargaDisc2 = HargaDisc1-Diskon2;
    //     Diskon3    = (HargaDisc2*Disc3)/100;
    //     HargaPokok = HargaDisc2-Diskon3;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc2)/100;
    //     HargaDisc2 = HargaDisc1-Diskon2;
    //     Diskon3    = (HargaDisc2*Disc3)/100;
    //     HargaDisc3 = HargaDisc2-Diskon3;
    //     Diskon4    = (HargaDisc3*Disc4)/100;
    //     HargaPokok = HargaDisc3-Diskon4;
    // } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
    //     Diskon1    = (HargaBeli*Disc1)/100;
    //     HargaDisc1 = HargaBeli-Diskon1;
    //     Diskon2    = (HargaDisc1*Disc2)/100;
    //     HargaDisc2 = HargaDisc1-Diskon2;
    //     Diskon3    = (HargaDisc2*Disc3)/100;
    //     HargaDisc3 = HargaDisc2-Diskon3;
    //     Diskon4    = (HargaDisc3*Disc4)/100;
    //     HargaPokok = HargaDisc3-Diskon4;
    // } else {
    //     HargaPokok = HargaBeli;
    // }

    var HargaPokok = myForm.harga_pokok.value;

    // if (PPN > 0) {
    //     Pajak      = (HargaPokok*PPN)/100;
    //     HargaPokok = (HargaPokok+Pajak);
    // } else {
    //     HargaPokok = HargaPokok;
    // }

    if (HargaPokok > 0) {
        myForm.harga_pokok.value = HargaPokok;
    } else {
        myForm.harga_pokok.value = 0;
    }

    var SubTotal;
    SubTotal    = (Qty*HargaPokok);

    if (SubTotal > 0) {
        myForm.total.value = formatter.format(SubTotal);
    } else {
        myForm.total.value = 0;
    }
}

function resetForm() {
    statusinput = 'Tambah';
    $('#barang_id').val('');
    $('#kode_barang').val('');
    $('#nama_barang').val('');
    $('#unit_id').val('');
    $('#satuan').val('');
    $('#qty').val('');
    $('#qty_lama').val('');
    $('#harga').val('');
    $('#disc1').val('');
    $('#disc2').val('');
    $('#disc3').val('');
    $('#disc4').val('');
    $('#ppn').val('');
    $('#harga_pokok').val('');
    $('#total').val('');
    $('#labeldisc1').text('Disc 1 : 0.00 %');
    $('#labeldisc2').text('Disc 2 : 0.00 %');
    $('#labeldisc3').text('Disc 3 : 0.00 %');
    $('#labeldisc4').text('Disc 4 : 0.00 %');
    $('#labelppn').text('PPN : 0.00 %');
    $('#labelstok').text('Stok : 0');
    document.formBarang.qty.disabled=true;
    document.formBarang.harga.disabled=true;
    $("#btn_item").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    dataBarang();
    $('#kode_barang').focus();
    var MValid = $("#formBarang");
    MValid.validate().resetForm();
    MValid.find(".has-success, .has-warning, .fa-warning, .fa-check").removeClass("has-success has-warning fa-warning fa-check");
    MValid.find("i.fa[data-original-title]").removeAttr('data-original-title');
}

// Simpan Transaksi
// $("#btn_bayar").click(function() {
//     dataString = $(".form").serialize();
//     $.ajax({
//         url: '<?=site_url('admin/pembelian/updatedata');?>',
//         type: "POST",
//         data: dataString,
//         success: function(data) {
//             swal({
//                 title:"Sukses",
//                 text: "Update Transaksi Berhasil",
//                 timer: 2000,
//                 showConfirmButton: false,
//                 type: "success"
//             }, function() {
//                 window.location="<?=site_url('admin/pembelian');?>";
//             })
//         },
//         error: function() {
//             swal({
//                 title:"Error",
//                 text: "Update Transaksi Gagal",
//                 timer: 2000,
//                 showConfirmButton: false,
//                 type: "error"
//             });
//         }
//     });
// });

$(document).ready(function() {
    var form        = $('#formEditHargaJual');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formEditHargaJual").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            edit_harga_beli: { required: true },
            edit_harga_jual: { required: true }
        },
        messages: {
            edit_harga_beli: { required :'Harga Beli required' },
            edit_harga_jual: { required :'Harga Jual required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_updateharga").click(function() {
        if($('#formEditHargaJual').valid()) {
            updateHargaJual();
        }
    });
});

function updateHargaJual() {
    dataString = $("#formEditHargaJual").serialize();
    $.ajax({
        url: '<?=site_url('admin/pembelian/updatedataunit');?>',
        type: "POST",
        data: dataString,
        dataType: 'JSON',
        success: function(data) {
            var unit_id = data.unit_id;
            $.ajax({
                url : "<?=site_url('admin/pembelian/get_data_unit/'); ?>" + unit_id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    var locale          = 'en';
                    var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                    var formatter       = new Intl.NumberFormat(locale, options);
                    $('#unit_id').val(data.unit_id);
                    $('#satuan').val(data.unit_nama);
                    $('#harga_beli').val(data.unit_hrg_beli);
                    $('#harga').val(formatter.format(data.unit_hrg_beli));
                    $('#disc1').val(data.unit_disc1);
                    $('#disc2').val(data.unit_disc2);
                    $('#disc3').val(data.unit_disc3);
                    $('#disc4').val(data.unit_disc4);
                    $('#ppn').val(data.unit_ppn);
                    $('#harga_pokok').val(data.unit_hrg_pokok);
                    $('#labeldisc1').text('Disc 1 : '+data.unit_disc1+' %');
                    $('#labeldisc2').text('Disc 2 : '+data.unit_disc2+' %');
                    $('#labeldisc3').text('Disc 3 : '+data.unit_disc3+' %');
                    $('#labeldisc4').text('Disc 4 : '+data.unit_disc4+' %');
                    $('#labelppn').text('PPN : '+data.unit_ppn+' %');
                    $('#labelstok').text('Stok : '+formatter.format(data.unit_qty));
                    $('#formModalHargaJual').modal('hide');
                    hitungSubTotal();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data from ajax');
                }
            });
        },
        error: function() {
            swal({
                title:"Error",
                text: "Update Data Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            $('#formModalHargaJual').modal('hide');
        }
    });
    return false;
}

$(document).ready(function() {
    var form    = $('form');
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);

    $("#formInput").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            no_faktur: { required: true },
            tanggal: { required: true }
        },
        messages: {
            no_faktur: { required :'No. Faktur required' },
            tanggal: { required :'Tanggal required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        }
    });

    $("#formBarang").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_barang: { required: true },
            qty: { required: true },
            harga: { required: true }
        },
        messages: {
            nama_barang: { required :'Nama Barang required' },
            qty: { required :'Jumlah required' },
            harga: { required :'Harga required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        }
    });

    $("#btn_item").click(function() {
        if($('form#formInput').valid()) {
            if($('form#formBarang').valid()) {
                simpanItem();
            }
        }
    });
});

function simpanItem() {
    if (statusinput == 'Tambah') {
        dataString = $(".form").serialize();
        $.ajax({
            url: '<?=site_url('admin/pembelian/saveitemdetail');?>',
            type: "POST",
            data: dataString,
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function() {
                swal({
                    title:"Error",
                    text: "Simpan Item Gagal",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "error"
                });
            }
        });
    } else {
        dataString = $(".form").serialize();
        $.ajax({
            url: '<?=site_url('admin/pembelian/updateitemdetail');?>',
            type: "POST",
            data: dataString,
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function() {
                swal({
                    title:"Error",
                    text: "Update Item Gagal",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "error"
                });
            }
        });
    }
}

// $(document).ready(function() {
//     var form    = $('form');
//     var error   = $('.alert-danger', form);
//     var success = $('.alert-success', form);

//     $("form").validate({
//         errorElement: 'span',
//         errorClass: 'help-block help-block-error',
//         focusInvalid: false,
//         ignore: "",
//         rules: {
//             no_faktur: { required: true },
//             tanggal: { required: true },
//             lstSuplier: { required: true },
//             nama_barang: { required: true },
//             qty: { required: true },
//             harga: { required: true }
//         },
//         messages: {
//             no_faktur: {
//                 required :'No. Faktur required'
//             },
//             tanggal: {
//                 required :'Tanggal required'
//             },
//             lstSuplier: {
//                 required :'Nama Suplier required'
//             },
//             nama_barang: {
//                 required :'Nama Barang required'
//             },
//             qty: {
//                 required :'Jumlah required'
//             },
//             harga: {
//                 required :'Harga required'
//             }
//         },
//         invalidHandler: function (event, validator) {
//             success.hide();
//             error.show();
//             Metronic.scrollTo(error, -200);
//         },
//         errorPlacement: function (error, element) {
//             var icon = $(element).parent('.input-icon').children('i');
//             icon.removeClass('fa-check').addClass("fa-warning");
//             icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
//         },
//         highlight: function (element) {
//             $(element)
//             .closest('.form-group').removeClass("has-success").addClass('has-error');
//         },
//         unhighlight: function (element) {
//         },
//         success: function (label, element) {
//             var icon = $(element).parent('.input-icon').children('i');
//             $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
//             icon.removeClass("fa-warning").addClass("fa-check");
//         },
//         submitHandler: function(form) {
//             if (statusinput == 'Tambah') {
//                 dataString = $(".form").serialize();
//                 $.ajax({
//                     url: '<?=site_url('admin/pembelian/saveitemdetail');?>',
//                     type: "POST",
//                     data: dataString,
//                     success: function(data) {
//                         resetForm();
//                         reload_table();
//                         hitungTotal();
//                     },
//                     error: function() {
//                         swal({
//                             title:"Error",
//                             text: "Simpan Item Gagal",
//                             timer: 2000,
//                             showConfirmButton: false,
//                             type: "error"
//                         });
//                     }
//                 });
//             } else {
//                 dataString = $(".form").serialize();
//                 $.ajax({
//                     url: '<?=site_url('admin/pembelian/updateitemdetail');?>',
//                     type: "POST",
//                     data: dataString,
//                     success: function(data) {
//                         resetForm();
//                         reload_table();
//                         hitungTotal();
//                     },
//                     error: function() {
//                         swal({
//                             title:"Error",
//                             text: "Update Item Gagal",
//                             timer: 2000,
//                             showConfirmButton: false,
//                             type: "error"
//                         });
//                     }
//                 });
//             }
//         }
//     });
// });

function hitungTotal() {
    var pembelian_id = '<?=$detail->pembelian_id;?>';
    $.ajax({
        url : "<?=site_url('admin/pembelian/get_data_total_detail/');?>"+pembelian_id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var Total;
            var TotalBeli;
            Total     = 0;
            TotalBeli = 0;
            if (data == null) {
                Total        = 0;
                TotalBeli    = 0;
            } else {
                var locale      = 'en';
                var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                var formatter   = new Intl.NumberFormat(locale, options);
                TotalBeli       = data.total;
                Total           = formatter.format(TotalBeli);
            }

            $('#totalpembelian').val(TotalBeli);
            $('#bayar_subtotal').val(Total);
            diskon = $('#diskon').val();
            diskon = diskon.replace(/[,]/g, ''); // Ini String
            diskon = parseInt(diskon); // Ini Integer
            totalinvoice = formatter.format(TotalBeli-diskon);
            $('#bayar_total').val(totalinvoice);
            $('#totalinvoice').text('Rp. '+totalinvoice);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get Total');
        }
    });
}

function edit_data(id) {
    statusinput = 'Edit';
    $.ajax({
        url : "<?=site_url('admin/pembelian/get_data_detail/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale      = 'en';
            var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter   = new Intl.NumberFormat(locale, options);
            var options1    = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1  = new Intl.NumberFormat(locale, options1);
            $('#pembelian_detail_id').val(data.pembelian_detail_id);
            $('#barang_id').val(data.barang_id);
            $('#kode_barang').val(data.barang_kode);
            $('#nama_barang').val(data.barang_nama);
            $('#qty').val(formatter1.format(data.pembelian_detail_qty));
            $('#qty_lama').val(data.pembelian_detail_qty);
            $('#unit_id').val(data.unit_id);
            $('#satuan').val(data.unit_nama);
            $('#harga').val(formatter.format(data.pembelian_detail_harga));
            $('#harga_beli').val(data.unit_hrg_beli);
            $('#disc1').val(data.pembelian_detail_disc1);
            $('#disc2').val(data.pembelian_detail_disc2);
            $('#disc3').val(data.pembelian_detail_disc3);
            $('#disc4').val(data.pembelian_detail_disc4);
            $('#ppn').val(data.pembelian_detail_ppn);
            $('#harga_pokok').val(data.pembelian_detail_harga_pokok);
            $('#labeldisc1').text('Disc 1 : '+data.pembelian_detail_disc1+' %');
            $('#labeldisc2').text('Disc 2 : '+data.pembelian_detail_disc2+' %');
            $('#labeldisc3').text('Disc 3 : '+data.pembelian_detail_disc3+' %');
            $('#labeldisc4').text('Disc 4 : '+data.pembelian_detail_disc4+' %');
            $('#labelppn').text('PPN : '+data.pembelian_detail_ppn+' %');
            $('#labelstok').text('');
            $('#total').val(formatter.format(data.pembelian_detail_total));
            document.formBarang.qty.disabled=false;
            document.formBarang.harga.disabled=false;
            document.formBarang.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
            $("#qty").focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hapusData(pembelian_detail_id) {
    var id = pembelian_detail_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Item ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/pembelian/deleteitemdetail')?>/"+id,
            type: "POST",
            success: function(data) {
                swal({
                    title:"Sukses",
                    text: "Hapus Item Berhasil",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Item Gagal');
            }
        });
    });
}

function ubahUnit() {
    var barang_id = document.getElementById("barang_id").value;
    if (barang_id != '') {
        tampilUnit(barang_id);
        $('#formDataUnit').modal('show');
    } else {
        swal({
            title:"Info",
            text: "Mohon Pilih Barang Dahulu.",
            showConfirmButton: false,
            type: "warning",
            timer: 2000
        });
    }
}

function tampilUnit(barang_id) {
    var tableUnit;
    tableUnit = $('#tableUnit').DataTable({
        "destroy": true,
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/pembelian/data_unit_list/'); ?>"+barang_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "text-center",
            },
            {
                "targets": [ 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihunit(id) {
    $.ajax({
        url : "<?=site_url('admin/pembelian/get_data_unit/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale          = 'en';
            var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter       = new Intl.NumberFormat(locale, options);
            $('#unit_id').val(data.unit_id);
            $('#satuan').val(data.unit_nama);
            $('#harga').val(formatter.format(data.unit_hrg_beli));
            $('#disc1').val(data.unit_disc1);
            $('#disc2').val(data.unit_disc2);
            $('#disc3').val(data.unit_disc3);
            $('#disc4').val(data.unit_disc4);
            $('#ppn').val(data.unit_ppn);
            $('#harga_pokok').val(data.unit_hrg_pokok);
            $('#total').val(formatter.format(data.unit_hrg_beli));
            $('#labeldisc1').text('Disc 1 : '+data.unit_disc1+' %');
            $('#labeldisc2').text('Disc 2 : '+data.unit_disc2+' %');
            $('#labeldisc3').text('Disc 3 : '+data.unit_disc3+' %');
            $('#labeldisc4').text('Disc 4 : '+data.unit_disc4+' %');
            $('#labelppn').text('PPN : '+data.unit_ppn+' %');
            $('#labelstok').text('Stok : '+formatter.format(data.unit_qty));
            $('#formDataUnit').modal('hide');
            hitungSubTotal();
            $('#qty').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formBayar');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formBayar").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            lstTipeBayar: { required: true }
        },
        messages: {
            lstTipeBayar: { required :'Tipe Bayar required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_simpan").click(function() {
        if($('#formBayar').valid()) {
            simpanTransaksi();
        }
    });
});

// Simpan Transaksi
function simpanTransaksi() {
    dataString = $(".form").serialize();
    $.ajax({
        url: '<?=site_url('admin/pembelian/updatedata');?>',
        type: "POST",
        data: dataString,
        success: function(data) {
            $('#formModalBayar').modal('hide');
            swal({
                title:"Sukses",
                text: "Update Transaksi Berhasil",
                timer: 2000,
                showConfirmButton: false,
                type: "success"
            }, function() {
                window.location="<?=site_url('admin/pembelian');?>";
            })
        },
        error: function() {
            swal({
                title:"Error",
                text: "Update Transaksi Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            $('#formModalBayar').modal('hide');
        }
    });
}
</script>

<div class="modal fade bs-modal-lg" id="formDataBarang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-list"></i> Daftar Barang</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataBarang">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="10%">Kode Barang</th>
                            <th width="40%">Nama Barang</th>
                            <th width="15%">Merk</th>
                            <th width="5%">Stok</th>
                            <th width="10%">Unit</th>
                            <th width="10%">Harga Beli</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="formCariSuplier" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-truck"></i> Daftar Suplier</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataSuplier">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="15%">Nama Suplier</th>
                            <th width="40%">Alamat</th>
                            <th width="15%">Kota</th>
                            <th width="10%">No. Telp</th>
                            <th width="10%">Kontak</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formDataUnit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-list"></i> Daftar Unit</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableUnit">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th>Unit</th>
                            <th width="10%">Multiple</th>
                            <th width="10%">Stok</th>
                            <th width="12%">Harga Beli</th>
                            <th width="5%">Disc1</th>
                            <th width="5%">Disc2</th>
                            <th width="5%">Disc3</th>
                            <th width="5%">Disc4</th>
                            <th width="5%">PPN</th>
                            <th width="12%">Harga Pokok</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="formModalHargaJual" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formEditHargaJual" name="formEditHargaJual" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Form Edit Harga Jual</h4>
                    <input type="hidden" name="barang_id_ubah" id="barang_id_ubah">
                    <input type="hidden" name="unit_id_ubah" id="unit_id_ubah">
                    <input type="hidden" name="unit_id_multiple" id="unit_id_multiple">
                    <input type="hidden" name="unit_set" id="unit_set">
                    <input type="hidden" name="unit_hrg_beli_old" id="unit_hrg_beli_old">
                    <input type="hidden" name="unit_hrg_pokok_old" id="unit_hrg_pokok_old">
                    <input type="hidden" name="unit_hrg_jual_old" id="unit_hrg_jual_old">
                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Nama Unit</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="unit" id="unit_nama" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Beli</label>
                        <div class="col-md-4">
                            <input type="text" name="edit_harga_beli" id="unit_hrg_beli" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Disc 1-4 (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="editdisc1" id="unit_disc1" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="editdisc2" id="unit_disc2" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="editdisc3" id="unit_disc3" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="editdisc4" id="unit_disc4" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">PPN (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="editppn" id="unit_ppn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Harga Pokok (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="0" name="edit_harga_pokok" id="unit_hrg_pokok" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Jual</label>
                        <div class="col-md-4">
                            <input type="text" name="edit_harga_jual" id="unit_hrg_jual" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" id="btn_updateharga"><i class="fa fa-floppy-o"></i> Update</a>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalBayar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formBayar" name="formBayar" class="form-horizontal form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Pembayaran Transaksi</h4>
                    <input type="hidden" name="totalpembelian" id="totalpembelian">
                </div>
                <div class="modal-body" id="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label">Sub Total</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="bayar_subtotal" id="bayar_subtotal" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Diskon (Rp)</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control number" name="diskon" id="diskon" value="<?=number_format($detail->pembelian_disc,0,'',',');?>" placeholder="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tipe Bayar</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="lstTipeBayar" id="lstTipeBayar">
                                        <option value="">- Pilih Tipe Bayar -</option>
                                        <?php foreach ($listTipe as $r) { ?>
                                        <option value="<?=$r->tipe_bayar_id;?>" <?=($detail->tipe_bayar_id==$r->tipe_bayar_id?'selected':'');?>><?=$r->tipe_bayar_nama;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label"><b>TOTAL</b></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="bayar_total" id="bayar_total" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Penerima</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="penerima" id="penerima" value="<?=$detail->pembelian_penerima;?>" placeholder="Input Penerima" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Checker</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="checker" id="checker" value="<?=$detail->pembelian_checker;?>" placeholder="Input Checker" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" id="btn_simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</a>
                </div>
            </form>
        </div>
    </div>
</div>
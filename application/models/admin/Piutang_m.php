<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Piutang_m extends CI_Model
{
    public $table        = 'v_piutang';
    public $column_order = array(null, null, 'piutang_no_faktur', 'piutang_tanggal', 'pelanggan_nama',
        'pelanggan_alamat', 'piutang_total', 'tipe_bayar_nama');
    public $column_search = array('piutang_no_faktur', 'piutang_tanggal', 'pelanggan_nama',
        'pelanggan_alamat', 'piutang_total');
    public $order = array('piutang_id' => 'desc');

    public $table1         = 'v_tmp_piutang';
    public $column_order1  = array(null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('piutang_temp_id' => 'asc');

    public $table2        = 'v_penjualan';
    public $column_order2 = array(null, null, 'penjualan_no_faktur', 'penjualan_tanggal', 'penjualan_total',
        'penjualan_dibayar', 'penjualan_sisa_piutang');
    public $column_search2 = array('penjualan_no_faktur');
    public $order2         = array('penjualan_id' => 'desc');

    public $table4         = 'v_piutang_detail';
    public $column_order4  = array(null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('piutang_detail_id' => 'asc');

    public $table5         = 'v_piutang_pelanggan';
    public $column_order5  = array(null, null, 'pelanggan_nama', 'pelanggan_alamat', 'pelanggan_kota', 'totalpiutang');
    public $column_search5 = array('pelanggan_nama', 'pelanggan_alamat', 'pelanggan_kota', 'totalpiutang');
    public $order5         = array('pelanggan_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('piutang_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('piutang_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstPelanggan', 'true')) {
            $this->db->where('pelanggan_id', $this->input->post('lstPelanggan', 'true'));
        }
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Pelanggan
    private function _get_pelanggan_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_pelanggan_datatables()
    {
        $this->_get_pelanggan_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_pelanggan_filtered()
    {
        $this->_get_pelanggan_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_pelanggan_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Pembelian
    private function _get_penjualan_datatables_query($id)
    {
        $this->db->from($this->table2);
        $this->db->where('pelanggan_id', $id);
        $this->db->where('tipe_bayar_set', 'K');
        $this->db->where('penjualan_sisa_piutang !=', 0);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_penjualan_datatables($id)
    {
        $this->_get_penjualan_datatables_query($id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_penjualan_filtered($id)
    {
        $this->_get_penjualan_datatables_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_penjualan_all($id)
    {
        $this->db->from($this->table2);
        $this->db->where('pelanggan_id', $id);
        $this->db->where('tipe_bayar_set', 'K');
        $this->db->where('penjualan_sisa_piutang !=', 0);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'penjualan_id'         => $this->input->post('penjualan_id', 'true'),
            'piutang_temp_tanggal' => date('Y-m-d'),
            'piutang_temp_total'   => intval(str_replace(",", "", $this->input->post('jumlahpiutang', 'true'))),
            'piutang_temp_bayar'   => intval(str_replace(",", "", $this->input->post('bayarpiutang', 'true'))),
            'piutang_temp_sisa'    => intval(str_replace(",", "", $this->input->post('sisa', 'true'))),
            'user_username'        => $username,
            'piutang_temp_update'  => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_piutang_temp', $data);

        // Update Kurangi Hutang Pelanggan
        $penjualan_id  = $this->input->post('penjualan_id', 'true');
        $dataPembelian = $this->db->get_where('ok_penjualan', array('penjualan_id' => $penjualan_id))->row();
        $sisapiutang   = $dataPembelian->penjualan_sisa_piutang;
        $dibayar       = $dataPembelian->penjualan_dibayar;
        $pembayaran    = intval(str_replace(",", "", $this->input->post('bayarpiutang', 'true')));
        $dataUpdate    = array(
            'penjualan_dibayar'      => ($dibayar + $pembayaran),
            'penjualan_sisa_piutang' => ($sisapiutang - $pembayaran),
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $dataUpdate);
    }

    public function update_data_item()
    {
        $piutang_temp_id = $this->input->post('piutang_temp_id', 'true');
        $data            = array(
            'piutang_temp_bayar'  => intval(str_replace(",", "", $this->input->post('bayarpiutang', 'true'))),
            'piutang_temp_sisa'   => intval(str_replace(",", "", $this->input->post('sisa', 'true'))),
            'piutang_temp_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('piutang_temp_id', $piutang_temp_id);
        $this->db->update('ok_piutang_temp', $data);

        // Update Kurangi Hutang Pelanggan
        $penjualan_id   = $this->input->post('penjualan_id', 'true');
        $dataPembelian  = $this->db->get_where('ok_penjualan', array('penjualan_id' => $penjualan_id))->row();
        $sisapiutang    = $dataPembelian->penjualan_sisa_piutang;
        $dibayar        = $dataPembelian->penjualan_dibayar;
        $pembayaran     = intval(str_replace(",", "", $this->input->post('bayarpiutang', 'true')));
        $pembayaranlama = $this->input->post('bayar_piutang_lama', 'true');
        $dataUpdate     = array(
            'penjualan_dibayar'      => (($dibayar - $pembayaranlama) + $pembayaran),
            'penjualan_sisa_piutang' => (($sisapiutang + $pembayaranlama) - $pembayaran),
            'penjualan_update'       => date('Y-m-d H:i:s'),
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $dataUpdate);
    }

    public function delete_data_item($id)
    {
        // Update Pembayaran Hutang
        $dataItem      = $this->db->get_where('ok_piutang_temp', array('piutang_temp_id' => $id))->row();
        $penjualan_id  = $dataItem->penjualan_id;
        $pembayaran    = $dataItem->piutang_temp_bayar;
        $dataPembelian = $this->db->get_where('ok_penjualan', array('penjualan_id' => $penjualan_id))->row();
        $sisapiutang   = $dataPembelian->penjualan_sisa_piutang;
        $dibayar       = $dataPembelian->penjualan_dibayar;
        $dataUpdate    = array(
            'penjualan_dibayar'      => ($dibayar - $pembayaran),
            'penjualan_sisa_piutang' => ($sisapiutang + $pembayaran),
            'penjualan_update'       => date('Y-m-d H:i:s'),
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $dataUpdate);

        // Hapus Item
        $this->db->where('piutang_temp_id', $id);
        $this->db->delete('ok_piutang_temp');
    }

    public function getNoFaktur()
    {
        $this->db->select('COUNT(piutang_id) as total', false);
        $this->db->where('YEAR(piutang_tanggal)', date('Y'));
        $this->db->where('MONTH(piutang_tanggal)', date('m'));
        $query = $this->db->get('ok_piutang');
        if ($query->num_rows() != 0) {
            $data = $query->row();
            $kode = intval($data->total) + 1;
        } else {
            $kode = 1;
        }

        $noUrut = str_pad($kode, 5, "0", STR_PAD_LEFT);
        $noJual = $noUrut . '/PTG-' . date('m') . '/' . date('Y');
        return $noJual;
    }

    public function insert_data_piutang()
    {
        $username = $this->session->userdata('username');
        $noFaktur = $this->getNoFaktur();
        $data     = array(
            'piutang_no_faktur' => $noFaktur,
            'piutang_tanggal'   => date('Y-m-d'),
            'pelanggan_id'      => $this->input->post('pelanggan_id', 'true'),
            'tipe_bayar_id'     => $this->input->post('lstTipeBayar', 'true'),
            'piutang_total'     => intval(str_replace(",", "", $this->input->post('totaltransaksi', 'true'))),
            'user_username'     => $username,
            'piutang_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_piutang', $data);
        $piutang_id = $this->db->insert_id();

        // Simpan Detail Barang
        $listTemp = $this->db->get_where('ok_piutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $dataDetail = array(
                'piutang_id'            => $piutang_id,
                'penjualan_id'          => $r->penjualan_id,
                'piutang_detail_total'  => $r->piutang_temp_total,
                'piutang_detail_bayar'  => $r->piutang_temp_bayar,
                'piutang_detail_sisa'   => $r->piutang_temp_sisa,
                'piutang_detail_update' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_piutang_detail', $dataDetail);
        }

        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_piutang_temp');

        $response = ['status' => 'success', 'id' => $piutang_id];
        echo json_encode($response);
    }

    // Detail Edit
    private function _get_detail_datatables_query($piutang_id)
    {
        $this->db->from($this->table4);
        $this->db->where('piutang_id', $piutang_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($piutang_id)
    {
        $this->_get_detail_datatables_query($piutang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($piutang_id)
    {
        $this->_get_detail_datatables_query($piutang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($piutang_id)
    {
        $this->db->from($this->table4);
        $this->db->where('piutang_id', $piutang_id);

        return $this->db->count_all_results();
    }

    public function insert_data_detail()
    {
        $data = array(
            'piutang_id'            => $this->input->post('piutang_id', 'true'),
            'barang_id'             => $this->input->post('barang_id', 'true'),
            'piutang_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'               => $this->input->post('unit_id', 'true'),
            'piutang_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'piutang_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
            'piutang_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
            'piutang_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'piutang_detail_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_piutang_detail', $data);

        // Update Stok
        $barang_id     = $this->input->post('barang_id', 'true');
        $unit_id       = $this->input->post('unit_id', 'true');
        $qty           = intval(str_replace(",", "", $this->input->post('qty', 'true')));
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama + $qty);
        } else {
            $Stok = (($qty * $Multiple) + $StokUtama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function update_data_detail()
    {
        $piutang_detail_id = $this->input->post('piutang_detail_id', 'true');
        $barang_id         = $this->input->post('barang_id', 'true');
        $unit_id           = $this->input->post('unit_id', 'true');
        $qty               = intval(str_replace(",", "", $this->input->post('qty', 'true')));
        $qty_lama          = $this->input->post('qty_lama', 'true');
        $data              = array(
            'piutang_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'               => $this->input->post('unit_id', 'true'),
            'piutang_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'piutang_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
            'piutang_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
            'piutang_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'piutang_detail_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('piutang_detail_id', $piutang_detail_id);
        $this->db->update('ok_piutang_detail', $data);

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = (($StokUtama - $qty_lama) + $qty); // Stok Utama - Qty Lama + Qty Baru
        } else {
            $MultipleLama = ($qty_lama * $Multiple);
            $Stok         = (($StokUtama - $MultipleLama) + ($qty * $Multiple));
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function delete_data_detail($id)
    {
        // Hitung Stok dulu
        $dataLama  = $this->db->get_where('ok_piutang_detail', array('piutang_detail_id' => $id))->row();
        $barang_id = $dataLama->barang_id;
        $unit_id   = $dataLama->unit_id;
        $qty       = $dataLama->piutang_detail_qty;

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama - $qty); // Stok Utama - Qty
        } else {
            $MultipleLama = ($qty * $Multiple);
            $Stok         = ($StokUtama - $MultipleLama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }

        // Hapus Data
        $this->db->where('piutang_detail_id', $id);
        $this->db->delete('ok_piutang_detail');
    }

    public function update_data_piutang()
    {
        $piutang_id = $this->input->post('piutang_id', 'true');
        $data       = array(
            'piutang_no_faktur' => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'piutang_tanggal'   => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'pelanggan_id'      => $this->input->post('pelanggan_id', 'true'),
            'piutang_total'     => intval(str_replace(",", "", $this->input->post('totalpiutang', 'true'))),
            'user_username'     => $username,
            'piutang_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('piutang_id', $piutang_id);
        $this->db->update('ok_piutang', $data);
    }

    public function insert_data_pelanggan()
    {
        $data = array(
            'pelanggan_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'pelanggan_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_pelanggan', 'true')))),
            'pelanggan_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'pelanggan_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'pelanggan_disc'   => $this->input->post('disc', 'true'),
            'pelanggan_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_pelanggan', $data);
    }

    public function delete_data_piutang($id)
    {
        $listDetail = $this->db->order_by('piutang_detail_id', 'asc')->get_where('ok_piutang_detail', array('piutang_id' => $id))->result();
        foreach ($listDetail as $d) {
            // Hitung Stok dulu
            $piutang_detail_id = $d->piutang_detail_id;
            $dataLama          = $this->db->get_where('ok_piutang_detail', array('piutang_detail_id' => $piutang_detail_id))->row();
            $barang_id         = $dataLama->barang_id;
            $unit_id           = $dataLama->unit_id;
            $qty               = $dataLama->piutang_detail_qty;

            // Update Stok
            $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple      = $dataUnit->unit_multiple;
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            if ($unit_id == $unit_id_utama) {
                $Stok = ($StokUtama - $qty); // Stok Utama - Qty
            } else {
                $MultipleLama = ($qty * $Multiple);
                $Stok         = ($StokUtama - $MultipleLama);
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'    => $Stok,
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'    => ($Stok / $u->unit_multiple),
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Data
        $this->db->where('piutang_id', $id);
        $this->db->delete('ok_piutang');
    }
}
/* Location: ./application/model/admin/Piutang_m.php */

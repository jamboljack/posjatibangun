<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_piutang_pelanggan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_piutang_pelanggan_m');
    }

    public function index()
    {
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $this->template->display('admin/reportpiutang/reportpiutangpelanggan_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_piutang_pelanggan_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->piutang_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->piutang_tanggal));
            $row[]  = $r->tipe_bayar_nama;
            $row[]  = $r->pelanggan_nama;
            $row[]  = $r->penjualan_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]  = number_format($r->piutang_detail_total, 0, '', ',');
            $row[]  = number_format($r->piutang_detail_bayar, 2, '.', ',');
            $row[]  = number_format($r->piutang_detail_sisa, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_piutang_pelanggan_m->count_all(),
            "recordsFiltered" => $this->lap_piutang_pelanggan_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printpelanggan($dari = 'all', $sampai = 'all', $pelanggan = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($pelanggan != 'all') {
            $data['listData'] = $this->db->order_by('pelanggan_nama', 'asc')->get_where('ok_pelanggan', array('pelanggan_id' => $pelanggan))->result();
        } else {
            $data['listData'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        }

        $this->load->view('admin/reportpiutang/printpelanggan_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_piutang_pelanggan.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');
$active_group  = 'okpos';
$query_builder = true;
$db['okpos']   = array(
    'dsn'          => '',
    'hostname'     => 'localhost:3311',
    'username'     => 'root',
    'password'     => '',
    'database'     => 'db_jatibangun',
    'dbdriver'     => 'mysqli',
    'dbprefix'     => '',
    'pconnect'     => false,
    'db_debug'     => (ENVIRONMENT !== 'production'),
    'cache_on'     => false,
    'cachedir'     => '',
    'char_set'     => 'utf8',
    'dbcollat'     => 'utf8_general_ci',
    'swap_pre'     => '',
    'encrypt'      => false,
    'compress'     => false,
    'stricton'     => false,
    'failover'     => array(),
    'save_queries' => true,
);

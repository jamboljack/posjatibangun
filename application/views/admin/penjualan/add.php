<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<style type="text/css">
    .nominal{
        text-align: right;
    }
</style>
<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Penjualan Barang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/penjualan');?>">Penjualan Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Tambah Penjualan Barang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Data Penjualan
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                <input type="hidden" name="pelanggan_id" id="pelanggan_id">
                                <input type="hidden" name="pelanggan_termin" id="pelanggan_termin">
                                <input type="hidden" name="disc_pelanggan" id="disc_pelanggan">

                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Tanggal/Hari</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?=date('d-m-Y');?>" readonly>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="hari" id="hari" value="<?=getDay(date('Y-m-d'));?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Kode</label>
                                            <div class="col-md-9">
                                                <div class="input-group">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" class="form-control" name="kode_pelanggan" id="kode_pelanggan" autocomplete="off" placeholder="Cari Kode Pelanggan" autofocus>
                                                    </div>
                                                    <span class="input-group-btn">
                                                        <a data-toggle="modal" data-target="#formCariPelanggan" class="btn btn-success"><i class="fa fa-search"></i></a>
                                                        <a data-toggle="modal" data-target="#formModalPelanggan" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Nama Pelanggan</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan" placeholder="-" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="-" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-credit-card"></i> Total Transaksi
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">Kasir :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$this->session->userdata('nama');?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalinvoice"></div>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Penjualan Barang 
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form method="post" id="formBarang" role="form" name="formBarang" class="form">
                        <input type="hidden" name="barang_id" id="barang_id">
                        <input type="hidden" name="harga_pokok" id="harga_pokok">
                        <input type="hidden" name="unit_id" id="unit_id">
                        <input type="hidden" name="disc_rupiah" id="disc_rupiah">
                        <input type="hidden" name="stok_akhir" id="stok_akhir">
                        <input type="hidden" name="penjualan_temp_id" id="penjualan_temp_id">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Kode Barang</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="kode_barang" id="kode_barang" placeholder="Cari Kode Barang" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <a data-toggle="modal" data-target="#formDataBarang" title="Cari Data Barang">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Nama Barang</label>
                                            <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="-" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Jumlah</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control digit" name="qty" id="qty" autocomplete="off" placeholder="0" onkeyup="hitungSubTotal()" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Unit <a onclick="ubahUnit()" title="Ubah Unit"><i class="icon-pencil"></i></a></label>
                                            <input type="text" class="form-control" name="satuan" id="satuan" readonly>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Harga</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control number" name="harga" id="harga" autocomplete="off" placeholder="0" onkeyup="hitungSubTotal()" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>Disc (%)</label>
                                            <input type="text" placeholder="0.00" class="form-control digit nominal" name="disc" id="disc" onkeyup="hitungSubTotal()" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Sub Total</label>
                                            <input type="text" placeholder="0" class="form-control nominal" name="total" id="total" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            <strong id="labelpokok"></strong>
                                            <strong id="labelstok"></strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions" align="center">
                                <button type="button" class="btn btn-primary" id="btn_item" name="btn_item" disabled><i class="fa fa-floppy-o"></i> Simpan</button>
                                <a onclick="resetForm()" class="btn btn-danger" id="btn_reset" name="btn_reset" disabled><i class="fa fa-refresh"></i> Reset</a>
                                <a class="btn btn-primary" id="btn_bayar" data-toggle="modal" data-target="#formModalBayar" disabled><i class="fa fa-credit-card"></i> Bayar</a>
                                <a href="<?=site_url('admin/penjualan');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </form>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th width="10%">Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th width="5%">Jumlah</th>
                                    <th width="10%">Unit</th>
                                    <th width="10%">Harga</th>
                                    <th width="5%">Disc (%)</th>
                                    <th width="10%">Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formDataBarang').on('shown.bs.modal', function () {
       var table = $('#tableDataBarang').DataTable();
       table.columns.adjust();
    });

    $('#formCariPelanggan').on('shown.bs.modal', function () {
       var table = $('#tableDataPelanggan').DataTable();
       table.columns.adjust();
    });

    $('#formDataUnit').on('shown.bs.modal', function () {
       var table = $('#tableUnit').DataTable();
       table.columns.adjust();
    });

    $('#labelpokok').text('Harga Pokok : 0');
    $('#labelstok').text('Stok : 0');
    $('#BayarError').hide();
    $('.bank').hide();
});

$('#kode_pelanggan').keydown(function (e) {
    if (e.which === 9 || e.which == 13){
        var kode_pelanggan = $('#kode_pelanggan').val();
        if (kode_pelanggan === '') {
            swal({
                title:"Info",
                text: "Mohon Isi Kode Pelanggan",
                timer: 2000,
                showConfirmButton: false,
                type: "info"
            });
        } else {
            $.ajax({
                url : "<?=site_url('admin/penjualan/get_data_pelanggan_by_kode/'); ?>" + kode_pelanggan,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    if (data === null) {
                        swal({
                            title:"Info",
                            text: "Kode Suplier tidak ditemukan",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "info"
                        });
                    } else {
                        $('#pelanggan_id').val(data.pelanggan_id);
                        $('#kode_pelanggan').val(data.pelanggan_kode);
                        $('#nama_pelanggan').val(data.pelanggan_nama);
                        $('#alamat').val(data.pelanggan_alamat);
                        $('#disc_pelanggan').val(data.pelanggan_disc);
                        $('#pelanggan_termin').val(data.pelanggan_termin);
                        $("#btn_bayar").attr("disabled", false);
                        document.getElementById("btn_bayar").style.pointerEvents = "auto";
                        $('#kode_barang').focus();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data Suplier from ajax');
                }
            });
        }
        e.preventDefault();
    }
});

$('#kode_barang').keydown(function (e) {
    if (e.which === 9 || e.which == 13){
        var kode_barang = $('#kode_barang').val();
        if (kode_barang === '') {
            swal({
                title:"Info",
                text: "Mohon Isi Kode Barang",
                timer: 2000,
                showConfirmButton: false,
                type: "info"
            });
        } else {
            $.ajax({
                url : "<?=site_url('admin/penjualan/get_data_barang_by_kode/'); ?>" + kode_barang,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    if (data === null) {
                        swal({
                            title:"Info",
                            text: "Kode Barang tidak ditemukan",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "info"
                        });
                    } else {
                        harga_pokok = data.unit_hrg_pokok;
                        harga       = data.unit_hrg_jual;
                        bagi        = ((harga/harga_pokok) < 1); 
                        if (bagi === true) {
                            swal({
                                title:"Info",
                                text: "Harga Jual lebih rendah dari Harga Pokok",
                                timer: 2000,
                                showConfirmButton: false,
                                type: "info"
                            });
                        }
                        var disc_pelanggan  = document.getElementById("disc_pelanggan").value;
                        $('#barang_id').val(data.barang_id);
                        $('#kode_barang').val(data.barang_kode);
                        $('#nama_barang').val(data.barang_nama);
                        $('#unit_id').val(data.unit_id);
                        $('#satuan').val(data.unit_nama);
                        $('#harga_pokok').val(data.unit_hrg_pokok);
                        $('#harga').val(formatNumber(data.unit_hrg_jual));
                        $('#qty').val('');
                        $('#disc').val(disc_pelanggan);
                        $('#stok_akhir').val(data.unit_qty);
                        $('#total').val(formatNumber(data.unit_hrg_jual));
                        $('#labelpokok').text('Harga Pokok : '+formatNumber(data.unit_hrg_pokok));
                        $('#labelstok').text('Stok : '+formatNumber(data.unit_qty));
                        document.formBarang.qty.disabled=false;
                        <?php if ($this->session->userdata('level') == 'Admin') { ?>
                        $("#disc").prop('readonly', false);
                        $("#harga").prop('readonly', false);
                        <?php } else { ?>
                        $("#disc").prop('readonly', true);
                        $("#harga").prop('readonly', true);
                        <?php } ?>
                        document.formBarang.btn_item.disabled=false;
                        $("#btn_reset").attr("disabled", false);
                        hitungSubTotal();
                        $('#qty').focus();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error get data Barang from ajax');
                }
            });
        }
        e.preventDefault();
    }
});

$('#qty').keydown(function (e) {
    if (e.which === 9 || e.which == 13) {
        // Cek Qty dengan Stok Akhir
        let qty         = $(this).val();
        let stokakhir   = $('#stok_akhir').val();

        if (parseFloat(qty) > parseFloat(stokakhir)) {
            e.preventDefault(); 
            swal({
                title:"Informasi",
                text: "Jumlah melebihi Stok Barang",
                timer: 2000,
                showConfirmButton: false,
                type: "info"
            });
        }

        level = '<?=$this->session->userdata('level');?>';
        if (level != 'Admin') {
            $('#btn_item').click();
        }
    }
});

$('#harga').keydown(function (e) {
    if (e.which === 9 || e.which == 13){
        harga       = $('#harga').val();
        harga       = harga.replace(/[,]/g, '');
        harga       = parseInt(harga);
        harga_pokok = $('#harga_pokok').val();
        harga_pokok = parseInt(harga_pokok);
        level = '<?=$this->session->userdata('level');?>';
        if (level === 'Admin') {
            $('#btn_item').click();
        }

        // if (harga < harga_pokok) {
        //     swal({
        //         title:"Info",
        //         text: "Harga Jual lebih rendah dari Harga Pokok",
        //         timer: 2000,
        //         showConfirmButton: false,
        //         type: "info"
        //     });
        // }
    }
});

var statusinput;
statusinput = 'Tambah';

$(document).ready(function() {
    const myNumber = {
        digitGroupSeparator: ',',
        decimalCharacter: '.',
        maximumValue: '999999999999999',
        minimumValue: '0',
        decimalPlaces:'0',
    };
    $('.number').autoNumeric('init', myNumber);

    const myDigit = {
        digitGroupSeparator: ',',
        decimalCharacter: '.',
        maximumValue: '999999999999999.00',
        minimumValue: '0',
        decimalPlaces:'2',
    };
    $('.digit').autoNumeric('init', myDigit);
});

$(document).ready(function() {
    hitungTotal();
    dataBarang();
    dataPelanggan();
    document.getElementById("btn_bayar").style.pointerEvents = "none";
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/penjualan/data_temp_list')?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 5 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 6, 7, 8 ],
                "className": "text-right",
            }
        ],
    });    
});

function dataPelanggan() {
    var tablePelanggan;
    tablePelanggan = $('#tableDataPelanggan').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/penjualan/data_pelanggan_list'); ?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            }
        ],
    });
}

function dataBarang() {
    var tableBarang;
    tableBarang = $('#tableDataBarang').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/penjualan/data_barang_list'); ?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 7 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 5, 6, 7 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihpelanggan(id) {
    $.ajax({
        url : "<?=site_url('admin/penjualan/get_data_pelanggan/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#pelanggan_id').val(data.pelanggan_id);
            $('#kode_pelanggan').val(data.pelanggan_kode);
            $('#nama_pelanggan').val(data.pelanggan_nama);
            $('#alamat').val(data.pelanggan_alamat);
            $('#disc_pelanggan').val(data.pelanggan_disc);
            $('#pelanggan_termin').val(data.pelanggan_termin);
            $("#btn_bayar").attr("disabled", false);
            document.getElementById("btn_bayar").style.pointerEvents = "auto";
            $('#formCariPelanggan').modal('hide');
            $('#nama_barang').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function pilihdata(id) {
    $.ajax({
        url : "<?=site_url('admin/penjualan/get_data_barang/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            harga_pokok = data.unit_hrg_pokok;
            harga       = data.unit_hrg_jual;
            bagi        = ((harga/harga_pokok) < 1); 
            if (bagi === true) {
                swal({
                    title:"Info",
                    text: "Harga Jual lebih rendah dari Harga Pokok",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "info"
                });
            }

            var disc_pelanggan  = document.getElementById("disc_pelanggan").value;
            $('#barang_id').val(data.barang_id);
            $('#kode_barang').val(data.barang_kode);
            $('#nama_barang').val(data.barang_nama);
            $('#unit_id').val(data.unit_id);
            $('#satuan').val(data.unit_nama);
            $('#harga_pokok').val(data.unit_hrg_pokok);
            $('#harga').val(formatNumber(data.unit_hrg_jual));
            $('#qty').val('');
            $('#disc').val(disc_pelanggan);
            $('#stok_akhir').val(data.unit_qty);
            $('#total').val(formatNumber(data.unit_hrg_jual));
            $('#labelpokok').text('Harga Pokok : '+formatNumber(data.unit_hrg_pokok));
            $('#labelstok').text('Stok : '+formatNumber(data.unit_qty));
            document.formBarang.qty.disabled=false;
            <?php if ($this->session->userdata('level') == 'Admin') { ?>
            $("#disc").prop('readonly', false);
            $("#harga").prop('readonly', false);
            <?php } else { ?>
            $("#disc").prop('readonly', true);
            $("#harga").prop('readonly', true);
            <?php } ?>
            document.formBarang.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
            $('#formDataBarang').modal('hide');
            hitungSubTotal();
            $('#qty').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hitungSubTotal() {
    // var locale      = 'en';
    // var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    // var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formBarang;
    var Qty         = myForm.qty.value;
    Qty             = Qty.replace(/[,]/g, ''); // Ini String
    Qty             = parseFloat(Qty); // Ini Integer
    var Harga       = myForm.harga.value;
    Harga           = Harga.replace(/[,]/g, ''); // Ini String
    Harga           = parseInt(Harga); // Ini Integer
    var Disc        = myForm.disc.value;
    
    var SubTotal;
    if (Disc == '') {
        Disc        = 0;
        SubTotal    = (Qty*Harga);
    } else {
        Disc        = parseFloat(Disc); // Ini Float
        Disc        = (((Qty*Harga)*Disc)/100);
        SubTotal    = ((Qty*Harga)-Disc);
    }

    myForm.disc_rupiah.value = Disc;
    if (SubTotal > 0) {
        myForm.total.value = formatNumber(SubTotal);
    } else {
        myForm.total.value = 0;
    }
}

function resetForm() {
    statusinput = 'Tambah';
    $('#barang_id').val('');
    $('#kode_barang').val('');
    $('#nama_barang').val('');
    $('#satuan').val('');
    $('#qty').val(0);
    $('#disc').val('');
    $('#disc_rupiah').val(0);
    $('#harga_pokok').val(0);
    $('#harga').val(0);
    $('#total').val(0);
    $('#stok_akhir').val(0);
    $('#labelpokok').text('Harga Pokok : 0');
    $('#labelstok').text('Stok : 0');
    document.formBarang.qty.disabled=true;
    $("#disc").prop('readonly', true);
    $("#harga").prop('readonly', true);
    $("#btn_item").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    dataBarang();
    $('#kode_barang').focus();

    var MValid = $("#formBarang");
    MValid.validate().resetForm();
    MValid.find(".has-success, .has-warning, .fa-warning, .fa-check").removeClass("has-success has-warning fa-warning fa-check");
    MValid.find("i.fa[data-original-title]").removeAttr('data-original-title');
}

$(document).ready(function() {
    var form    = $('form');
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);

    $("#formInput").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            kode_pelanggan: { required: true }
        },
        messages: {
            kode_pelanggan: { required :'Kode Pelanggan required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        }
    });

    $("#formBarang").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            kode_barang: { required: true },
            qty: { required: true }
        },
        messages: {
            kode_barang: { required :'Kode Barang required' },
            qty: { required :'Jumlah required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        }
    });

    $("#btn_item").click(function() {
        if($('form#formInput').valid()) {
            if($('form#formBarang').valid()) {
                simpanItem();
            }
        }
    });
});

function simpanItem() {
    if (statusinput == 'Tambah') {
        dataString = $(".form").serialize();
        $.ajax({
            url: '<?=site_url('admin/penjualan/saveitem');?>',
            type: "POST",
            data: dataString,
            dataType:'JSON',
            success: function(data) {
                if (data.status === 'failed') {
                    swal({
                        title:"Perhatian",
                        text: "Total tidak boleh 0",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "warning"
                    });
                } else {
                    resetForm();
                    reload_table();
                    hitungTotal();
                }
            },
            error: function() {
                swal({
                    title:"Error",
                    text: "Simpan Item Gagal",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "error"
                });
            }
        });
    } else {
        dataString = $(".form").serialize();
        $.ajax({
            url: '<?=site_url('admin/penjualan/updateitem');?>',
            type: "POST",
            data: dataString,
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function() {
                swal({
                    title:"Error",
                    text: "Update Item Gagal",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "error"
                });
            }
        });
    }
}

// $(document).ready(function() {
//     var form    = $('form');
//     var error   = $('.alert-danger', form);
//     var success = $('.alert-success', form);

//     $("form").validate({
//         errorElement: 'span',
//         errorClass: 'help-block help-block-error',
//         focusInvalid: false,
//         ignore: "",
//         rules: {
//             kode_pelanggan: { required: true },
//             kode_barang: { required: true }
//         },
//         messages: {
//             kode_pelanggan: {
//                 required :'Kode Pelanggan required'
//             },
//             kode_barang: {
//                 required :'Kode Barang required'
//             }
//         },
//         invalidHandler: function (event, validator) {
//             success.hide();
//             error.show();
//             Metronic.scrollTo(error, -200);
//         },
//         errorPlacement: function (error, element) {
//             var icon = $(element).parent('.input-icon').children('i');
//             icon.removeClass('fa-check').addClass("fa-warning");
//             icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
//         },
//         highlight: function (element) {
//             $(element)
//             .closest('.form-group').removeClass("has-success").addClass('has-error');
//         },
//         unhighlight: function (element) {
//         },
//         success: function (label, element) {
//             var icon = $(element).parent('.input-icon').children('i');
//             $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
//             icon.removeClass("fa-warning").addClass("fa-check");
//         },
//         submitHandler: function(form) {
//             if (statusinput == 'Tambah') {
//                 dataString = $(".form").serialize();
//                 $.ajax({
//                     url: '<?=site_url('admin/penjualan/saveitem');?>',
//                     type: "POST",
//                     data: dataString,
//                     success: function(data) {
//                         resetForm();
//                         reload_table();
//                         hitungTotal();
//                     },
//                     error: function() {
//                         swal({
//                             title:"Error",
//                             text: "Simpan Item Gagal",
//                             timer: 2000,
//                             showConfirmButton: false,
//                             type: "error"
//                         });
//                     }
//                 });
//             } else {
//                 dataString = $(".form").serialize();
//                 $.ajax({
//                     url: '<?=site_url('admin/penjualan/updateitem');?>',
//                     type: "POST",
//                     data: dataString,
//                     success: function(data) {
//                         resetForm();
//                         reload_table();
//                         hitungTotal();
//                     },
//                     error: function() {
//                         swal({
//                             title:"Error",
//                             text: "Update Item Gagal",
//                             timer: 2000,
//                             showConfirmButton: false,
//                             type: "error"
//                         });
//                     }
//                 });
//             }
//         }
//     });
// });

function hitungTotal() {
    $.ajax({
        url : "<?=site_url('admin/penjualan/get_data_total_temp');?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var Total;
            var TotalJual;
            Total     = 0;
            TotalJual = 0;
            if (data == null) {
                Total        = 0;
                TotalJual    = 0;
            } else {
                // var locale      = 'en';
                // var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                // var formatter   = new Intl.NumberFormat(locale, options);
                if (data.total != null) {
                    TotalJual       = parseInt(data.total);
                    Total           = formatNumber(TotalJual);
                } else {
                    Total        = 0;
                    TotalJual    = 0;
                }
            }

            // $('#totalpenjualan').val(TotalJual);
            $('#bayar_subtotal').val(Total);
            $('#totalinvoice').text('Rp. '+Total);

            var PPNJual     = '<?=$meta->meta_ppn;?>';
            var PPN         = 0;
            var Bayar_Total = 0;
            if (PPNJual === 0) {
                PPN             = 0;
                var Bayar_Total = TotalJual;
            } else {
                PPN             = parseInt(((PPNJual*TotalJual)/100));
                var Bayar_Total = (parseInt(TotalJual)+PPN);
            }

            $('#ppn_rupiah').val(PPN);
            $('#totalpenjualan').val(Bayar_Total);
            $('#bayar_total').val(formatNumber(Bayar_Total));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get Total');
        }
    });
}

function edit_data(id) {
    statusinput = 'Edit';
    $.ajax({
        url : "<?=site_url('admin/penjualan/get_data_item/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale      = 'en';
            var options1    = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1  = new Intl.NumberFormat(locale, options1);
            $('#penjualan_temp_id').val(data.penjualan_temp_id);
            $('#barang_id').val(data.barang_id);
            $('#kode_barang').val(data.barang_kode);
            $('#nama_barang').val(data.barang_nama);
            $('#unit_id').val(data.unit_id);
            $('#qty').val(formatter1.format(data.penjualan_temp_qty));
            $('#satuan').val(data.unit_nama);
            $('#harga_pokok').val(data.penjualan_temp_harga_pokok);
            $('#disc').val(data.penjualan_temp_disc);
            $('#disc_rupiah').val(data.penjualan_temp_disc_rp);
            $('#harga').val(formatNumber(data.penjualan_temp_harga));
            $('#total').val(formatNumber(data.penjualan_temp_total));
            $('#labelpokok').text('Harga Pokok : '+formatNumber(data.penjualan_temp_harga_pokok));
            $('#labelstok').text('Stok : 0');
            <?php if ($this->session->userdata('level') == 'Admin') { ?>
            $("#disc").prop('readonly', false);
            $("#harga").prop('readonly', false);
            <?php } else { ?>
            $("#disc").prop('readonly', true);
            $("#harga").prop('readonly', true);
            <?php } ?>
            document.formBarang.qty.disabled=false;
            document.formBarang.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hapusData(penjualan_temp_id) {
    var id = penjualan_temp_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Item ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/penjualan/deleteitem')?>/"+id,
            type: "POST",
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Item Gagal');
            }
        });
    });
}

function ubahUnit() {
    var barang_id = document.getElementById("barang_id").value;
    if (barang_id != '') {
        tampilUnit(barang_id);
        $('#formDataUnit').modal('show');
    } else {
        swal({
            title:"Info",
            text: "Mohon Pilih Barang Dahulu.",
            showConfirmButton: false,
            type: "warning",
            timer: 2000
        });
    }
}

function tampilUnit(barang_id) {
    var tableUnit;
    tableUnit = $('#tableUnit').DataTable({
        "destroy": true,
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/penjualan/data_unit_list/'); ?>"+barang_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "text-center",
            },
            {
                "targets": [ 2, 3, 4 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihunit(id) {
    $.ajax({
        url : "<?=site_url('admin/penjualan/get_data_unit/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            // var locale          = 'en';
            // var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            // var formatter       = new Intl.NumberFormat(locale, options);
            $('#unit_id').val(data.unit_id);
            $('#satuan').val(data.unit_nama);
            $('#harga').val(formatNumber(data.unit_hrg_jual));
            $('#harga_pokok').val(data.unit_hrg_pokok);
            $('#total').val(formatNumber(data.unit_hrg_jual));
            $('#labelpokok').text('Harga Pokok : '+formatNumber(data.unit_hrg_pokok));
            $('#labelstok').text('Stok : '+formatNumber(data.unit_qty));
            $('#formDataUnit').modal('hide');
            hitungSubTotal();
            $('#qty').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function resetFormJual() {
    statusinput = 'Tambah';
    $('#pelanggan_id').val('');
    $('#kode_pelanggan').val('');
    $('#nama_pelanggan').val('');
    $('#disc_pelanggan').val('');
    $('#pelanggan_termin').val('');
    $('#lstTipeBayar').val('');
    $('#lstKirim').val('');
    $('#lstSales').val('');
    $('#alamat').val('');
    $('#barang_id').val('');
    $('#kode_barang').val('');
    $('#nama_barang').val('');
    $('#satuan').val('');
    $('#qty').val('');
    $('#disc').val('');
    $('#disc_rupiah').val('');
    $('#harga_pokok').val('');
    $('#harga').val('');
    $('#total').val('');
    $('#totalpenjualan').val('');
    $('#ppn_rupiah').val('');
    $('#tipe_set').val('');
    $('#bank').val('');
    $('#lstBank').val('');
    $('#nokartu').val('');
    $('#bayar_subtotal').val('');
    $('#diskon').val('');
    $('#ongkos').val('');
    $('#bayar_total').val('');
    $('#bayar').val('');
    $('#kembali').val('');
    $('#BayarError').hide();
    $('#labelpokok').text('Harga Pokok : 0');
    $('#labelstok').text('Stok : 0');
    document.formBarang.qty.disabled=true;
    $("#disc").prop('readonly', true);
    $("#harga").prop('readonly', true);
    document.formBarang.btn_item.disabled=false;
    $("#btn_bayar").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    document.getElementById("btn_bayar").style.pointerEvents = "none";
    dataBarang();
    dataPelanggan();
    hitungTotal();

    var MValid = $("form");
    MValid.validate().resetForm();
    MValid.find(".has-success, .has-warning, .fa-warning, .fa-check").removeClass("has-success has-warning fa-warning fa-check");
    MValid.find("i.fa[data-original-title]").removeAttr('data-original-title');
    document.getElementById("nama_pelanggan").focus();
}

function resetformPelanggan() {
    $("#kode").val('');
    $("#nama").val('');
    $("#alamat_pelanggan").val('');
    $("#kota").val('');
    $("#telp").val('');
    $("#disc_plg").val('');
    $("#termin_plg").val('');

    var MValid = $("#formPelanggan");
    MValid.validate().resetForm();
    MValid.find(".has-error").removeClass("has-error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

$(document).ready(function() {
    var form        = $('#formPelanggan');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formPelanggan").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            kode: { required: true, maxlength:6,
                remote: {
                    url: "<?=site_url('admin/penjualan/register_kode_exists');?>",
                    type: "post",
                    data: {
                        kode: function() {
                            return $("#kode").val();
                        }
                    }
                } 
            },
            nama: { required: true },
            alamat_pelanggan: { required: true },
            kota: { required: true },
            telp: { required: true }
        },
        messages: {
            kode: { required :'Kode required', maxlength:'Max. 6 Karakter', remote:'Kode sudah Ada' },
            nama: {
                required :'Nama Pelanggan required'
            },
            alamat_pelanggan: {
                required :'Alamat required'
            },
            kota: {
                required :'Kota required'
            },
            telp: {
                required :'No. Telp required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_pelanggan").click(function() {
        if($('#formPelanggan').valid()) {
            simpanPelanggan();
        }
    });
});

function simpanPelanggan() {
    dataString = $("#formPelanggan").serialize();
    $.ajax({
        url: '<?=site_url('admin/penjualan/savedatapelanggan');?>',
        type: "POST",
        data: dataString,
        success: function(data) {
            swal({
                title:"Sukses",
                text: "Simpan Data Pelanggan Sukses",
                timer: 2000,
                showConfirmButton: false,
                type: "success"
            });
            $('#formModalPelanggan').modal('hide');
            resetformPelanggan();
            dataPelanggan();
        },
        error: function() {
            swal({
                title:"Error",
                text: "Simpan Data Pelanggan Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            $('#formModalPelanggan').modal('hide');
            resetformPelanggan();
        }
    });
    return false;
}

function hitungDiskon() {
    // var locale              = 'en';
    // var options             = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    // var formatter           = new Intl.NumberFormat(locale, options);
    var myForm              = document.formBayar;
    var Subtotal            = myForm.bayar_subtotal.value;
    Subtotal                = Subtotal.replace(/[,]/g, '');
    Subtotal                = parseInt(Subtotal);
    var Diskon              = myForm.diskon.value;
    Diskon                  = Diskon.replace(/[,]/g, '');
    Diskon                  = parseInt(Diskon);
    var Ppn                 = myForm.bayar_ppn.value;
    Ppn                     = Ppn.replace(/[,]/g, '');
    Ppn                     = parseFloat(Ppn);
    var Ongkir              = myForm.ongkos.value;
    Ongkir                  = Ongkir.replace(/[,]/g, '');
    Ongkir                  = parseInt(Ongkir);
    var Total               = myForm.bayar_total.value;
    Total                   = Total.replace(/[,]/g, '');
    Total                   = parseInt(Total);

    if (Ppn === 0 || isNaN(Ppn)) {
        Pajak = 0;
    } else {
        Pajak = Ppn;
    }

    if (isNaN(Ongkir)){
        Ongkir = 0;
    }

    if (Diskon === 0 || isNaN(Diskon)) {
        var SubTotalAkhir   = Subtotal;
        var PPN             = ((Pajak*SubTotalAkhir)/100);
        var TotalAkhir      = (SubTotalAkhir+PPN+Ongkir);
    } else {
        var SubTotalAkhir   = (Subtotal-Diskon);
        var PPN             = ((Pajak*SubTotalAkhir)/100);
        var TotalAkhir      = (SubTotalAkhir+PPN+Ongkir);
    }
    
    if (isNaN(TotalAkhir)) {        
        myForm.bayar_total.value = 0;
        $('#totalpenjualan').val(0);
    } else {
        myForm.bayar_total.value = formatNumber(TotalAkhir);
        $('#totalpenjualan').val(TotalAkhir);
    }
}

function settingBayar() {
    // var locale    = 'en';
    // var options   = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    // var formatter = new Intl.NumberFormat(locale, options);
    var tipe      = document.getElementById("lstTipeBayar"); 
    var Setting   = tipe.options[(tipe.selectedIndex)].getAttribute('data-set');
    var Bank      = tipe.options[(tipe.selectedIndex)].getAttribute('data-bank');
    var myForm    = document.formBayar;
    var Total     = myForm.bayar_total.value;
    Total         = Total.replace(/[,]/g, '');
    Total         = parseInt(Total);
    if (Setting === 'D') {
        myForm.bayar.value   = '';
        myForm.kembali.value = 0;
        document.formBayar.bayar.disabled = false;
        document.formBayar.tipe_set.value = 'D';
    } else if (Setting === 'K') {
        myForm.bayar.value   = '';
        myForm.kembali.value = formatNumber(Total);
        document.formBayar.bayar.disabled = true;
        document.formBayar.tipe_set.value = 'K';
    } else {
        myForm.bayar.value   = '';
        myForm.kembali.value = 0;
        document.formBayar.bayar.disabled = false;
        document.formBayar.tipe_set.value = '';
    }

    if (Bank == 'Y') {
        $('.bank').show();
    } else {
        $('.bank').hide();
    }

    document.formBayar.bank.value = Bank;
}

function hitungKembalian() {
    // var locale              = 'en';
    // var options             = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    // var formatter           = new Intl.NumberFormat(locale, options);
    var myForm              = document.formBayar;
    var Total               = myForm.bayar_total.value;
    Total                   = Total.replace(/[,]/g, '');
    Total                   = parseInt(Total);
    var Bayar               = myForm.bayar.value;
    Bayar                   = Bayar.replace(/[,]/g, '');
    Bayar                   = parseInt(Bayar);
    var Kembali;
    if (Bayar < Total) {
        Kembali = 0;
    } else {
        Kembali = (Bayar-Total);
    }
    if (isNaN(Kembali)) {
        myForm.kembali.value     = 0;
    } else {
        myForm.kembali.value     = formatNumber(Kembali);
    }
}

$(document).ready(function() {
    var form        = $('#formBayar');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formBayar").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            lstTipeBayar: { required: true },
            lstKirim: { required: true },
            lstBank: {
                required: function(){
                    return $('#bank').val() == 'Y';
                }
            }
        },
        messages: {
            lstTipeBayar: { required :'Tipe Bayar required' },
            lstKirim: { required :'Status Kirim required' },
            lstBank: { required :'Bank required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_simpan").click(function() {
        if($('#formBayar').valid()) {
            simpanTransaksi();
        }
    });
});

function loading() {
    Metronic.blockUI({
        target: '#portlet-body',
        boxed: true,
        message: 'Proses Simpan Data...',
        textOnly: true
    });

    window.setTimeout(function() {
        Metronic.unblockUI('#portlet-body');
    }, 2000);
}

var statusprinter = '<?=$meta->meta_print_status;?>';
var printer       = new Recta('<?=$meta->meta_print_key;?>', '<?=$meta->meta_print_port;?>');
function simpanTransaksi() {
    loading();
    dataString = $(".form").serialize();
    $.ajax({
        url: '<?=site_url('admin/penjualan/savedata');?>',
        type: "POST",
        data: dataString,
        dataType: 'JSON',
        success: function(data) {
            if (data.status === 'error') {
                $('#BayarError').show();
                $('#BayarError').html(data.message);
            } else {
                if (data.id != '') {
                    swal({
                        title:"Sukses",
                        text: "Simpan Transaksi Sukses",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    
                    if (statusprinter == 2) {
                        var penjualan_id = data.id;
                        $.ajax({
                            url: '<?=site_url('admin/penjualan/get_data/');?>'+penjualan_id,
                            type: "POST",
                            dataType: 'JSON',
                            success: function(datap1) {
                                var locale        = 'en';
                                var options       = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                                var formatter     = new Intl.NumberFormat(locale, options);
                                var options1      = {minimumFractionDigits: 2, maximumFractionDigits: 2};
                                var formatter1    = new Intl.NumberFormat(locale, options1);
                                var NoOrder       = datap1.penjualan_no_faktur;
                                var Tanggal       = moment(datap1.penjualan_update).format('DD-MM-YYYY HH:mm');
                                var NamaPelanggan = datap1.pelanggan_nama;
                                var Kasir         = datap1.user_username;
                                Header(NoOrder, Tanggal, NamaPelanggan, Kasir);
                                $.ajax({
                                    url: '<?=site_url('admin/penjualan/get_list_item/');?>'+penjualan_id,
                                    type: "POST",
                                    dataType: 'JSON',
                                    success: function(dataitem1) {
                                        if (dataitem1 != null) {
                                            var x1 = dataitem1.length;
                                            for(var i = 0; i < x1; i++) {
                                                var NamaBarang = dataitem1[i].barang_nama;
                                                var Harga      = formatter.format(dataitem1[i].penjualan_detail_harga);
                                                var Qty        = formatter1.format(dataitem1[i].penjualan_detail_qty);
                                                var Unit       = dataitem1[i].unit_nama;
                                                var Subtotal   = formatter.format(dataitem1[i].penjualan_detail_total);
                                                ListItem(NamaBarang, Harga, Qty, Unit, Subtotal);
                                            }

                                            var TipeBayar  = datap1.tipe_bayar_nama;
                                            var SubTotal   = formatter.format(datap1.penjualan_bruto);
                                            var Diskon     = formatter.format(datap1.penjualan_diskon);
                                            var PPN        = datap1.penjualan_ppn;
                                            var Ongkir     = formatter.format(datap1.penjualan_ongkos);
                                            var Total      = formatter.format(datap1.total);
                                            var Bayar      = formatter.format(datap1.penjualan_bayar);
                                            var Kembali    = formatter.format(datap1.penjualan_kembali);
                                            Footer(TipeBayar, SubTotal, Diskon, PPN, Ongkir, Total, Bayar, Kembali);
                                            FooterEnd();
                                        }
                                    }
                                });
                            }
                        });
                    }
                } else {
                    swal({
                        title:"Info",
                        text: "Data Penjualan Tidak Ada",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "info"
                    });
                }

                $('#formModalBayar').modal('hide');
                reload_table();
                resetFormJual();
            }
        },
        error: function() {
            swal({
                title:"Error",
                text: "Simpan Transaksi Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            $('#formModalBayar').modal('hide');
            reload_table();
            resetFormJual();
        }
    });
    return false;
}

function Header(NoOrder, Tanggal, NamaPelanggan, Kasir) {
    var LimitChar = 36;
    if(NoOrder.length <= LimitChar) {
        txtNoOrder = NoOrder;
    } else {
        txtNoOrder = NoOrder.substring(0, LimitChar);
    }

    if(NamaPelanggan.length <= LimitChar) {
        txtNamaPelanggan = NamaPelanggan;
    } else {
        txtNamaPelanggan = NamaPelanggan.substring(0, LimitChar);
    }

    printer.open().then(function () {
      printer.align('center')
        .bold(true)
        .text('TOKO JATI BANGUN')
        .text('Jl. AKBP Agil Kusumadya No.110A, Jatiwetan')
        .text('KUDUS')
        .text('(0291)2911225')
        .text('WA : 087831346729')
        .bold(false)
        .text('------------------------------------------------')
        .print()
    })

    printer.open().then(function () {
      printer.align('left')
        .text('No. Nota  : '+txtNoOrder)
        .text('Tanggal   : '+Tanggal)
        .text('Pelanggan : '+txtNamaPelanggan)
        .text('Kasir     : '+Kasir)
        .text('------------------------------------------------')
        .text('Nama Barang                          Total Harga')
        .print()
    })
}

function ListItem(NamaBarang, Harga, Qty, Unit, Subtotal) {
    var limitNamaBarang = 48;
    var limitQty        = 6;
    var limitUnit       = 3;
    var limitHarga      = 15;
    var limitSubtotal   = 20;
    var txtBarang       = '';
    var txtHarga        = 0;
    var txtQty          = 0;
    var txtUnit         = '';
    var txtSubtotal     = 0;

    if(NamaBarang.length <= limitNamaBarang) {
        txtBarang = NamaBarang.padEnd(limitNamaBarang, ' ')
    } else {
        txtBarang = NamaBarang.substring(0, limitNamaBarang);
    }

    if (Harga.length <= limitHarga) {
        txtHarga = Harga.padStart(limitHarga, ' ')
    } else {
        txtHarga = Harga.substring(0, limitHarga);
    }

    if (Qty.length <= limitQty) {
        txtQty = Qty.padStart(limitQty, ' ')
    } else {
        txtQty = Qty.substring(0, limitQty);
    }

    if(Unit.length <= limitUnit) {
        txtUnit = Unit.padEnd(limitUnit, ' ')
    } else {
        txtUnit = Unit.substring(0, limitUnit);
    }

    if (Subtotal.length <= limitSubtotal) {
        txtSubtotal = Subtotal.padStart(limitSubtotal, ' ')
    } else {
        txtSubtotal = Subtotal.substring(0, limitSubtotal);
    }

    printer.open().then(function () {
      printer.align('left')
        .text(txtBarang)
        .text(txtQty+" "+txtUnit+"x "+txtHarga+" "+txtSubtotal)
        .print()
    })
}

function Footer(TipeBayar, SubTotal, Diskon, PPN, Ongkir, Total, Bayar, Kembali) {
    var limitNominal    = 13;
    var txtSubTotal     = 0;
    var txtDiskon       = 0;
    var txtPPN          = 0;
    var txtOngkir       = 0;
    var txtTotal        = 0;
    var txtBayar        = 0;
    var txtKembali      = 0;

    if (SubTotal.length <= limitNominal) {
        txtSubTotal = SubTotal.padStart(limitNominal, ' ')
    } else {
        txtSubTotal = SubTotal.substring(0, limitNominal);
    }

    if (Diskon.length <= limitNominal) {
        txtDiskon = Diskon.padStart(limitNominal, ' ')
    } else {
        txtDiskon = Diskon.substring(0, limitNominal);
    }

    if (PPN.length <= limitNominal) {
        txtPPN = PPN.padStart(limitNominal, ' ')
    } else {
        txtPPN = PPN.substring(0, limitNominal);
    }

    if (Ongkir.length <= limitNominal) {
        txtOngkir = Ongkir.padStart(limitNominal, ' ')
    } else {
        txtOngkir = Ongkir.substring(0, limitNominal);
    }

    if (Total.length <= limitNominal) {
        txtTotal = Total.padStart(limitNominal, ' ')
    } else {
        txtTotal = Total.substring(0, limitNominal);
    }

    if (Bayar.length <= limitNominal) {
        txtBayar = Bayar.padStart(limitNominal, ' ')
    } else {
        txtBayar = Bayar.substring(0, limitNominal);
    }
    
    if (Kembali.length <= limitNominal) {
        txtKembali = Kembali.padStart(limitNominal, ' ')
    } else {
        txtKembali = Kembali.substring(0, limitNominal);
    }

    printer.open().then(function () {
        printer.align('left')
        .text(TipeBayar)
        .text("                       Sub Total : "+txtSubTotal)
        .print()
    })

    if (Diskon != 0) {
        printer.open().then(function () {
            printer.align('left')
            .text("                          Diskon : "+txtDiskon)
            .text('                                   -------------')
            .print()
        })
    }

    if (txtPPN != 0) {
        printer.open().then(function () {
        printer.align('left')
            .text("                         PPN (%) : "+txtPPN)
            .print()
        })
    }

    if (txtOngkir != 0) {
        printer.open().then(function () {
        printer.align('left')
            .text("                    Ongkos Kirim : "+txtOngkir)
            .text('                                   -------------')
            .print()
        })
    }

    printer.open().then(function () {
        printer.align('left')
        .text("                           TOTAL : "+txtTotal)
        .text('                                   -------------')
        .print()
    })
    printer.open().then(function () {
        printer.align('left')
        .text("                           Bayar : "+txtBayar)
        .print()
    })
    printer.open().then(function () {
        printer.align('left')
        .text("                       Kembalian : "+txtKembali)
        .print()
    })
}

function FooterEnd() {
    printer.open().then(function () {
        printer.align('center')
        .text('------------------------------------------------')
        .text('Barang yang sudah dibeli,')
        .text('tidak bisa dikembalikan')
        .text('- TERIMA KASIH -')
        .text('')
        .feed(3)
        .cut()
        .print()
    })
}

$(document).ready(function() {
    $('#formModalBayar').on('shown.bs.modal', function() {
        $('#diskon').focus();
    });

    $("body").on('keyup', "#diskon", function(){
        hitungDiskon();
    });

    $("body").on('keyup', "#ongkos", function(){
        hitungDiskon();
    });

    $("body").on('keyup', "#bayar", function(){
        hitungKembalian();
    });
});
</script>

<div class="modal fade bs-modal-lg" id="formDataBarang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-list"></i> Daftar Barang</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataBarang">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="10%">Kode Barang</th>
                            <th width="30%">Nama Barang</th>
                            <th width="15%">Merk</th>
                            <th width="5%">Stok</th>
                            <th width="15%">Satuan</th>
                            <th width="15%">Kemasan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="formCariPelanggan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-users"></i> Daftar Pelanggan</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataPelanggan">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="10%">Kode</th>
                            <th width="15%">Nama Pelanggan</th>
                            <th width="40%">Alamat</th>
                            <th width="15%">Kota</th>
                            <th width="10%">No. Telp</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="formDataUnit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-list"></i> Daftar Unit</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableUnit">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="60%">Unit</th>
                            <th width="10%">Multiple</th>
                            <th width="10%">Stok</th>
                            <th width="15%">Harga</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="formModalPelanggan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formPelanggan" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Form Tambah Pelanggan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kode</label>
                        <div class="col-md-3">
                            <input type="text" class="form-control" placeholder="Input Kode" name="kode" id="kode" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Pelanggan</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Nama Pelanggan" name="nama" id="nama" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Alamat</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Alamat" name="alamat_pelanggan" id="alamat_pelanggan" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kota</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Kota" name="kota" id="kota" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">No. Telp</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input No. Telp" name="telp" id="telp" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Disc (%)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc_plg" id="disc_plg" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Termin (Hari)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="termin_plg" id="termin_plg" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" id="btn_pelanggan"><i class="fa fa-floppy-o"></i> Simpan</a>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalBayar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formBayar" name="formBayar" class="form-horizontal form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Pembayaran Transaksi</h4>
                    <input type="hidden" name="totalpenjualan" id="totalpenjualan">
                    <input type="hidden" name="ppn_rupiah" id="ppn_rupiah">
                    <input type="hidden" name="tipe_set" id="tipe_set">
                    <input type="hidden" name="bank" id="bank">
                </div>
                <div class="modal-body" id="portlet-body">
                    <div class="alert alert-warning" id="BayarError"></div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" style="text-align:center;" name="bayar_tanggal" id="bayar_tanggal" autocomplete="off" value="<?=date('d-m-Y');?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label">Sub Total</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" style="text-align:right;" name="bayar_subtotal" id="bayar_subtotal" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Diskon (Rp)</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control number" style="text-align:right;" name="diskon" id="diskon" placeholder="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label">PPN (%)</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control digit" style="text-align:right;" name="bayar_ppn" id="bayar_ppn" autocomplete="off" value="<?=$meta->meta_ppn;?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Ongkos</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control number" style="text-align:right;" name="ongkos" id="ongkos" placeholder="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label"><b>TOTAL</b></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" style="text-align:right;" name="bayar_total" id="bayar_total" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tipe Bayar</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="lstTipeBayar" id="lstTipeBayar" onchange="settingBayar()">
                                        <option value="">- Pilih Tipe Bayar -</option>
                                        <?php foreach ($listTipe as $r) { ?>
                                        <option value="<?=$r->tipe_bayar_id;?>" data-set="<?=$r->tipe_bayar_set;?>" data-bank="<?=$r->tipe_bayar_bank;?>"><?=$r->tipe_bayar_nama;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Sales</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="lstSales" id="lstSales">
                                        <option value="">- Pilih Sales -</option>
                                        <?php foreach ($listSales as $r) { ?>
                                        <option value="<?=$r->sales_id;?>"><?=$r->sales_kode.'-'.$r->sales_nama;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row bank">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Bank</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="lstBank" id="lstBank">
                                        <option value="">- Pilih Nama Bank -</option>
                                        <?php foreach ($listBank as $r) { ?>
                                        <option value="<?=$r->bank_id;?>"><?=$r->bank_nama;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">No. Kartu</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="nokartu" id="nokartu" placeholder="Input No. Kartu" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Kirim ?</label>
                                <div class="col-md-8">
                                    <select class="form-control" name="lstKirim" id="lstKirim">
                                        <option value="">- Pilih Kirim -</option>
                                        <option value="T">TIDAK</option>
                                        <option value="Y">YA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Bayar</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control number" style="text-align:right;" name="bayar" id="bayar" placeholder="0" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group form-md-line-input">
                                <label class="col-md-4 control-label">Kembali</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" style="text-align:right; font-weight: bold;" name="kembali" id="kembali" placeholder="0" autocomplete="off" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a class="btn btn-primary" id="btn_simpan"><i class="fa fa-floppy-o"></i> Simpan Transaksi</a>
                </div>
            </form>
        </div>
    </div>
</div>
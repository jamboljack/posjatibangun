<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Jatuh Tempo Piutang</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:16px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<?php 
$periode    = 'PERIODE : '.$this->uri->segment(5).' s/d '.$this->uri->segment(6);
$tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(5)));
$tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(6)));
?>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="font-size: 15px; font-weight: bold;"><u>JATUH TEMPO PIUTANG</u></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?=$periode;?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <th width="3%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="40%" style="border-left: 0.5px solid black; border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO. FAKTUR</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TANGGAL</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TOTAL</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">DIBAYAR</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">SISA</th>
            <th width="16%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TGL. TEMPO</th>
        </tr>
        <?php 
        $ttl   = 0;
        $dbyr  = 0;
        $ss    = 0;
        foreach($listData as $r) { 
            $pelanggan_id = $r->pelanggan_id;
            $listDetail   = $this->db->order_by('penjualan_tgl_tempo', 'asc')->get_where('v_penjualan', array('pelanggan_id' => $pelanggan_id, 'tipe_bayar_set' => 'K', 'penjualan_sisa_piutang !=' => 0, 'penjualan_tgl_tempo >=' => $tgl_dari, 'penjualan_tgl_tempo <=' => $tgl_sampai))->result();

            if (count($listDetail) > 0) {
        ?>
        <tr>
            <th align="left" colspan="7" style="border-left: 0.5px solid black; border-right: 0.5px solid black;">Pelanggan : <?=$r->pelanggan_nama;?></th>
        </tr>
        <?php
            $no      = 1;
            $total   = 0;
            $dibayar = 0;
            $sisa    = 0;
            foreach($listDetail as $d) {
        ?>
        <tr>
            <td align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$d->penjualan_no_faktur;?></td>
            <td align="center" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=date('d-m-Y', strtotime($d->penjualan_tanggal));?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($d->penjualan_total,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($d->penjualan_dibayar,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($d->penjualan_sisa_piutang,0,'',',');?></td>
            <td align="center" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=date('d-m-Y', strtotime($d->penjualan_tgl_tempo));?></td>
        </tr>
        <?php
            $total   = ($total+$d->penjualan_total);
            $dibayar = ($dibayar+$d->penjualan_dibayar);
            $sisa    = ($sisa+$d->penjualan_sisa_piutang);
            $no++; 
            }
        ?>
        <tr>
            <td colspan="3" align="center" style="border-left: 0.5px solid black; border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b>SUB TOTAL</b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($total,0,'',',');?></b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($dibayar,0,'',',');?></b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($sisa,0,'',',');?></b></td>
            <td style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"></td>
        </tr>
        <?php 
            $ttl   = ($total+$ttl);
            $dbyr  = ($dibayar+$dbyr);
            $ss    = ($sisa+$ss);
            }
        }
        ?>
        <tr>
            <td colspan="3" align="center" style="border-left: 0.5px solid black; border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b>TOTAL</b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($ttl,0,'',',');?></b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($dbyr,0,'',',');?></b></td>
            <td align="right" style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><b><?=number_format($ss,0,'',',');?></b></td>
            <td style="border-right: 0.5px solid black; border-bottom: 0.5px solid black;"></td>
        </tr>
    </table>
</div>
</body>
</html>
<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Pembayaran Hutang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Pembayaran Hutang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pembayaran Hutang
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData">
                                <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Filter Data</button>
                            </a>
                            <a href="<?=site_url('admin/hutang/adddata');?>">
                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th width="10%">No. Faktur</th>
                                    <th width="10%">Tanggal</th>
                                    <th width="20%">Nama Suplier</th>
                                    <th>Alamat</th>
                                    <th width="10%">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/hutang/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstSuplier = $('#lstSuplier').val();
                data.tgl_dari     = $('#tgl_dari').val();
                data.tgl_sampai   = $('#tgl_sampai').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3 ],
                "className": "text-center",
            },
            {
                "targets": [ 6 ],
                "className": "text-right",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

var printer = new Recta('<?=$meta->meta_print_key;?>', '<?=$meta->meta_print_port;?>');
function printNota(hutang_id) {
    $.ajax({
        url: '<?=site_url('admin/hutang/get_data/');?>'+hutang_id,
        type: "POST",
        dataType: 'JSON',
        success: function(datap1) {
            var locale        = 'en';
            var options       = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter     = new Intl.NumberFormat(locale, options);
            var options1      = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1    = new Intl.NumberFormat(locale, options1);
            var NoOrder       = datap1.hutang_no_faktur;
            var Tanggal       = datap1.hutang_tanggal;
            var NamaSuplier   = datap1.suplier_nama;
            var Kasir         = datap1.user_username;
            Header(NoOrder, Tanggal, NamaSuplier, Kasir);
            $.ajax({
                url: '<?=site_url('admin/hutang/get_list_item/');?>'+hutang_id,
                type: "POST",
                dataType: 'JSON',
                success: function(dataitem1) {
                    if (dataitem1 != null) {
                        var x1 = dataitem1.length;
                        for(var i = 0; i < x1; i++) {
                            var NoFaktur   = dataitem1[i].pembelian_no_faktur;
                            var TotalItem  = formatter.format(dataitem1[i].hutang_detail_total);
                            var DibayarItem= formatter.format(dataitem1[i].hutang_detail_bayar);
                            var Sisa       = formatter.format(dataitem1[i].hutang_detail_sisa);
                            ListItem(NoFaktur, TotalItem, DibayarItem, Sisa);
                        }
                        var Total = formatter.format(datap1.hutang_total);
                        Footer(Total);
                        FooterEnd();
                    }
                }
            });
        }
    });
}

function Header(NoOrder, Tanggal, NamaSuplier, Kasir) {
    var LimitChar = 36;
    if(NoOrder.length <= LimitChar) {
        txtNoOrder = NoOrder;
    } else {
        txtNoOrder = NoOrder.substring(0, LimitChar);
    }

    if(NamaSuplier.length <= LimitChar) {
        txtNamaSuplier = NamaSuplier;
    } else {
        txtNamaSuplier = NamaSuplier.substring(0, LimitChar);
    }

    printer.open().then(function () {
      printer.align('center')
        .bold(true)
        .text('TOKO JATI BANGUN')
        .text('Jl. AKBP Agil Kusumadya No.110A, Jatiwetan')
        .text('KUDUS')
        .text('(0291)2911225')
        .bold(false)
        .text('------------------------------------------------')
        .print()
    })

    printer.open().then(function () {
      printer.align('left')
        .text('Bayar HTG : '+txtNoOrder)
        .text('Tanggal   : '+Tanggal.split("-").reverse().join("-"))
        .text('Suplier   : '+txtNamaSuplier)
        .text('Kasir     : '+Kasir)
        .text('------------------------------------------------')
        .text('NO. FAKTUR           TOTAL    DIBAYAR       SISA')
        .print()
    })
}

function ListItem(NoFaktur, TotalItem, DibayarItem, Sisa) {
    var limitNoFaktur = 15;
    var limitTotal    = 10;
    var limitDibayar  = 10;
    var limitSisa     = 10;
    var txtNoFaktur   = '';
    var txtTotal      = 0;
    var txtDibayar    = 0;
    var txtSisa       = 0;

    if(NoFaktur.length <= limitNoFaktur) {
        txtNoFaktur = NoFaktur.padEnd(limitNoFaktur, ' ')
    } else {
        txtNoFaktur = NoFaktur.substring(0, limitNoFaktur);
    }

    if (TotalItem.length <= limitTotal) {
        txtTotal = TotalItem.padStart(limitTotal, ' ')
    } else {
        txtTotal = TotalItem.substring(0, limitTotal);
    }

    if (DibayarItem.length <= limitDibayar) {
        txtDibayar = DibayarItem.padStart(limitDibayar, ' ')
    } else {
        txtDibayar = DibayarItem.substring(0, limitDibayar);
    }

    if (Sisa.length <= limitSisa) {
        txtSisa = Sisa.padStart(limitSisa, ' ')
    } else {
        txtSisa = Sisa.substring(0, limitSisa);
    }

    printer.open().then(function () {
      printer.align('left')
        .text(txtNoFaktur+" "+txtTotal+" "+txtDibayar+" "+txtSisa)
        .print()
    })
}

function Footer(Total) {
    var limitNominal    = 13;
    var txtTotal        = 0;

    if (Total.length <= limitNominal) {
        txtTotal = Total.padStart(limitNominal, ' ')
    } else {
        txtTotal = Total.substring(0, limitNominal);
    }

    printer.open().then(function () {
        printer.align('left')
        .text("                           TOTAL : "+txtTotal)
        .print()
    })

    printer.open().then(function () {
        printer.align('center')
        .text("")
		.text("Penerima")
        .text("")
        .text("")
        .text("-----------------")
        .print()
    })
}

function FooterEnd() {
    printer.open().then(function () {
        printer.align('left')
        .text('')
        .feed(3)
        .cut()
        .print()
    })
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" placeholder="Dari Tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y');?>" autocomplete="off">
                                <span class="input-group-addon"><b>s/d</b></span>
                                <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" placeholder="Sampai Tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y');?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Suplier</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstSuplier" id="lstSuplier">
                                <option value="">- SEMUA DATA -</option>
                                <?php foreach ($listSuplier as $r) { ?>
                                <option value="<?=$r->suplier_id;?>"><?=$r->suplier_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>

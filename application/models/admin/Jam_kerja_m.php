<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jam_kerja_m extends CI_Model
{
    public $table         = 'ok_jam_kerja';
    public $column_order  = array(null, null, 'jam_kerja_hari', 'jam_kerja_masuk', 'jam_kerja_pulang', 'jam_kerja_libur');
    public $column_search = array('jam_kerja_hari');
    public $order         = array('jam_kerja_id' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'jam_kerja_hari'   => trim($this->input->post('nama', 'true')),
            'jam_kerja_masuk'  => trim(stripHTMLtags($this->input->post('masuk', 'true'))),
            'jam_kerja_pulang' => trim(stripHTMLtags($this->input->post('pulang', 'true'))),
            'jam_kerja_libur'  => trim(stripHTMLtags($this->input->post('lstLibur', 'true'))),
            'jam_kerja_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_jam_kerja', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ok_jam_kerja');
        $this->db->where('jam_kerja_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $jam_kerja_id = $this->input->post('id', 'true');
        $data         = array(
            'jam_kerja_hari'   => trim($this->input->post('nama', 'true')),
            'jam_kerja_masuk'  => trim(stripHTMLtags($this->input->post('masuk', 'true'))),
            'jam_kerja_pulang' => trim(stripHTMLtags($this->input->post('pulang', 'true'))),
            'jam_kerja_libur'  => trim(stripHTMLtags($this->input->post('lstLibur', 'true'))),
            'jam_kerja_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('jam_kerja_id', $jam_kerja_id);
        $this->db->update('ok_jam_kerja', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('jam_kerja_id', $id);
        $this->db->delete('ok_jam_kerja');
    }
}
/* Location: ./application/models/admin/Jam_kerja_m.php */

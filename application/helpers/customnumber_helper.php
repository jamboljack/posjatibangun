<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function customNumberFormat($value)
{
    if (intval($value) == $value) {
        return number_format($value, 0, '', '');
    } else {
        return number_format($value, 2, ',', '');
    }
}

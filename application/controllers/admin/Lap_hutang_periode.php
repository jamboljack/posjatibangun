<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_hutang_periode extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_hutang_periode_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $data['listTipe']    = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/reporthutang/reporthutangperiode_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_hutang_periode_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->hutang_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->hutang_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = $r->suplier_alamat;
            $row[]  = number_format($r->hutang_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_hutang_periode_m->count_all(),
            "recordsFiltered" => $this->lap_hutang_periode_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printperiode($dari = 'all', $sampai = 'all', $suplier = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($dari != 'all' && $sampai != 'all' && $suplier == 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('hutang_tanggal', 'asc')->get_where('v_hutang', array('hutang_tanggal >=' => $tgl_dari, 'hutang_tanggal <=' => $tgl_sampai))->result();
        } elseif ($dari == 'all' && $sampai == 'all' && $suplier != 'all') {
            $data['listData'] = $this->db->order_by('hutang_tanggal', 'asc')->get_where('v_hutang', array('suplier_id' => $suplier))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $suplier != 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('hutang_tanggal', 'asc')->get_where('v_hutang', array('hutang_tanggal >=' => $tgl_dari, 'hutang_tanggal <=' => $tgl_sampai, 'suplier_id' => $suplier))->result();
        } else {
            $data['listData'] = $this->db->order_by('hutang_tanggal', 'asc')->get('v_hutang')->result();
        }

        $this->load->view('admin/reporthutang/printperiode_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_hutang_periode.php */

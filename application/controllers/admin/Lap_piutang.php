<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_piutang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_piutang_m');
    }

    public function index()
    {
        $this->template->display('admin/reportpiutang/reportpiutang_v');
    }

    public function data_list()
    {
        $List = $this->lap_piutang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->pelanggan_nama;
            $row[]  = $r->pelanggan_alamat . ' - ' . $r->pelanggan_kota;
            $row[]  = $r->pelanggan_telp;
            $row[]  = number_format($r->total, 0, '', ',');
            $row[]  = number_format($r->totalbayar, 0, '', ',');
            $row[]  = number_format($r->totalpiutang, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_piutang_m->count_all(),
            "recordsFiltered" => $this->lap_piutang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printpiutang()
    {
        $data['header']   = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['listData'] = $this->db->order_by('pelanggan_nama', 'asc')->get('v_piutang_pelanggan')->result();
        $this->load->view('admin/reportpiutang/printpiutang_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_piutang.php */

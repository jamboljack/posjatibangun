<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">
            Barang
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Data Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                 <li>
                    <a href="<?=site_url('admin/barang');?>">Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Daftar Unit</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-edit"></i> Detail Barang
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="control-label col-md-4">Kode Barang :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kode" value="<?=$detail->barang_kode;?>" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="control-label col-md-4">Nama Barang :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="nama" value="<?=$detail->barang_nama;?>" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="control-label col-md-4">Kategori :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kategori" value="<?=$detail->kategori_nama;?>" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="control-label col-md-4">Merk :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="merk" value="<?=$detail->barang_merk;?>" readonly />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Harga dan Unit
                        </div>
                        <div class="actions">
                            <a href="<?=site_url('admin/barang');?>" type="button" class="btn btn-warning btn-xs"><i class="fa fa-angle-double-left"></i> Kembali</a>
                            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#formModalAdd">
                                <i class="fa fa-plus-circle"></i> Tambah
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="8%"></th>
                                    <th width="5%">No</th>
                                    <th>Unit</th>
                                    <th width="5%">Multiple</th>
                                    <th width="15%">Unit Atasan</th>
                                    <th width="5%">Stock</th>
                                    <th width="10%">Harga Beli</th>
                                    <th width="5%">Disc1</th>
                                    <th width="5%">Disc2</th>
                                    <th width="5%">Disc3</th>
                                    <th width="5%">Disc4</th>
                                    <th width="5%">PPN (%)</th>
                                    <th width="10%">Harga Pokok</th>
                                    <th width="10%">Harga Jual</th>
                                    <th width="10%">Terjual</th>
                                    <th width="10%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#tableDataLog').on('shown.bs.modal', function () {
       var table = $('#tableHistory').DataTable();
       table.columns.adjust();
    });
});

$(document).ready(function() {
    $('.number').maskMoney({thousands:',', precision:0});
    $('.digit').maskMoney({thousands:',', precision:2});
});

function HitungHarga() {  
    var locale          = 'en';
    var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter       = new Intl.NumberFormat(locale, options);
    var myForm          = document.formInput;
    var Multiple        = myForm.multiple.value;
    Multiple            = Multiple.replace(/[,]/g, '');
    Multiple            = parseFloat(Multiple);
    // Cari Harga Beli dan Jual Utama
    var barang_id       = '<?=$detail->barang_id;?>';
    $.ajax({
        url:"<?=site_url('admin/barang/check_harga/');?>"+barang_id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var harga_beli      = parseInt(data.unit_hrg_beli);
            harga_beli          = (harga_beli * Multiple);
            var harga_jual      = parseInt(data.unit_hrg_jual);
            harga_jual          = (harga_jual * Multiple);
            document.getElementById("harga_beli").value = formatter.format(harga_beli);
            document.getElementById("harga_jual").value = formatter.format(harga_jual);
        }
    });
}

function HitungHargaEdit() {  
    var locale          = 'en';
    var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter       = new Intl.NumberFormat(locale, options);
    var myForm          = document.formEdit;
    var Multiple        = myForm.multiple.value;
    Multiple            = Multiple.replace(/[,]/g, '');
    Multiple            = parseFloat(Multiple);
    // Cari Harga Beli dan Jual Utama
    var barang_id       = '<?=$detail->barang_id;?>';
    $.ajax({
        url:"<?=site_url('admin/barang/check_harga/');?>"+barang_id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var harga_beli      = parseInt(data.unit_hrg_beli);
            harga_beli          = (harga_beli * Multiple);
            var harga_jual      = parseInt(data.unit_hrg_jual);
            harga_jual          = (harga_jual * Multiple);
            document.getElementById("unit_hrg_beli").value = formatter.format(harga_beli);
            document.getElementById("unit_hrg_jual").value = formatter.format(harga_jual);
        }
    });
}

var table;
$(document).ready(function() {
    barang_id = '<?=$detail->barang_id;?>';
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/barang/data_unit_list/');?>"+barang_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 15 ],
                "className": "text-center",
            },
            {
                "targets": [ 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 ],
                "className": "text-right",
            }
        ],
    });
});

function reload_table() {
    table.ajax.reload(null,false);
}

function reload_table_history() {
    tableHistory.ajax.reload(null,false);
}

function resetformInput() {
    $("#unit").val('');
    $("#multiple").val('');
    $("#harga_beli").val('');
    $("#disc1").val('');
    $("#disc2").val('');
    $("#disc3").val('');
    $("#disc4").val('');
    $("#ppn").val('');
    $("#harga_pokok").val('');
    $("#harga_jual").val('');

    var MValid = $("#formInput");
    MValid.validate().resetForm();
    MValid.find(".has-error").removeClass("has-error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

$(document).ready(function() {
    var form        = $('#formInput');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formInput").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            unit: { required: true },
            multiple: { required: true },
            harga_beli: { required: true },
            harga_jual: { required: true }
        },
        messages: {
            unit: {
                required :'Nama Unit required'
            },
            multiple: {
                required :'Multiple required'
            },
            harga_beli: {
                required :'Harga Beli required'
            },
            harga_jual: {
                required :'Harga Jual required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: '<?=site_url('admin/barang/savedataunit');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal({
                        title:"Sukses",
                        text: "Simpan Data Sukses",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                    reload_table();
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Simpan Data Failed",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                }
            });
        }
    });
});

function edit_data(id) {
    $('#formEdit')[0].reset();
    $.ajax({
        url : "<?=site_url('admin/barang/get_data_unit/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale     = 'en';
            var options    = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter  = new Intl.NumberFormat(locale, options);
            var options2   = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter2 = new Intl.NumberFormat(locale, options2);
            $('#id').val(data.unit_id);
            $('#unit_nama').val(data.unit_nama);
            $('#unit_multiple').val(formatter2.format(data.unit_multiple));
            $('#unit_hrg_beli').val(formatter.format(data.unit_hrg_beli));
            $('#unit_hrg_beli_old').val(data.unit_hrg_beli);
            $('#unit_disc1').val(formatter2.format(data.unit_disc1));
            $('#unit_disc2').val(formatter2.format(data.unit_disc2));
            $('#unit_disc3').val(formatter2.format(data.unit_disc3));
            $('#unit_disc4').val(formatter2.format(data.unit_disc4));
            $('#unit_ppn').val(formatter2.format(data.unit_ppn));
            $('#unit_hrg_pokok').val(formatter.format(data.unit_hrg_pokok));
            $('#unit_hrg_pokok_old').val(data.unit_hrg_pokok);
            $('#unit_hrg_jual').val(formatter.format(data.unit_hrg_jual));
            $('#unit_hrg_jual_old').val(data.unit_hrg_jual);
            var unit_id      = data.unit_id;
            var unit_id_head = data.unit_id_head;
            $.ajax({
                url:"<?=site_url('admin/barang/pilih_unit_head_edit/');?>"+unit_id_head,
                success: function(response) {
                    $("#lstUnitHeadEdit").html(response);
                },
                dataType:"html"
            });
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formEdit');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formEdit").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            unit: { required: true },
            harga_beli: { required: true },
            harga_jual: { required: true }
        },
        messages: {
            unit: {
                required :'Nama Unit required'
            },
            harga_beli: {
                required :'Harga Beli required'
            },
            harga_jual: {
                required :'Harga Jual required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formEdit").serialize();
            $.ajax({
                url: '<?=site_url('admin/barang/updatedataunit');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal({
                        title:"Sukses",
                        text: "Update Data Sukses",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    $('#formModalEdit').modal('hide');
                    reload_table();
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Update Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalEdit').modal('hide');
                }
            });
        }
    });
});

function log_data(unit_id) {
    listHistory(unit_id);
    $('#tableDataLog').modal('show');
}

function listHistory(unit_id) {
    var tableLog;
    tableLog = $('#tableHistory').DataTable({
        "destroy": true,
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/barang/data_unit_log_list/');?>"+unit_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 2, 3, 4, 5, 6, 7, 8, 9 ],
                "className": "text-right",
            }
        ],
    });
}

$(document).ready(function() {
    $("body").on('keyup', "#harga_beli", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc1", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc2", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc3", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc4", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#ppn", function(){
        hitungPokok();
    });
});

function hitungPokok() {
    var locale      = 'en';
    var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formInput;
    var HargaBeli   = myForm.harga_beli.value;
    HargaBeli       = HargaBeli.replace(/[,]/g, '');
    HargaBeli       = parseInt(HargaBeli);
    var Disc1       = myForm.disc1.value;
    Disc1           = Disc1.replace(/[,]/g, '');
    Disc1           = parseFloat(Disc1);
    var Disc2       = myForm.disc2.value;
    Disc2           = Disc2.replace(/[,]/g, '');
    Disc2           = parseFloat(Disc2);
    var Disc3       = myForm.disc3.value;
    Disc3           = Disc3.replace(/[,]/g, '');
    Disc3           = parseFloat(Disc3);
    var Disc4       = myForm.disc4.value;
    Disc4           = Disc4.replace(/[,]/g, '');
    Disc4           = parseFloat(Disc4);
    var PPN         = myForm.ppn.value;
    PPN             = PPN.replace(/[,]/g, '');
    PPN             = parseFloat(PPN);
    if (isNaN(Disc1)) {
        Disc1 = 0;
    } else {
        Disc1 = Disc1;
    }
    if (isNaN(Disc2)) {
        Disc2 = 0;
    } else {
        Disc2 = Disc2;
    }
    if (isNaN(Disc3)) {
        Disc3 = 0;
    } else {
        Disc3 = Disc3;
    }
    if (isNaN(Disc4)) {
        Disc4 = 0;
    } else {
        Disc4 = Disc4;
    }
    if (isNaN(PPN)) {
        PPN = 0;
    } else {
        PPN = PPN;
    }
    
    var HargaPokok;
    if (HargaBeli == 0) {
        HargaPokok = 0;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc1)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc2)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc3)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon     = (HargaBeli*Disc4)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc3)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc4)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaPokok = HargaDisc2-Diskon3;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else {
        HargaPokok = HargaBeli;
    }

    if (PPN > 0) {
        Pajak      = (HargaPokok*PPN)/100;
        HargaPokok = (HargaPokok+Pajak);
    } else {
        HargaPokok = HargaPokok;
    }

    if (HargaPokok > 0) {
        myForm.harga_pokok.value = formatter.format(HargaPokok);
    } else {
        myForm.harga_pokok.value = 0;
    }
}

$(document).ready(function() {
    $("body").on('keyup', "#unit_hrg_beli", function(){
        hitungPokokEdit();
    });

    $("body").on('keyup', "#unit_disc1", function(){
        hitungPokokEdit();
    });

    $("body").on('keyup', "#unit_disc2", function(){
        hitungPokokEdit();
    });

    $("body").on('keyup', "#unit_disc3", function(){
        hitungPokokEdit();
    });

    $("body").on('keyup', "#unit_disc4", function(){
        hitungPokokEdit();
    });

    $("body").on('keyup', "#unit_ppn", function(){
        hitungPokokEdit();
    });
});

function hitungPokokEdit() {
    var locale      = 'en';
    var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formEdit;
    var HargaBeli   = myForm.unit_hrg_beli.value;
    HargaBeli       = HargaBeli.replace(/[,]/g, '');
    HargaBeli       = parseInt(HargaBeli);
    var Disc1       = myForm.unit_disc1.value;
    Disc1           = Disc1.replace(/[,]/g, '');
    Disc1           = parseFloat(Disc1);
    var Disc2       = myForm.unit_disc2.value;
    Disc2           = Disc2.replace(/[,]/g, '');
    Disc2           = parseFloat(Disc2);
    var Disc3       = myForm.unit_disc3.value;
    Disc3           = Disc3.replace(/[,]/g, '');
    Disc3           = parseFloat(Disc3);
    var Disc4       = myForm.unit_disc4.value;
    Disc4           = Disc4.replace(/[,]/g, '');
    Disc4           = parseFloat(Disc4);
    var PPN         = myForm.unit_ppn.value;
    PPN             = PPN.replace(/[,]/g, '');
    PPN             = parseFloat(PPN);
    if (isNaN(Disc1)) {
        Disc1 = 0;
    } else {
        Disc1 = Disc1;
    }
    if (isNaN(Disc2)) {
        Disc2 = 0;
    } else {
        Disc2 = Disc2;
    }
    if (isNaN(Disc3)) {
        Disc3 = 0;
    } else {
        Disc3 = Disc3;
    }
    if (isNaN(Disc4)) {
        Disc4 = 0;
    } else {
        Disc4 = Disc4;
    }
    if (isNaN(PPN)) {
        PPN = 0;
    } else {
        PPN = PPN;
    }
    
    var HargaPokok;
    if (HargaBeli == 0) {
        HargaPokok = 0;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc1)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc2)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc3)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon     = (HargaBeli*Disc4)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc3)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc4)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaPokok = HargaDisc2-Diskon3;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else {
        HargaPokok = HargaBeli;
    }

    if (PPN > 0) {
        Pajak      = (HargaPokok*PPN)/100;
        HargaPokok = (HargaPokok+Pajak);
    } else {
        HargaPokok = HargaPokok;
    }

    if (HargaPokok > 0) {
        myForm.unit_hrg_pokok.value = formatter.format(HargaPokok);
    } else {
        myForm.unit_hrg_pokok.value = 0;
    }
}

function setUtama(unit_id, barang_id) {
    var id = unit_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Unit ini akan di Set Utama !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/barang/setutama')?>/"+id+'/'+barang_id,
            type: "POST",
            success: function(data) {
                swal({
                    title:"Sukses",
                    text: "Set Data Utama Sukses",
                    showConfirmButton: false,
                    type: "success",
                    timer: 2000
                });
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Set Data Utama Gagal');
            }
        });
    });
}
</script>

<div class="modal" id="formModalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formInput" name="formInput" class="form-horizontal">
                <input type="hidden" name="barang_id" value="<?=$detail->barang_id;?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Form Tambah Unit dan Harga</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Unit Atasan</label>
                        <div class="col-md-8">
                            <select class="form-control" name="lstUnitHead" id="lstUnitHead" readonly>
                                <?php 
                                foreach($listUnit as $r) {
                                    if ($detail->unit_id==$r->unit_id) {
                                        $selected = 'selected';
                                    } else {
                                        $selected = '';
                                    }
                                ?>
                                <option value="<?=$r->unit_id;?>" <?=$selected;?>><?=$r->unit_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Unit</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="unit" id="unit" class="form-control" placeholder="Input Nama Unit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Multiple</label>
                        <div class="col-md-4">
                            <input type="text" autocomplete="off" name="multiple" id="multiple" class="form-control digit" placeholder="0.00" onkeydown="HitungHarga()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Beli</label>
                        <div class="col-md-4">
                            <input type="text" name="harga_beli" id="harga_beli" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Disc 1-4 (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc1" id="disc1" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc2" id="disc2" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc3" id="disc3" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc4" id="disc4" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">PPN (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="ppn" id="ppn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Harga Pokok (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="0" name="harga_pokok" id="harga_pokok" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Jual</label>
                        <div class="col-md-4">
                            <input type="text" name="harga_jual" id="harga_jual" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formEdit" name="formEdit" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Form Edit Unit dan Harga</h4>
                    <input type="hidden" name="id" id="id">
                    <input type="hidden" name="unit_hrg_beli_old" id="unit_hrg_beli_old">
                    <input type="hidden" name="unit_hrg_pokok_old" id="unit_hrg_pokok_old">
                    <input type="hidden" name="unit_hrg_jual_old" id="unit_hrg_jual_old">
                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Unit Atasan</label>
                        <div class="col-md-8">
                            <select class="form-control" name="lstUnitHead" id="lstUnitHeadEdit" disabled>
                                <option value="">- Pilih Unit Atasan -</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Unit</label>
                        <div class="col-md-8">
                            <input type="text" autocomplete="off" name="unit" id="unit_nama" class="form-control" placeholder="Input Nama Unit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Multiple</label>
                        <div class="col-md-4">
                            <input type="text" autocomplete="off" name="multiple" id="unit_multiple" class="form-control digit" placeholder="0.00" onkeydown="HitungHargaEdit()">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Beli</label>
                        <div class="col-md-4">
                            <input type="text" name="harga_beli" id="unit_hrg_beli" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Disc 1-4 (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc1" id="unit_disc1" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc2" id="unit_disc2" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc3" id="unit_disc3" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="disc4" id="unit_disc4" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">PPN (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control digit" placeholder="0.00" name="ppn" id="unit_ppn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Harga Pokok (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="0" name="harga_pokok" id="unit_hrg_pokok" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Harga Jual</label>
                        <div class="col-md-4">
                            <input type="text" name="harga_jual" id="unit_hrg_jual" class="form-control number" autocomplete="off" placeholder="0">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Update</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="tableDataLog" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-list"></i> Log History List</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableHistory" width="100%">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th width="10%">Tanggal</th>
                            <th width="10%">Beli Lama</th>
                            <th width="10%">Beli Baru</th>
                            <th width="5%">(%)</th>
                            <th width="10%">Pokok Lama</th>
                            <th width="10%">Pokok Baru</th>
                            <th width="10%">Jual Lama</th>
                            <th width="10%">Jual Baru</th>
                            <th width="5%">(%)</th>
                            <th>User</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Pembayaran Hutang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/hutang');?>">Pembayaran Hutang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Tambah Pembayaran Hutang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Form Data Suplier 
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                <input type="hidden" name="suplier_id" id="suplier_id">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tanggal</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control date-picker" style="text-align:center;" name="tanggal" id="tanggal" value="<?=date('d-m-Y');?>" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Nama Suplier</label>
                                            <div class="col-md-9">
                                                <div class="input-group" style="text-align:left">
                                                    <div class="input-icon right">
                                                    <i class="fa"></i>
                                                        <input type="text" class="form-control" name="nama_suplier" id="nama_suplier" autocomplete="off" placeholder="Cari Nama Suplier" autofocus>
                                                    </div>
                                                    <span class="input-group-btn">
                                                        <a id="btn-suplier" class="btn green" title="Cari Data Suplier"><i class="fa fa-search"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="alamat" id="alamat" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">No. Telp</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="no_telp" id="no_telp" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Total Pembayaran
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                <input type="hidden" name="totaltransaksi" id="totaltransaksi">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">User :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$this->session->userdata('nama');?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalpembayaran"></div>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pembayaran Hutang 
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form method="post" id="formBeli" role="form" name="formBeli" class="form">
                        <input type="hidden" name="pembelian_id" id="pembelian_id">
                        <input type="hidden" name="hutang_temp_id" id="hutang_temp_id">
                        <input type="hidden" name="bayar_hutang_lama" id="bayar_hutang_lama">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>No. Faktur</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="no_faktur" id="no_faktur" placeholder="Cari No. Faktur" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <a data-toggle="modal" data-target="#formDataHutang" title="Cari Data Pembelian">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control" style="text-align:center;" name="tanggalbeli" id="tanggalbeli" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Jumlah Hutang</label>
                                            <input type="text" class="form-control" style="text-align:right;" placeholder="0" name="jumlahhutang" id="jumlahhutang" readonly>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Jumlah Bayar</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control number" name="bayarhutang" id="bayarhutang" autocomplete="off" placeholder="0" onkeyup="hitungSisa()" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Sisa</label>
                                            <input type="text" class="form-control" name="sisa" id="sisa" placeholder="0" readonly style="text-align:right;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions" align="center">
                                <button class="btn btn-primary" id="btn_item" name="btn_item" disabled><i class="fa fa-floppy-o"></i> Simpan Pembayaran</button>
                                <a onclick="resetForm()" class="btn btn-danger" id="btn_reset" name="btn_reset" disabled><i class="fa fa-refresh"></i> Reset</a>
                                <button type="button" class="btn btn-primary" id="btn_simpan" name="btn_simpan" disabled><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
                                <a href="<?=site_url('admin/hutang');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </form>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th>No. Faktur</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="15%">Jumlah Hutang</th>
                                    <th width="15%">Jumlah Bayar</th>
                                    <th width="15%">Sisa</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formCariSuplier').on('shown.bs.modal', function () {
       var table = $('#tableDataSuplier').DataTable();
       table.columns.adjust();
    });

    $('#formDataHutang').on('shown.bs.modal', function () {
       var table = $('#tableDataHutang').DataTable();
       table.columns.adjust();
    });
});

var statusinput;
statusinput = 'Tambah';


$("#btn-suplier").click(function() {
    $('#formCariSuplier').modal('show');
});

$(document).ready(function() {
    $('.number').maskMoney({thousands:',', precision:0});
    $('.digit').maskMoney({thousands:'', precision:2});
    hitungTotal();
    dataSuplier();
    var id = '99999999';
    dataHutang(id);
    document.getElementById("btn_simpan").style.pointerEvents = "none";
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "destoy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/hutang/data_temp_list')?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 5, 6 ],
                "className": "text-right",
            }
        ],
    });    
});

function dataSuplier() {
    var tableSuplier;
    tableSuplier = $('#tableDataSuplier').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/hutang/data_hutang_suplier_list'); ?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 5 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihsuplier(id) {
    $.ajax({
        url : "<?=site_url('admin/hutang/get_data_suplier/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#suplier_id').val(data.suplier_id);
            $('#nama_suplier').val(data.suplier_nama);
            $('#alamat').val(data.suplier_alamat);
            $('#no_telp').val(data.suplier_telp);
            $("#btn_simpan").attr("disabled", false);
            document.getElementById("btn_simpan").style.pointerEvents = "auto";
            $('#formCariSuplier').modal('hide');
            dataHutang(id);
            $('#no_faktur').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function dataHutang(id) {
    var tableHutang;
    tableHutang = $('#tableDataHutang').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/hutang/data_pembelian_list/'); ?>"+id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 5, 6 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihdata(id) {
    $.ajax({
        url : "<?=site_url('admin/hutang/get_data_pembelian/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale          = 'en';
            var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter       = new Intl.NumberFormat(locale, options);
            $('#pembelian_id').val(data.pembelian_id);
            $('#no_faktur').val(data.pembelian_no_faktur);
            $('#tanggalbeli').val((data.pembelian_tanggal).split('-').reverse().join('-'));
            $('#jumlahhutang').val(formatter.format(data.pembelian_sisa_hutang));
            $('#sisa').val(formatter.format(data.pembelian_sisa_hutang));
            document.formBeli.bayarhutang.disabled=false;
            document.formBeli.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
            $('#formDataHutang').modal('hide');
            $('#bayarhutang').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function hitungSisa() {
    var locale       = 'en';
    var options      = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter    = new Intl.NumberFormat(locale, options);
    var myForm       = document.formBeli;
    var JumlahHutang = myForm.jumlahhutang.value;
    JumlahHutang     = JumlahHutang.replace(/[,]/g, ''); // Ini String
    JumlahHutang     = parseInt(JumlahHutang); // Ini Integer
    var Bayar        = myForm.bayarhutang.value;
    Bayar            = Bayar.replace(/[,]/g, ''); // Ini String
    Bayar            = parseInt(Bayar); // Ini Integer
    if (Bayar > 0 && Bayar <= JumlahHutang) {
        Sisa        = (JumlahHutang-Bayar);
    } else if(Bayar > 0 && Bayar > JumlahHutang) {
        $('#bayarhutang').val(formatNumber(JumlahHutang));
        Sisa        = 0;
    } else if (Bayar == 0) {
        Sisa        = JumlahHutang;
    } else {
        Sisa        = JumlahHutang;
    }

    if (Sisa > 0) {
        myForm.sisa.value = formatter.format(Sisa);
    } else {
        myForm.sisa.value = 0;
    }
}

function resetForm() {
    statusinput = 'Tambah';
    $('#pembelian_id').val('');
    $('#no_faktur').val('');
    $('#tanggalbeli').val('');
    $('#jumlahhutang').val('');
    $('#bayarhutang').val('');
    $('#sisa').val('');
    document.formBeli.bayarhutang.disabled=true;
    $("#btn_item").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    var id = document.getElementById('suplier_id').value;
    dataHutang(id);
    $('#no_faktur').focus();
    var MValid = $("#formBeli");
    MValid.validate().resetForm();
}

$(document).ready(function() {
    var form    = $('form');
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);

    $("form").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_suplier: { required: true },
            no_faktur: { required: true },
            bayarhutang: { required: true }
        },
        messages: {
            nama_suplier: {
                required :'Nama Suplier required'
            },
            no_faktur: {
                required :'No. Faktur required'
            },
            bayarhutang: {
                required :'Jumlah Pembayaran required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        },
        submitHandler: function(form) {
            if (statusinput == 'Tambah') {
                dataString = $(".form").serialize();
                $.ajax({
                    url: '<?=site_url('admin/hutang/saveitem');?>',
                    type: "POST",
                    data: dataString,
                    success: function(data) {
                        resetForm();
                        reload_table();
                        hitungTotal();
                    },
                    error: function() {
                        swal({
                            title:"Error",
                            text: "Simpan Item Gagal",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                    }
                });
            } else {
                dataString = $(".form").serialize();
                $.ajax({
                    url: '<?=site_url('admin/hutang/updateitem');?>',
                    type: "POST",
                    data: dataString,
                    success: function(data) {
                        resetForm();
                        reload_table();
                        hitungTotal();
                    },
                    error: function() {
                        swal({
                            title:"Error",
                            text: "Update Item Gagal",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                    }
                });
            }
        }
    });
});

function hitungTotal() {
    $.ajax({
        url : "<?=site_url('admin/hutang/get_data_total_temp');?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var Total;
            var TotalHutang;
            Total     = 0;
            TotalHutang = 0;
            if (data == null) {
                Total        = 0;
                TotalHutang  = 0;
            } else {
                var locale      = 'en';
                var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                var formatter   = new Intl.NumberFormat(locale, options);
                TotalHutang     = data.total;
                Total           = formatter.format(TotalHutang);
            }

            $('#totaltransaksi').val(TotalHutang);
            $('#totalpembayaran').text('Rp. '+Total);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get Total');
        }
    });
}

function edit_data(id) {
    statusinput = 'Edit';
    $.ajax({
        url : "<?=site_url('admin/hutang/get_data_item/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale      = 'en';
            var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter   = new Intl.NumberFormat(locale, options);
            var options1    = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1  = new Intl.NumberFormat(locale, options1);
            $('#hutang_temp_id').val(data.hutang_temp_id);
            $('#pembelian_id').val(data.pembelian_id);
            $('#no_faktur').val(data.pembelian_no_faktur);
            document.formBeli.no_faktur.disabled=true;
            $('#tanggalbeli').val((data.pembelian_tanggal).split('-').reverse().join('-'));
            $('#jumlahhutang').val(formatter.format(data.hutang_temp_total));
            $('#bayarhutang').val(formatter.format(data.hutang_temp_bayar));
            $('#bayar_hutang_lama').val(data.hutang_temp_bayar);
            $('#sisa').val(formatter.format(data.hutang_temp_sisa));
            document.formBeli.bayarhutang.disabled=false;
            document.formBeli.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hapusData(hutang_temp_id) {
    var id = hutang_temp_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Item ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/hutang/deleteitem')?>/"+id,
            type: "POST",
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Item Gagal');
            }
        });
    });
}

function resetFormHutang() {
    statusinput = 'Tambah';
    $('#suplier_id').val('');
    $('#nama_suplier').val('');
    $('#alamat').val('');
    $('#no_telp').val('');
    $('#lstTipeBayar').val('');
    $('#pembelian_id').val('');
    $('#no_faktur').val('');
    $('#tanggalbeli').val('');
    $('#jumlahhutang').val('');
    $('#bayarhutang').val('');
    $('#sisa').val('');
    document.formBeli.bayarhutang.disabled=true;
    document.formBeli.btn_item.disabled=false;
    $("#btn_simpan").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    document.getElementById("btn_simpan").style.pointerEvents = "none";
    dataSuplier();
    hitungTotal();
    var id = '99999999';
    dataHutang(id);

    var MValid = $("form");
    MValid.validate().resetForm();
    MValid.find(".has-success, .has-warning, .fa-warning, .fa-check").removeClass("has-success has-warning fa-warning fa-check");
    MValid.find("i.fa[data-original-title]").removeAttr('data-original-title');
    document.getElementById("no_faktur").focus();
}

$(document).ready(function() {
    var form        = $('form');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("form").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_suplier: { required: true }
        },
        messages: {
            nama_suplier: {
                required :'Nama Suplier required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_simpan").click(function() {
        if($('form').valid()) {
            simpanTransaksi();
        }
    });
});

var statusprinter = '<?=$meta->meta_print_status;?>';
var printer       = new Recta('<?=$meta->meta_print_key;?>', '<?=$meta->meta_print_port;?>');
function simpanTransaksi() {
    dataString = $(".form").serialize();
    $.ajax({
        url: '<?=site_url('admin/hutang/savedata');?>',
        type: "POST",
        data: dataString,
        dataType: 'JSON',
        success: function(data) {
            if (data.id != '') {
                swal({
                    title:"Sukses",
                    text: "Simpan Pembayaran Hutang Sukses",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
                
                if (statusprinter == 2) {
                    var hutang_id = data.id;
                    $.ajax({
                        url: '<?=site_url('admin/hutang/get_data/');?>'+hutang_id,
                        type: "POST",
                        dataType: 'JSON',
                        success: function(datap1) {
                            var locale        = 'en';
                            var options       = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                            var formatter     = new Intl.NumberFormat(locale, options);
                            var options1      = {minimumFractionDigits: 2, maximumFractionDigits: 2};
                            var formatter1    = new Intl.NumberFormat(locale, options1);
                            var NoOrder       = datap1.hutang_no_faktur;
                            var Tanggal       = datap1.hutang_tanggal;
                            var NamaSuplier   = datap1.suplier_nama;
                            var Kasir         = datap1.user_username;
                            Header(NoOrder, Tanggal, NamaSuplier, Kasir);
                            $.ajax({
                                url: '<?=site_url('admin/hutang/get_list_item/');?>'+hutang_id,
                                type: "POST",
                                dataType: 'JSON',
                                success: function(dataitem1) {
                                    if (dataitem1 != null) {
                                        var x1 = dataitem1.length;
                                        for(var i = 0; i < x1; i++) {
                                            var NoFaktur   = dataitem1[i].pembelian_no_faktur;
                                            var TotalItem  = formatter.format(dataitem1[i].hutang_detail_total);
                                            var DibayarItem= formatter.format(dataitem1[i].hutang_detail_bayar);
                                            var Sisa       = formatter.format(dataitem1[i].hutang_detail_sisa);
                                            ListItem(NoFaktur, TotalItem, DibayarItem, Sisa);
                                        }

                                        var Total      = formatter.format(datap1.hutang_total);
                                        Footer(Total);
                                        FooterEnd();
                                    }
                                }
                            });
                        }
                    });
                }
            } else {
                swal({
                    title:"Info",
                    text: "Data Penjualan Tidak Ada",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "info"
                });
            }
            reload_table();
            resetFormHutang();
        },
        error: function() {
            swal({
                title:"Error",
                text: "Simpan Pembayaran Hutang Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            reload_table();
            resetFormHutang();
        }
    });
    return false;
}

function Header(NoOrder, Tanggal, NamaSuplier, Kasir) {
    var LimitChar = 36;
    if(NoOrder.length <= LimitChar) {
        txtNoOrder = NoOrder;
    } else {
        txtNoOrder = NoOrder.substring(0, LimitChar);
    }

    if(NamaSuplier.length <= LimitChar) {
        txtNamaSuplier = NamaSuplier;
    } else {
        txtNamaSuplier = NamaSuplier.substring(0, LimitChar);
    }

    printer.open().then(function () {
      printer.align('center')
        .bold(true)
        .text('TOKO JATI BANGUN')
        .text('Jl. AKBP Agil Kusumadya No.110A, Jatiwetan')
        .text('KUDUS')
        .text('(0291)2911225')
        .bold(false)
        .text('------------------------------------------------')
        .print()
    })

    printer.open().then(function () {
      printer.align('left')
        .text('Bayar HTG : '+txtNoOrder)
        .text('Tanggal   : '+Tanggal.split("-").reverse().join("-"))
        .text('Suplier   : '+txtNamaSuplier)
        .text('Kasir     : '+Kasir)
        .text('------------------------------------------------')
        .text('NO. FAKTUR           TOTAL    DIBAYAR       SISA')
        .print()
    })
}

function ListItem(NoFaktur, TotalItem, DibayarItem, Sisa) {
    var limitNoFaktur = 15;
    var limitTotal    = 10;
    var limitDibayar  = 10;
    var limitSisa     = 10;
    var txtNoFaktur   = '';
    var txtTotal      = 0;
    var txtDibayar    = 0;
    var txtSisa       = 0;

    if(NoFaktur.length <= limitNoFaktur) {
        txtNoFaktur = NoFaktur.padEnd(limitNoFaktur, ' ')
    } else {
        txtNoFaktur = NoFaktur.substring(0, limitNoFaktur);
    }

    if (TotalItem.length <= limitTotal) {
        txtTotal = TotalItem.padStart(limitTotal, ' ')
    } else {
        txtTotal = TotalItem.substring(0, limitTotal);
    }

    if (DibayarItem.length <= limitDibayar) {
        txtDibayar = DibayarItem.padStart(limitDibayar, ' ')
    } else {
        txtDibayar = DibayarItem.substring(0, limitDibayar);
    }

    if (Sisa.length <= limitSisa) {
        txtSisa = Sisa.padStart(limitSisa, ' ')
    } else {
        txtSisa = Sisa.substring(0, limitSisa);
    }

    printer.open().then(function () {
      printer.align('left')
        .text(txtNoFaktur+" "+txtTotal+" "+txtDibayar+" "+txtSisa)
        .print()
    })
}

function Footer(Total) {
    var limitNominal    = 13;
    var txtTotal        = 0;

    if (Total.length <= limitNominal) {
        txtTotal = Total.padStart(limitNominal, ' ')
    } else {
        txtTotal = Total.substring(0, limitNominal);
    }

    printer.open().then(function () {
        printer.align('left')
        .text("                           TOTAL : "+txtTotal)
        .print()
    })

    printer.open().then(function () {
        printer.align('center')
        .text("")
		.text("Penerima")
        .text("")
        .text("")
        .text("-----------------")
        .print()
    })
}

function FooterEnd() {
    printer.open().then(function () {
        printer.align('left')
        .text('')
        .feed(3)
        .cut()
        .print()
    })
}
</script>

<div class="modal fade bs-modal-lg" id="formCariSuplier" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-truck"></i> Daftar Suplier</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataSuplier">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="15%">Nama Suplier</th>
                            <th width="55%">Alamat</th>
                            <th width="15%">Kota</th>
                            <th width="10%">Total Hutang</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="formDataHutang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Daftar Hutang</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataHutang">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="50%">No. Faktur</th>
                            <th width="10%">Tanggal</th>
                            <th width="10%">Total</th>
                            <th width="10%">Dibayar</th>
                            <th width="10%">Sisa</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
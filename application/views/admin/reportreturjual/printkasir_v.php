<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Transaksi Retur Penjualan Kasir</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:16px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<?php 
if ($this->uri->segment(4) != 'all' && $this->uri->segment(5) != 'all') {
    $periode = 'PERIODE : '.$this->uri->segment(4).' s/d '.$this->uri->segment(5);
} else {
    $periode = '';
}

?>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="font-size: 15px; font-weight: bold;"><u>REKAP RETUR PENJUALAN PER KASIR</u></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?=$periode;?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <th width="5%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="50%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KASIR</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">DEBET</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KREDIT</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">SALDO AKHIR</th>
        </tr>
        <?php 
        $no         = 1;
        $total      = 0;
        $totalongkos= 0;
        $totaljual  = 0;
        $totaldebet = 0;
        $totaldebet = 0;
        $totalkredit= 0;
        foreach($listData as $r) { 
            $user_username = $r->user_username;
            if ($this->uri->segment(4) == 'all') {
                $dataPenjualan = $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username))->row();
                $dataDebet = $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username, 'tipe_bayar_set' => 'D'))->row();
                $dataKredit= $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username, 'tipe_bayar_set' => 'K'))->row();
            } else {
                $tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(4)));
                $tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(5)));
                $dataPenjualan = $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username, 'retur_jual_tanggal >=' => $tgl_dari,  'retur_jual_tanggal <=' => $tgl_sampai))->row();
                $dataDebet = $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username, 'retur_jual_tanggal >=' => $tgl_dari,  'retur_jual_tanggal <=' => $tgl_sampai, 'tipe_bayar_set' => 'D'))->row();
                $dataKredit = $this->db->select_sum('retur_jual_total', 'total')->get_where('v_retur_jual', array('user_username' => $user_username, 'retur_jual_tanggal >=' => $tgl_dari,  'retur_jual_tanggal <=' => $tgl_sampai, 'tipe_bayar_set' => 'K'))->row();
            }
        ?>
        <tr>
            <td align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->user_username;?></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($dataDebet->total,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($dataKredit->total,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($dataPenjualan->total,0,'',',');?></td>
        </tr>
        <?php 
            $total       = ($total+$dataPenjualan->total);
            $totaldebet  = ($totaldebet+$dataDebet->total);
            $totalkredit = ($totalkredit+$dataKredit->total);
            $no++;
        } 
        ?>
        <tr>
            <td colspan="2" align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TOTAL</td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($totaldebet,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($totalkredit,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($total,0,'',',');?></td>
        </tr>
    </table>
</div>
</body>
</html>
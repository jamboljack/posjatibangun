<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Pembelian_m extends CI_Model
{
    public $table        = 'v_pembelian';
    public $column_order = array(null, null, 'pembelian_no_faktur', 'pembelian_tanggal', 'suplier_nama',
        'tipe_bayar_nama', 'pembelian_disc', 'pembelian_ppn', 'pembelian_total');
    public $column_search = array('pembelian_no_faktur', 'pembelian_tanggal', 'suplier_nama',
        'tipe_bayar_nama', 'pembelian_disc', 'pembelian_total');
    public $order = array('pembelian_tanggal' => 'desc');

    public $table1         = 'v_tmp_pembelian';
    public $column_order1  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('pembelian_temp_id' => 'asc');

    public $table2        = 'v_barang';
    public $column_order2 = array(null, null, 'barang_kode', 'barang_nama', 'barang_merk',
        'unit_qty', 'unit_nama', 'unit_hrg_pokok');
    public $column_search2 = array('barang_kode', 'barang_nama', 'barang_merk');
    public $order2         = array('barang_kode' => 'asc');

    public $table3         = 'v_unit';
    public $column_order3  = array(null, null, null, null, null, null, null, null, null, null, null);
    public $column_search3 = array();
    public $order3         = array('unit_nama' => 'asc');

    public $table4         = 'v_pembelian_detail';
    public $column_order4  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('pembelian_detail_id' => 'asc');

    public $table5         = 'ok_suplier';
    public $column_order5  = array(null, null, 'suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $column_search5 = array('suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $order5         = array('suplier_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('pembelian_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('pembelian_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        if ($this->input->post('lstTipeBayar', 'true')) {
            $this->db->where('tipe_bayar_id', $this->input->post('lstTipeBayar', 'true'));
        }

        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Suplier
    private function _get_suplier_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_suplier_datatables()
    {
        $this->_get_suplier_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_suplier_filtered()
    {
        $this->_get_suplier_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_suplier_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Barang
    private function _get_barang_datatables_query()
    {
        $this->db->from($this->table2);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_barang_datatables()
    {
        $this->_get_barang_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_barang_filtered()
    {
        $this->_get_barang_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_barang_all()
    {
        $this->db->from($this->table2);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'pembelian_temp_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'pembelian_temp_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'barang_id'                  => $this->input->post('barang_id', 'true'),
            'pembelian_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                    => $this->input->post('unit_id', 'true'),
            'pembelian_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'pembelian_temp_disc1'       => $this->input->post('disc1', 'true'),
            'pembelian_temp_disc2'       => $this->input->post('disc2', 'true'),
            'pembelian_temp_disc3'       => $this->input->post('disc3', 'true'),
            'pembelian_temp_disc4'       => $this->input->post('disc4', 'true'),
            'pembelian_temp_ppn'         => $this->input->post('ppn', 'true'),
            'pembelian_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'pembelian_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'user_username'              => $username,
            'pembelian_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_pembelian_temp', $data);
    }

    public function update_data_item()
    {
        $pembelian_temp_id = $this->input->post('pembelian_temp_id', 'true');
        $data              = array(
            'barang_id'                  => $this->input->post('barang_id', 'true'),
            'pembelian_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                    => $this->input->post('unit_id', 'true'),
            'pembelian_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'pembelian_temp_disc1'       => $this->input->post('disc1', 'true'),
            'pembelian_temp_disc2'       => $this->input->post('disc2', 'true'),
            'pembelian_temp_disc3'       => $this->input->post('disc3', 'true'),
            'pembelian_temp_disc4'       => $this->input->post('disc4', 'true'),
            'pembelian_temp_ppn'         => $this->input->post('ppn', 'true'),
            'pembelian_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'pembelian_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'pembelian_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_temp_id', $pembelian_temp_id);
        $this->db->update('ok_pembelian_temp', $data);
    }

    public function delete_data_item($id)
    {
        $this->db->where('pembelian_temp_id', $id);
        $this->db->delete('ok_pembelian_temp');
    }

    // Unit
    private function _get_unit_datatables_query($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        $i = 0;
        foreach ($this->column_search3 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search3) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order3[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order3)) {
            $order = $this->order3;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_datatables($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_filtered($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_all($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        return $this->db->count_all_results();
    }

    public function insert_data_pembelian()
    {
        $username     = $this->session->userdata('username');
        $kode_suplier = $this->input->post('kode_suplier', 'true');
        $tanggal      = date('Y-m-d', strtotime($this->input->post('tanggal', 'true')));
        $termin       = $this->input->post('suplier_termin', 'true');
        if ($kode_suplier == '') {
            $response = ['status' => 'kosong'];
            echo json_encode($response);
        } else {
            $tipebayar  = $this->input->post('lstTipeBayar', 'true');
            $jenisbayar = $this->db->get_where('ok_tipe_bayar', array('tipe_bayar_id' => $tipebayar))->row();
            $tipe_set   = $jenisbayar->tipe_bayar_set;
            if ($tipe_set == 'D') {
                $sisa_hutang = 0;
                $tgl_tempo   = '';
            } else {
                $sisa_hutang = intval(str_replace(",", "", $this->input->post('bayar_total', 'true')));
                $tgl_tempo   = date('Y-m-d', strtotime('+' . $termin . ' days', strtotime($tanggal)));
            }

            $data = array(
                'pembelian_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
                'pembelian_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
                'suplier_id'            => $this->input->post('suplier_id', 'true'),
                'tipe_bayar_id'         => $this->input->post('lstTipeBayar', 'true'),
                'pembelian_subtotal'    => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
                'pembelian_disc'        => intval(str_replace(",", "", $this->input->post('diskon', 'true'))),
                'pembelian_total'       => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
                'pembelian_sisa_hutang' => $sisa_hutang,
                'user_username'         => $username,
                'pembelian_checker'     => trim(stripHTMLtags($this->input->post('checker', 'true'))),
                'pembelian_penerima'    => trim(stripHTMLtags($this->input->post('penerima', 'true'))),
                'pembelian_tgl_tempo'   => $tgl_tempo,
                'pembelian_update'      => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_pembelian', $data);
            $pembelian_id = $this->db->insert_id();

            // Simpan Detail Barang
            $listTemp = $this->db->get_where('ok_pembelian_temp', array('user_username' => $username))->result();
            foreach ($listTemp as $r) {
                $dataItem = array(
                    'pembelian_id'                 => $pembelian_id,
                    'barang_id'                    => $r->barang_id,
                    'pembelian_detail_qty'         => $r->pembelian_temp_qty,
                    'unit_id'                      => $r->unit_id,
                    'pembelian_detail_harga'       => $r->pembelian_temp_harga,
                    'pembelian_detail_disc1'       => $r->pembelian_temp_disc1,
                    'pembelian_detail_disc2'       => $r->pembelian_temp_disc2,
                    'pembelian_detail_disc3'       => $r->pembelian_temp_disc3,
                    'pembelian_detail_disc4'       => $r->pembelian_temp_disc4,
                    'pembelian_detail_ppn'         => $r->pembelian_temp_ppn,
                    'pembelian_detail_harga_pokok' => $r->pembelian_temp_harga_pokok,
                    'pembelian_detail_total'       => $r->pembelian_temp_total,
                    'pembelian_detail_update'      => date('Y-m-d H:i:s'),
                );

                $this->db->insert('ok_pembelian_detail', $dataItem);

                // Check Stok Unit
                $barang_id = $r->barang_id;
                $unit_id   = $r->unit_id;
                $dataUnit  = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
                $Multiple  = $dataUnit->unit_multiple;
                // Data Unit Utama
                $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
                $unit_id_utama = $dataUnitUtama->unit_id;
                $StokUtama     = $dataUnitUtama->unit_qty;
                if ($unit_id == $unit_id_utama) {
                    $Stok = ($StokUtama + $r->pembelian_temp_qty);
                } else {
                    $Stok = ($StokUtama + ($r->pembelian_temp_qty * $Multiple));
                }

                // Update Stok Utama
                $dataStok = array(
                    'unit_qty'    => $Stok,
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_utama);
                $this->db->update('ok_unit', $dataStok);

                // Konversi ke Unit Lain selain Utama
                $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
                foreach ($listUnit as $u) {
                    $dataStokUnit = array(
                        'unit_qty'    => ($Stok / $u->unit_multiple),
                        'unit_update' => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('unit_id', $u->unit_id);
                    $this->db->update('ok_unit', $dataStokUnit);
                }

                // Update Suplier Barang
                $barang_id  = $r->barang_id;
                $dataBarang = array(
                    'suplier_id'      => $this->input->post('suplier_id', 'true'),
                    'barang_tgl_beli' => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
                );

                $this->db->where('barang_id', $barang_id);
                $this->db->update('ok_barang', $dataBarang);
            }

            // Hapus Temp by Username dan Tanggal
            $username = $this->session->userdata('username');
            $this->db->where('user_username', $username);
            $this->db->delete('ok_pembelian_temp');

            $response = ['status' => 'success'];
            echo json_encode($response);
        }
    }

    // Detail Edit
    private function _get_detail_datatables_query($pembelian_id)
    {
        $this->db->from($this->table4);
        $this->db->where('pembelian_id', $pembelian_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($pembelian_id)
    {
        $this->_get_detail_datatables_query($pembelian_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($pembelian_id)
    {
        $this->_get_detail_datatables_query($pembelian_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($pembelian_id)
    {
        $this->db->from($this->table4);
        $this->db->where('pembelian_id', $pembelian_id);

        return $this->db->count_all_results();
    }

    public function insert_data_detail()
    {
        $data = array(
            'pembelian_id'                 => $this->input->post('pembelian_id', 'true'),
            'barang_id'                    => $this->input->post('barang_id', 'true'),
            'pembelian_detail_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                      => $this->input->post('unit_id', 'true'),
            'pembelian_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'pembelian_detail_disc1'       => $this->input->post('disc1', 'true'),
            'pembelian_detail_disc2'       => $this->input->post('disc2', 'true'),
            'pembelian_detail_disc3'       => $this->input->post('disc3', 'true'),
            'pembelian_detail_disc4'       => $this->input->post('disc4', 'true'),
            'pembelian_detail_ppn'         => $this->input->post('ppn', 'true'),
            'pembelian_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'pembelian_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'pembelian_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_pembelian_detail', $data);

        // Update Stok
        $barang_id     = $this->input->post('barang_id', 'true');
        $unit_id       = $this->input->post('unit_id', 'true');
        $qty           = floatval(str_replace(",", "", $this->input->post('qty', 'true')));
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama + $qty);
        } else {
            $Stok = (($qty * $Multiple) + $StokUtama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }

        $dataBarang = array(
            'suplier_id'      => $this->input->post('suplier_id', 'true'),
            'barang_tgl_beli' => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
        );

        $this->db->where('barang_id', $barang_id);
        $this->db->update('ok_barang', $dataBarang);
    }

    public function update_data_detail()
    {
        $pembelian_detail_id = $this->input->post('pembelian_detail_id', 'true');
        $barang_id           = $this->input->post('barang_id', 'true');
        $unit_id             = $this->input->post('unit_id', 'true');
        $qty                 = floatval(str_replace(",", "", $this->input->post('qty', 'true')));
        $qty_lama            = $this->input->post('qty_lama', 'true');
        $data                = array(
            'pembelian_detail_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                      => $this->input->post('unit_id', 'true'),
            'pembelian_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'pembelian_detail_disc1'       => $this->input->post('disc1', 'true'),
            'pembelian_detail_disc2'       => $this->input->post('disc2', 'true'),
            'pembelian_detail_disc3'       => $this->input->post('disc3', 'true'),
            'pembelian_detail_disc4'       => $this->input->post('disc4', 'true'),
            'pembelian_detail_ppn'         => $this->input->post('ppn', 'true'),
            'pembelian_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'pembelian_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'pembelian_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_detail_id', $pembelian_detail_id);
        $this->db->update('ok_pembelian_detail', $data);

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = (($StokUtama - $qty_lama) + $qty); // Stok Utama - Qty Lama + Qty Baru
        } else {
            $MultipleLama = ($qty_lama * $Multiple);
            $Stok         = (($StokUtama - $MultipleLama) + ($qty * $Multiple));
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function delete_data_detail($id)
    {
        // Hitung Stok dulu
        $dataLama  = $this->db->get_where('ok_pembelian_detail', array('pembelian_detail_id' => $id))->row();
        $barang_id = $dataLama->barang_id;
        $unit_id   = $dataLama->unit_id;
        $qty       = $dataLama->pembelian_detail_qty;

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama - $qty); // Stok Utama - Qty
        } else {
            $MultipleLama = ($qty * $Multiple);
            $Stok         = ($StokUtama - $MultipleLama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }

        // Hapus Data
        $this->db->where('pembelian_detail_id', $id);
        $this->db->delete('ok_pembelian_detail');
    }

    public function update_data_pembelian()
    {
        $pembelian_id = $this->input->post('pembelian_id', 'true');
        $username     = $this->session->userdata('username');
        $dibayar      = $this->input->post('dibayar', 'true');
        $tipebayar    = $this->input->post('lstTipeBayar', 'true');
        $jenisbayar   = $this->db->get_where('ok_tipe_bayar', array('tipe_bayar_id' => $tipebayar))->row();
        $tipe_set     = $jenisbayar->tipe_bayar_set;
        if ($tipe_set == 'D') {
            $sisa_hutang = 0;
        } else {
            $sisa_hutang = intval(str_replace(",", "", $this->input->post('bayar_total', 'true')));
        }

        $data = array(
            'pembelian_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'pembelian_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'suplier_id'            => $this->input->post('suplier_id', 'true'),
            'tipe_bayar_id'         => $this->input->post('lstTipeBayar', 'true'),
            'pembelian_subtotal'    => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
            'pembelian_disc'        => intval(str_replace(",", "", $this->input->post('diskon', 'true'))),
            'pembelian_total'       => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
            'pembelian_sisa_hutang' => $sisa_hutang,
            'user_username'         => $username,
            'pembelian_checker'     => trim(stripHTMLtags($this->input->post('checker', 'true'))),
            'pembelian_penerima'    => trim(stripHTMLtags($this->input->post('penerima', 'true'))),
            'pembelian_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_id', $pembelian_id);
        $this->db->update('ok_pembelian', $data);
    }

    public function insert_data_suplier()
    {
        $data = array(
            'suplier_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_suplier', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_email'  => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'suplier_kontak' => strtoupper(trim(stripHTMLtags($this->input->post('kontak', 'true')))),
            'suplier_termin' => $this->input->post('termin', 'true'),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_suplier', $data);
    }

    public function delete_data_pembelian($id)
    {
        $listDetail = $this->db->order_by('pembelian_detail_id', 'asc')->get_where('ok_pembelian_detail', array('pembelian_id' => $id))->result();
        foreach ($listDetail as $d) {
            // Hitung Stok dulu
            $pembelian_detail_id = $d->pembelian_detail_id;
            $dataLama            = $this->db->get_where('ok_pembelian_detail', array('pembelian_detail_id' => $pembelian_detail_id))->row();
            $barang_id           = $dataLama->barang_id;
            $unit_id             = $dataLama->unit_id;
            $qty                 = $dataLama->pembelian_detail_qty;

            // Update Stok
            $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple      = $dataUnit->unit_multiple;
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            if ($unit_id == $unit_id_utama) {
                $Stok = ($StokUtama - $qty); // Stok Utama - Qty
            } else {
                $MultipleLama = ($qty * $Multiple);
                $Stok         = ($StokUtama - $MultipleLama);
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'    => $Stok,
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'    => ($Stok / $u->unit_multiple),
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Data
        $this->db->where('pembelian_id', $id);
        $this->db->delete('ok_pembelian');
    }

    public function update_data_unit()
    {
        $unit_id = $this->input->post('unit_id_ubah', 'true');
        $data    = array(
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('editdisc1', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('editdisc2', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('editdisc3', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('editdisc4', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('editppn', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('edit_harga_pokok', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true'))),
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id);
        $this->db->update('ok_unit', $data);

        // Log harga Beli
        $unit_buy_old = $this->input->post('unit_hrg_beli_old', 'true');
        $unit_buy_new = intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true')));
        // Log harga jual
        $unit_sell_old = $this->input->post('unit_hrg_jual_old', 'true');
        $unit_sell_new = intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true')));

        if ($unit_buy_old != $unit_buy_new || $unit_sell_old != $unit_sell_new) {
            $margin_buy   = ($unit_buy_new - $unit_buy_old);
            $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
            $margin_sell  = ($unit_sell_new - $unit_sell_old);
            $margin       = @($margin_sell / $unit_sell_old);
            $percent_sell = ($margin * 100);
            $dataLog      = array(
                'unit_id'               => $unit_id,
                'log_date'              => date('Y-m-d'),
                'log_harga_beli_lama'   => $unit_buy_old,
                'log_harga_beli_baru'   => $unit_buy_new,
                'log_harga_beli_persen' => $percent_buy,
                'log_harga_pokok_lama'  => $this->input->post('unit_hrg_pokok_old', 'true'),
                'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
                'log_harga_jual_lama'   => $unit_sell_old,
                'log_harga_jual_baru'   => $unit_sell_new,
                'log_harga_jual_persen' => $percent_sell,
                'user_username'         => $this->session->userdata('username'),
                'log_update'            => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_unit_log', $dataLog);
        }

        // Check Apakah Utama
        $barang_id  = $this->input->post('barang_id_ubah', 'true');
        $harga_beli = intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true')));
        $harga_jual = intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true')));
        $multiple   = $this->input->post('unit_id_multiple', 'true');
        if ($multiple > 0) {
            $harga_beli_baru = ($harga_beli / $multiple);
            $harga_jual_baru = ($harga_jual / $multiple);
        } else {
            $harga_beli_baru = $harga_beli;
            $harga_jual_baru = $harga_jual;
        }

        $status_unit = $this->input->post('unit_set', 'true');
        if ($status_unit == 1) {
            // Langsung Ubah Harga Beli dan Pokok Unit lain
            $listUnitLain = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id))->result();
            foreach ($listUnitLain as $r) {
                $unit_id_lain    = $r->unit_id;
                $multiple_lain   = $r->unit_multiple;
                $unit_disc1      = $r->unit_disc1;
                $unit_disc2      = $r->unit_disc2;
                $unit_disc3      = $r->unit_disc3;
                $unit_disc4      = $r->unit_disc4;
                $unit_ppn        = $r->unit_ppn;
                $unit_beli_lama  = $r->unit_hrg_beli;
                $unit_pokok_lama = $r->unit_hrg_pokok;
                $unit_jual_lama  = $r->unit_hrg_jual;
                $harga_beli_lain = ($harga_beli_baru * $multiple_lain);
                $harga_jual_lain = ($harga_jual_baru * $multiple_lain);
                if ($harga_beli_lain == 0) {
                    $HargaPokok = 0;
                } elseif ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc2) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc3) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc4) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc2 - $Diskon3;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else {
                    $HargaPokok = $harga_beli_lain;
                }

                if ($unit_ppn > 0) {
                    $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                    $HargaPokok = ($HargaPokok + $Pajak);
                } else {
                    $HargaPokok = $HargaPokok;
                }

                // Update Data
                $dataUpdateLain = array(
                    'unit_hrg_beli'  => $harga_beli_lain,
                    'unit_hrg_jual'  => $harga_jual_lain,
                    'unit_disc1'     => $unit_disc1,
                    'unit_disc2'     => $unit_disc2,
                    'unit_disc3'     => $unit_disc3,
                    'unit_disc4'     => $unit_disc4,
                    'unit_ppn'       => $unit_ppn,
                    'unit_hrg_pokok' => $HargaPokok,
                    'unit_update'    => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_lain);
                $this->db->update('ok_unit', $dataUpdateLain);

                // Log harga Beli
                $unit_buy_old  = $unit_beli_lama;
                $unit_buy_new  = $harga_beli_lain;
                $unit_sell_old = $unit_jual_lama;
                $unit_sell_new = $harga_jual_lain;
                if ($unit_buy_old != $unit_buy_new) {
                    $margin_buy   = ($unit_buy_new - $unit_buy_old);
                    $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                    $margin_sell  = ($unit_sell_new - $unit_sell_old);
                    $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                    $dataLogLain  = array(
                        'unit_id'               => $unit_id_lain,
                        'log_date'              => date('Y-m-d'),
                        'log_harga_beli_lama'   => $unit_buy_old,
                        'log_harga_beli_baru'   => $unit_buy_new,
                        'log_harga_beli_persen' => $percent_buy,
                        'log_harga_pokok_lama'  => $unit_pokok_lama,
                        'log_harga_pokok_baru'  => $HargaPokok,
                        // 'log_harga_jual_lama'   => $unit_sell_old,
                        // 'log_harga_jual_baru'   => $unit_sell_new,
                        // 'log_harga_jual_persen' => $percent_sell,
                        'user_username'         => $this->session->userdata('username'),
                        'log_update'            => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_unit_log', $dataLogLain);
                }
            }
        } else {
            // Cari yang Utama dulu, ubah Harga Beli dan Harga Pokok dl
            $dataUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            if (count($dataUtama) > 0) {
                $unit_id_utama    = $dataUtama->unit_id;
                $multiple_utama   = $dataUtama->unit_multiple;
                $unit_disc1       = $dataUtama->unit_disc1;
                $unit_disc2       = $dataUtama->unit_disc2;
                $unit_disc3       = $dataUtama->unit_disc3;
                $unit_disc4       = $dataUtama->unit_disc4;
                $unit_ppn         = $dataUtama->unit_ppn;
                $unit_beli_lama   = $dataUtama->unit_hrg_beli;
                $unit_pokok_lama  = $dataUtama->unit_hrg_pokok;
                $unit_jual_lama   = $dataUtama->unit_hrg_jual;
                $harga_beli_utama = $harga_beli_baru;
                $harga_jual_utama = $harga_jual_baru;
                if ($harga_beli_utama == 0) {
                    $HargaPokok = 0;
                } elseif ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc2) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc3) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc4) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc2 - $Diskon3;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else {
                    $HargaPokok = $harga_beli_utama;
                }

                if ($unit_ppn > 0) {
                    $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                    $HargaPokok = ($HargaPokok + $Pajak);
                } else {
                    $HargaPokok = $HargaPokok;
                }

                // Update Data
                $dataUpdateUtama = array(
                    'unit_hrg_beli'  => $harga_beli_utama,
                    // 'unit_hrg_jual'  => $harga_jual_utama,
                    'unit_disc1'     => $unit_disc1,
                    'unit_disc2'     => $unit_disc2,
                    'unit_disc3'     => $unit_disc3,
                    'unit_disc4'     => $unit_disc4,
                    'unit_ppn'       => $unit_ppn,
                    'unit_hrg_pokok' => $HargaPokok,
                    'unit_update'    => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_utama);
                $this->db->update('ok_unit', $dataUpdateUtama);

                // Log harga Beli
                $unit_buy_old  = $unit_beli_lama;
                $unit_buy_new  = $harga_beli_utama;
                $unit_sell_old = $unit_jual_lama;
                $unit_sell_new = $harga_jual_utama;
                if ($unit_buy_old != $unit_buy_new) {
                    $margin_buy   = ($unit_buy_new - $unit_buy_old);
                    $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                    $margin_sell  = ($unit_sell_new - $unit_sell_old);
                    $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                    $dataLogLain  = array(
                        'unit_id'               => $unit_id_utama,
                        'log_date'              => date('Y-m-d'),
                        'log_harga_beli_lama'   => $unit_buy_old,
                        'log_harga_beli_baru'   => $unit_buy_new,
                        'log_harga_beli_persen' => $percent_buy,
                        'log_harga_pokok_lama'  => $unit_pokok_lama,
                        'log_harga_pokok_baru'  => $HargaPokok,
                        // 'log_harga_jual_lama'   => $unit_sell_old,
                        // 'log_harga_jual_baru'   => $unit_sell_new,
                        // 'log_harga_jual_persen' => $percent_sell,
                        'user_username'         => $this->session->userdata('username'),
                        'log_update'            => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_unit_log', $dataLogLain);
                }

                // Cari Unit Lain selain Utama dan Unit Edit
                $listUnitLain = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id, 'unit_set !=' => 1))->result();
                foreach ($listUnitLain as $r) {
                    $unit_id_lain    = $r->unit_id;
                    $multiple_lain   = $r->unit_multiple;
                    $unit_disc1      = $r->unit_disc1;
                    $unit_disc2      = $r->unit_disc2;
                    $unit_disc3      = $r->unit_disc3;
                    $unit_disc4      = $r->unit_disc4;
                    $unit_ppn        = $r->unit_ppn;
                    $unit_beli_lama  = $r->unit_hrg_beli;
                    $unit_pokok_lama = $r->unit_hrg_pokok;
                    $harga_beli_lain = ($harga_beli_baru * $multiple_lain);
                    $harga_jual_lain = ($harga_jual_baru * $multiple_lain);
                    if ($harga_beli_lain == 0) {
                        $HargaPokok = 0;
                    } elseif ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc2) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc3) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc4) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaPokok = $HargaDisc2 - $Diskon3;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaDisc3 = $HargaDisc2 - $Diskon3;
                        $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc3 - $Diskon4;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaDisc3 = $HargaDisc2 - $Diskon3;
                        $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc3 - $Diskon4;
                    } else {
                        $HargaPokok = $harga_beli_lain;
                    }

                    if ($unit_ppn > 0) {
                        $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                        $HargaPokok = ($HargaPokok + $Pajak);
                    } else {
                        $HargaPokok = $HargaPokok;
                    }

                    // Update Data
                    $dataUpdateLain = array(
                        'unit_hrg_beli'  => $harga_beli_lain,
                        'unit_hrg_jual'  => $harga_jual_lain,
                        'unit_disc1'     => $unit_disc1,
                        'unit_disc2'     => $unit_disc2,
                        'unit_disc3'     => $unit_disc3,
                        'unit_disc4'     => $unit_disc4,
                        'unit_ppn'       => $unit_ppn,
                        'unit_hrg_pokok' => $HargaPokok,
                        'unit_update'    => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('unit_id', $unit_id_lain);
                    $this->db->update('ok_unit', $dataUpdateLain);

                    // Log harga Beli
                    $unit_buy_old  = $unit_beli_lama;
                    $unit_buy_new  = $harga_beli_lain;
                    $unit_sell_old = $unit_jual_lama;
                    $unit_sell_new = $harga_jual_lain;
                    if ($unit_buy_old != $unit_buy_new) {
                        $margin_buy   = ($unit_buy_new - $unit_buy_old);
                        $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                        $margin_sell  = ($unit_sell_new - $unit_sell_old);
                        $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                        $dataLogLain  = array(
                            'unit_id'               => $unit_id_lain,
                            'log_date'              => date('Y-m-d'),
                            'log_harga_beli_lama'   => $unit_buy_old,
                            'log_harga_beli_baru'   => $unit_buy_new,
                            'log_harga_beli_persen' => $percent_buy,
                            'log_harga_pokok_lama'  => $unit_pokok_lama,
                            'log_harga_pokok_baru'  => $HargaPokok,
                            'log_harga_jual_lama'   => $unit_sell_old,
                            'log_harga_jual_baru'   => $unit_sell_new,
                            'log_harga_jual_persen' => $percent_sell,
                            'user_username'         => $this->session->userdata('username'),
                            'log_update'            => date('Y-m-d H:i:s'),
                        );

                        $this->db->insert('ok_unit_log', $dataLogLain);
                    }
                }
            }
        }

        $response = ['unit_id' => $unit_id];
        echo json_encode($response);
    }

    public function insert_data_barang()
    {
        $data = array(
            'barang_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode_add', 'true')))),
            'barang_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama_add', 'true')))),
            'barang_seo'    => seo_title(trim(stripHTMLtags($this->input->post('nama_add', 'true')))),
            'barang_merk'   => strtoupper(trim(stripHTMLtags($this->input->post('merk_add', 'true')))),
            'kategori_id'   => trim($this->input->post('lstKategori', 'true')),
            'barang_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_barang', $data);
        $barang_id = $this->db->insert_id();

        // Insert ke Table Price Stock
        $dataStock = array(
            'barang_id'      => $barang_id,
            'unit_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('unit_add', 'true')))),
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('harga_beli_add', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('disc1_add', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('disc2_add', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('disc3_add', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('disc4_add', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('ppn_add', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('harga_pokok_add', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('harga_jual_add', 'true'))),
            'unit_set'       => 1,
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit', $dataStock);
        $unit_id = $this->db->insert_id();

        // Update Satuan Barang
        $dataUnit = array(
            'unit_id' => $unit_id,
        );

        $this->db->where('barang_id', $barang_id);
        $this->db->update('ok_barang', $dataUnit);

        // Insert ke Log Price
        $dataLog = array(
            'unit_id'               => $unit_id,
            'log_date'              => date('Y-m-d'),
            'log_harga_beli_lama'   => 0,
            'log_harga_beli_baru'   => intval(str_replace(",", "", $this->input->post('harga_beli_add', 'true'))),
            'log_harga_pokok_lama'  => 0,
            'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok_add', 'true'))),
            'log_harga_beli_persen' => 0,
            'log_harga_jual_lama'   => 0,
            'log_harga_jual_baru'   => intval(str_replace(",", "", $this->input->post('harga_jual_add', 'true'))),
            'log_harga_jual_persen' => 0,
            'user_username'         => $this->session->userdata('username'),
            'log_update'            => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit_log', $dataLog);

        $response = ['status' => 'success', 'id' => $barang_id];
        echo json_encode($response);
    }
}
/* Location: ./application/model/admin/Pembelian_m.php */

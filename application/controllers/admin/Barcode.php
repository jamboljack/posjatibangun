<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barcode extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('zend');
        $this->load->library('template');
        $this->load->model('admin/barcode_m');
    }

    public function index()
    {
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $this->template->display('admin/master/barcode_v', $data);
    }

    public function data_list()
    {
        $List = $this->barcode_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a title="Tambahkan Data" href="javascript:void(0)" onclick="addData(' . "'" . $barang_id . "'" . ')"><i class="fa fa-plus-circle"></i></a>';
            $row[]     = $no;
            $row[]     = $r->barang_kode;
            $row[]     = $r->barang_nama;
            $row[]     = number_format($r->unit_qty, 2, '.', ',');
            $row[]     = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[]    = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->barcode_m->count_all(),
            "recordsFiltered" => $this->barcode_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_barcode_list()
    {
        $List = $this->barcode_m->get_barcode_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row        = array();
            $barcode_id = $r->barcode_id;
            $row[]      = '<a onclick="hapusData(' . $barcode_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]      = $no;
            $row[]      = $r->barang_kode;
            $row[]      = $r->barang_nama;
            $row[]      = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[]     = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->barcode_m->count_barcode_all(),
            "recordsFiltered" => $this->barcode_m->count_barcode_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function savedata()
    {
        $barang_id  = $this->input->post('id', 'true');
        $dataBarang = $this->db->get_where('ok_barang', array('barang_id' => $barang_id))->row();
        $Kode       = $dataBarang->barang_kode;
        $this->zend->load('Zend/Barcode');
        $image_resource = Zend_Barcode::factory('code128', 'image', array('text' => $Kode, 'barHeight' => 30, 'drawText' => false))->draw();
        $image_name     = $Kode . '_' . time() . '.jpg';
        $image_dir      = './img/barcode/';
        imagejpeg($image_resource, $image_dir . $image_name);
        $jumlah = intval(str_replace(",", "", $this->input->post('jumlah', 'true')));
        for ($i = 1; $i <= $jumlah; $i++) {
            $data = array(
                'barang_id'      => $barang_id,
                'barcode_image'  => $image_name,
                'barcode_update' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_barcode', $data);
        }
    }

    public function deletedata($id)
    {
        $this->db->where('barcode_id', $id);
        $this->db->delete('ok_barcode');
    }

    public function deletedataall()
    {
        $this->db->truncate('ok_barcode');
    }

    public function printbarcode()
    {
        $data['kontak'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $listData       = $this->db->order_by('barang_kode', 'asc')->get('v_barcode')->result();
        $time           = time();
        $filename       = 'Barcode-' . $time;
        $mpdf           = new \Mpdf\Mpdf(['format' => [40, 25], 'default_font_size' => 7, 'default_font' => 'tahoma']);
        $mpdf->SetTitle('Cetak Barcode');
        foreach ($listData as $r) {
            $barcode_id     = $r->barcode_id;
            $data['detail'] = $this->db->get_where('v_barcode', array('barcode_id' => $barcode_id))->row();
            $mpdf->AddPage('P', '', '', '', '', 2, 2, 2, 2, 6, 3);
            $html = $this->load->view('print/previewbarcode_v.php', $data, true);
            $mpdf->WriteHTML($html);
        }
        $pdfFilePath = FCPATH . "download/$filename.pdf";
        $mpdf->Output($pdfFilePath, 'F');
        header("Content-type: application/pdf");
        header("Content-Length:" . filesize($pdfFilePath));
        readfile($pdfFilePath);
        exit;
    }

    public function printnonharga()
    {
        $data['kontak'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $listData       = $this->db->order_by('barang_kode', 'asc')->get('v_barcode')->result();
        $time           = time();
        $filename       = 'Barcode-' . $time;
        $mpdf           = new \Mpdf\Mpdf(['format' => [40, 25], 'default_font_size' => 7, 'default_font' => 'tahoma']);
        $mpdf->SetTitle('Cetak Barcode');
        foreach ($listData as $r) {
            $barcode_id     = $r->barcode_id;
            $data['detail'] = $this->db->get_where('v_barcode', array('barcode_id' => $barcode_id))->row();
            $mpdf->AddPage('P', '', '', '', '', 2, 2, 2, 2, 6, 3);
            $html = $this->load->view('print/previewbarcodenon_v.php', $data, true);
            $mpdf->WriteHTML($html);
        }
        $pdfFilePath = FCPATH . "download/$filename.pdf";
        $mpdf->Output($pdfFilePath, 'F');
        header("Content-type: application/pdf");
        header("Content-Length:" . filesize($pdfFilePath));
        readfile($pdfFilePath);
        exit;
    }

    public function printrak()
    {
        $data['kontak']   = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['listData'] = $this->db->order_by('barang_kode', 'asc')->get('v_barcode')->result();
        $this->load->view('print/printrak_v', $data);
    }
}
/* Location: ./application/controller/admin/Barcode.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/penjualan_m');
    }

    public function index()
    {
        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $data['listTipe']      = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/penjualan/view', $data);
    }

    public function data_list()
    {
        $List = $this->penjualan_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $penjualan_id = $r->penjualan_id;
            $linkedit     = site_url('admin/penjualan/editdata/' . $penjualan_id);
            $linkprint    = site_url('admin/penjualan/printfaktur/' . $penjualan_id);
            $row[]        = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                      <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>
                      <a onclick="printNota(' . $penjualan_id . ')" title="Print Nota"><i class="icon-screen-tablet"></i></a>
                      <a onclick="printSurat(' . $penjualan_id . ')" title="Print Surat Jalan"><i class="fa fa-truck"></i></a>';
            $row[]  = $no;
            $row[]  = $r->penjualan_no_faktur;
            $row[]  = date('d-m-Y H:i', strtotime($r->penjualan_update));
            $row[]  = $r->pelanggan_nama;
            $row[]  = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]  = number_format($r->penjualan_bruto, 0, '', ',');
            $row[]  = number_format($r->penjualan_diskon, 0, '', ',');
            $row[]  = number_format($r->penjualan_total, 0, '', ',');
            $row[]  = ($r->penjualan_kirim == 'Y' ? '<span class="label label-success">YA</span>' : '<span class="label label-danger">TIDAK</span>');
            $row[]  = $r->user_username;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_all(),
            "recordsFiltered" => $this->penjualan_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_penjualan_temp');

        $data['meta']         = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listTipe']     = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $data['listSales']    = $this->db->order_by('sales_nama', 'asc')->get('ok_sales')->result();
        $data['listBank']     = $this->db->order_by('bank_nama', 'asc')->get('ok_bank')->result();
        $this->template->display('admin/penjualan/add', $data);
    }

    private function kode_exists($kode)
    {
        $this->db->where('pelanggan_kode', $kode);
        $query = $this->db->get('ok_pelanggan');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function kodebarang_exists($kode_add)
    {
        $this->db->where('barang_kode', $kode_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kodebarang_exists()
    {
        if (array_key_exists('kode_add', $_POST)) {
            if ($this->kodebarang_exists($this->input->post('kode_add', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function namabarang_exists($nama_add)
    {
        $this->db->where('barang_nama', $nama_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_namabarang_exists()
    {
        if (array_key_exists('nama_add', $_POST)) {
            if ($this->namabarang_exists(stripHTMLtags($this->input->post('nama_add', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function data_temp_list()
    {
        $List = $this->penjualan_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row               = array();
            $penjualan_temp_id = $r->penjualan_temp_id;
            $row[]             = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $penjualan_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $penjualan_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->penjualan_temp_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->penjualan_temp_harga, 0, '', ',');
            $row[]  = number_format($r->penjualan_temp_disc, 2, '.', ',');
            $row[]  = number_format($r->penjualan_temp_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_tmp_all(),
            "recordsFiltered" => $this->penjualan_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_pelanggan_list()
    {
        $List = $this->penjualan_m->get_pelanggan_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pelanggan_id = $r->pelanggan_id;
            $row[]        = '<a href="javascript:void(0)" title="Pilih" onclick="pilihpelanggan(' . "'" . $pelanggan_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]        = $no;
            $row[]        = $r->pelanggan_kode;
            $row[]        = $r->pelanggan_nama;
            $row[]        = $r->pelanggan_alamat;
            $row[]        = $r->pelanggan_kota;
            $row[]        = $r->pelanggan_telp;
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_pelanggan_all(),
            "recordsFiltered" => $this->penjualan_m->count_pelanggan_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_barang_list()
    {
        $List = $this->penjualan_m->get_barang_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $barang_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]     = $no;
            if ($r->unit_qty <= $r->barang_min_stok) {
                $kode = '<span class="label label-danger">' . $r->barang_kode . '</span>';
                $nama = '<span class="label label-danger">' . $r->barang_nama . '</span>';
                $merk = '<span class="label label-danger">' . $r->barang_merk . '</span>';
                $qty  = '<span class="label label-danger">' . number_format($r->unit_qty, 2, '.', ',') . '</span>';
            } else {
                $kode = $r->barang_kode;
                $nama = $r->barang_nama;
                $merk = $r->barang_merk;
                $qty  = number_format($r->unit_qty, 2, '.', ',');
            }
            $row[]    = $kode;
            $row[]    = $nama;
            $row[]    = $merk;
            $row[]    = $qty;
            $row[]    = number_format($r->unit_hrg_jual, 0, '', ',') . ' - ' . $r->unit_nama;
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set !=' => 1))->result();
            $unit     = '';
            foreach ($listUnit as $u) {
                $unit .= number_format($u->unit_hrg_jual, 0, '', ',') . ' - ' . $u->unit_nama . '<br>';
            }
            $row[]  = $unit;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_barang_all(),
            "recordsFiltered" => $this->penjualan_m->count_barang_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_surat($id)
    {
        // Update Status Print jadi 2
        // $data = array(
        //     'penjualan_print' => 2,
        // );
        // $this->db->where('penjualan_id', $id);
        // $this->db->update('ok_penjualan', $data);

        $data = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_list_item($id)
    {
        $data = $this->db->get_where('v_penjualan_detail', array('penjualan_id' => $id))->result();
        echo json_encode($data);
    }

    public function get_data_pelanggan($id)
    {
        $data = $this->db->get_where('ok_pelanggan', array('pelanggan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_pelanggan_by_kode($kode_pelanggan)
    {
        $data = $this->db->get_where('ok_pelanggan', array('pelanggan_kode' => trim(strtoupper($kode_pelanggan))))->row();
        echo json_encode($data);
    }

    public function get_data_barang($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_barang_by_kode($kode_barang)
    {
        $data = $this->db->get_where('v_barang', array('barang_kode' => trim(strtoupper($kode_barang))))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_penjualan', array('penjualan_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_harga_terakhir($barang_id, $pelanggan_id)
    {
        $data = $this->db->order_by('penjualan_tanggal', 'desc')->limit(1)->get_where('v_penjualan_detail', array('barang_id' => $barang_id, 'pelanggan_id' => $pelanggan_id))->row();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $total = intval(str_replace(",", "", $this->input->post('total', 'true')));
        if ($total == 0) {
            $response = ['status' => 'failed'];
        } else {
            $this->penjualan_m->insert_data_item();
            $response = ['status' => 'success'];
        }

        echo json_encode($response);
    }

    public function updateitem()
    {
        $this->penjualan_m->update_data_item();
    }

    public function updatedatapelanggan()
    {
        $penjualan_id = $this->input->post('id', 'true');
        $data         = array(
            'penjualan_nama'   => strtoupper(stripHTMLtags($this->input->post('nama_pelanggan', 'true'))),
            'penjualan_alamat' => strtoupper(stripHTMLtags($this->input->post('alamat_pelanggan', 'true'))),
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $data);
    }

    public function deleteitem($id)
    {
        $this->penjualan_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('penjualan_temp_total', 'total')->get_where('ok_penjualan_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function data_unit_list($barang_id)
    {
        $List = $this->penjualan_m->get_unit_datatables($barang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row     = array();
            $unit_id = $r->unit_id;
            $row[]   = '<a href="javascript:void(0)" title="Pilih" onclick="pilihunit(' . "'" . $unit_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]   = $r->unit_nama;
            $row[]   = number_format($r->unit_multiple, 2, '.', ',');
            $row[]   = number_format($r->unit_qty, 0, '', ',');
            $row[]   = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[]  = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_unit_all($barang_id),
            "recordsFiltered" => $this->penjualan_m->count_unit_filtered($barang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_unit($id)
    {
        $data = $this->db->get_where('v_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function savedata()
    {
        $this->penjualan_m->insert_data_penjualan();
    }

    public function editdata($penjualan_id)
    {
        $data['listTipe'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $data['detail']   = $this->db->get_where('v_penjualan', array('penjualan_id' => $penjualan_id))->row();
        $this->template->display('admin/penjualan/edit', $data);
    }

    public function data_detail_list($penjualan_id)
    {
        $List = $this->penjualan_m->get_detail_datatables($penjualan_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->penjualan_detail_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->penjualan_detail_harga, 0, '', ',');
            $row[]  = number_format($r->penjualan_detail_disc, 2, '.', ',');
            $row[]  = number_format($r->penjualan_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->penjualan_m->count_detail_all($penjualan_id),
            "recordsFiltered" => $this->penjualan_m->count_detail_filtered($penjualan_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($penjualan_id)
    {
        $data = $this->db->select_sum('penjualan_detail_total', 'total')->get_where('ok_penjualan_detail', array('penjualan_id' => $penjualan_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->penjualan_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_penjualan_detail', array('penjualan_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->penjualan_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->penjualan_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->penjualan_m->update_data_penjualan();
    }

    public function savedatapelanggan()
    {
        $this->penjualan_m->insert_data_pelanggan();
    }

    public function deletedata($id)
    {
        $this->penjualan_m->delete_data_penjualan($id);
    }

    public function printfaktur($id)
    {
        // $data = array(
        //     'penjualan_print' => 2,
        // );

        // $this->db->where('penjualan_id', $id);
        // $this->db->update('ok_penjualan', $data);

        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_penjualan_detail', array('penjualan_id' => $id))->result();
        $this->load->view('admin/penjualan/printfaktur_v', $data);
    }

    public function printsuratjalan($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_penjualan_detail', array('penjualan_id' => $id))->result();
        $this->load->view('admin/penjualan/printsuratjalan_v', $data);
    }

    public function savedatabarang()
    {
        $this->penjualan_m->insert_data_barang();
    }
}
/* Location: ./application/controller/admin/Penjualan.php */

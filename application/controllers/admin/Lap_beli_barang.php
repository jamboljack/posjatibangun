<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_beli_barang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/lap_beli_barang_m');
    }

    public function index()
    {
        $this->template->display('admin/reportbeli/reportbelibarang_v');
    }

    public function data_barang_list()
    {
        $List = $this->lap_beli_barang_m->get_barang_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $barang_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]     = $no;
            $row[]     = $r->barang_kode;
            $row[]     = $r->barang_nama;
            $row[]     = $r->kategori_nama;
            $row[]     = number_format($r->unit_qty, 2, '.', ',');
            $row[]     = $r->unit_nama;
            $row[]     = number_format($r->unit_hrg_beli, 0, '', ',');
            $data[]    = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_beli_barang_m->count_barang_all(),
            "recordsFiltered" => $this->lap_beli_barang_m->count_barang_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_list()
    {
        $List = $this->lap_beli_barang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = number_format($r->pembelian_detail_qty, 0, '', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->pembelian_detail_harga, 0, '', ',');
            $row[]  = number_format($r->pembelian_detail_ppn, 2, '.', ',');
            $row[]  = number_format($r->pembelian_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_beli_barang_m->count_all(),
            "recordsFiltered" => $this->lap_beli_barang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_barang($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function printbarang($dari = 'all', $sampai = 'all', $barang = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($barang != 'all') {
            $data['listData'] = $this->db->order_by('barang_nama', 'asc')->get_where('ok_barang', array('barang_id' => $barang))->result();
        } else {
            $data['listData'] = $this->db->order_by('barang_nama', 'asc')->get('ok_barang')->result();
        }

        $this->load->view('admin/reportbeli/printbarang_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_beli_barang.php */

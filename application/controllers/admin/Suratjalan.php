<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suratjalan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/suratjalan_m');
    }

    public function index()
    {
        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $this->template->display('admin/suratjalan/view', $data);
    }

    public function data_list()
    {
        $List = $this->suratjalan_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $penjualan_id = $r->penjualan_id;
            $row[]        = '<a onclick="printSurat(' . $penjualan_id . ')" title="Print Surat Jalan"><i class="fa fa-truck"></i></a>';
            $row[]        = $no;
            $row[]        = $r->penjualan_no_faktur;
            $row[]        = date('d-m-Y H:i', strtotime($r->penjualan_update));
            $row[]        = $r->pelanggan_nama . '<br><em><b>Kirim Ke :</b></em><br>' . $r->penjualan_nama . '<br>' . $r->penjualan_alamat;
            $row[]        = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]        = number_format($r->penjualan_bruto, 0, '', ',');
            $row[]        = number_format($r->penjualan_diskon, 0, '', ',');
            $row[]        = number_format($r->penjualan_total, 0, '', ',');
            $row[]        = $r->user_username;
            $row[]        = ($r->penjualan_print == 1 ? '<span class="label label-danger">BELUM</span>' : '<span class="label label-success">PRINT</span>');
            $row[]        = '<input class="form-check-input" type="checkbox" id="' . $penjualan_id . '" value="1" onchange="setKirim(this)" ' . ($r->penjualan_status_kirim == 'S' ? 'checked' : '') . '>';
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->suratjalan_m->count_all(),
            "recordsFiltered" => $this->suratjalan_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data($id)
    {
        // Update Status Print jadi 2
        $data = array(
            'penjualan_print' => 2,
        );
        $this->db->where('penjualan_id', $id);
        $this->db->update('ok_penjualan', $data);
        $data = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedatapelanggan()
    {
        $penjualan_id = $this->input->post('id', 'true');
        $data         = array(
            'penjualan_nama'   => strtoupper(stripHTMLtags($this->input->post('nama_pelanggan', 'true'))),
            'penjualan_alamat' => strtoupper(stripHTMLtags($this->input->post('alamat_pelanggan', 'true'))),
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $data);
    }

    public function printsuratjalan($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_penjualan_detail', array('penjualan_id' => $id))->result();
        $this->load->view('admin/penjualan/printsuratjalan_v', $data);
    }

    public function updatestatus($penjualan_id, $isi)
    {
        $data = array(
            'penjualan_status_kirim' => $isi,
        );

        $this->db->where('penjualan_id', $penjualan_id);
        $this->db->update('ok_penjualan', $data);
    }
}
/* Location: ./application/controller/admin/Suratjalan.php */

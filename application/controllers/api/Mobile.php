<?php
defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Mobile extends REST_Controller
{
    public function __construct($config = 'rest')
    {
        parent::__construct($config);
    }

    // Login
    public function login_post()
    {
        $username = strtolower(trim($this->post('username')));
        $password = trim($this->post('password'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username harus diisi.',
            ];

            $this->response($response, 403);
        } elseif ($password == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Password harus diisi.',
            ];

            $this->response($response, 403);
        } else {
            $checkUser = $this->db->get_where('ok_users', array('user_username' => $username, 'user_status' => 'Aktif'));
            $num_user  = $checkUser->num_rows();
            if ($num_user == 0) {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Username Tidak Terdaftar.',
                ];

                $this->response($response, 403);
            } else {
                $checkLogin = $this->db->get_where('ok_users', array('user_username' => $username, 'user_password' => sha1($password), 'user_status' => 'Aktif'));
                $num_check  = $checkLogin->num_rows();
                if ($num_check == 0) {
                    $response = [
                        'resp_error' => true,
                        'resp_msg'   => 'Password Anda Salah.',
                    ];

                    $this->response($response, 403);
                } else {
                    $dataUsers = $this->db->get_where('ok_users', array('user_username' => $username))->row();
                    $level     = $dataUsers->user_level;
                    if ($level == 'Admin') {
                        if ($dataUsers->user_avatar != '') {
                            $avatar = base_url('img/icon/' . $dataUsers->user_avatar);
                        } else {
                            $avatar = base_url('img/no-avatar.png');
                        }

                        $response = [
                            'resp_error' => false,
                            'resp_msg'   => 'success',
                            'username'   => trim($dataUsers->user_username),
                            'nama'       => trim($dataUsers->user_name),
                            'email'      => trim($dataUsers->user_email),
                            'avatar'     => $avatar,
                            'level'      => trim($dataUsers->user_level),
                        ];

                        $this->response($response, 200);
                    } else {
                        // Cek Jam Kerja
                        $tanggal = date('Y-m-d');
                        $hari    = getDay($tanggal);
                        $dataJam = $this->db->get_where('ok_jam_kerja', array('jam_kerja_hari' => $hari))->row();
                        $libur   = $dataJam->jam_kerja_libur;
                        $masuk   = $dataJam->jam_kerja_masuk;
                        $pulang  = $dataJam->jam_kerja_pulang;
                        if ($libur == 'Y') {
                            $response = [
                                'resp_error' => true,
                                'resp_msg'   => 'Login ditolak, Toko Libur.',
                            ];

                            $this->response($response, 403);
                        } else {
                            $current_time = date("H:i");
                            if ($current_time >= $masuk && $current_time <= $pulang) {
                                if ($dataUsers->user_avatar != '') {
                                    $avatar = base_url('img/icon/' . $dataUsers->user_avatar);
                                } else {
                                    $avatar = base_url('img/no-avatar.png');
                                }

                                $response = [
                                    'resp_error' => false,
                                    'resp_msg'   => 'success',
                                    'username'   => trim($dataUsers->user_username),
                                    'nama'       => trim($dataUsers->user_name),
                                    'email'      => trim($dataUsers->user_email),
                                    'avatar'     => $avatar,
                                    'level'      => trim($dataUsers->user_level),
                                ];

                                $this->response($response, 200);
                            } else {
                                $response = [
                                    'resp_error' => true,
                                    'resp_msg'   => 'Login ditolak, Toko Libur.',
                                ];

                                $this->response($response, 403);
                            }
                        }
                    }
                }
            }
        }
    }

    // Profil
    public function profil_post()
    {
        $username = trim($this->input->get_request_header('username', true));
        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $dataCheck = $this->db->get_where('ok_users', array('user_username' => $username));
            $num_check = $dataCheck->num_rows();
            if ($num_check == 0) {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Data User tidak ditemukan.',
                ];

                $this->response($response, 403);
            } else {
                $dataUsers = $this->db->get_where('ok_users', array('user_username' => $username))->row();
                if ($dataUsers->user_avatar != '') {
                    $avatar = base_url('img/icon/' . $dataUsers->user_avatar);
                } else {
                    $avatar = base_url('img/no-image.jpg');
                }

                $response = [
                    'resp_error' => false,
                    'resp_msg'   => 'success',
                    'username'   => trim($dataUsers->user_username),
                    'nama'       => trim($dataUsers->user_name),
                    'avatar'     => $avatar,
                    'level'      => trim($dataUsers->user_level),
                ];

                $this->response($response, 200);
            }
        }
    }

    // Update Profil
    public function updateprofil_post()
    {
        $username = trim($this->input->get_request_header('username', true));
        $nama     = trim($this->post('nama'));
        $email    = trim($this->post('email'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } elseif ($nama == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Nama Lengkap harus diisi.',
            ];

            $this->response($response, 403);
        } elseif ($email == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Email harus diisi.',
            ];

            $this->response($response, 403);
        } else {
            $data = array(
                'user_name'        => strtoupper($nama),
                'user_email'       => trim($email),
                'user_date_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('user_username', $username);
            $this->db->update('ok_users', $data);

            $response = [
                'resp_error' => false,
                'resp_msg'   => 'success',
            ];

            $this->response($response, 200);
        }
    }

    public function imageResizeThumbs($imageResourceId, $width, $height)
    {
        $targetWidth  = 200;
        $targetHeight = 200;
        $targetLayer  = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }

    public function imageResizeBarang($imageResourceId, $width, $height)
    {
        $targetWidth  = 500;
        $targetHeight = 500;
        $targetLayer  = imagecreatetruecolor($targetWidth, $targetHeight);
        imagecopyresampled($targetLayer, $imageResourceId, 0, 0, 0, 0, $targetWidth, $targetHeight, $width, $height);
        return $targetLayer;
    }

    // Update Avatar
    public function updateavatar_post()
    {
        $username = trim($this->input->get_request_header('username', true));
        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else {
            if (!empty($_FILES['foto']['name'])) {
                $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg');
                $namafile               = $_FILES['foto']['name'];
                $x                      = explode('.', $namafile);
                $ekstensi               = strtolower(end($x));
                $ukuran                 = $_FILES['foto']['size'];
                $file                   = $_FILES['foto']['tmp_name'];
                $sourceProperties       = getimagesize($file);
                $jam                    = time();
                $folderPath             = "img/icon/";
                $newfilename            = 'Avatar_' . $username . '_' . $jam . '.jpg';
                if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
                    if ($ukuran != 0) {
                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer     = $this->imageResizeThumbs($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                        $result          = imagejpeg($targetLayer, $folderPath . $newfilename);
                        move_uploaded_file($result, $folderPath . $newfilename);

                        $data = array(
                            'user_avatar'      => $newfilename,
                            'user_date_update' => date('Y-m-d'),
                        );

                        $this->db->where('user_username', $username);
                        $this->db->update('ok_users', $data);

                        $response = [
                            'resp_error' => false,
                            'resp_msg'   => 'success',
                            'avatar'     => base_url('img/icon/' . $newfilename),
                        ];

                        $this->response($response, 200);
                    } else {
                        $response = [
                            'resp_error' => true,
                            'resp_msg'   => 'File terlalu Besar',
                        ];

                        $this->response($response, 403);
                    }
                } else {
                    $response = [
                        'resp_error' => true,
                        'resp_msg'   => 'Tipe File Salah',
                    ];

                    $this->response($response, 403);
                }
            } else {
                $response = [
                    'resp_error' => false,
                    'resp_msg'   => 'File Avatar Kosong',
                ];

                $this->response($response, 403);
            }
        }
    }

    // Ganti Password
    public function gantipassword_post()
    {
        $username = trim($this->input->get_request_header('username', true));
        $password = trim($this->post('password'));
        $konfirm  = trim($this->post('konfirm'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } elseif ($password == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Password harus diisi.',
            ];

            $this->response($response, 403);
        } elseif (preg_match('/\s/', $password)) {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Password tidak boleh spasi.',
            ];

            $this->response($response, 403);
        } elseif ($konfirm == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Konfirmasi Password harus diisi.',
            ];

            $this->response($response, 403);
        } elseif (preg_match('/\s/', $konfirm)) {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Konfirmasi Password tidak boleh spasi.',
            ];

            $this->response($response, 403);
        } else {
            if ($konfirm != $password) {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Konfirmasi Password harus sama dengan Password.',
                ];

                $this->response($response, 403);
            } else {
                $data = array(
                    'user_password'    => sha1($password),
                    'user_date_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('user_username', $username);
                $this->db->update('ok_users', $data);

                $response = [
                    'resp_error' => false,
                    'resp_msg'   => 'success',
                ];

                $this->response($response, 200);
            }
        }
    }

    // Detail Barang
    public function getbarang_post()
    {
        $username    = trim($this->input->get_request_header('username', true));
        $barang_kode = trim($this->post('barang_kode'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else if ($barang_kode == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Kode Barang Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $dataBarang = $this->db->select('barang_id, barang_kode, barang_nama, kategori_nama, barang_merk, barang_foto, unit_nama, rak_id, rak_nama, unit_id')->get_where('v_barang', array('barang_kode' => $barang_kode))->row();
            if (count($dataBarang) > 0) {
                if ($dataBarang->barang_foto != '') {
                    $foto = base_url('img/barang/' . $dataBarang->barang_foto);
                } else {
                    $foto = base_url('img/no-image.png');
                }
                $response = [
                    'resp_error'    => false,
                    'resp_msg'      => 'success',
                    'barang_id'     => $dataBarang->barang_id,
                    'barang_kode'   => $dataBarang->barang_kode,
                    'barang_nama'   => $dataBarang->barang_nama,
                    'kategori_nama' => $dataBarang->kategori_nama,
                    'barang_merk'   => $dataBarang->barang_merk,
                    'unit_nama'     => $dataBarang->unit_nama,
                    'rak_id'        => $dataBarang->rak_id,
                    'rak_nama'      => ($dataBarang->rak_nama != '' ? $dataBarang->rak_nama : ''),
                    'unit_id'       => $dataBarang->unit_id,
                    'barang_foto'   => $foto,
                ];

                $this->response($response, 200);
            } else {
                $response = [
                    'resp_error' => true,
                    'resp_msg'   => 'Data Barang Tidak Ada.',
                ];

                $this->response($response, 403);
            }
        }
    }

    // Simpan Data
    public function simpandata_post()
    {
        $username  = trim($this->input->get_request_header('username', true));
        $barang_id = trim($this->post('barang_id'));
        $unit_id   = trim($this->post('unit_id'));
        $jumlah    = trim($this->post('jumlah'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else if ($barang_id == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'ID Barang Kosong.',
            ];

            $this->response($response, 403);
        } else if ($unit_id == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'ID Unit Kosong.',
            ];

            $this->response($response, 403);
        } else if ($jumlah == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Jumlah harus diisi.',
            ];

            $this->response($response, 403);
        } else {
            // Simpan ke Tabel SO
            $dataInsert = array(
                'barang_id'     => $barang_id,
                'unit_id'       => $unit_id,
                'user_username' => $username,
                'so_tanggal'    => date('Y-m-d H:i:s'),
                'so_jumlah'     => $jumlah,
                'so_update'     => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_so', $dataInsert);

            // Hitung Rekap per Tanggal
            $tanggal    = date('Y-m-d');
            $dataBarang = $this->db->select('unit_qty')->get_where('v_barang', array('barang_id' => $barang_id))->row();
            $qty        = $dataBarang->unit_qty;
            $dataTotal  = $this->db->select_sum('so_jumlah', 'total')->get_where('ok_so', array('barang_id' => $barang_id, 'DATE(so_tanggal)' => $tanggal))->row();
            $real       = $dataTotal->total;
            $selisih    = ($real - $qty);

            $dataCheck = $this->db->select('so_rekap_id')->get_where('ok_so_rekap', array('barang_id' => $barang_id, 'so_rekap_tanggal' => $tanggal))->row();
            if (count($dataCheck) > 0) {
                $so_rekap_id = $dataCheck->so_rekap_id;
                $dataRekap   = array(
                    'so_rekap_qty'     => $qty,
                    'so_rekap_real'    => $real,
                    'so_rekap_selisih' => $selisih,
                    'so_rekap_update'  => date('Y-m-d H:i:s'),
                );

                $this->db->where('so_rekap_id', $so_rekap_id);
                $this->db->update('ok_so_rekap', $dataRekap);
            } else {
                $dataRekap = array(
                    'barang_id'        => $barang_id,
                    'unit_id'          => $unit_id,
                    'so_rekap_tanggal' => date('Y-m-d'),
                    'so_rekap_qty'     => $qty,
                    'so_rekap_real'    => $real,
                    'so_rekap_selisih' => $selisih,
                    'so_rekap_update'  => date('Y-m-d H:i:s'),
                );

                $this->db->insert('ok_so_rekap', $dataRekap);
            }

            $response = [
                'resp_error' => false,
                'resp_msg'   => 'success',
            ];

            $this->response($response, 200);
        }
    }

    public function customNumberFormat($value)
    {
        if (intval($value) == $value) {
            return number_format($value, 0, '', '');
        } else {
            return number_format($value, 2, ',', '');
        }
    }

    // Dashboard
    public function dashboard_post()
    {
        $username = trim($this->input->get_request_header('username', true));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $listData = $this->db->order_by('so_id', 'desc')->limit(5)->get_where('v_so', array('user_username' => $username))->result();
            $response = array(
                'resp_error' => false,
                'resp_msg'   => 'success',
            );
            $response['items'] = array();
            foreach ($listData as $r) {
                $response['items'][] = array(
                    'so_id'         => $r->so_id,
                    'so_tanggal'    => date('d-m-Y H:i', strtotime($r->so_tanggal)),
                    'barang_kode'   => $r->barang_kode,
                    'barang_nama'   => $r->barang_nama,
                    'kategori_nama' => $r->kategori_nama,
                    'unit_nama'     => $r->unit_nama,
                    'so_jumlah'     => $this->customNumberFormat($r->so_jumlah),
                );
            }

            $this->response($response, 200);
        }
    }

    // List History
    public function listhistory_post()
    {
        $username = trim($this->input->get_request_header('username', true));
        $tanggal  = trim($this->post('tanggal'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } elseif ($tanggal == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Tanggal Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $tanggal  = date('Y-m-d', strtotime($tanggal));
            $listData = $this->db->order_by('so_id', 'asc')->get_where('v_so', array('DATE(so_tanggal)' => $tanggal, 'user_username' => $username))->result();
            $response = array(
                'resp_error' => false,
                'resp_msg'   => 'success',
            );
            $response['items'] = array();
            foreach ($listData as $r) {
                $response['items'][] = array(
                    'so_id'         => $r->so_id,
                    'so_tanggal'    => date('d-m-Y H:i', strtotime($r->so_tanggal)),
                    'barang_kode'   => $r->barang_kode,
                    'barang_nama'   => $r->barang_nama,
                    'kategori_nama' => $r->kategori_nama,
                    'unit_nama'     => $r->unit_nama,
                    'so_jumlah'     => $this->customNumberFormat($r->so_jumlah),
                );
            }

            $this->response($response, 200);
        }
    }

    // List Rak
    public function listrak_post()
    {
        $username = trim($this->input->get_request_header('username', true));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } else {
            $listData = $this->db->order_by('rak_nama', 'asc')->get('ok_rak')->result();
            $response = array(
                'resp_error' => false,
                'resp_msg'   => 'success',
            );
            $response['items'] = array();
            foreach ($listData as $r) {
                $response['items'][] = array(
                    'rak_id'   => $r->rak_id,
                    'rak_nama' => $r->rak_nama,
                );
            }

            $this->response($response, 200);
        }
    }

    // Update Foto dan Rak
    public function updatebarang_post()
    {
        $username  = trim($this->input->get_request_header('username', true));
        $barang_id = trim($this->post('barang_id'));
        $rak_id    = trim($this->post('rak_id'));

        if ($username == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'Username Kosong.',
            ];

            $this->response($response, 403);
        } elseif ($barang_id == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'ID Barang Kosong.',
            ];

            $this->response($response, 403);
        } elseif ($rak_id == '') {
            $response = [
                'resp_error' => true,
                'resp_msg'   => 'ID Rak Kosong.',
            ];

            $this->response($response, 403);
        } else {
            if (!empty($_FILES['foto']['name'])) {
                $ekstensi_diperbolehkan = array('png', 'jpg', 'jpeg');
                $namafile               = $_FILES['foto']['name'];
                $x                      = explode('.', $namafile);
                $ekstensi               = strtolower(end($x));
                $ukuran                 = $_FILES['foto']['size'];
                $file                   = $_FILES['foto']['tmp_name'];
                $sourceProperties       = getimagesize($file);
                $jam                    = time();
                $folderPath             = "img/barang/";
                $newfilename            = 'Barang_' . $jam . '.jpg';
                if (in_array($ekstensi, $ekstensi_diperbolehkan) === true) {
                    if ($ukuran != 0) {
                        $imageResourceId = imagecreatefromjpeg($file);
                        $targetLayer     = $this->imageResizeBarang($imageResourceId, $sourceProperties[0], $sourceProperties[1]);
                        $result          = imagejpeg($targetLayer, $folderPath . $newfilename);
                        move_uploaded_file($result, $folderPath . $newfilename);

                        $data = array(
                            'rak_id'        => $rak_id,
                            'barang_foto'   => $newfilename,
                            'barang_update' => date('Y-m-d'),
                        );

                        $this->db->where('barang_id', $barang_id);
                        $this->db->update('ok_barang', $data);

                        $response = [
                            'resp_error' => false,
                            'resp_msg'   => 'success',
                        ];

                        $this->response($response, 200);
                    } else {
                        $response = [
                            'resp_error' => true,
                            'resp_msg'   => 'File terlalu Besar',
                        ];

                        $this->response($response, 403);
                    }
                } else {
                    $response = [
                        'resp_error' => true,
                        'resp_msg'   => 'Tipe File Salah',
                    ];

                    $this->response($response, 403);
                }
            } else {
                $data = array(
                    'rak_id'        => $rak_id,
                    'barang_update' => date('Y-m-d'),
                );

                $this->db->where('barang_id', $barang_id);
                $this->db->update('ok_barang', $data);

                $response = [
                    'resp_error' => false,
                    'resp_msg'   => 'success',
                ];

                $this->response($response, 200);
            }
        }
    }
}

/* Location: ./application/controllers/api/Mobile.php */

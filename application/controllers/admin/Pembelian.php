<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembelian extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/pembelian_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $data['listTipe']    = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/pembelian/view', $data);
    }

    public function data_list()
    {
        $List = $this->pembelian_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pembelian_id = $r->pembelian_id;
            $linkedit     = site_url('admin/pembelian/editdata/' . $pembelian_id);
            $linkprint    = site_url('admin/pembelian/printfaktur/' . $pembelian_id);
            // $linkslip     = site_url('admin/pembelian/printslip/' . $pembelian_id);
            $row[] = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                            <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>
                            <a onclick="hapusData(' . $pembelian_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->pembelian_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]  = number_format($r->pembelian_disc, 0, '', ',');
            $row[]  = number_format($r->pembelian_ppn, 0, '', ',');
            $row[]  = number_format($r->pembelian_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_all(),
            "recordsFiltered" => $this->pembelian_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_pembelian_temp');

        $data['listTipe']     = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $this->template->display('admin/pembelian/add', $data);
    }

    private function kode_exists($kode)
    {
        $this->db->where('suplier_kode', $kode);
        $query = $this->db->get('ok_suplier');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function kodebarang_exists($kode_add)
    {
        $this->db->where('barang_kode', $kode_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kodebarang_exists()
    {
        if (array_key_exists('kode_add', $_POST)) {
            if ($this->kodebarang_exists($this->input->post('kode_add', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function namabarang_exists($nama_add)
    {
        $this->db->where('barang_nama', $nama_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_namabarang_exists()
    {
        if (array_key_exists('nama_add', $_POST)) {
            if ($this->namabarang_exists(stripHTMLtags($this->input->post('nama_add', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function data_temp_list()
    {
        $List = $this->pembelian_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row               = array();
            $pembelian_temp_id = $r->pembelian_temp_id;
            $row[]             = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $pembelian_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                                 <a onclick="hapusData(' . $pembelian_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->pembelian_temp_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->pembelian_temp_harga, 0, '', ',');
            $row[]  = number_format($r->pembelian_temp_disc1, 2, '.', '');
            $row[]  = number_format($r->pembelian_temp_disc2, 2, '.', '');
            $row[]  = number_format($r->pembelian_temp_disc3, 2, '.', '');
            $row[]  = number_format($r->pembelian_temp_disc4, 2, '.', '');
            $row[]  = number_format($r->pembelian_temp_ppn, 2, '.', '');
            $row[]  = number_format($r->pembelian_temp_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_tmp_all(),
            "recordsFiltered" => $this->pembelian_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_suplier_list()
    {
        $List = $this->pembelian_m->get_suplier_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row        = array();
            $suplier_id = $r->suplier_id;
            $row[]      = '<a href="javascript:void(0)" title="Pilih" onclick="pilihsuplier(' . "'" . $suplier_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]      = $no;
            $row[]      = $r->suplier_kode;
            $row[]      = $r->suplier_nama;
            $row[]      = $r->suplier_alamat;
            $row[]      = $r->suplier_kota;
            $row[]      = $r->suplier_telp;
            $data[]     = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_suplier_all(),
            "recordsFiltered" => $this->pembelian_m->count_suplier_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_barang_list()
    {
        $List = $this->pembelian_m->get_barang_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $barang_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]     = $no;
            if ($r->unit_qty <= $r->barang_min_stok) {
                $kode = '<span class="label label-danger">' . $r->barang_kode . '</span>';
                $nama = '<span class="label label-danger">' . $r->barang_nama . '</span>';
                $merk = '<span class="label label-danger">' . $r->barang_merk . '</span>';
                $qty  = '<span class="label label-danger">' . number_format($r->unit_qty, 2, '.', ',') . '</span>';
            } else {
                $kode = $r->barang_kode;
                $nama = $r->barang_nama;
                $merk = $r->barang_merk;
                $qty  = number_format($r->unit_qty, 2, '.', ',');
            }
            $row[]  = $kode;
            $row[]  = $nama;
            $row[]  = $merk;
            $row[]  = $qty;
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->unit_hrg_beli, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_barang_all(),
            "recordsFiltered" => $this->pembelian_m->count_barang_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_suplier($id)
    {
        $data = $this->db->get_where('ok_suplier', array('suplier_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_suplier_by_kode($kode_suplier)
    {
        $data = $this->db->get_where('ok_suplier', array('suplier_kode' => trim(strtoupper($kode_suplier))))->row();
        echo json_encode($data);
    }

    public function get_data_barang($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_barang_by_kode($kode_barang)
    {
        $data = $this->db->get_where('v_barang', array('barang_kode' => trim(strtoupper($kode_barang))))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_pembelian', array('pembelian_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $this->pembelian_m->insert_data_item();
    }

    public function updateitem()
    {
        $this->pembelian_m->update_data_item();
    }

    public function deleteitem($id)
    {
        $this->pembelian_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('pembelian_temp_total', 'total')->get_where('ok_pembelian_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function data_unit_list($barang_id)
    {
        $List = $this->pembelian_m->get_unit_datatables($barang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row     = array();
            $unit_id = $r->unit_id;
            $row[]   = '<a href="javascript:void(0)" title="Pilih" onclick="pilihunit(' . "'" . $unit_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]   = $r->unit_nama;
            $row[]   = customNumberFormat($r->unit_multiple);
            $row[]   = number_format($r->unit_qty, 0, '', ',');
            $row[]   = number_format($r->unit_hrg_beli, 0, '', ',');
            $row[]   = number_format($r->unit_disc1, 2, '.', '');
            $row[]   = number_format($r->unit_disc2, 2, '.', '');
            $row[]   = number_format($r->unit_disc3, 2, '.', '');
            $row[]   = number_format($r->unit_disc4, 2, '.', '');
            $row[]   = number_format($r->unit_ppn, 2, '.', '');
            $row[]   = number_format($r->unit_hrg_pokok, 0, '', ',');
            $data[]  = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_unit_all($barang_id),
            "recordsFiltered" => $this->pembelian_m->count_unit_filtered($barang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_unit($id)
    {
        $data = $this->db->get_where('v_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedataunit()
    {
        $this->pembelian_m->update_data_unit();
    }

    private function faktur_exists($no_faktur)
    {
        $this->db->where('pembelian_no_faktur', $no_faktur);
        $query = $this->db->get('ok_pembelian');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_faktur_exists()
    {
        if (array_key_exists('no_faktur', $_POST)) {
            if ($this->faktur_exists($this->input->post('no_faktur', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->pembelian_m->insert_data_pembelian();
    }

    public function savedatabarang()
    {
        $this->pembelian_m->insert_data_barang();
    }

    public function editdata($pembelian_id)
    {
        $data['listTipe'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $data['detail']   = $this->db->get_where('v_pembelian', array('pembelian_id' => $pembelian_id))->row();
        $this->template->display('admin/pembelian/edit', $data);
    }

    public function data_detail_list($pembelian_id)
    {
        $List = $this->pembelian_m->get_detail_datatables($pembelian_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row                 = array();
            $pembelian_detail_id = $r->pembelian_detail_id;
            $row[]               = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $pembelian_detail_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $pembelian_detail_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->pembelian_detail_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->pembelian_detail_harga, 0, '', ',');
            $row[]  = number_format($r->pembelian_detail_disc1, 2, '.', '');
            $row[]  = number_format($r->pembelian_detail_disc2, 2, '.', '');
            $row[]  = number_format($r->pembelian_detail_disc3, 2, '.', '');
            $row[]  = number_format($r->pembelian_detail_disc4, 2, '.', '');
            $row[]  = number_format($r->pembelian_detail_ppn, 2, '.', '');
            $row[]  = number_format($r->pembelian_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pembelian_m->count_detail_all($pembelian_id),
            "recordsFiltered" => $this->pembelian_m->count_detail_filtered($pembelian_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($pembelian_id)
    {
        $data = $this->db->select_sum('pembelian_detail_total', 'total')->get_where('ok_pembelian_detail', array('pembelian_id' => $pembelian_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->pembelian_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_pembelian_detail', array('pembelian_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->pembelian_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->pembelian_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->pembelian_m->update_data_pembelian();
    }

    public function savedatasuplier()
    {
        $this->pembelian_m->insert_data_suplier();
    }

    public function deletedata($id)
    {
        $this->pembelian_m->delete_data_pembelian($id);
    }

    public function printfaktur($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_pembelian', array('pembelian_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_pembelian_detail', array('pembelian_id' => $id))->result();
        $this->load->view('admin/pembelian/printfaktur_v', $data);
    }

    public function printslip($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_pembelian', array('pembelian_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_pembelian_detail', array('pembelian_id' => $id))->result();
        $this->load->view('admin/pembelian/printslip_v', $data);
    }

    public function tempo()
    {
        $startDate  = date('Y-m-d');
        $futureDate = date('Y-m-d', strtotime('+30 days', strtotime($startDate)));
        echo $futureDate;
    }
}
/* Location: ./application/controller/admin/Pembelian.php */

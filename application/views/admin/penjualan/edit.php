<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Penjualan Barang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/penjualan');?>">Penjualan Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Detail Penjualan Barang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-edit"></i> Form Data Penjualan
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">No. Faktur</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="no_faktur" value="<?=$detail->penjualan_no_faktur;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Tanggal/Hari</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="tanggal" value="<?=date('d-m-Y', strtotime($detail->penjualan_tanggal));?>" readonly>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="hari" value="<?=getDay($detail->penjualan_tanggal);?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Nama Pelanggan</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan" autocomplete="off" value="<?=$detail->pelanggan_nama;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="alamat" id="alamat" autocomplete="off" value="<?=$detail->pelanggan_alamat;?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Total Transaksi
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">Kasir :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$detail->user_username;?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalinvoice">Rp. <?=number_format($detail->penjualan_total,0,'',',');?></div>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Penjualan Barang 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="10%">Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th width="5%">Jumlah</th>
                                    <th width="10%">Unit</th>
                                    <th width="10%">Harga</th>
                                    <th width="5%">Disc (%)</th>
                                    <th width="10%">Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <?php if ($this->session->userdata('level') == 'Admin') { ?>
                <a class="btn btn-danger" href="<?=site_url('admin/penjualan/printfaktur/'.$detail->penjualan_id);?>" target="_blank"><i class="fa fa-print"></i> Print Faktur</a>
                <?php } ?>
                <a class="btn btn-primary" href="<?=site_url('admin/penjualan/printsuratjalan/'.$detail->penjualan_id);?>" target="_blank"><i class="fa fa-truck"></i> Print Surat Jalan</a>
                <a href="<?=site_url('admin/penjualan');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Kembali</a>
                <?php if ($detail->penjualan_kirim == 'Y') { ?>
                <br><br>
                <div class="well">
                    <b>Alamat Pengiriman :</b> <?=$detail->penjualan_alamat;?>
                </div>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <div class="row static-info align-reverse">
                        <div class="col-md-7 name">Tipe Bayar :</div>
                        <div class="col-md-4 name"><b><?=$detail->tipe_bayar_nama;?></b></div>
                    </div>
                    <div class="row static-info align-reverse">
                        <div class="col-md-7 name">Sub Total :</div>
                        <div class="col-md-4 name"><b><?=number_format($detail->penjualan_bruto,0,'',',');?></b></div>
                    </div>
                    <div class="row static-info align-reverse">
                        <div class="col-md-7 name">Diskon :</div>
                        <div class="col-md-4 name"><b><?=number_format($detail->penjualan_diskon,0,'',',');?></b></div>
                    </div>
                    <div class="row static-info align-reverse">
                        <div class="col-md-7 name">PPN (%) :</div>
                        <div class="col-md-4 name"><b><?=number_format($detail->penjualan_ppn,2,'.',',');?></b></div>
                    </div>
                    <div class="row static-info align-reverse">
                        <div class="col-md-7 name">Total Penjualan :</div>
                        <div class="col-md-4 name"><b><?=number_format($detail->penjualan_total,0,'',',');?></b></div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript">
var table;
$(document).ready(function() {
    var penjualan_id = '<?=$detail->penjualan_id;?>';
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "destoy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/penjualan/data_detail_list/')?>"+penjualan_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6, 7],
                "orderable": false,
            },
            {
                "targets": [ 0, 4 ],
                "className": "text-center",
            },
            {
                "targets": [ 3, 5, 6, 7 ],
                "className": "text-right",
            }
        ],
    });    
});
</script>
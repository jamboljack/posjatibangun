<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_m');
        $this->load->library('template_login');
    }

    public function index()
    {
        $session = $this->session->userdata('logged_in_vivopos');
        if ($session == false) {
            $this->template_login->display('admin/login_v');
        } else {
            redirect(site_url('admin/home'));
        }
    }

    public function validasi()
    {
        $username     = trim(stripHTMLtags($this->input->post('username', 'true')));
        $password     = trim(stripHTMLtags($this->input->post('password', 'true')));
        $temp_account = $this->login_m->check_user_account($username, sha1($password));
        $num_rows     = $temp_account->num_rows();
        if ($num_rows > 0) {
            $account = $temp_account->row();
            $level   = $account->user_level;
            if ($level == 'Admin') {
                $array_item = array(
                    'username'          => trim($account->user_username),
                    'pass'              => $account->user_password,
                    'nama'              => strtoupper(trim($account->user_name)),
                    'avatar'            => $account->user_avatar,
                    'level'             => $account->user_level,
                    'logged_in_vivopos' => true,
                );

                $this->session->set_userdata($array_item);

                $response = ['status' => 'success'];
            } else {
                // Cek Jam Kerja
                $tanggal = date('Y-m-d');
                $hari    = getDay($tanggal);
                $dataJam = $this->db->get_where('ok_jam_kerja', array('jam_kerja_hari' => $hari))->row();
                $libur   = $dataJam->jam_kerja_libur;
                $masuk   = $dataJam->jam_kerja_masuk;
                $pulang  = $dataJam->jam_kerja_pulang;
                if ($libur == 'Y') {
                    $response = ['status' => 'failed', 'msg' => 'Login ditolak, Toko Libur.'];
                } else {
                    $current_time = date("H:i");
                    if ($current_time >= $masuk && $current_time <= $pulang) {
                        $array_item = array(
                            'username'          => trim($account->user_username),
                            'pass'              => $account->user_password,
                            'nama'              => strtoupper(trim($account->user_name)),
                            'avatar'            => $account->user_avatar,
                            'level'             => $account->user_level,
                            'logged_in_vivopos' => true,
                        );

                        $this->session->set_userdata($array_item);

                        $response = ['status' => 'success'];
                    } else {
                        $response = ['status' => 'failed', 'msg' => 'Login ditolak, Diluar Jam Kerja'];
                    }
                }
            }
        } else {
            $response = ['status' => 'failed', 'msg' => 'Maaf! Password Anda Salah.'];
        }

        echo json_encode($response);
    }

    private function login_exists($username)
    {
        $this->db->where('user_username', $username);
        $this->db->where('user_status', 'Aktif');
        $query = $this->db->get('ok_users');
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public function check_login_exists()
    {
        if (array_key_exists('username', $_POST)) {
            if ($this->login_exists(trim(stripHTMLtags($this->input->post('username', 'true')))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function logout()
    {
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . 'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-chace');
        $this->session->sess_destroy();
        redirect(base_url());
    }
}

/* Location: ./application/controller/Login.php */

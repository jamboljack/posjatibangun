<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_pajak extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_pajak_m');
    }

    public function index()
    {
        $this->template->display('admin/reportpajak/reportpajak_v');
    }

    public function data_list()
    {
        $List = $this->lap_pajak_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = number_format($r->penjualan_detail_qty, 0, '', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->penjualan_detail_harga, 0, '', ',');
            $row[]  = number_format($r->penjualan_detail_disc, 2, '.', ',');
            $row[]  = number_format($r->penjualan_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_pajak_m->count_all(),
            "recordsFiltered" => $this->lap_pajak_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printpajak($dari = 'all', $sampai = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($dari != 'all' && $sampai != 'all') {
            $tgl_dari           = date('Y-m-d', strtotime($dari));
            $tgl_sampai         = date('Y-m-d', strtotime($sampai));
            $data['listDetail'] = $this->db->order_by('penjualan_tanggal', 'asc')->get_where('v_penjualan_detail', array('penjualan_tanggal >=' => $tgl_dari, 'penjualan_tanggal <=' => $tgl_sampai, 'kategori_pajak' => 2))->result();
        } else {
            $data['listDetail'] = $this->db->order_by('penjualan_tanggal', 'asc')->get_where('v_penjualan_detail', array('kategori_pajak' => 2))->result();
        }

        $this->load->view('admin/reportpajak/printpajak_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_pajak.php */

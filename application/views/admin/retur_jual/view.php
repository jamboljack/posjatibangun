<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Retur Penjualan</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Retur Penjualan</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Retur Penjualan
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData">
                                <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Filter Data</button>
                            </a>
                            <a href="<?=site_url('admin/returjual/adddata');?>">
                                <button type="submit" class="btn btn-primary btn-xs"><i class="fa fa-plus-circle"></i> Tambah</button>
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th width="10%">No. Faktur</th>
                                    <th width="10%">Tanggal</th>
                                    <th>Nama Pelanggan</th>
                                    <th width="10%">Tipe Bayar</th>
                                    <th width="10%">Total</th>
                                    <th width="10%">Kasir</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/returjual/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstPelanggan = $('#lstPelanggan').val();
                data.tgl_dari     = $('#tgl_dari').val();
                data.tgl_sampai   = $('#tgl_sampai').val();
                data.lstTipeBayar = $('#lstTipeBayar').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3, 5 ],
                "className": "text-center",
            },
            {
                "targets": [ 6 ],
                "className": "text-right",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

var printer = new Recta('<?=$meta->meta_print_key;?>', '<?=$meta->meta_print_port;?>');
function printNota(retur_jual_id) {
    $.ajax({
        url: '<?=site_url('admin/returjual/get_data/');?>'+retur_jual_id,
        type: "POST",
        dataType: 'JSON',
        success: function(datap1) {
            var locale        = 'en';
            var options       = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter     = new Intl.NumberFormat(locale, options);
            var options1      = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1    = new Intl.NumberFormat(locale, options1);
            var NoOrder       = datap1.retur_jual_no_faktur;
            var Tanggal       = datap1.retur_jual_tanggal;
            var NamaPelanggan = datap1.pelanggan_nama;
            var Kasir         = datap1.user_username;
            Header(NoOrder, Tanggal, NamaPelanggan, Kasir);
            $.ajax({
                url: '<?=site_url('admin/returjual/get_list_item/');?>'+retur_jual_id,
                type: "POST",
                dataType: 'JSON',
                success: function(dataitem1) {
                    if (dataitem1 != null) {
                        var x1 = dataitem1.length;
                        for(var i = 0; i < x1; i++) {
                            var NamaBarang = dataitem1[i].barang_nama;
                            var Harga      = formatter.format(dataitem1[i].retur_jual_detail_harga);
                            var Qty        = formatter1.format(dataitem1[i].retur_jual_detail_qty);
                            var Unit       = dataitem1[i].unit_nama;
                            var Subtotal   = formatter.format(dataitem1[i].retur_jual_detail_total);
                            ListItem(NamaBarang, Harga, Qty, Unit, Subtotal);
                        }

                        var TipeBayar  = datap1.tipe_bayar_nama;
                        var SubTotal   = formatter.format(datap1.retur_jual_bruto);
                        var Diskon     = formatter.format(datap1.retur_jual_diskon);
                        var Total      = formatter.format(datap1.retur_jual_total);
                        Footer(TipeBayar, SubTotal, Diskon, Total);
                        FooterEnd();
                    }
                }
            });
        }
    });
}

function Header(NoOrder, Tanggal, NamaPelanggan, Kasir) {
    var LimitChar = 36;
    if(NoOrder.length <= LimitChar) {
        txtNoOrder = NoOrder;
    } else {
        txtNoOrder = NoOrder.substring(0, LimitChar);
    }

    if(NamaPelanggan.length <= LimitChar) {
        txtNamaPelanggan = NamaPelanggan;
    } else {
        txtNamaPelanggan = NamaPelanggan.substring(0, LimitChar);
    }

    printer.open().then(function () {
      printer.align('center')
        .bold(true)
        .text('TOKO JATI BANGUN')
        .text('Jl. AKBP Agil Kusumadya No.110A, Jatiwetan')
        .text('KUDUS')
        .text('(0291)2911225')
        .text('WA : 087831346729')
        .bold(false)
        .text('------------------------------------------------')
        .print()
    })

    printer.open().then(function () {
      printer.align('left')
        .text('Retur JL  : '+txtNoOrder)
        .text('Tanggal   : '+Tanggal.split("-").reverse().join("-"))
        .text('Pelanggan : '+txtNamaPelanggan)
        .text('Kasir     : '+Kasir)
        .text('------------------------------------------------')
        .text('Nama Barang                          Total Harga')
        .print()
    })
}

function ListItem(NamaBarang, Harga, Qty, Unit, Subtotal) {
    var limitNamaBarang = 48;
    var limitQty        = 6;
    var limitUnit       = 3;
    var limitHarga      = 15;
    var limitSubtotal   = 20;
    var txtBarang       = '';
    var txtHarga        = 0;
    var txtQty          = 0;
    var txtUnit         = '';
    var txtSubtotal     = 0;

    if(NamaBarang.length <= limitNamaBarang) {
        txtBarang = NamaBarang.padEnd(limitNamaBarang, ' ')
    } else {
        txtBarang = NamaBarang.substring(0, limitNamaBarang);
    }

    if (Harga.length <= limitHarga) {
        txtHarga = Harga.padStart(limitHarga, ' ')
    } else {
        txtHarga = Harga.substring(0, limitHarga);
    }

    if (Qty.length <= limitQty) {
        txtQty = Qty.padStart(limitQty, ' ')
    } else {
        txtQty = Qty.substring(0, limitQty);
    }

    if(Unit.length <= limitUnit) {
        txtUnit = Unit.padEnd(limitUnit, ' ')
    } else {
        txtUnit = Unit.substring(0, limitUnit);
    }

    if (Subtotal.length <= limitSubtotal) {
        txtSubtotal = Subtotal.padStart(limitSubtotal, ' ')
    } else {
        txtSubtotal = Subtotal.substring(0, limitSubtotal);
    }

    printer.open().then(function () {
      printer.align('left')
        .text(txtBarang)
        .text(txtQty+" "+txtUnit+"x "+txtHarga+" "+txtSubtotal)
        .print()
    })
}

function Footer(TipeBayar, SubTotal, Diskon, Total) {
    var limitNominal    = 13;
    var txtSubTotal     = 0;
    var txtDiskon       = 0;
    var txtTotal        = 0;

    if (SubTotal.length <= limitNominal) {
        txtSubTotal = SubTotal.padStart(limitNominal, ' ')
    } else {
        txtSubTotal = SubTotal.substring(0, limitNominal);
    }

    if (Diskon.length <= limitNominal) {
        txtDiskon = Diskon.padStart(limitNominal, ' ')
    } else {
        txtDiskon = Diskon.substring(0, limitNominal);
    }

    if (Total.length <= limitNominal) {
        txtTotal = Total.padStart(limitNominal, ' ')
    } else {
        txtTotal = Total.substring(0, limitNominal);
    }

    printer.open().then(function () {
        printer.align('left')
        .text(TipeBayar)
        .text("                       Sub Total : "+txtSubTotal)
        .print()
    })

    if (Diskon != 0) {
        printer.open().then(function () {
            printer.align('left')
            .text("                          Diskon : "+txtDiskon)
            .text('                                   -------------')
            .print()
        })
    }

    printer.open().then(function () {
        printer.align('left')
        .text("                           TOTAL : "+txtTotal)
        .print()
    })
}

function FooterEnd() {
    printer.open().then(function () {
        printer.align('center')
        .text('------------------------------------------------')
        .text('- TERIMA KASIH -')
        .text('')
        .feed(3)
        .cut()
        .print()
    })
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control date-picker" name="tgl_dari" id="tgl_dari" placeholder="Dari Tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y');?>" autocomplete="off">
                                <span class="input-group-addon"><b>s/d</b></span>
                                <input type="text" class="form-control date-picker" name="tgl_sampai" id="tgl_sampai" placeholder="Sampai Tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y');?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pelanggan</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstPelanggan" id="lstPelanggan">
                                <option value="">- SEMUA DATA -</option>
                                <?php foreach ($listPelanggan as $r) { ?>
                                <option value="<?=$r->pelanggan_id;?>"><?=$r->pelanggan_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Tipe Bayar</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstTipeBayar" id="lstTipeBayar">
                                <option value="">- SEMUA DATA -</option>
                                <?php foreach ($listTipe as $r) { ?>
                                <option value="<?=$r->tipe_bayar_id;?>"><?=$r->tipe_bayar_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>

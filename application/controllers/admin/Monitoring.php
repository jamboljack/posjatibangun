<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/monitoring_m');
    }

    public function index()
    {
        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $data['listTipe']      = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/monitoring/view', $data);
    }

    public function data_list()
    {
        $List = $this->monitoring_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $penjualan_id = $r->penjualan_id;
            $row[]        = '<a onclick="printNota(' . $penjualan_id . ')" title="Print Nota"><i class="icon-screen-tablet"></i></a>';
            $row[]        = $no;
            $row[]        = $r->penjualan_no_faktur;
            $row[]        = date('d-m-Y H:i', strtotime($r->penjualan_update));
            $row[]        = $r->pelanggan_nama;
            $row[]        = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]        = number_format($r->penjualan_bruto, 0, '', ',');
            $row[]        = number_format($r->penjualan_diskon, 0, '', ',');
            $row[]        = number_format($r->penjualan_total, 0, '', ',');
            $row[]        = $r->user_username;
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->monitoring_m->count_all(),
            "recordsFiltered" => $this->monitoring_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data($id)
    {
        // Update Status Print jadi 2
        // $data = array(
        //     'penjualan_print' => 2,
        // );
        // $this->db->where('penjualan_id', $id);
        // $this->db->update('ok_penjualan', $data);
        $data = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_list_item($id)
    {
        $data = $this->db->get_where('v_penjualan_detail', array('penjualan_id' => $id))->result();
        echo json_encode($data);
    }
}
/* Location: ./application/controller/admin/Monitoring.php */

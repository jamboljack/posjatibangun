<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Label Rak</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }

    body{
        font-family: "Franklin Gothic Medium";
        font-size:17px;
    }
    
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    table { page-break-inside:auto }
    tr    { page-break-inside:avoid; page-break-after:auto }
    thead { display:table-header-group }
    tfoot { display:table-footer-group }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <table width="80%" align="center" cellpadding="0" cellspacing="0" border="1">
        <tr>
            <?php 
            $no = 1;
            foreach($listData as $r) {
            ?>
            <td width="50%" align="center">
                <?=$r->barang_kode;?><br>
                <?=trim($r->barang_nama);?><br>
                <?=($r->suplier_kode!=''?$r->suplier_kode:'-');?><br>
                <b style="font-size:33px;">Rp. <?=number_format($r->unit_hrg_jual,0,'',',');?></b> /<?=$r->unit_nama;?>
            </td>
            <?php 
                if ($no%2==0) {
                    echo '</tr><tr>';
                }
                $no++;
            }
            ?>
        </tr>
    </table>
</div>
</body>
</html>
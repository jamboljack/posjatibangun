<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<style type="text/css">
    .number, .decimal {
        text-align: right;
    }

    .background-color-class {
        background-color: #f54242;
    }

</style>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Barang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Data Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Barang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Barang
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData">
                                <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Filter Data</button>
                            </a>
                            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#formModalAdd">
                                <i class="fa fa-plus-circle"></i> Tambah
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="6%"></th>
                                    <th width="5%">No</th>
                                    <th width="8%">Kode</th>
                                    <th>Nama Barang</th>
                                    <th width="10%">Kategori</th>
                                    <th width="10%">Merk</th>
                                    <th width="5%">Stok</th>
                                    <th width="5%">Unit</th>
                                    <th width="10%">Harga Pokok</th>
                                    <th width="10%">Tgl. Beli</th>
                                    <th width="10%">Suplier</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.number').maskMoney({thousands:',', precision:0});
    $('.decimal').maskMoney({thousands:'', precision:2});
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/barang/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstKategoriFilter = $('#lstKategoriFilter').val();
                data.lstSuplier        = $('#lstSuplier').val();
                data.lstRak            = $('#lstRakFilter').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 6, 7, 8 ],
                "className": "text-right",
            },
            {
                "targets": [ 9 ],
                "className": "text-center",
            }
        ]
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

$(document).ready(function() {
    var form        = $('#formInput');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formInput").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: { 
            kode: { required: true, maxlength:15,
                remote: {
                    url: "<?=site_url('admin/barang/register_kode_exists');?>",
                    type: "post",
                    data: {
                        kode: function() {
                            return $("#kode").val();
                        }
                    }
                } 
            },
            nama: {
                required: true,
                remote: {
                    url: "<?=site_url('admin/barang/register_nama_exists');?>",
                    type: "post",
                    data: {
                        nama: function() { 
                            return $("#nama").val(); 
                        }
                    }
                }
            },
            lstKategori: { required: true },
            lstRak: { required: true },
            unit: { required: true },
            minstok: { required: true },
            merk: { required: true },
            harga_beli: { required: true },
            harga_jual: { required: true }
        },
        messages: {
            kode: { required :'Kode Barang required', maxlength:'Max. 15 Karakter', remote:'Kode sudah Ada' },
            nama: { required:'Nama Barang harus diisi', remote: 'Nama Barang sudah Ada' },
            lstKategori: { required:'Kategori harus dipilih' },
            lstRak: { required: 'Rak harus dipilih' },
            unit: { required:'Unit harus diisi' },
            minstok: { required:'Min. Stok harus diisi' },
            merk: { required:'Merk harus diisi' },
            harga_beli: { required:'Harga Beli harus diisi' },
            harga_jual: { required:'Harga Jual harus diisi' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            var formData = new FormData($('#formInput')[0]);
            $.ajax({
                dataType: 'JSON',
                data: formData,
                async: true,
                url: '<?=site_url('admin/barang/savedata');?>',
                type: "POST",
                success: function(data) {
                    if (data.status === 'success') {
                        swal({
                            title:"Sukses",
                            text: "Simpan Data Sukses",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "success"
                        });
                        $('#formModalAdd').modal('hide');
                        resetformInput();
                        reload_table();
                    } else {
                        swal({
                            title:"Error",
                            text: "Tipe File (JPG, PNG, JPEG)",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                        $('#formModalAdd').modal('hide');
                    }
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Simpan Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});

function resetformInput() {
    $("#kode").val('');
    $("#nama").val('');
    $("#lstKategori").val('');
    $("#lstRak").val('');
    $("#unit").val('');
    $("#merk").val('');
    $("#harga_beli").val('');
    $("#disc1").val('');
    $("#disc2").val('');
    $("#disc3").val('');
    $("#disc4").val('');
    $("#minstok").val('');
    $("#ppn").val('');
    $("#harga_pokok").val('');
    $("#harga_jual").val('');
    $("#foto").val('');

    var MValid = $("#formInput");
    MValid.validate().resetForm();
    MValid.find(".has-error").removeClass("has-error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

function edit_data(id) {
    $('#formEdit')[0].reset();
    $.ajax({
        url : "<?=site_url('admin/barang/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale    = 'en';
            var options   = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter = new Intl.NumberFormat(locale, options);
            $('#id').val(data.barang_id);
            $('#barang_kode').val(data.barang_kode);
            $('#barang_nama').val(data.barang_nama);
            $('#kategori_id').val(data.kategori_id);
            $('#rak_id').val(data.rak_id);
            $('#unit_nama').val(data.unit_nama);
            $('#barang_merk').val(data.barang_merk);
            $('#barang_min_stok').val(formatter.format(data.barang_min_stok));
            $('#unit_hrg_beli').val(formatter.format(data.unit_hrg_beli));
            $('#unit_hrg_jual').val(formatter.format(data.unit_hrg_jual));
            $path = '<?=base_url('img/');?>';
            if (data.barang_foto != null) {
                $('#previewFoto').attr('src', $path+'barang/'+data.barang_foto);
            } else {
                $('#previewFoto').attr('src', $path+'no-image.png');
            }
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formEdit');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formEdit").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: { 
            nama: { required: true },
            lstKategori: { required: true },
            lstRak: { required: true },
            merk: { required: true },
            minstok: { required: true }
        },
        messages: {
            nama: { required:'Nama Barang harus diisi' },
            lstKategori: { required:'Kategori harus dipilih' },
            lstRak: { required:'Rak harus dipilih' },
            merk: { required:'Merk harus diisi' },
            minstok: { required:'Min. Stok harus diisi' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            var formData = new FormData($('#formEdit')[0]);
            $.ajax({
                dataType: 'JSON',
                data: formData,
                async: true,
                url: '<?=site_url('admin/barang/updatedata');?>',
                type: "POST",
                success: function(data) {
                    if (data.status === 'success') {
                        swal({
                            title:"Sukses",
                            text: "Update Data Sukses",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "success"
                        });
                        $('#formModalEdit').modal('hide');
                        reload_table();
                    } else {
                        swal({
                            title:"Error",
                            text: "Tipe File (JPG, PNG, JPEG)",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                        $('#formModalEdit').modal('hide');
                    }
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Update Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalEdit').modal('hide');
                },

                cache: false,
                contentType: false,
                processData: false
            });
        }
    });
});

function hapusData(barang_id) {
    var id = barang_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Data ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/barang/deletedata')?>/"+id,
            type: "POST",
            success: function(data) {
                swal({
                    title:"Sukses",
                    text: "Hapus Data Sukses",
                    showConfirmButton: false,
                    type: "success",
                    timer: 2000
                });
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Data Gagal');
            }
        });
    });
}

$(document).ready(function() {
    $("body").on('keyup', "#harga_beli", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc1", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc2", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc3", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#disc4", function(){
        hitungPokok();
    });

    $("body").on('keyup', "#ppn", function(){
        hitungPokok();
    });
});

function hitungPokok() {
    var locale      = 'en';
    var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter   = new Intl.NumberFormat(locale, options);
    var myForm      = document.formInput;
    var HargaBeli   = myForm.harga_beli.value;
    HargaBeli       = HargaBeli.replace(/[,]/g, '');
    HargaBeli       = parseInt(HargaBeli);
    var Disc1       = myForm.disc1.value;
    Disc1           = Disc1.replace(/[,]/g, '');
    Disc1           = parseFloat(Disc1);
    var Disc2       = myForm.disc2.value;
    Disc2           = Disc2.replace(/[,]/g, '');
    Disc2           = parseFloat(Disc2);
    var Disc3       = myForm.disc3.value;
    Disc3           = Disc3.replace(/[,]/g, '');
    Disc3           = parseFloat(Disc3);
    var Disc4       = myForm.disc4.value;
    Disc4           = Disc4.replace(/[,]/g, '');
    Disc4           = parseFloat(Disc4);
    var PPN         = myForm.ppn.value;
    PPN             = PPN.replace(/[,]/g, '');
    PPN             = parseFloat(PPN);
    if (isNaN(Disc1)) {
        Disc1 = 0;
    } else {
        Disc1 = Disc1;
    }
    if (isNaN(Disc2)) {
        Disc2 = 0;
    } else {
        Disc2 = Disc2;
    }
    if (isNaN(Disc3)) {
        Disc3 = 0;
    } else {
        Disc3 = Disc3;
    }
    if (isNaN(Disc4)) {
        Disc4 = 0;
    } else {
        Disc4 = Disc4;
    }
    if (isNaN(PPN)) {
        PPN = 0;
    } else {
        PPN = PPN;
    }
    
    var HargaPokok;
    if (HargaBeli == 0) {
        HargaPokok = 0;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc1)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc2)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon     = (HargaBeli*Disc3)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 == 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon     = (HargaBeli*Disc4)/100;
        HargaPokok = HargaBeli-Diskon;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 == 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc3)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 == 0 && Disc3 == 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc4)/100;
        HargaPokok = HargaDisc1-Diskon2;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 == 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaPokok = HargaDisc2-Diskon3;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else if (HargaBeli > 0 && Disc1 > 0 && Disc2 > 0 && Disc3 > 0 && Disc4 > 0) {
        Diskon1    = (HargaBeli*Disc1)/100;
        HargaDisc1 = HargaBeli-Diskon1;
        Diskon2    = (HargaDisc1*Disc2)/100;
        HargaDisc2 = HargaDisc1-Diskon2;
        Diskon3    = (HargaDisc2*Disc3)/100;
        HargaDisc3 = HargaDisc2-Diskon3;
        Diskon4    = (HargaDisc3*Disc4)/100;
        HargaPokok = HargaDisc3-Diskon4;
    } else {
        HargaPokok = HargaBeli;
    }

    if (PPN > 0) {
        Pajak      = (HargaPokok*PPN)/100;
        HargaPokok = (HargaPokok+Pajak);
    } else {
        HargaPokok = HargaPokok;
    }

    if (HargaPokok > 0) {
        myForm.harga_pokok.value = formatter.format(HargaPokok);
    } else {
        myForm.harga_pokok.value = 0;
    }
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Kategori</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstKategoriFilter" id="lstKategoriFilter">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listKategori as $r) { ?>
                            <option value="<?=$r->kategori_id;?>"><?=$r->kategori_nama;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Suplier</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstSuplier" id="lstSuplier">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listSuplier as $r) { ?>
                            <option value="<?=$r->suplier_id;?>"><?=$r->suplier_nama;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Rak</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstRakFilter" id="lstRakFilter">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listRak as $r) { ?>
                            <option value="<?=$r->rak_id;?>"><?=$r->rak_nama;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formInput" name="formInput" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Form Tambah Barang</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kode Barang</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Input Kode Barang" name="kode" id="kode" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Barang</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Nama Barang" name="nama" id="nama" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kategori</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstKategori" id="lstKategori">
                                <option value="">- Pilih Kategori -</option>
                                <?php foreach ($listKategori as $r) { ?>
                                <option value="<?=$r->kategori_id;?>"><?=$r->kategori_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Rak</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstRak" id="lstRak">
                                <option value="">- Pilih Rak -</option>
                                <?php foreach ($listRak as $r) { ?>
                                <option value="<?=$r->rak_id;?>"><?=$r->rak_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Merk</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Merk" name="merk" id="merk" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Unit & Min. Stok</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Input Unit" name="unit" id="unit" autocomplete="off">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control number" placeholder="0" name="minstok" id="minstok" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Harga Beli (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="harga_beli" id="harga_beli" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Disc 1-4 (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control decimal" placeholder="0.00" name="disc1" id="disc1" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control decimal" placeholder="0.00" name="disc2" id="disc2" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control decimal" placeholder="0.00" name="disc3" id="disc3" autocomplete="off">
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control decimal" placeholder="0.00" name="disc4" id="disc4" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">PPN (%)</label>
                        <div class="col-md-2">
                            <input type="text" class="form-control decimal" placeholder="0.00" name="ppn" id="ppn" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Harga Pokok (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="harga_pokok" id="harga_pokok" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Harga Jual (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="harga_jual" id="harga_jual" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Upload Foto</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control" name="foto" id="foto" accept=".jpg,.png,.jpeg" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" action="" method="post" id="formEdit" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Form Edit Barang</h4>
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Kode Barang</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Input Kode Barang" name="kode" id="barang_kode" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Barang</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Nama Barang" name="nama" id="barang_nama" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kategori</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstKategori" id="kategori_id">
                                <option value="">- Pilih Kategori -</option>
                                <?php foreach ($listKategori as $r) { ?>
                                <option value="<?=$r->kategori_id;?>"><?=$r->kategori_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Rak</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstRak" id="rak_id">
                                <option value="">- Pilih Rak -</option>
                                <?php foreach ($listRak as $r) { ?>
                                <option value="<?=$r->rak_id;?>"><?=$r->rak_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Merk</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" placeholder="Input Merk" name="merk" id="barang_merk" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Unit & Min. Stok</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Input Unit" name="unit" id="unit_nama" autocomplete="off" readonly>
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control number" placeholder="0" name="minstok" id="barang_min_stok" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Harga Beli (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="harga_beli" id="unit_hrg_beli" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label">Harga Jual (Rp)</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" placeholder="0" name="harga_jual" id="unit_hrg_jual" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Foto Barang</label>
                        <div class="col-md-9">
                            <img src="" style="width:60%;" id="previewFoto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Upload Foto</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control" name="foto" id="barang_foto" accept=".jpg,.png,.jpeg" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Update</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>


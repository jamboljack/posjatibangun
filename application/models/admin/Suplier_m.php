<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suplier_m extends CI_Model
{
    public $table        = 'ok_suplier';
    public $column_order = array(null, null, 'suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_telp',
        'suplier_kontak', 'suplier_termin');
    public $column_search = array('suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_telp', 'suplier_email',
        'suplier_kontak');
    public $order = array('suplier_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'suplier_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_email'  => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'suplier_kontak' => strtoupper(trim(stripHTMLtags($this->input->post('kontak', 'true')))),
            'suplier_termin' => trim(stripHTMLtags($this->input->post('termin', 'true'))),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_suplier', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ok_suplier');
        $this->db->where('suplier_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $suplier_id = $this->input->post('id', 'true');
        $data       = array(
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_email'  => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'suplier_kontak' => strtoupper(trim(stripHTMLtags($this->input->post('kontak', 'true')))),
            'suplier_termin' => trim(stripHTMLtags($this->input->post('termin', 'true'))),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('suplier_id', $suplier_id);
        $this->db->update('ok_suplier', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('suplier_id', $id);
        $this->db->delete('ok_suplier');
    }
}
/* Location: ./application/models/admin/Suplier_m.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Suplier extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/suplier_m');
    }

    public function index()
    {
        $this->template->display('admin/master/suplier_v');
    }

    public function data_list()
    {
        $List = $this->suplier_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row        = array();
            $suplier_id = $r->suplier_id;
            $row[]      = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $suplier_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a onclick="hapusData(' . $suplier_id . ')" title="Delete Data">
                            <i class="icon-close"></i>
                        </a>';
            $row[]  = $no;
            $row[]  = $r->suplier_kode;
            $row[]  = $r->suplier_nama;
            $row[]  = $r->suplier_alamat . ' - ' . $r->suplier_kota;
            $row[]  = $r->suplier_telp;
            $row[]  = $r->suplier_kontak;
            $row[]  = number_format($r->suplier_termin, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->suplier_m->count_all(),
            "recordsFiltered" => $this->suplier_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function kode_exists($kode)
    {
        $this->db->where('suplier_kode', $kode);
        $query = $this->db->get('ok_suplier');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->suplier_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->suplier_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->suplier_m->update_data();
    }

    public function deletedata($id)
    {
        $this->suplier_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Suplier.php */

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Faktur <?=$detail->purchase_no_faktur;?></title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 2px;
    }

    th {
        height: 15px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Arial";
        font-size:14px;
    }
    h1{
        font-size:20px
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" colspan="4" align="right" valign="top"><h1><u>PURCHASE ORDER</u></h1></td>
        </tr>
        <tr>
            <td width="50%" align="left" rowspan="5" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
            <td width="15%" valign="top">Tanggal</td>
            <td width="2%" valign="top">:</td>
            <td width="33%" valign="top"><?=date('d-m-Y', strtotime($detail->purchase_tanggal));?></td>
        </tr>
        <tr>
            <td valign="top">No. Faktur</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->purchase_no_faktur);?></td>
        </tr>
        <tr>
            <td valign="top">Nama Suplier</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->suplier_nama);?></td>
        </tr>
        <tr>
            <td valign="top">Alamat</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->suplier_alamat.' '.$detail->suplier_kota.'<br>'.$detail->suplier_telp);?></td>
        </tr>
    </table>
    <br>
    <table width="100%" align="center">
        <tr>
            <th width="3%" rowspan="2" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="10%" rowspan="2" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KODE</th>
            <th rowspan="2" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NAMA BARANG</th>
            <th rowspan="2" width="15%" colspan="2" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">JUMLAH</th>
            <th rowspan="2" width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">HARGA BELI</th>
            <th colspan="4" width="20%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">DISC (%)</th>
            <th rowspan="2" width="7%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">PPN</th>
            <th rowspan="2" width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">SUBTOTAL</th>
        </tr>
        <tr>
            <th width="5%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">1</th>
            <th width="5%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">2</th>
            <th width="5%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">3</th>
            <th width="5%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">4</th>
        </tr>
        <?php 
        $no    = 1;
        $total = 0;
        foreach($listDetail as $r) {
        ?>
        <tr>
            <td valign="top" align="center" style="border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td valign="top" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->barang_kode;?></td>
            <td valign="top" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->barang_nama;?></td>
            <td valign="top" align="right" width="8" style="border-bottom: 0.5px solid black;"><?=number_format($r->purchase_detail_qty,2,'.',',');?></td>
            <td valign="top" width="7" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->unit_nama;?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_harga,0,'',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_disc1,2,'.',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_disc2,2,'.',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_disc3,2,'.',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_disc4,2,'.',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_ppn,2,'.',',');?></td>
            <td valign="top" align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->purchase_detail_total,0,'',',');?></td>
        </tr>
        <?php 
            $total = $total+$r->purchase_detail_total;
            $no++; 
        } 
        ?>
        <tr>
            <td colspan="8"><br></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="3"></td>
            <td colspan="6" align="right"><b>Sub Total :</b></td>
            <td align="right"><b><?=number_format($detail->purchase_subtotal,0,'',',');?></b></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td colspan="3"></td>
            <td colspan="6" align="right"><b>TOTAL :</b></td>
            <td align="right"><b><?=number_format($detail->purchase_total,0,'',',');?></b></td>
        </tr>
        <tr>
            <td colspan="8"><br></td>
        </tr>
        <tr>
            <td colspan="8"><b>Terbilang : <?=Terbilang($detail->purchase_total);?> Rupiah</b></td>
        </tr>
    </table>
    <br>
    <table cellpadding="2" cellspacing="2">
        <tr>
            <td align="center" width="50%">Pemesan<br><br><br><br><?=$detail->user_name;?></td>
            <td align="center" width="50%">Suplier<br><br><br><br><?=$detail->suplier_nama;?></td>
        </tr>
    </table>
</div>
</body>
</html>
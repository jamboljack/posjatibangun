<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/home_m');
    }

    public function index()
    {
        $bulan                  = date('m');
        $tahun                  = date('Y');
        $data['TotalPembelian'] = $this->db->select_sum('pembelian_total', 'total')->get_where('ok_pembelian', array('MONTH(pembelian_tanggal)' => $bulan, 'YEAR(pembelian_tanggal)' => $tahun))->row();
        $data['TotalHutang']    = $this->db->select_sum('pembelian_total', 'total')->get_where('v_pembelian', array('tipe_bayar_set' => 'K', 'MONTH(pembelian_tanggal)' => $bulan, 'YEAR(pembelian_tanggal)' => $tahun))->row();
        $data['TotalPenjualan'] = $this->db->select_sum('penjualan_total', 'total')->get_where('ok_penjualan', array('MONTH(penjualan_tanggal)' => $bulan, 'YEAR(penjualan_tanggal)' => $tahun))->row();
        $data['TotalPiutang']   = $this->db->select_sum('penjualan_total', 'total')->get_where('v_penjualan', array('tipe_bayar_set' => 'K', 'MONTH(penjualan_tanggal)' => $bulan, 'YEAR(penjualan_tanggal)' => $tahun))->row();
        $this->template->display('admin/home_v', $data);
    }
}
/* Location: ./application/controller/admin/Home.php */

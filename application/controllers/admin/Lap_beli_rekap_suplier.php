<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_beli_rekap_suplier extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
    }

    public function index()
    {
        $this->template->display('admin/reportbeli/reportbelirekapsuplier_v');
    }

    public function printdata($dari, $sampai)
    {
        $data['header']   = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $tgl_dari         = date('Y-m-d', strtotime($dari));
        $tgl_sampai       = date('Y-m-d', strtotime($sampai));
        $sql              = "call total_suplier('$tgl_dari','$tgl_sampai')";
        $data['listData'] = $this->db->query($sql)->result();
        $this->load->view('admin/reportbeli/printrekapsuplier_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_beli_rekap_suplier.php */

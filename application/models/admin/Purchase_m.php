<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Purchase_m extends CI_Model
{
    public $table         = 'v_purchase';
    public $column_order  = array(null, null, 'purchase_no_faktur', 'purchase_tanggal', 'suplier_nama', 'purchase_total');
    public $column_search = array('purchase_no_faktur', 'purchase_tanggal', 'suplier_nama', 'purchase_total');
    public $order         = array('purchase_tanggal' => 'desc');

    public $table1         = 'v_tmp_purchase';
    public $column_order1  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('purchase_temp_id' => 'asc');

    public $table2        = 'v_barang';
    public $column_order2 = array(null, null, 'barang_kode', 'barang_nama', 'barang_merk',
        'unit_qty', 'unit_nama', 'unit_hrg_pokok');
    public $column_search2 = array('barang_kode', 'barang_nama', 'barang_merk');
    public $order2         = array('barang_kode' => 'asc');

    public $table3         = 'v_unit';
    public $column_order3  = array(null, null, null, null, null, null, null, null, null, null);
    public $column_search3 = array();
    public $order3         = array('unit_nama' => 'asc');

    public $table4         = 'v_purchase_detail';
    public $column_order4  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('purchase_detail_id' => 'asc');

    public $table5         = 'ok_suplier';
    public $column_order5  = array(null, null, 'suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $column_search5 = array('suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $order5         = array('suplier_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('purchase_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('purchase_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }

        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Suplier
    private function _get_suplier_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_suplier_datatables()
    {
        $this->_get_suplier_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_suplier_filtered()
    {
        $this->_get_suplier_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_suplier_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Barang
    private function _get_barang_datatables_query()
    {
        $this->db->from($this->table2);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_barang_datatables()
    {
        $this->_get_barang_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_barang_filtered()
    {
        $this->_get_barang_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_barang_all()
    {
        $this->db->from($this->table2);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'suplier_id'                => trim(stripHTMLtags($this->input->post('suplier_id', 'true'))),
            'purchase_temp_tanggal'     => date('Y-m-d'),
            'barang_id'                 => $this->input->post('barang_id', 'true'),
            'purchase_temp_qty'         => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                   => $this->input->post('unit_id', 'true'),
            'purchase_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'purchase_temp_disc1'       => $this->input->post('disc1', 'true'),
            'purchase_temp_disc2'       => $this->input->post('disc2', 'true'),
            'purchase_temp_disc3'       => $this->input->post('disc3', 'true'),
            'purchase_temp_disc4'       => $this->input->post('disc4', 'true'),
            'purchase_temp_ppn'         => $this->input->post('ppn', 'true'),
            'purchase_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'purchase_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'user_username'             => $username,
            'purchase_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_purchase_temp', $data);
    }

    public function update_data_item()
    {
        $purchase_temp_id = $this->input->post('purchase_temp_id', 'true');
        $data             = array(
            'barang_id'                 => $this->input->post('barang_id', 'true'),
            'purchase_temp_qty'         => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                   => $this->input->post('unit_id', 'true'),
            'purchase_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'purchase_temp_disc1'       => $this->input->post('disc1', 'true'),
            'purchase_temp_disc2'       => $this->input->post('disc2', 'true'),
            'purchase_temp_disc3'       => $this->input->post('disc3', 'true'),
            'purchase_temp_disc4'       => $this->input->post('disc4', 'true'),
            'purchase_temp_ppn'         => $this->input->post('ppn', 'true'),
            'purchase_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'purchase_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'purchase_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('purchase_temp_id', $purchase_temp_id);
        $this->db->update('ok_purchase_temp', $data);
    }

    public function delete_data_item($id)
    {
        $this->db->where('purchase_temp_id', $id);
        $this->db->delete('ok_purchase_temp');
    }

    // Unit
    private function _get_unit_datatables_query($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        $i = 0;
        foreach ($this->column_search3 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search3) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order3[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order3)) {
            $order = $this->order3;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_datatables($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_filtered($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_all($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        return $this->db->count_all_results();
    }

    public function getNoFaktur()
    {
        $this->db->select('COUNT(purchase_id) as total', false);
        $this->db->where('YEAR(purchase_tanggal)', date('Y'));
        $this->db->where('MONTH(purchase_tanggal)', date('m'));
        $query = $this->db->get('ok_purchase');
        if ($query->num_rows() != 0) {
            $data = $query->row();
            $kode = intval($data->total) + 1;
        } else {
            $kode = 1;
        }

        $thn        = substr(date('Y'), -2);
        $bln        = date('m');
        $noUrut     = str_pad($kode, 6, "0", STR_PAD_LEFT);
        $noPurchase = $thn . '.' . $bln . '.' . $noUrut;
        return $noPurchase;
    }

    public function insert_data_purchase()
    {
        $username     = $this->session->userdata('username');
        $kode_suplier = $this->input->post('kode_suplier', 'true');
        $tanggal      = date('Y-m-d', strtotime($this->input->post('tanggal', 'true')));
        $termin       = $this->input->post('suplier_termin', 'true');
        if ($kode_suplier == '') {
            $response = ['status' => 'kosong'];
            echo json_encode($response);
        } else {
            $listTemp = $this->db->get_where('ok_purchase_temp', array('user_username' => $username))->result();
            if (count($listTemp) == 0) {
                $response = ['status' => 'item'];
                echo json_encode($response);
            } else {
                $NoPurchase = $this->getNoFaktur();
                $data       = array(
                    'purchase_no_faktur' => $NoPurchase,
                    'purchase_tanggal'   => date('Y-m-d'),
                    'suplier_id'         => $this->input->post('suplier_id', 'true'),
                    'purchase_subtotal'  => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
                    'purchase_total'     => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
                    'user_username'      => $username,
                    'purchase_update'    => date('Y-m-d H:i:s'),
                );

                $this->db->insert('ok_purchase', $data);
                $purchase_id = $this->db->insert_id();

                // Simpan Detail Barang
                $listTemp = $this->db->get_where('ok_purchase_temp', array('user_username' => $username))->result();
                foreach ($listTemp as $r) {
                    $dataItem = array(
                        'purchase_id'                 => $purchase_id,
                        'barang_id'                   => $r->barang_id,
                        'purchase_detail_qty'         => $r->purchase_temp_qty,
                        'unit_id'                     => $r->unit_id,
                        'purchase_detail_harga'       => $r->purchase_temp_harga,
                        'purchase_detail_disc1'       => $r->purchase_temp_disc1,
                        'purchase_detail_disc2'       => $r->purchase_temp_disc2,
                        'purchase_detail_disc3'       => $r->purchase_temp_disc3,
                        'purchase_detail_disc4'       => $r->purchase_temp_disc4,
                        'purchase_detail_ppn'         => $r->purchase_temp_ppn,
                        'purchase_detail_harga_pokok' => $r->purchase_temp_harga_pokok,
                        'purchase_detail_total'       => $r->purchase_temp_total,
                        'purchase_detail_update'      => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_purchase_detail', $dataItem);
                }

                // Hapus Temp by Username dan Tanggal
                $username = $this->session->userdata('username');
                $this->db->where('user_username', $username);
                $this->db->delete('ok_purchase_temp');

                $response = ['status' => 'success'];
                echo json_encode($response);
            }
        }
    }

    // Detail Edit
    private function _get_detail_datatables_query($purchase_id)
    {
        $this->db->from($this->table4);
        $this->db->where('purchase_id', $purchase_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($purchase_id)
    {
        $this->_get_detail_datatables_query($purchase_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($purchase_id)
    {
        $this->_get_detail_datatables_query($purchase_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($purchase_id)
    {
        $this->db->from($this->table4);
        $this->db->where('purchase_id', $purchase_id);

        return $this->db->count_all_results();
    }

    public function insert_data_detail()
    {
        $data = array(
            'purchase_id'                 => $this->input->post('purchase_id', 'true'),
            'barang_id'                   => $this->input->post('barang_id', 'true'),
            'purchase_detail_qty'         => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'purchase_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'purchase_detail_disc1'       => $this->input->post('disc1', 'true'),
            'purchase_detail_disc2'       => $this->input->post('disc2', 'true'),
            'purchase_detail_disc3'       => $this->input->post('disc3', 'true'),
            'purchase_detail_disc4'       => $this->input->post('disc4', 'true'),
            'purchase_detail_ppn'         => $this->input->post('ppn', 'true'),
            'purchase_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'purchase_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'purchase_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_purchase_detail', $data);
    }

    public function update_data_detail()
    {
        $purchase_detail_id = $this->input->post('purchase_detail_id', 'true');
        $barang_id          = $this->input->post('barang_id', 'true');
        $unit_id            = $this->input->post('unit_id', 'true');
        $qty                = intval(str_replace(",", "", $this->input->post('qty', 'true')));
        $qty_lama           = $this->input->post('qty_lama', 'true');
        $data               = array(
            'purchase_detail_qty'         => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'purchase_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'purchase_detail_disc1'       => $this->input->post('disc1', 'true'),
            'purchase_detail_disc2'       => $this->input->post('disc2', 'true'),
            'purchase_detail_disc3'       => $this->input->post('disc3', 'true'),
            'purchase_detail_disc4'       => $this->input->post('disc4', 'true'),
            'purchase_detail_ppn'         => $this->input->post('ppn', 'true'),
            'purchase_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'purchase_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'purchase_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('purchase_detail_id', $purchase_detail_id);
        $this->db->update('ok_purchase_detail', $data);
    }

    public function delete_data_detail($id)
    {
        $this->db->where('purchase_detail_id', $id);
        $this->db->delete('ok_purchase_detail');
    }

    public function update_data_purchase()
    {
        $purchase_id = $this->input->post('purchase_id', 'true');
        $username    = $this->session->userdata('username');
        $data        = array(
            'purchase_no_faktur' => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'purchase_tanggal'   => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'suplier_id'         => $this->input->post('suplier_id', 'true'),
            'purchase_subtotal'  => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
            'purchase_total'     => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
            'user_username'      => $username,
            'purchase_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('purchase_id', $purchase_id);
        $this->db->update('ok_purchase', $data);
    }

    public function insert_data_suplier()
    {
        $data = array(
            'suplier_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_suplier', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_email'  => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'suplier_kontak' => strtoupper(trim(stripHTMLtags($this->input->post('kontak', 'true')))),
            'suplier_termin' => $this->input->post('termin', 'true'),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_suplier', $data);
    }

    public function delete_data_purchase($id)
    {
        $this->db->where('purchase_id', $id);
        $this->db->delete('ok_purchase');
    }

    public function update_data_unit()
    {
        $unit_id = $this->input->post('unit_id_ubah', 'true');
        $data    = array(
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('editdisc1', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('editdisc2', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('editdisc3', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('editdisc4', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('editppn', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('edit_harga_pokok', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true'))),
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id);
        $this->db->update('ok_unit', $data);

        // Log harga Beli
        $unit_buy_old = $this->input->post('unit_hrg_beli_old', 'true');
        $unit_buy_new = intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true')));
        // Log harga jual
        $unit_sell_old = $this->input->post('unit_hrg_jual_old', 'true');
        $unit_sell_new = intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true')));

        if ($unit_buy_old != $unit_buy_new || $unit_sell_old != $unit_sell_new) {
            $margin_buy   = ($unit_buy_new - $unit_buy_old);
            $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
            $margin_sell  = ($unit_sell_new - $unit_sell_old);
            $margin       = @($margin_sell / $unit_sell_old);
            $percent_sell = ($margin * 100);
            $dataLog      = array(
                'unit_id'               => $unit_id,
                'log_date'              => date('Y-m-d'),
                'log_harga_beli_lama'   => $unit_buy_old,
                'log_harga_beli_baru'   => $unit_buy_new,
                'log_harga_beli_persen' => $percent_buy,
                'log_harga_pokok_lama'  => $this->input->post('unit_hrg_pokok_old', 'true'),
                'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
                'log_harga_jual_lama'   => $unit_sell_old,
                'log_harga_jual_baru'   => $unit_sell_new,
                'log_harga_jual_persen' => $percent_sell,
                'user_username'         => $this->session->userdata('username'),
                'log_update'            => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_unit_log', $dataLog);
        }

        // Check Apakah Utama
        $barang_id  = $this->input->post('barang_id_ubah', 'true');
        $harga_beli = intval(str_replace(",", "", $this->input->post('edit_harga_beli', 'true')));
        $harga_jual = intval(str_replace(",", "", $this->input->post('edit_harga_jual', 'true')));
        $multiple   = $this->input->post('unit_id_multiple', 'true');
        if ($multiple > 0) {
            $harga_beli_baru = ($harga_beli / $multiple);
            $harga_jual_baru = ($harga_jual / $multiple);
        } else {
            $harga_beli_baru = $harga_beli;
            $harga_jual_baru = $harga_jual;
        }

        $status_unit = $this->input->post('unit_set', 'true');
        if ($status_unit == 1) {
            // Langsung Ubah Harga Beli dan Pokok Unit lain
            $listUnitLain = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id))->result();
            foreach ($listUnitLain as $r) {
                $unit_id_lain    = $r->unit_id;
                $multiple_lain   = $r->unit_multiple;
                $unit_disc1      = $r->unit_disc1;
                $unit_disc2      = $r->unit_disc2;
                $unit_disc3      = $r->unit_disc3;
                $unit_disc4      = $r->unit_disc4;
                $unit_ppn        = $r->unit_ppn;
                $unit_beli_lama  = $r->unit_hrg_beli;
                $unit_pokok_lama = $r->unit_hrg_pokok;
                $unit_jual_lama  = $r->unit_hrg_jual;
                $harga_beli_lain = ($harga_beli_baru * $multiple_lain);
                $harga_jual_lain = ($harga_jual_baru * $multiple_lain);
                if ($harga_beli_lain == 0) {
                    $HargaPokok = 0;
                } elseif ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc2) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc3) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon     = ($harga_beli_lain * $unit_disc4) / 100;
                    $HargaPokok = $harga_beli_lain - $Diskon;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc2 - $Diskon3;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_lain - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else {
                    $HargaPokok = $harga_beli_lain;
                }

                if ($unit_ppn > 0) {
                    $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                    $HargaPokok = ($HargaPokok + $Pajak);
                } else {
                    $HargaPokok = $HargaPokok;
                }

                // Update Data
                $dataUpdateLain = array(
                    'unit_hrg_beli'  => $harga_beli_lain,
                    'unit_hrg_jual'  => $harga_jual_lain,
                    'unit_disc1'     => $unit_disc1,
                    'unit_disc2'     => $unit_disc2,
                    'unit_disc3'     => $unit_disc3,
                    'unit_disc4'     => $unit_disc4,
                    'unit_ppn'       => $unit_ppn,
                    'unit_hrg_pokok' => $HargaPokok,
                    'unit_update'    => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_lain);
                $this->db->update('ok_unit', $dataUpdateLain);

                // Log harga Beli
                $unit_buy_old  = $unit_beli_lama;
                $unit_buy_new  = $harga_beli_lain;
                $unit_sell_old = $unit_jual_lama;
                $unit_sell_new = $harga_jual_lain;
                if ($unit_buy_old != $unit_buy_new) {
                    $margin_buy   = ($unit_buy_new - $unit_buy_old);
                    $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                    $margin_sell  = ($unit_sell_new - $unit_sell_old);
                    $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                    $dataLogLain  = array(
                        'unit_id'               => $unit_id_lain,
                        'log_date'              => date('Y-m-d'),
                        'log_harga_beli_lama'   => $unit_buy_old,
                        'log_harga_beli_baru'   => $unit_buy_new,
                        'log_harga_beli_persen' => $percent_buy,
                        'log_harga_pokok_lama'  => $unit_pokok_lama,
                        'log_harga_pokok_baru'  => $HargaPokok,
                        // 'log_harga_jual_lama'   => $unit_sell_old,
                        // 'log_harga_jual_baru'   => $unit_sell_new,
                        // 'log_harga_jual_persen' => $percent_sell,
                        'user_username'         => $this->session->userdata('username'),
                        'log_update'            => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_unit_log', $dataLogLain);
                }
            }
        } else {
            // Cari yang Utama dulu, ubah Harga Beli dan Harga Pokok dl
            $dataUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            if (count($dataUtama) > 0) {
                $unit_id_utama    = $dataUtama->unit_id;
                $multiple_utama   = $dataUtama->unit_multiple;
                $unit_disc1       = $dataUtama->unit_disc1;
                $unit_disc2       = $dataUtama->unit_disc2;
                $unit_disc3       = $dataUtama->unit_disc3;
                $unit_disc4       = $dataUtama->unit_disc4;
                $unit_ppn         = $dataUtama->unit_ppn;
                $unit_beli_lama   = $dataUtama->unit_hrg_beli;
                $unit_pokok_lama  = $dataUtama->unit_hrg_pokok;
                $unit_jual_lama   = $dataUtama->unit_hrg_jual;
                $harga_beli_utama = $harga_beli_baru;
                $harga_jual_utama = $harga_jual_baru;
                if ($harga_beli_utama == 0) {
                    $HargaPokok = 0;
                } elseif ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc2) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc3) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon     = ($harga_beli_utama * $unit_disc4) / 100;
                    $HargaPokok = $harga_beli_utama - $Diskon;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc1 - $Diskon2;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaPokok = $HargaDisc2 - $Diskon3;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else if ($harga_beli_utama > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                    $Diskon1    = ($harga_beli_utama * $unit_disc1) / 100;
                    $HargaDisc1 = $harga_beli_utama - $Diskon1;
                    $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                    $HargaDisc2 = $HargaDisc1 - $Diskon2;
                    $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                    $HargaDisc3 = $HargaDisc2 - $Diskon3;
                    $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                    $HargaPokok = $HargaDisc3 - $Diskon4;
                } else {
                    $HargaPokok = $harga_beli_utama;
                }

                if ($unit_ppn > 0) {
                    $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                    $HargaPokok = ($HargaPokok + $Pajak);
                } else {
                    $HargaPokok = $HargaPokok;
                }

                // Update Data
                $dataUpdateUtama = array(
                    'unit_hrg_beli'  => $harga_beli_utama,
                    // 'unit_hrg_jual'  => $harga_jual_utama,
                    'unit_disc1'     => $unit_disc1,
                    'unit_disc2'     => $unit_disc2,
                    'unit_disc3'     => $unit_disc3,
                    'unit_disc4'     => $unit_disc4,
                    'unit_ppn'       => $unit_ppn,
                    'unit_hrg_pokok' => $HargaPokok,
                    'unit_update'    => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_utama);
                $this->db->update('ok_unit', $dataUpdateUtama);

                // Log harga Beli
                $unit_buy_old  = $unit_beli_lama;
                $unit_buy_new  = $harga_beli_utama;
                $unit_sell_old = $unit_jual_lama;
                $unit_sell_new = $harga_jual_utama;
                if ($unit_buy_old != $unit_buy_new) {
                    $margin_buy   = ($unit_buy_new - $unit_buy_old);
                    $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                    $margin_sell  = ($unit_sell_new - $unit_sell_old);
                    $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                    $dataLogLain  = array(
                        'unit_id'               => $unit_id_utama,
                        'log_date'              => date('Y-m-d'),
                        'log_harga_beli_lama'   => $unit_buy_old,
                        'log_harga_beli_baru'   => $unit_buy_new,
                        'log_harga_beli_persen' => $percent_buy,
                        'log_harga_pokok_lama'  => $unit_pokok_lama,
                        'log_harga_pokok_baru'  => $HargaPokok,
                        // 'log_harga_jual_lama'   => $unit_sell_old,
                        // 'log_harga_jual_baru'   => $unit_sell_new,
                        // 'log_harga_jual_persen' => $percent_sell,
                        'user_username'         => $this->session->userdata('username'),
                        'log_update'            => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_unit_log', $dataLogLain);
                }

                // Cari Unit Lain selain Utama dan Unit Edit
                $listUnitLain = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id, 'unit_set !=' => 1))->result();
                foreach ($listUnitLain as $r) {
                    $unit_id_lain    = $r->unit_id;
                    $multiple_lain   = $r->unit_multiple;
                    $unit_disc1      = $r->unit_disc1;
                    $unit_disc2      = $r->unit_disc2;
                    $unit_disc3      = $r->unit_disc3;
                    $unit_disc4      = $r->unit_disc4;
                    $unit_ppn        = $r->unit_ppn;
                    $unit_beli_lama  = $r->unit_hrg_beli;
                    $unit_pokok_lama = $r->unit_hrg_pokok;
                    $harga_beli_lain = ($harga_beli_baru * $multiple_lain);
                    $harga_jual_lain = ($harga_jual_baru * $multiple_lain);
                    if ($harga_beli_lain == 0) {
                        $HargaPokok = 0;
                    } elseif ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc2) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc3) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 == 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                        $Diskon     = ($harga_beli_lain * $unit_disc4) / 100;
                        $HargaPokok = $harga_beli_lain - $Diskon;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 == 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc3) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 == 0 && $unit_disc3 == 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc1 - $Diskon2;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 == 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaPokok = $HargaDisc2 - $Diskon3;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaDisc3 = $HargaDisc2 - $Diskon3;
                        $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc3 - $Diskon4;
                    } else if ($harga_beli_lain > 0 && $unit_disc1 > 0 && $unit_disc2 > 0 && $unit_disc3 > 0 && $unit_disc4 > 0) {
                        $Diskon1    = ($harga_beli_lain * $unit_disc1) / 100;
                        $HargaDisc1 = $harga_beli_lain - $Diskon1;
                        $Diskon2    = ($HargaDisc1 * $unit_disc2) / 100;
                        $HargaDisc2 = $HargaDisc1 - $Diskon2;
                        $Diskon3    = ($HargaDisc2 * $unit_disc3) / 100;
                        $HargaDisc3 = $HargaDisc2 - $Diskon3;
                        $Diskon4    = ($HargaDisc3 * $unit_disc4) / 100;
                        $HargaPokok = $HargaDisc3 - $Diskon4;
                    } else {
                        $HargaPokok = $harga_beli_lain;
                    }

                    if ($unit_ppn > 0) {
                        $Pajak      = ($HargaPokok * $unit_ppn) / 100;
                        $HargaPokok = ($HargaPokok + $Pajak);
                    } else {
                        $HargaPokok = $HargaPokok;
                    }

                    // Update Data
                    $dataUpdateLain = array(
                        'unit_hrg_beli'  => $harga_beli_lain,
                        'unit_hrg_jual'  => $harga_jual_lain,
                        'unit_disc1'     => $unit_disc1,
                        'unit_disc2'     => $unit_disc2,
                        'unit_disc3'     => $unit_disc3,
                        'unit_disc4'     => $unit_disc4,
                        'unit_ppn'       => $unit_ppn,
                        'unit_hrg_pokok' => $HargaPokok,
                        'unit_update'    => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('unit_id', $unit_id_lain);
                    $this->db->update('ok_unit', $dataUpdateLain);

                    // Log harga Beli
                    $unit_buy_old  = $unit_beli_lama;
                    $unit_buy_new  = $harga_beli_lain;
                    $unit_sell_old = $unit_jual_lama;
                    $unit_sell_new = $harga_jual_lain;
                    if ($unit_buy_old != $unit_buy_new) {
                        $margin_buy   = ($unit_buy_new - $unit_buy_old);
                        $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
                        $margin_sell  = ($unit_sell_new - $unit_sell_old);
                        $percent_sell = (($margin_sell / $unit_sell_old) * 100);
                        $dataLogLain  = array(
                            'unit_id'               => $unit_id_lain,
                            'log_date'              => date('Y-m-d'),
                            'log_harga_beli_lama'   => $unit_buy_old,
                            'log_harga_beli_baru'   => $unit_buy_new,
                            'log_harga_beli_persen' => $percent_buy,
                            'log_harga_pokok_lama'  => $unit_pokok_lama,
                            'log_harga_pokok_baru'  => $HargaPokok,
                            'log_harga_jual_lama'   => $unit_sell_old,
                            'log_harga_jual_baru'   => $unit_sell_new,
                            'log_harga_jual_persen' => $percent_sell,
                            'user_username'         => $this->session->userdata('username'),
                            'log_update'            => date('Y-m-d H:i:s'),
                        );

                        $this->db->insert('ok_unit_log', $dataLogLain);
                    }
                }
            }
        }

        $response = ['unit_id' => $unit_id];
        echo json_encode($response);
    }

    public function insert_data_barang()
    {
        $data = array(
            'barang_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode_add', 'true')))),
            'barang_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama_add', 'true')))),
            'barang_seo'    => seo_title(trim(stripHTMLtags($this->input->post('nama_add', 'true')))),
            'barang_merk'   => strtoupper(trim(stripHTMLtags($this->input->post('merk_add', 'true')))),
            'kategori_id'   => trim($this->input->post('lstKategori', 'true')),
            'barang_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_barang', $data);
        $barang_id = $this->db->insert_id();

        // Insert ke Table Price Stock
        $dataStock = array(
            'barang_id'      => $barang_id,
            'unit_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('unit_add', 'true')))),
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('harga_beli_add', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('disc1_add', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('disc2_add', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('disc3_add', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('disc4_add', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('ppn_add', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('harga_pokok_add', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('harga_jual_add', 'true'))),
            'unit_set'       => 1,
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit', $dataStock);
        $unit_id = $this->db->insert_id();

        // Update Satuan Barang
        $dataUnit = array(
            'unit_id' => $unit_id,
        );

        $this->db->where('barang_id', $barang_id);
        $this->db->update('ok_barang', $dataUnit);

        // Insert ke Log Price
        $dataLog = array(
            'unit_id'               => $unit_id,
            'log_date'              => date('Y-m-d'),
            'log_harga_beli_lama'   => 0,
            'log_harga_beli_baru'   => intval(str_replace(",", "", $this->input->post('harga_beli_add', 'true'))),
            'log_harga_pokok_lama'  => 0,
            'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok_add', 'true'))),
            'log_harga_beli_persen' => 0,
            'log_harga_jual_lama'   => 0,
            'log_harga_jual_baru'   => intval(str_replace(",", "", $this->input->post('harga_jual_add', 'true'))),
            'log_harga_jual_persen' => 0,
            'user_username'         => $this->session->userdata('username'),
            'log_update'            => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit_log', $dataLog);

        $response = ['status' => 'success', 'id' => $barang_id];
        echo json_encode($response);
    }
}
/* Location: ./application/model/admin/Purchase_m.php */

<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Hutang_m extends CI_Model
{
    public $table        = 'v_hutang';
    public $column_order = array(null, null, 'hutang_no_faktur', 'hutang_tanggal', 'suplier_nama',
        'suplier_alamat', 'hutang_total');
    public $column_search = array('hutang_no_faktur', 'hutang_tanggal', 'suplier_nama',
        'suplier_alamat', 'hutang_total');
    public $order = array('hutang_id' => 'desc');

    public $table1         = 'v_tmp_hutang';
    public $column_order1  = array(null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('hutang_temp_id' => 'asc');

    public $table2        = 'v_pembelian';
    public $column_order2 = array(null, null, 'pembelian_no_faktur', 'pembelian_tanggal', 'pembelian_total',
        'pembelian_dibayar', 'pembelian_sisa_hutang');
    public $column_search2 = array('pembelian_no_faktur');
    public $order2         = array('pembelian_id' => 'desc');

    public $table4         = 'v_hutang_detail';
    public $column_order4  = array(null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('hutang_detail_id' => 'asc');

    public $table5         = 'v_hutang_suplier';
    public $column_order5  = array(null, null, 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'totalhutang');
    public $column_search5 = array('suplier_nama', 'suplier_alamat', 'suplier_kota', 'totalhutang');
    public $order5         = array('suplier_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('hutang_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('hutang_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Suplier
    private function _get_suplier_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_suplier_datatables()
    {
        $this->_get_suplier_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_suplier_filtered()
    {
        $this->_get_suplier_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_suplier_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Pembelian
    private function _get_pembelian_datatables_query($id)
    {
        $this->db->from($this->table2);
        $this->db->where('suplier_id', $id);
        $this->db->where('tipe_bayar_set', 'K');
        $this->db->where('pembelian_sisa_hutang !=', 0);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_pembelian_datatables($id)
    {
        $this->_get_pembelian_datatables_query($id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_pembelian_filtered($id)
    {
        $this->_get_pembelian_datatables_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_pembelian_all($id)
    {
        $this->db->from($this->table2);
        $this->db->where('suplier_id', $id);
        $this->db->where('tipe_bayar_set', 'K');
        $this->db->where('pembelian_sisa_hutang !=', 0);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'pembelian_id'        => $this->input->post('pembelian_id', 'true'),
            'hutang_temp_tanggal' => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'hutang_temp_total'   => intval(str_replace(",", "", $this->input->post('jumlahhutang', 'true'))),
            'hutang_temp_bayar'   => intval(str_replace(",", "", $this->input->post('bayarhutang', 'true'))),
            'hutang_temp_sisa'    => intval(str_replace(",", "", $this->input->post('sisa', 'true'))),
            'user_username'       => $username,
            'hutang_temp_update'  => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_hutang_temp', $data);

        // Update Kurangi Hutang Suplier
        $pembelian_id  = $this->input->post('pembelian_id', 'true');
        $dataPembelian = $this->db->get_where('ok_pembelian', array('pembelian_id' => $pembelian_id))->row();
        $sisahutang    = $dataPembelian->pembelian_sisa_hutang;
        $dibayar       = $dataPembelian->pembelian_dibayar;
        $pembayaran    = intval(str_replace(",", "", $this->input->post('bayarhutang', 'true')));
        $dataUpdate    = array(
            'pembelian_dibayar'     => ($dibayar + $pembayaran),
            'pembelian_sisa_hutang' => ($sisahutang - $pembayaran),
            'pembelian_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_id', $pembelian_id);
        $this->db->update('ok_pembelian', $dataUpdate);
    }

    public function update_data_item()
    {
        $hutang_temp_id = $this->input->post('hutang_temp_id', 'true');
        $data           = array(
            'hutang_temp_bayar'  => intval(str_replace(",", "", $this->input->post('bayarhutang', 'true'))),
            'hutang_temp_sisa'   => intval(str_replace(",", "", $this->input->post('sisa', 'true'))),
            'hutang_temp_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('hutang_temp_id', $hutang_temp_id);
        $this->db->update('ok_hutang_temp', $data);

        // Update Kurangi Hutang Suplier
        $pembelian_id   = $this->input->post('pembelian_id', 'true');
        $dataPembelian  = $this->db->get_where('ok_pembelian', array('pembelian_id' => $pembelian_id))->row();
        $sisahutang     = $dataPembelian->pembelian_sisa_hutang;
        $dibayar        = $dataPembelian->pembelian_dibayar;
        $pembayaran     = intval(str_replace(",", "", $this->input->post('bayarhutang', 'true')));
        $pembayaranlama = $this->input->post('bayar_hutang_lama', 'true');
        $dataUpdate     = array(
            'pembelian_dibayar'     => (($dibayar - $pembayaranlama) + $pembayaran),
            'pembelian_sisa_hutang' => (($sisahutang + $pembayaranlama) - $pembayaran),
            'pembelian_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_id', $pembelian_id);
        $this->db->update('ok_pembelian', $dataUpdate);
    }

    public function delete_data_item($id)
    {
        // Update Pembayaran Hutang
        $dataItem      = $this->db->get_where('ok_hutang_temp', array('hutang_temp_id' => $id))->row();
        $pembelian_id  = $dataItem->pembelian_id;
        $pembayaran    = $dataItem->hutang_temp_bayar;
        $dataPembelian = $this->db->get_where('ok_pembelian', array('pembelian_id' => $pembelian_id))->row();
        $sisahutang    = $dataPembelian->pembelian_sisa_hutang;
        $dibayar       = $dataPembelian->pembelian_dibayar;
        $dataUpdate    = array(
            'pembelian_dibayar'     => ($dibayar - $pembayaran),
            'pembelian_sisa_hutang' => ($sisahutang + $pembayaran),
            'pembelian_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('pembelian_id', $pembelian_id);
        $this->db->update('ok_pembelian', $dataUpdate);

        // Hapus Item
        $this->db->where('hutang_temp_id', $id);
        $this->db->delete('ok_hutang_temp');
    }

    public function getNoFaktur()
    {
        $this->db->select('COUNT(hutang_id) as total', false);
        $this->db->where('YEAR(hutang_tanggal)', date('Y'));
        $this->db->where('MONTH(hutang_tanggal)', date('m'));
        $query = $this->db->get('ok_hutang');
        if ($query->num_rows() != 0) {
            $data = $query->row();
            $kode = intval($data->total) + 1;
        } else {
            $kode = 1;
        }

        $noUrut = str_pad($kode, 5, "0", STR_PAD_LEFT);
        $noJual = $noUrut . '/HTG-' . date('m') . '/' . date('Y');
        return $noJual;
    }

    public function insert_data_hutang()
    {

        $username = $this->session->userdata('username');
        $noFaktur = $this->getNoFaktur();
        $data     = array(
            'hutang_no_faktur' => $noFaktur,
            'hutang_tanggal'   => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'suplier_id'       => $this->input->post('suplier_id', 'true'),
            'hutang_total'     => intval(str_replace(",", "", $this->input->post('totaltransaksi', 'true'))),
            'user_username'    => $username,
            'hutang_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_hutang', $data);
        $hutang_id = $this->db->insert_id();

        // Simpan Detail Barang
        $listTemp = $this->db->get_where('ok_hutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $dataDetail = array(
                'hutang_id'            => $hutang_id,
                'pembelian_id'         => $r->pembelian_id,
                'hutang_detail_total'  => $r->hutang_temp_total,
                'hutang_detail_bayar'  => $r->hutang_temp_bayar,
                'hutang_detail_sisa'   => $r->hutang_temp_sisa,
                'hutang_detail_update' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_hutang_detail', $dataDetail);
        }

        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_hutang_temp');

        $response = ['status' => 'success', 'id' => $hutang_id];
        echo json_encode($response);
    }

    // Detail Edit
    private function _get_detail_datatables_query($hutang_id)
    {
        $this->db->from($this->table4);
        $this->db->where('hutang_id', $hutang_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($hutang_id)
    {
        $this->_get_detail_datatables_query($hutang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($hutang_id)
    {
        $this->_get_detail_datatables_query($hutang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($hutang_id)
    {
        $this->db->from($this->table4);
        $this->db->where('hutang_id', $hutang_id);

        return $this->db->count_all_results();
    }

    public function insert_data_detail()
    {
        $data = array(
            'hutang_id'            => $this->input->post('hutang_id', 'true'),
            'barang_id'            => $this->input->post('barang_id', 'true'),
            'hutang_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'              => $this->input->post('unit_id', 'true'),
            'hutang_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'hutang_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
            'hutang_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
            'hutang_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'hutang_detail_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_hutang_detail', $data);

        // Update Stok
        $barang_id     = $this->input->post('barang_id', 'true');
        $unit_id       = $this->input->post('unit_id', 'true');
        $qty           = intval(str_replace(",", "", $this->input->post('qty', 'true')));
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama + $qty);
        } else {
            $Stok = (($qty * $Multiple) + $StokUtama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function update_data_detail()
    {
        $hutang_detail_id = $this->input->post('hutang_detail_id', 'true');
        $barang_id        = $this->input->post('barang_id', 'true');
        $unit_id          = $this->input->post('unit_id', 'true');
        $qty              = intval(str_replace(",", "", $this->input->post('qty', 'true')));
        $qty_lama         = $this->input->post('qty_lama', 'true');
        $data             = array(
            'hutang_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'              => $this->input->post('unit_id', 'true'),
            'hutang_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'hutang_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
            'hutang_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
            'hutang_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'hutang_detail_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('hutang_detail_id', $hutang_detail_id);
        $this->db->update('ok_hutang_detail', $data);

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = (($StokUtama - $qty_lama) + $qty); // Stok Utama - Qty Lama + Qty Baru
        } else {
            $MultipleLama = ($qty_lama * $Multiple);
            $Stok         = (($StokUtama - $MultipleLama) + ($qty * $Multiple));
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function delete_data_detail($id)
    {
        // Hitung Stok dulu
        $dataLama  = $this->db->get_where('ok_hutang_detail', array('hutang_detail_id' => $id))->row();
        $barang_id = $dataLama->barang_id;
        $unit_id   = $dataLama->unit_id;
        $qty       = $dataLama->hutang_detail_qty;

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama - $qty); // Stok Utama - Qty
        } else {
            $MultipleLama = ($qty * $Multiple);
            $Stok         = ($StokUtama - $MultipleLama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }

        // Hapus Data
        $this->db->where('hutang_detail_id', $id);
        $this->db->delete('ok_hutang_detail');
    }

    public function update_data_hutang()
    {
        $hutang_id = $this->input->post('hutang_id', 'true');
        $data      = array(
            'hutang_no_faktur' => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'hutang_tanggal'   => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'suplier_id'       => $this->input->post('suplier_id', 'true'),
            'hutang_total'     => intval(str_replace(",", "", $this->input->post('totalhutang', 'true'))),
            'user_username'    => $username,
            'hutang_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('hutang_id', $hutang_id);
        $this->db->update('ok_hutang', $data);
    }

    public function insert_data_suplier()
    {
        $data = array(
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_suplier', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_disc'   => $this->input->post('disc', 'true'),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_suplier', $data);
    }

    public function delete_data_hutang($id)
    {
        $listDetail = $this->db->order_by('hutang_detail_id', 'asc')->get_where('ok_hutang_detail', array('hutang_id' => $id))->result();
        foreach ($listDetail as $d) {
            // Hitung Stok dulu
            $hutang_detail_id = $d->hutang_detail_id;
            $dataLama         = $this->db->get_where('ok_hutang_detail', array('hutang_detail_id' => $hutang_detail_id))->row();
            $barang_id        = $dataLama->barang_id;
            $unit_id          = $dataLama->unit_id;
            $qty              = $dataLama->hutang_detail_qty;

            // Update Stok
            $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple      = $dataUnit->unit_multiple;
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            if ($unit_id == $unit_id_utama) {
                $Stok = ($StokUtama - $qty); // Stok Utama - Qty
            } else {
                $MultipleLama = ($qty * $Multiple);
                $Stok         = ($StokUtama - $MultipleLama);
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'    => $Stok,
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'    => ($Stok / $u->unit_multiple),
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Data
        $this->db->where('hutang_id', $id);
        $this->db->delete('ok_hutang');
    }
}
/* Location: ./application/model/admin/Penjualan_m.php */

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Dashboard</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat blue-madison">
                    <div class="visual">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?=number_format($TotalPembelian->total, 0, '', '.');?>
                        </div>
                        <div class="desc">
                            Pembelian : <?=getBulan(date('m')).' '.date('Y');?>
                        </div>
                    </div>
                    <a class="more" href="<?=site_url('admin/pembelian');?>">
                    Detail <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat red-intense">
                    <div class="visual">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?=number_format($TotalHutang->total, 0, '', '.');?>
                        </div>
                        <div class="desc">
                            Hutang : <?=getBulan(date('m')).' '.date('Y');?>
                        </div>
                    </div>
                    <a class="more" href="<?=site_url('admin/hutang');?>">
                    Detail <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat green-haze">
                    <div class="visual">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?=number_format($TotalPenjualan->total, 0, '', '.');?>
                        </div>
                        <div class="desc">
                            Penjualan : <?=getBulan(date('m')).' '.date('Y');?>
                        </div>
                    </div>
                    <a class="more" href="<?=site_url('admin/penjualan');?>">
                    Detail <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat yellow-gold">
                    <div class="visual">
                        <i class="icon-basket"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <?=number_format($TotalPiutang->total, 0, '', '.');?>
                        </div>
                        <div class="desc">
                            Piutang : <?=getBulan(date('m')).' '.date('Y');?>
                        </div>
                    </div>
                    <a class="more" href="<?=site_url('admin/piutang');?>">
                    Detail <i class="m-icon-swapright m-icon-white"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>
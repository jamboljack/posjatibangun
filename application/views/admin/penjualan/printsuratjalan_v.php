<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Surat Jalan <?=$detail->penjualan_no_faktur;?></title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 2px;
    }

    th {
        height: 25px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Arial";
        font-size:14px;
    }
    h1{
        font-size:20px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 14.8cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="5" align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td colspan="5" align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td colspan="5" align="center" valign="top"><h1><u>SURAT JALAN</u></h1></td>
        </tr>
        <tr>
            <td width="9%" valign="top">Tanggal</td>
            <td width="1%" valign="top">:</td>
            <td width="40%" valign="top"><?=date('d-m-Y H:i', strtotime($detail->penjualan_update));?> WIB</td>
            <td width="13%" valign="top">Lokasi Kirim :</td>
            <td width="47%" valign="top"><b><?=trim($detail->penjualan_nama);?></b></td>
        </tr>
        <tr>
            <td valign="top">No. Faktur</td>
            <td width="1%" valign="top">:</td>
            <td valign="top"><?=trim($detail->penjualan_no_faktur);?></td>
            <td valign="top" colspan="2" rowspan="4"><?=($detail->penjualan_alamat!=''?trim($detail->penjualan_alamat):$detail->pelanggan_alamat.' '.$detail->pelanggan_kota);?></td>
        </tr>
        <tr>
            <td valign="top">Pelanggan</td>
            <td width="1%" valign="top">:</td>
            <td valign="top"><?=trim($detail->pelanggan_nama);?></td>
        </tr>
        <tr>
            <td valign="top" valign="top">Alamat</td>
            <td width="1%" valign="top">:</td>
            <td valign="top" valign="top"><?=trim($detail->pelanggan_alamat.' '.$detail->pelanggan_kota.' ('.$detail->pelanggan_telp.')');?></td>
        </tr>
        <tr>
            <td valign="top" valign="top">Operator</td>
            <td width="1%" valign="top">:</td>
            <td valign="top" valign="top"><?=trim($detail->user_name);?></td>
        </tr>
    </table>
    <p>Dengan ini kami kirimkan barang-barang sebagai berikut :</p>
    <table width="100%" align="center">
        <tr>
            <th width="3%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KODE</th>
            <th width="30%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NAMA BARANG</th>
            <th width="15%" colspan="2" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">JUMLAH</th>
            <th width="42%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KETERANGAN</th>
        </tr>
        <?php 
        $no  = 1;
        foreach($listDetail as $r) {
        ?>
        <tr>
            <td align="center" style="border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td align="center" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->barang_kode;?></td>
            <td style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->barang_nama;?></td>
            <td align="right" width="8" style="border-bottom: 0.5px solid black;"><?=customNumberFormat($r->penjualan_detail_qty);?></td>
            <td align="right" width="7" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->unit_nama;?></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"></td>
        </tr>
        <?php 
            $no++; 
        } 
        ?>
    </table>
    <p>Bila telah di Tandatangani, maka Anda dianggap setuju dengan segala yang tertulis.</p>
    <br>
    <table width="100%" align="center">
        <tr>
            <td width="25%" align="center">Penerima<br><br><br><br></td>
            <td width="25%" align="center">Gudang<br><br><br><br></td>
            <td width="25%" align="center">Sopir<br><br><br><br></td>
            <td width="25%" align="center">Petugas<br><br><br><br></td>
        </tr>
        <tr>
            <td align="center">___________________</td>
            <td align="center">___________________</td>
            <td align="center">___________________</td>
            <td align="center"><u><?=$this->session->userdata('nama');?></u></td>
        </tr>
    </table>
    <p align="right">Printed : <?=date('d-m-Y H:i:s');?></p>
</div>
</body>
</html>
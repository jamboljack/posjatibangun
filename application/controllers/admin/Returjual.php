<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Returjual extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/returjual_m');
    }

    public function index()
    {
        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $data['listTipe']      = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/retur_jual/view', $data);
    }

    public function data_list()
    {
        $List = $this->returjual_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row           = array();
            $retur_jual_id = $r->retur_jual_id;
            $linkedit      = site_url('admin/returjual/editdata/' . $retur_jual_id);
            $linkprint     = site_url('admin/returjual/printfaktur/' . $retur_jual_id);
            $row[]         = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                              <a onclick="printNota(' . $retur_jual_id . ')" title="Print Nota"><i class="icon-screen-tablet"></i></a>
                              <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>';
            $row[]  = $no;
            $row[]  = $r->retur_jual_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->retur_jual_tanggal));
            $row[]  = $r->pelanggan_nama;
            $row[]  = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]  = number_format($r->retur_jual_total, 0, '', ',');
            $row[]  = $r->user_username;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_all(),
            "recordsFiltered" => $this->returjual_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_retur_jual_temp');

        $data['meta']     = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listTipe'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/retur_jual/add', $data);
    }

    private function kode_exists($kode)
    {
        $this->db->where('pelanggan_kode', $kode);
        $query = $this->db->get('ok_pelanggan');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function data_temp_list()
    {
        $List = $this->returjual_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row                = array();
            $retur_jual_temp_id = $r->retur_jual_temp_id;
            $row[]              = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $retur_jual_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $retur_jual_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->retur_jual_temp_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->retur_jual_temp_harga, 0, '', ',');
            $row[]  = number_format($r->retur_jual_temp_disc, 2, '.', ',');
            $row[]  = number_format($r->retur_jual_temp_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_tmp_all(),
            "recordsFiltered" => $this->returjual_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_pelanggan_list()
    {
        $List = $this->returjual_m->get_pelanggan_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pelanggan_id = $r->pelanggan_id;
            $row[]        = '<a href="javascript:void(0)" title="Pilih" onclick="pilihpelanggan(' . "'" . $pelanggan_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]        = $no;
            $row[]        = $r->pelanggan_kode;
            $row[]        = $r->pelanggan_nama;
            $row[]        = $r->pelanggan_alamat;
            $row[]        = $r->pelanggan_kota;
            $row[]        = $r->pelanggan_telp;
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_pelanggan_all(),
            "recordsFiltered" => $this->returjual_m->count_pelanggan_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_barang_list()
    {
        $List = $this->returjual_m->get_barang_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $barang_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]     = $no;
            if ($r->unit_qty <= $r->barang_min_stok) {
                $kode = '<span class="label label-danger">' . $r->barang_kode . '</span>';
                $nama = '<span class="label label-danger">' . $r->barang_nama . '</span>';
                $merk = '<span class="label label-danger">' . $r->barang_merk . '</span>';
                $qty  = '<span class="label label-danger">' . number_format($r->unit_qty, 2, '.', ',') . '</span>';
            } else {
                $kode = $r->barang_kode;
                $nama = $r->barang_nama;
                $merk = $r->barang_merk;
                $qty  = number_format($r->unit_qty, 2, '.', ',');
            }
            $row[]  = $kode;
            $row[]  = $nama;
            $row[]  = $merk;
            $row[]  = $qty;
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_barang_all(),
            "recordsFiltered" => $this->returjual_m->count_barang_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_retur_jual', array('retur_jual_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_list_item($id)
    {
        $data = $this->db->get_where('v_retur_jual_detail', array('retur_jual_id' => $id))->result();
        echo json_encode($data);
    }

    public function get_data_pelanggan($id)
    {
        $data = $this->db->get_where('ok_pelanggan', array('pelanggan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_pelanggan_by_kode($kode_pelanggan)
    {
        $data = $this->db->get_where('ok_pelanggan', array('pelanggan_kode' => trim(strtoupper($kode_pelanggan))))->row();
        echo json_encode($data);
    }

    public function get_data_barang($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_barang_by_kode($kode_barang)
    {
        $data = $this->db->get_where('v_barang', array('barang_kode' => trim(strtoupper($kode_barang))))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_retur_jual', array('retur_jual_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $this->returjual_m->insert_data_item();
    }

    public function updateitem()
    {
        $this->returjual_m->update_data_item();
    }

    public function deleteitem($id)
    {
        $this->returjual_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('retur_jual_temp_total', 'total')->get_where('ok_retur_jual_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function data_unit_list($barang_id)
    {
        $List = $this->returjual_m->get_unit_datatables($barang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row     = array();
            $unit_id = $r->unit_id;
            $row[]   = '<a href="javascript:void(0)" title="Pilih" onclick="pilihunit(' . "'" . $unit_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]   = $r->unit_nama;
            $row[]   = number_format($r->unit_qty, 0, '', ',');
            $row[]   = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[]  = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_unit_all($barang_id),
            "recordsFiltered" => $this->returjual_m->count_unit_filtered($barang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_unit($id)
    {
        $data = $this->db->get_where('v_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function savedata()
    {
        $this->returjual_m->insert_data_retur_jual();
    }

    public function editdata($retur_jual_id)
    {
        $data['listTipe'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $data['detail']   = $this->db->get_where('v_retur_jual', array('retur_jual_id' => $retur_jual_id))->row();
        $this->template->display('admin/retur_jual/edit', $data);
    }

    public function data_detail_list($retur_jual_id)
    {
        $List = $this->returjual_m->get_detail_datatables($retur_jual_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = customNumberFormat($r->retur_jual_detail_qty);
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->retur_jual_detail_harga, 0, '', ',');
            $row[]  = number_format($r->retur_jual_detail_disc, 2, '.', ',');
            $row[]  = number_format($r->retur_jual_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->returjual_m->count_detail_all($retur_jual_id),
            "recordsFiltered" => $this->returjual_m->count_detail_filtered($retur_jual_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($retur_jual_id)
    {
        $data = $this->db->select_sum('retur_jual_detail_total', 'total')->get_where('ok_retur_jual_detail', array('retur_jual_id' => $retur_jual_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->returjual_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_retur_jual_detail', array('retur_jual_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->returjual_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->returjual_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->returjual_m->update_data_retur_jual();
    }

    public function savedatapelanggan()
    {
        $this->returjual_m->insert_data_pelanggan();
    }

    public function deletedata($id)
    {
        $this->returjual_m->delete_data_retur_jual($id);
    }

    public function printfaktur($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_retur_jual', array('retur_jual_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_retur_jual_detail', array('retur_jual_id' => $id))->result();
        $this->load->view('admin/retur_jual/printfaktur_v', $data);
    }
}
/* Location: ./application/controller/admin/Returjual.php */

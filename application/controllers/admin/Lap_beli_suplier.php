<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_beli_suplier extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/lap_beli_suplier_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $this->template->display('admin/reportbeli/reportbelisuplier_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_beli_suplier_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = number_format($r->pembelian_detail_qty, 0, '', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->pembelian_detail_harga, 0, '', ',');
            $row[]  = number_format($r->pembelian_detail_ppn, 2, '.', ',');
            $row[]  = number_format($r->pembelian_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_beli_suplier_m->count_all(),
            "recordsFiltered" => $this->lap_beli_suplier_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printsuplier($dari = 'all', $sampai = 'all', $suplier = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($suplier != 'all') {
            $data['listData'] = $this->db->order_by('suplier_nama', 'asc')->get_where('ok_suplier', array('suplier_id' => $suplier))->result();
        } else {
            $data['listData'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        }

        $this->load->view('admin/reportbeli/printsuplier_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_beli_suplier.php */

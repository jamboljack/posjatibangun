<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Jurnal Penjualan</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:16px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<?php 
if ($this->uri->segment(4) != 'all' && $this->uri->segment(5) != 'all') {
    $periode = 'PERIODE : '.$this->uri->segment(4).' s/d '.$this->uri->segment(5);
} else {
    $periode = '';
}

?>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="font-size: 15px; font-weight: bold;"><u>JURNAL PENJUALAN</u></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?=$periode;?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <th width="3%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TANGGAL</th>
            <th width="20%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO. FAKTUR</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TIPE BAYAR</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">BRUTO</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">DISKON</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NETTO</th>
            <th width="7%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">PPN (%)</th>
            <th width="12%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TOTAL</th>
        </tr>
        <?php 
        $no          = 1;
        $totalbruto  = 0;
        $totaldiskon = 0;
        $totalnetto  = 0;
        $totalppn    = 0;
        $totalppnrp  = 0;
        $total       = 0;
        $totaldebet  = 0;
        $totalkredit = 0;
        foreach($listData as $r) { 
        ?>
        <tr>
            <td align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td align="center" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=date('d-m-Y', strtotime($r->penjualan_tanggal));?></td>
            <td style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->penjualan_no_faktur;?></td>
            <td align="center" style="border-top: 0.5px solid black; border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><?=$r->tipe_bayar_nama;?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->penjualan_bruto,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->penjualan_diskon,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->penjualan_netto,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->penjualan_ppn,2,'.',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->penjualan_total,0,'',',');?></td>
        </tr>
        <?php
            $totalbruto  = ($totalbruto+$r->penjualan_bruto);
            $totaldiskon = ($totaldiskon+$r->penjualan_diskon);
            $totalnetto  = ($totalnetto+$r->penjualan_netto);
            $totalppn    = ($totalppn+$r->penjualan_ppn);
            $totalppnrp  = ($totalppnrp+$r->penjualan_ppn_rp);
            $total       = ($total+$r->penjualan_total);
            if ($r->tipe_bayar_set == 'D') {
                $totaldebet  = ($totaldebet+$r->penjualan_total);
            } else {
                $totalkredit = ($totalkredit+$r->penjualan_total);
            }

            $no++; 
        }
        ?>
        <tr>
            <td colspan="4" align="center" style="border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b>TOTAL</b></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalbruto,0,'',',');?></b></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totaldiskon,0,'',',');?></b></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalnetto,0,'',',');?></b></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalppn,2,'.',',');?></b></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($total,0,'',',');?></b></td>
        </tr>
    </table>
    <br><br>
    <table width="100%">
        <tr>
            <td width="25%"><b>TOTAL BRUTO</b></td>
            <td width="1%"><b>:</b></td>
            <td width="20%" align="right"><b><?=number_format($totalbruto,0,'',',');?></b></td>
            <td width="4%"></td>
            <td width="25%"><b>TOTAL DEBET</b></td>
            <td width="1%"><b>:</b></td>
            <td width="20%" align="right"><b><?=number_format($totaldebet,0,'',',');?></b></td>
            <td width="4%"></td>
        </tr>
        <tr>
            <td><b>TOTAL DISKON</b></td>
            <td><b>:</b></td>
            <td align="right" style="border-bottom: 0.5px dotted black;"><b><?=number_format($totaldiskon,0,'',',');?></b></td>
            <td>-</td>
            <td><b>TOTAL KREDIT</b></td>
            <td><b>:</b></td>
            <td align="right" style="border-bottom: 0.5px dotted black;"><b><?=number_format($totalkredit,0,'',',');?></b></td>
            <td>+</td>
        </tr>
        <tr>
            <td><b>TOTAL NETTO</b></td>
            <td><b>:</b></td>
            <td align="right"><b><?=number_format($totalnetto,0,'',',');?></b></td>
            <td></td>
            <td><b>TOTAL</b></td>
            <td><b>:</b></td>
            <td align="right"><b><?=number_format($total,0,'',',');?></b></td>
            <td></td>
        </tr>
        <tr>
            <td><b>TOTAL PPN</b></td>
            <td><b>:</b></td>
            <td align="right" style="border-bottom: 0.5px dotted black;"><b><?=number_format($totalppnrp,0,'',',');?></b></td>
            <td>+</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td><b>TOTAL</b></td>
            <td><b>:</b></td>
            <td align="right"><b><?=number_format($total,0,'',',');?></b></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</div>
</body>
</html>
<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lap_stok_barang_m extends CI_Model
{
    public $table         = 'v_barang';
    public $column_order  = array(null, 'barang_kode', 'barang_nama', 'barang_merk', 'kategori_nama', 'unit_qty', 'barang_min_stok', 'unit_nama');
    public $column_search = array('barang_kode', 'barang_nama', 'barang_merk', 'kategori_nama');
    public $order         = array('barang_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('lstKategori', 'true')) {
            $this->db->where('kategori_id', $this->input->post('lstKategori', 'true'));
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        if ($this->input->post('lstRak', 'true')) {
            $this->db->where('rak_id', $this->input->post('lstRak', 'true'));
        }
        if ($this->input->post('merk', 'true')) {
            $this->db->like('barang_merk', $this->input->post('merk', 'true'));
        }
        if ($this->input->post('lstStok', 'true')) {
            if ($this->input->post('lstStok', 'true') == 1) {
                $this->db->where('unit_qty <', 'barang_min_stok');
            } else {
                $this->db->where('unit_qty >=', 'barang_min_stok');
            }
        }

        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
}
/* Location: ./application/model/admin/Lap_stok_barang_m.php */

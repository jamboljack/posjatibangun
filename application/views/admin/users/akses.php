<link rel="stylesheet" type="text/css" href="<?=base_url();?>backend/js/sweetalert2.css">
<script src="<?=base_url();?>backend/js/sweetalert2.min.js"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">
            Hak Akses Users
        </h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/users');?>">Users</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Setting Hak Akses</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-settings"></i> Hak Akses User
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <form id="formInput" method="post" class="form" role="form">
                        <input type="hidden" name="id" value="<?=$detailakses->akses_id;?>">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>A. Master Umum</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk01" <?=($detailakses->c01==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Tipe Bayar
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk02" <?=($detailakses->c02==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Suplier
                                                    </label>
                                                    <!-- <label>
                                                        <input type="checkbox" class="icheck" name="chk03" <?=($detailakses->c03==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Level
                                                    </label> -->
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk04" <?=($detailakses->c04==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pelanggan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk05" <?=($detailakses->c05==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Sales
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk06" <?=($detailakses->c06==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Bank
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>B. Master Barang</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk11" <?=($detailakses->c11==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Kategori Barang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk12" <?=($detailakses->c12==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Barang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk13" <?=($detailakses->c13==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Cetak Barcode
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>C. Transaksi</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk27" <?=($detailakses->c27==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Purchase Order
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk21" <?=($detailakses->c21==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembelian
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk22" <?=($detailakses->c22==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Retur Pembelian
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk23" <?=($detailakses->c23==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Penjualan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk24" <?=($detailakses->c24==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Retur Penjualan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk25" <?=($detailakses->c25==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Hutang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk26" <?=($detailakses->c26==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Piutang
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>D. Users</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk31" <?=($detailakses->c31==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Users
                                                    </label>
                                                    <!-- <label>
                                                        <input type="checkbox" class="icheck" name="chk32" <?=($detailakses->c32==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Log User
                                                    </label> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>E. Lap. Pembelian</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk41" <?=($detailakses->c41==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembelian per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk42" <?=($detailakses->c42==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembelian per Suplier
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk43" <?=($detailakses->c43==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembelian per Barang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk44" <?=($detailakses->c44==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap Pembelian
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk45" <?=($detailakses->c45==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap Suplier
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk46" <?=($detailakses->c46==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Daftar Harga Beli
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>F. Lap. Retur Pembelian</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk51" <?=($detailakses->c51==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Retur per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk52" <?=($detailakses->c52==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap per Periode
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>G. Lap. Penjualan</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk61" <?=($detailakses->c61==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Penjualan per Faktur
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk62" <?=($detailakses->c62==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Penjualan per Pelanggan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk63" <?=($detailakses->c63==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Penjualan per Barang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk64" <?=($detailakses->c64==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap Penjualan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk65" <?=($detailakses->c65==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap Pelanggan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk66" <?=($detailakses->c66==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Transaksi Kasir
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk67" <?=($detailakses->c67==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Jurnal Penjualan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk68" <?=($detailakses->c68==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Laba / Rugi
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk69" <?=($detailakses->c69==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Daftar Harga Jual
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>H. Lap. Retur Penjualan</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk71" <?=($detailakses->c71==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Retur per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk72" <?=($detailakses->c72==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk73" <?=($detailakses->c73==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Rekap per Kasir
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>I. Lap. Hutang</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk81" <?=($detailakses->c81==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Daftar Hutang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk82" <?=($detailakses->c82==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembayaran per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk83" <?=($detailakses->c83==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembayaran per Suplier
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk84" <?=($detailakses->c84==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Status Hutang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk85" <?=($detailakses->c85==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Jatuh Tempo
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>J. Lap. Piutang</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk91" <?=($detailakses->c91==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Daftar Piutang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk92" <?=($detailakses->c92==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembayaran per Periode
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk93" <?=($detailakses->c93==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Pembayaran per Pelanggan
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk94" <?=($detailakses->c94==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Status Piutang
                                                    </label>
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk95" <?=($detailakses->c95==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Jatuh Tempo
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><b>K. Lap. Barang</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk101" <?=($detailakses->c101==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Daftar Stok
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk102" <?=($detailakses->c102==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Stok Opname
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label><b>L. Menu Pengiriman</b></label>
                                            <div class="input-group">
                                                <div class="icheck-list">
                                                    <label>
                                                        <input type="checkbox" class="icheck" name="chk20" <?=($detailakses->c20==1?'checked':'');?> value="1">
                                                        <span></span>
                                                        Surat Jalan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-actions" align="center">
                                <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Update</button>
                                <a href="<?=site_url('admin/users');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Batal</a>
                            </div>

                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $("#formInput").validate({
        submitHandler: function(form) {
            dataString = $('#formInput').serialize();
            $.ajax({
                url: "<?=site_url('admin/users/updatedataakses');?>",
                type: "POST",
                data: dataString,
                dataType: 'JSON',
                success: function(data) {
                    swal({
                        title:"Sukses",
                        text: "Update Data Sukses",
                        showConfirmButton: false,
                        type: "success",
                        timer: 2000
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('Error Update Data');
                }
            });
        }
    });
});
</script>
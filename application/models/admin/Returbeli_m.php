<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Returbeli_m extends CI_Model
{
    public $table        = 'v_retur_beli';
    public $column_order = array(null, null, 'retur_beli_no_faktur', 'retur_beli_tanggal', 'suplier_nama',
        'tipe_bayar_nama', 'retur_beli_disc', 'retur_beli_ppn', 'retur_beli_total');
    public $column_search = array('retur_beli_no_faktur', 'retur_beli_tanggal', 'suplier_nama',
        'tipe_bayar_nama', 'retur_beli_disc', 'retur_beli_total');
    public $order = array('retur_beli_tanggal' => 'desc');

    public $table1         = 'v_tmp_retur_beli';
    public $column_order1  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('retur_beli_temp_id' => 'asc');

    public $table2        = 'v_barang';
    public $column_order2 = array(null, null, 'barang_kode', 'barang_nama', 'barang_merk',
        'unit_qty', 'unit_nama', 'unit_hrg_beli');
    public $column_search2 = array('barang_kode', 'barang_nama', 'barang_merk');
    public $order2         = array('barang_kode' => 'asc');

    public $table3         = 'v_unit';
    public $column_order3  = array();
    public $column_search3 = array();
    public $order3         = array('unit_nama' => 'asc');

    public $table4         = 'v_retur_beli_detail';
    public $column_order4  = array(null, null, null, null, null, null, null, null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('retur_beli_detail_id' => 'asc');

    public $table5         = 'ok_suplier';
    public $column_order5  = array(null, null, 'suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $column_search5 = array('suplier_kode', 'suplier_nama', 'suplier_alamat', 'suplier_kota', 'suplier_telp');
    public $order5         = array('suplier_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('retur_beli_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('retur_beli_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        if ($this->input->post('lstTipeBayar', 'true')) {
            $this->db->where('tipe_bayar_id', $this->input->post('lstTipeBayar', 'true'));
        }

        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Suplier
    private function _get_suplier_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_suplier_datatables()
    {
        $this->_get_suplier_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_suplier_filtered()
    {
        $this->_get_suplier_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_suplier_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Barang
    private function _get_barang_datatables_query()
    {
        $this->db->from($this->table2);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_barang_datatables()
    {
        $this->_get_barang_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_barang_filtered()
    {
        $this->_get_barang_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_barang_all()
    {
        $this->db->from($this->table2);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'retur_beli_temp_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'retur_beli_temp_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'barang_id'                   => $this->input->post('barang_id', 'true'),
            'retur_beli_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'retur_beli_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_beli_temp_disc1'       => $this->input->post('disc1', 'true'),
            'retur_beli_temp_disc2'       => $this->input->post('disc2', 'true'),
            'retur_beli_temp_disc3'       => $this->input->post('disc3', 'true'),
            'retur_beli_temp_disc4'       => $this->input->post('disc4', 'true'),
            'retur_beli_temp_ppn'         => $this->input->post('ppn', 'true'),
            'retur_beli_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_beli_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'user_username'               => $username,
            'retur_beli_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_retur_beli_temp', $data);
    }

    public function update_data_item()
    {
        $retur_beli_temp_id = $this->input->post('retur_beli_temp_id', 'true');
        $data               = array(
            'barang_id'                   => $this->input->post('barang_id', 'true'),
            'retur_beli_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'retur_beli_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_beli_temp_disc1'       => $this->input->post('disc1', 'true'),
            'retur_beli_temp_disc2'       => $this->input->post('disc2', 'true'),
            'retur_beli_temp_disc3'       => $this->input->post('disc3', 'true'),
            'retur_beli_temp_disc4'       => $this->input->post('disc4', 'true'),
            'retur_beli_temp_ppn'         => $this->input->post('ppn', 'true'),
            'retur_beli_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_beli_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'retur_beli_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('retur_beli_temp_id', $retur_beli_temp_id);
        $this->db->update('ok_retur_beli_temp', $data);
    }

    public function delete_data_item($id)
    {
        $this->db->where('retur_beli_temp_id', $id);
        $this->db->delete('ok_retur_beli_temp');
    }

    // Unit
    private function _get_unit_datatables_query($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        $i = 0;
        foreach ($this->column_search3 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search3) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order3[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order3)) {
            $order = $this->order3;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_datatables($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_filtered($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_all($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        return $this->db->count_all_results();
    }

    public function insert_data_retur_beli()
    {
        $username     = $this->session->userdata('username');
        $kode_suplier = $this->input->post('kode_suplier', 'true');
        if ($kode_suplier == '') {
            $response = ['status' => 'kosong'];
            echo json_encode($response);
        } else {
            $tipebayar  = $this->input->post('lstTipeBayar', 'true');
            $jenisbayar = $this->db->get_where('ok_tipe_bayar', array('tipe_bayar_id' => $tipebayar))->row();
            $tipe_set   = $jenisbayar->tipe_bayar_set;
            if ($tipe_set == 'D') {
                $sisa_hutang = 0;
            } else {
                $sisa_hutang = intval(str_replace(",", "", $this->input->post('bayar_total', 'true')));
            }

            $data = array(
                'retur_beli_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
                'retur_beli_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
                'suplier_id'             => $this->input->post('suplier_id', 'true'),
                'tipe_bayar_id'          => $this->input->post('lstTipeBayar', 'true'),
                'retur_beli_subtotal'    => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
                'retur_beli_disc'        => intval(str_replace(",", "", $this->input->post('diskon', 'true'))),
                'retur_beli_total'       => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
                'retur_beli_sisa_hutang' => $sisa_hutang,
                'user_username'          => $username,
                'retur_beli_checker'     => trim(stripHTMLtags($this->input->post('checker', 'true'))),
                'retur_beli_penerima'    => trim(stripHTMLtags($this->input->post('penerima', 'true'))),
                'retur_beli_update'      => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_retur_beli', $data);
            $retur_beli_id = $this->db->insert_id();

            // Simpan Detail Barang
            $listTemp = $this->db->get_where('ok_retur_beli_temp', array('user_username' => $username))->result();
            foreach ($listTemp as $r) {
                $dataItem = array(
                    'retur_beli_id'                 => $retur_beli_id,
                    'barang_id'                     => $r->barang_id,
                    'retur_beli_detail_qty'         => $r->retur_beli_temp_qty,
                    'unit_id'                       => $r->unit_id,
                    'retur_beli_detail_harga'       => $r->retur_beli_temp_harga,
                    'retur_beli_detail_disc1'       => $r->retur_beli_temp_disc1,
                    'retur_beli_detail_disc2'       => $r->retur_beli_temp_disc2,
                    'retur_beli_detail_disc3'       => $r->retur_beli_temp_disc3,
                    'retur_beli_detail_disc4'       => $r->retur_beli_temp_disc4,
                    'retur_beli_detail_ppn'         => $r->retur_beli_temp_ppn,
                    'retur_beli_detail_harga_pokok' => $r->retur_beli_temp_harga_pokok,
                    'retur_beli_detail_total'       => $r->retur_beli_temp_total,
                    'retur_beli_detail_update'      => date('Y-m-d H:i:s'),
                );

                $this->db->insert('ok_retur_beli_detail', $dataItem);

                // Check Stok Unit
                $barang_id = $r->barang_id;
                $unit_id   = $r->unit_id;
                $dataUnit  = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
                $Multiple  = $dataUnit->unit_multiple;
                // Data Unit Utama
                $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
                $unit_id_utama = $dataUnitUtama->unit_id;
                $StokUtama     = $dataUnitUtama->unit_qty;
                if ($unit_id == $unit_id_utama) {
                    $Stok = ($StokUtama - $r->retur_beli_temp_qty);
                } else {
                    $Stok = ($StokUtama - ($r->retur_beli_temp_qty * $Multiple));
                }

                // Update Stok Utama
                $dataStok = array(
                    'unit_qty'    => $Stok,
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $unit_id_utama);
                $this->db->update('ok_unit', $dataStok);

                // Konversi ke Unit Lain selain Utama
                $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
                foreach ($listUnit as $u) {
                    $dataStokUnit = array(
                        'unit_qty'    => ($Stok / $u->unit_multiple),
                        'unit_update' => date('Y-m-d H:i:s'),
                    );

                    $this->db->where('unit_id', $u->unit_id);
                    $this->db->update('ok_unit', $dataStokUnit);
                }
            }

            // Hapus Temp by Username dan Tanggal
            $username = $this->session->userdata('username');
            $this->db->where('user_username', $username);
            $this->db->delete('ok_retur_beli_temp');

            $response = ['status' => 'success'];
            echo json_encode($response);
        }
    }

    // Detail Edit
    private function _get_detail_datatables_query($retur_beli_id)
    {
        $this->db->from($this->table4);
        $this->db->where('retur_beli_id', $retur_beli_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($retur_beli_id)
    {
        $this->_get_detail_datatables_query($retur_beli_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($retur_beli_id)
    {
        $this->_get_detail_datatables_query($retur_beli_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($retur_beli_id)
    {
        $this->db->from($this->table4);
        $this->db->where('retur_beli_id', $retur_beli_id);

        return $this->db->count_all_results();
    }

    public function insert_data_detail()
    {
        $data = array(
            'retur_beli_id'                 => $this->input->post('retur_beli_id', 'true'),
            'barang_id'                     => $this->input->post('barang_id', 'true'),
            'retur_beli_detail_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                       => $this->input->post('unit_id', 'true'),
            'retur_beli_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_beli_detail_disc1'       => $this->input->post('disc1', 'true'),
            'retur_beli_detail_disc2'       => $this->input->post('disc2', 'true'),
            'retur_beli_detail_disc3'       => $this->input->post('disc3', 'true'),
            'retur_beli_detail_disc4'       => $this->input->post('disc4', 'true'),
            'retur_beli_detail_ppn'         => $this->input->post('ppn', 'true'),
            'retur_beli_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_beli_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'retur_beli_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_retur_beli_detail', $data);

        // Update Stok
        $barang_id     = $this->input->post('barang_id', 'true');
        $unit_id       = $this->input->post('unit_id', 'true');
        $qty           = floatval(str_replace(",", "", $this->input->post('qty', 'true')));
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama - $qty);
        } else {
            $Stok = (($qty * $Multiple) - $StokUtama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function update_data_detail()
    {
        $retur_beli_detail_id = $this->input->post('retur_beli_detail_id', 'true');
        $barang_id            = $this->input->post('barang_id', 'true');
        $unit_id              = $this->input->post('unit_id', 'true');
        $qty                  = floatval(str_replace(",", "", $this->input->post('qty', 'true')));
        $qty_lama             = $this->input->post('qty_lama', 'true');
        $data                 = array(
            'retur_beli_detail_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                       => $this->input->post('unit_id', 'true'),
            'retur_beli_detail_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_beli_detail_disc1'       => $this->input->post('disc1', 'true'),
            'retur_beli_detail_disc2'       => $this->input->post('disc2', 'true'),
            'retur_beli_detail_disc3'       => $this->input->post('disc3', 'true'),
            'retur_beli_detail_disc4'       => $this->input->post('disc4', 'true'),
            'retur_beli_detail_ppn'         => $this->input->post('ppn', 'true'),
            'retur_beli_detail_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_beli_detail_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'retur_beli_detail_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('retur_beli_detail_id', $retur_beli_detail_id);
        $this->db->update('ok_retur_beli_detail', $data);

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = (($StokUtama + $qty_lama) - $qty); // Stok Utama - Qty Lama + Qty Baru
        } else {
            $MultipleLama = ($qty_lama * $Multiple);
            $Stok         = (($StokUtama + $MultipleLama) - ($qty * $Multiple));
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }
    }

    public function delete_data_detail($id)
    {
        // Hitung Stok dulu
        $dataLama  = $this->db->get_where('ok_retur_beli_detail', array('retur_beli_detail_id' => $id))->row();
        $barang_id = $dataLama->barang_id;
        $unit_id   = $dataLama->unit_id;
        $qty       = $dataLama->retur_beli_detail_qty;

        // Update Stok
        $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
        $Multiple      = $dataUnit->unit_multiple;
        $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
        $unit_id_utama = $dataUnitUtama->unit_id;
        $StokUtama     = $dataUnitUtama->unit_qty;
        if ($unit_id == $unit_id_utama) {
            $Stok = ($StokUtama + $qty); // Stok Utama - Qty
        } else {
            $MultipleLama = ($qty * $Multiple);
            $Stok         = ($StokUtama + $MultipleLama);
        }

        // Update Stok Utama
        $dataStok = array(
            'unit_qty'    => $Stok,
            'unit_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id_utama);
        $this->db->update('ok_unit', $dataStok);

        // Konversi ke Unit Lain selain Utama
        $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
        foreach ($listUnit as $u) {
            $dataStokUnit = array(
                'unit_qty'    => ($Stok / $u->unit_multiple),
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $u->unit_id);
            $this->db->update('ok_unit', $dataStokUnit);
        }

        // Hapus Data
        $this->db->where('retur_beli_detail_id', $id);
        $this->db->delete('ok_retur_beli_detail');
    }

    public function update_data_retur_beli()
    {
        $retur_beli_id = $this->input->post('retur_beli_id', 'true');
        $username      = $this->session->userdata('username');
        $dibayar       = $this->input->post('dibayar', 'true');
        $tipebayar     = $this->input->post('lstTipeBayar', 'true');
        $jenisbayar    = $this->db->get_where('ok_tipe_bayar', array('tipe_bayar_id' => $tipebayar))->row();
        $tipe_set      = $jenisbayar->tipe_bayar_set;
        if ($tipe_set == 'D') {
            $sisa_hutang = 0;
        } else {
            $sisa_hutang = intval(str_replace(",", "", $this->input->post('bayar_total', 'true')));
        }

        $data = array(
            'retur_beli_no_faktur'   => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'retur_beli_tanggal'     => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'suplier_id'             => $this->input->post('suplier_id', 'true'),
            'tipe_bayar_id'          => $this->input->post('lstTipeBayar', 'true'),
            'retur_beli_subtotal'    => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
            'retur_beli_disc'        => intval(str_replace(",", "", $this->input->post('diskon', 'true'))),
            'retur_beli_total'       => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
            'retur_beli_sisa_hutang' => $sisa_hutang,
            'user_username'          => $username,
            'retur_beli_checker'     => trim(stripHTMLtags($this->input->post('checker', 'true'))),
            'retur_beli_penerima'    => trim(stripHTMLtags($this->input->post('penerima', 'true'))),
            'retur_beli_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('retur_beli_id', $retur_beli_id);
        $this->db->update('ok_retur_beli', $data);
    }

    public function insert_data_suplier()
    {
        $data = array(
            'suplier_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'suplier_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'suplier_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_suplier', 'true')))),
            'suplier_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'suplier_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'suplier_email'  => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'suplier_kontak' => strtoupper(trim(stripHTMLtags($this->input->post('kontak', 'true')))),
            'suplier_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_suplier', $data);
    }

    public function delete_data_retur_beli($id)
    {
        $listDetail = $this->db->order_by('retur_beli_detail_id', 'asc')->get_where('ok_retur_beli_detail', array('retur_beli_id' => $id))->result();
        foreach ($listDetail as $d) {
            // Hitung Stok dulu
            $retur_beli_detail_id = $d->retur_beli_detail_id;
            $dataLama             = $this->db->get_where('ok_retur_beli_detail', array('retur_beli_detail_id' => $retur_beli_detail_id))->row();
            $barang_id            = $dataLama->barang_id;
            $unit_id              = $dataLama->unit_id;
            $qty                  = $dataLama->retur_beli_detail_qty;

            // Update Stok
            $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple      = $dataUnit->unit_multiple;
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            if ($unit_id == $unit_id_utama) {
                $Stok = ($StokUtama + $qty); // Stok Utama - Qty
            } else {
                $MultipleLama = ($qty * $Multiple);
                $Stok         = ($StokUtama + $MultipleLama);
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'    => $Stok,
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'    => ($Stok / $u->unit_multiple),
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Data
        $this->db->where('retur_beli_id', $id);
        $this->db->delete('ok_retur_beli');
    }
}
/* Location: ./application/model/admin/Returbeli_m.php */

<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Returjual_m extends CI_Model
{
    public $table        = 'v_retur_jual';
    public $column_order = array(null, null, 'retur_jual_no_faktur', 'retur_jual_tanggal', 'pelanggan_nama',
        'tipe_bayar_nama', 'retur_jual_total', 'user_username');
    public $column_search = array('retur_jual_no_faktur', 'retur_jual_tanggal', 'pelanggan_nama',
        'tipe_bayar_nama', 'retur_jual_total');
    public $order = array('retur_jual_id' => 'desc');

    public $table1         = 'v_tmp_retur_jual';
    public $column_order1  = array(null, null, null, null, null, null, null, null, null);
    public $column_search1 = array();
    public $order1         = array('retur_jual_temp_id' => 'asc');

    public $table2        = 'v_barang';
    public $column_order2 = array(null, null, 'barang_kode', 'barang_nama', 'barang_merk',
        'unit_qty', 'unit_nama', 'unit_hrg_jual');
    public $column_search2 = array('barang_kode', 'barang_nama', 'barang_merk');
    public $order2         = array('barang_kode' => 'asc');

    public $table3         = 'v_unit';
    public $column_order3  = array();
    public $column_search3 = array();
    public $order3         = array('unit_nama' => 'asc');

    public $table4         = 'v_retur_jual_detail';
    public $column_order4  = array(null, null, null, null, null, null, null, null);
    public $column_search4 = array();
    public $order4         = array('retur_jual_detail_id' => 'asc');

    public $table5        = 'ok_pelanggan';
    public $column_order5 = array(null, null, 'pelanggan_kode', 'pelanggan_nama', 'pelanggan_alamat', 'pelanggan_kota',
        'pelanggan_telp');
    public $column_search5 = array('pelanggan_kode', 'pelanggan_nama', 'pelanggan_alamat', 'pelanggan_kota', 'pelanggan_telp');
    public $order5         = array('pelanggan_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('tgl_dari', 'true')) {
            $tgl_dari = date('Y-m-d', strtotime($this->input->post('tgl_dari', 'true')));
            $this->db->where('retur_jual_tanggal >=', $tgl_dari);
        }
        if ($this->input->post('tgl_sampai', 'true')) {
            $tgl_sampai = date('Y-m-d', strtotime($this->input->post('tgl_sampai', 'true')));
            $this->db->where('retur_jual_tanggal <=', $tgl_sampai);
        }
        if ($this->input->post('lstPelanggan', 'true')) {
            $this->db->where('pelanggan_id', $this->input->post('lstPelanggan', 'true'));
        }
        if ($this->input->post('lstTipeBayar', 'true')) {
            $this->db->where('tipe_bayar_id', $this->input->post('lstTipeBayar', 'true'));
        }
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // Temp
    private function _get_tmp_datatables_query()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_tmp_datatables()
    {
        $this->_get_tmp_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_tmp_filtered()
    {
        $this->_get_tmp_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_tmp_all()
    {
        $username = $this->session->userdata('username');
        $this->db->from($this->table1);
        $this->db->where('user_username', $username);

        return $this->db->count_all_results();
    }

    // Suplier
    private function _get_pelanggan_datatables_query()
    {
        $this->db->from($this->table5);

        $i = 0;
        foreach ($this->column_search5 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search5) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order5[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order5)) {
            $order = $this->order5;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_pelanggan_datatables()
    {
        $this->_get_pelanggan_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_pelanggan_filtered()
    {
        $this->_get_pelanggan_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_pelanggan_all()
    {
        $this->db->from($this->table5);
        return $this->db->count_all_results();
    }

    // Barang
    private function _get_barang_datatables_query()
    {
        $this->db->from($this->table2);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_barang_datatables()
    {
        $this->_get_barang_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_barang_filtered()
    {
        $this->_get_barang_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_barang_all()
    {
        $this->db->from($this->table2);

        return $this->db->count_all_results();
    }

    public function insert_data_item()
    {
        $username = $this->session->userdata('username');
        $data     = array(
            'retur_jual_temp_tanggal'     => date('Y-m-d'),
            'barang_id'                   => $this->input->post('barang_id', 'true'),
            'retur_jual_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'retur_jual_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_jual_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_jual_temp_disc'        => $this->input->post('disc', 'true'),
            'retur_jual_temp_disc_rp'     => $this->input->post('disc_rupiah', 'true'),
            'retur_jual_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'user_username'               => $username,
            'retur_jual_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_retur_jual_temp', $data);
    }

    public function update_data_item()
    {
        $retur_jual_temp_id = $this->input->post('retur_jual_temp_id', 'true');
        $data               = array(
            'barang_id'                   => $this->input->post('barang_id', 'true'),
            'retur_jual_temp_qty'         => floatval(str_replace(",", "", $this->input->post('qty', 'true'))),
            'unit_id'                     => $this->input->post('unit_id', 'true'),
            'retur_jual_temp_harga_pokok' => $this->input->post('harga_pokok', 'true'),
            'retur_jual_temp_harga'       => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
            'retur_jual_temp_disc'        => $this->input->post('disc', 'true'),
            'retur_jual_temp_disc_rp'     => $this->input->post('disc_rupiah', 'true'),
            'retur_jual_temp_total'       => intval(str_replace(",", "", $this->input->post('total', 'true'))),
            'retur_jual_temp_update'      => date('Y-m-d H:i:s'),
        );

        $this->db->where('retur_jual_temp_id', $retur_jual_temp_id);
        $this->db->update('ok_retur_jual_temp', $data);
    }

    public function delete_data_item($id)
    {
        $this->db->where('retur_jual_temp_id', $id);
        $this->db->delete('ok_retur_jual_temp');
    }

    // Unit
    private function _get_unit_datatables_query($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        $i = 0;
        foreach ($this->column_search3 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search3) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order3[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order3)) {
            $order = $this->order3;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_datatables($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_filtered($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_all($barang_id)
    {
        $this->db->from($this->table3);
        $this->db->where('barang_id', $barang_id);

        return $this->db->count_all_results();
    }

    public function getNoFaktur()
    {
        $this->db->select('COUNT(retur_jual_id) as total', false);
        $this->db->where('YEAR(retur_jual_tanggal)', date('Y'));
        $this->db->where('MONTH(retur_jual_tanggal)', date('m'));
        $query = $this->db->get('ok_retur_jual');
        if ($query->num_rows() != 0) {
            $data = $query->row();
            $kode = intval($data->total) + 1;
        } else {
            $kode = 1;
        }

        $thn    = substr(date('Y'), -2);
        $bln    = date('m');
        $noUrut = str_pad($kode, 6, "0", STR_PAD_LEFT);
        $noJual = $thn . '.' . $bln . '.' . $noUrut;
        return $noJual;
    }

    public function insert_data_retur_jual()
    {
        $username = $this->session->userdata('username');
        $tipe_set = $this->input->post('tipe_set', 'true');
        if ($tipe_set == 'D') {
            $sisa_piutang = 0;
        } else {
            $sisa_piutang = intval(str_replace(",", "", $this->input->post('bayar_total', 'true')));
        }
        
        $noFaktur = $this->getNoFaktur();
        $netto    = intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true')));
        $data     = array(
            'retur_jual_no_faktur'    => $noFaktur,
            'retur_jual_tanggal'      => date('Y-m-d'),
            'pelanggan_id'            => $this->input->post('pelanggan_id', 'true'),
            'tipe_bayar_id'           => $this->input->post('lstTipeBayar', 'true'),
            // 'retur_jual_ppn'          => str_replace(",", "", $this->input->post('bayar_ppn', 'true')),
            // 'retur_jual_ppn_rp'       => $this->input->post('ppn_rupiah', 'true'),
            // 'retur_jual_diskon'       => intval(str_replace(",", "", $this->input->post('diskon', 'true'))),
            'retur_jual_bruto'        => intval(str_replace(",", "", $this->input->post('bayar_subtotal', 'true'))),
            'retur_jual_netto'        => $netto,
            'retur_jual_total'        => intval(str_replace(",", "", $this->input->post('bayar_total', 'true'))),
            // 'retur_jual_bayar'        => intval(str_replace(",", "", $this->input->post('bayar', 'true'))),
            // 'retur_jual_kembali'      => intval(str_replace(",", "", $this->input->post('kembali', 'true'))),
            'retur_jual_sisa_piutang' => $sisa_piutang,
            'user_username'           => $username,
            'retur_jual_update'       => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_retur_jual', $data);
        $retur_jual_id = $this->db->insert_id();
        // Simpan Detail Barang
        $listTemp = $this->db->get_where('ok_retur_jual_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $dataItem = array(
                'retur_jual_id'                 => $retur_jual_id,
                'barang_id'                     => $r->barang_id,
                'retur_jual_detail_qty'         => $r->retur_jual_temp_qty,
                'unit_id'                       => $r->unit_id,
                'retur_jual_detail_harga_pokok' => $r->retur_jual_temp_harga_pokok,
                'retur_jual_detail_harga'       => $r->retur_jual_temp_harga,
                'retur_jual_detail_disc'        => $r->retur_jual_temp_disc,
                'retur_jual_detail_disc_rp'     => $r->retur_jual_temp_disc_rp,
                'retur_jual_detail_total'       => $r->retur_jual_temp_total,
                'retur_jual_detail_update'      => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_retur_jual_detail', $dataItem);

            // Check Stok Unit
            $barang_id = $r->barang_id;
            $unit_id   = $r->unit_id;
            $dataUnit  = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple  = $dataUnit->unit_multiple;
            // Data Unit Utama
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            $JualUtama     = $dataUnitUtama->unit_jml_jual;
            if ($unit_id == $unit_id_utama) {
                $terjual = $r->retur_jual_temp_qty;
                $Stok    = ($StokUtama + $r->retur_jual_temp_qty);
            } else {
                $terjual = ($r->retur_jual_temp_qty * $Multiple);
                $Stok    = ($StokUtama + ($r->retur_jual_temp_qty * $Multiple));
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'      => $Stok,
                'unit_jml_jual' => ($JualUtama + $terjual),
                'unit_update'   => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'      => ($Stok / $u->unit_multiple),
                    'unit_jml_jual' => ($u->unit_jml_jual + $terjual),
                    'unit_update'   => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_retur_jual_temp');
        $response = ['status' => 'success', 'id' => $retur_jual_id];
        echo json_encode($response);
    }

    public function cetaknotabayar($retur_jual_id)
    {
        $dataToko      = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $dataPenjualan = $this->db->get_where('v_retur_jual', array('retur_jual_id' => $retur_jual_id))->row();
        $listItem      = $this->db->get_where('v_retur_jual_detail', array('retur_jual_id' => $retur_jual_id))->result();
        $tmpdir        = sys_get_temp_dir();
        $file          = tempnam($tmpdir, 'cetak');
        $handle        = fopen($file, 'w');
        $bold0         = Chr(27) . Chr(69);
        $bold1         = Chr(27) . Chr(70);
        $initialized   = chr(27) . chr(64);
        $leftMargin    = chr(27) . chr(108) . chr(1);
        $condensed     = Chr(27) . Chr(33) . Chr(4);
        $draft         = Chr(27) . Chr(120);
        $Data          = $initialized;
        $Data .= $leftMargin;
        $Data .= $draft;
        $NamaToko      = trim($dataToko->contact_name);
        $AlamatToko    = trim($dataToko->contact_address);
        $TelpToko      = trim($dataToko->contact_phone);
        $NoOrder       = trim($dataPenjualan->retur_jual_no_faktur);
        $NamaPelanggan = trim($dataPenjualan->pelanggan_nama);
        $Tanggal       = date('d-m-Y', strtotime($dataPenjualan->retur_jual_tanggal));
        $Kasir         = trim($dataPenjualan->user_username);
        $Data .= $this->addHeader($NamaToko, $AlamatToko, $TelpToko, $NoOrder, $Tanggal, $NamaPelanggan, $Kasir);
        foreach ($listItem as $r) {
            $NamaBarang = trim($r->barang_nama);
            $Harga      = number_format($r->retur_jual_detail_harga, 0, '', ',');
            $Qty        = number_format($r->retur_jual_detail_qty, 0, '', ',');
            $Unit       = $r->unit_nama;
            $Subtotal   = number_format($r->retur_jual_detail_total, 0, '', ',');
            $Data .= $this->addItem($NamaBarang, $Harga, $Qty, $Unit, $Subtotal);
        }
        $SubTotal = number_format($dataPenjualan->retur_jual_bruto, 0, '', ',');
        $Diskon   = number_format($dataPenjualan->retur_jual_diskon, 0, '', ',');
        $Pajak    = number_format($dataPenjualan->retur_jual_ppn, 2, '.', ',');
        $Total    = number_format($dataPenjualan->retur_jual_total, 0, '', ',');
        $Data .= $this->addFooter($SubTotal, $Diskon, $Pajak, $Total);
        fwrite($handle, $Data);
        fclose($handle);
        $printer        = $this->db->get_where('ok_printer', array('printer_tipe' => 'Nota'))->row();
        $lokasi_printer = $printer->printer_lokasi;
        // $time           = time();
        // $filename       = "Nota_" . $time;
        // $pdfFilePath    = FCPATH . "/download/$filename.txt";
        // copy($file, $pdfFilePath);
        // copy($file, $lokasi_printer);
        // unlink($file);

    }

    public function addHeader($NamaToko, $AlamatToko, $TelpToko, $NoOrder, $Tanggal, $NamaPelanggan, $Kasir)
    {
        $returnValue  = "";
        $limitHeader  = 20;
        $txtToko      = "";
        $txtAlamat    = "";
        $txtTelp      = "";
        $txtPelanggan = "";

        if (strlen($NamaToko) <= $limitHeader) {
            $txtToko = str_pad($NamaToko, $limitHeader);
        } else {
            $txtToko = substr($NamaToko, 0, $limitHeader);
        }

        if (strlen($AlamatToko) <= $limitHeader) {
            $txtAlamat = str_pad($AlamatToko, $limitHeader);
        } else {
            $txtAlamat = substr($AlamatToko, 0, $limitHeader);
        }

        if (strlen($TelpToko) <= $limitHeader) {
            $txtTelp = str_pad($TelpToko, $limitHeader);
        } else {
            $txtTelp = substr($TelpToko, 0, $limitHeader);
        }
        if (strlen($NamaPelanggan) <= $limitHeader) {
            $txtPelanggan = str_pad($NamaPelanggan, $limitHeader);
        } else {
            $txtPelanggan = substr($NamaPelanggan, 0, $limitHeader);
        }
        $returnValue .= "           JATI BANGUN          " . chr(10);
        $returnValue .= " Jl. AKBP Agil Kusumadya No.110 " . chr(10);
        $returnValue .= "         (0291) 2911225         " . chr(10);
        $returnValue .= "No        : " . $NoOrder . chr(10);
        $returnValue .= "Tanggal   : " . $Tanggal . chr(10);
        $returnValue .= "Pelanggan : " . $txtPelanggan . chr(10);
        $returnValue .= "Kasir     : " . $Kasir . chr(10);
        $returnValue .= "--------------------------------" . chr(10);
        return $returnValue;
    }

    public function addItem($NamaBarang, $Harga, $Qty, $Unit, $Subtotal)
    {
        // LimitCharacter
        $limitNamaBarang = 11;
        $limitHarga      = 7;
        $limitQty        = 3;
        // $limitDisc     = 4;
        $limitSubtotal = 9;
        // Variabel
        $txtNamaBarang = "";
        $txtHarga      = 0;
        $txtQty        = 0;
        // $txtDisc     = 0.00;
        $txtSubtotal = 0;

        // Nama Menu
        if (strlen($NamaBarang) <= $limitNamaBarang) {
            $txtNamaBarang = str_pad($NamaBarang, $limitNamaBarang);
        } else {
            $txtNamaBarang = substr($NamaBarang, 0, $limitNamaBarang);
        }

        // Harga
        if (strlen($Harga) <= $limitHarga) {
            $txtHarga = str_pad($Harga, $limitHarga, " ", STR_PAD_LEFT);
        } else {
            $txtHarga = substr($Harga, 0, $limitHarga);
        }

        // Qty
        if (strlen($Qty) <= $limitQty) {
            $txtQty = str_pad($Qty, $limitQty, " ", STR_PAD_LEFT);
        } else {
            $txtQty = substr($Qty, 0, $limitQty);
        }

        // Disc
        // if (strlen($Disc) <= $limitDisc) {
        //     $txtDisc = str_pad($Disc, $limitDisc, " ", STR_PAD_LEFT);
        // } else {
        //     $txtDisc = substr($Disc, 0, $limitDisc);
        // }

        // Subtotal
        if (strlen($Subtotal) <= $limitSubtotal) {
            $txtSubtotal = str_pad($Subtotal, $limitSubtotal, " ", STR_PAD_LEFT);
        } else {
            $txtSubtotal = substr($Subtotal, 0, $limitSubtotal);
        }

        $returnValue = "" . $txtNamaBarang . " " . $txtHarga . " " . $txtQty . "" . $txtSubtotal . chr(10);
        return $returnValue;
    }

    public function addFooter($SubTotal, $Diskon, $Pajak, $Total)
    {

        // LimitCharacter
        $limitNominal = 9;
        // Variabel
        $txtSubTotal = 0;
        $txtDiskon   = 0;
        $txtPajak    = 0;
        $txtTotal    = 0;

        // Sub Total
        if (strlen($SubTotal) <= $limitNominal) {
            $txtSubTotal = str_pad($SubTotal, $limitNominal, " ", STR_PAD_LEFT);
        } else {
            $txtSubTotal = substr($SubTotal, 0, $limitNominal);
        }

        // Diskon
        if (strlen($Diskon) <= $limitNominal) {
            $txtDiskon = str_pad($Diskon, $limitNominal, " ", STR_PAD_LEFT);
        } else {
            $txtDiskon = substr($Diskon, 0, $limitNominal);
        }

        // Pajak
        if (strlen($Pajak) <= $limitNominal) {
            $txtPajak = str_pad($Pajak, $limitNominal, " ", STR_PAD_LEFT);
        } else {
            $txtPajak = substr($Pajak, 0, $limitNominal);
        }

        // Total
        if (strlen($Total) <= $limitNominal) {
            $txtTotal = str_pad($Total, $limitNominal, " ", STR_PAD_LEFT);
        } else {
            $txtTotal = substr($Total, 0, $limitNominal);
        }

        $returnValue = "" . chr(10);
        $returnValue .= "           Sub Total : " . $txtSubTotal . chr(10);
        $returnValue .= "               TOTAL : " . $txtTotal . chr(10);
        $returnValue .= "--------------------------------" . chr(10);
        $returnValue .= "Terima Kasih atas kunjungan Anda" . chr(10);
        $returnValue .= "" . chr(10);
        $returnValue .= "" . chr(10);
        $returnValue .= "" . chr(10);
        return $returnValue;
    }

    // Detail Edit
    private function _get_detail_datatables_query($retur_jual_id)
    {
        $this->db->from($this->table4);
        $this->db->where('retur_jual_id', $retur_jual_id);

        $i = 0;
        foreach ($this->column_search4 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search4) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order4[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order4)) {
            $order = $this->order4;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_detail_datatables($retur_jual_id)
    {
        $this->_get_detail_datatables_query($retur_jual_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_detail_filtered($retur_jual_id)
    {
        $this->_get_detail_datatables_query($retur_jual_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_detail_all($retur_jual_id)
    {
        $this->db->from($this->table4);
        $this->db->where('retur_jual_id', $retur_jual_id);

        return $this->db->count_all_results();
    }

    // public function insert_data_detail()
    // {
    //     $data = array(
    //         'retur_jual_id'            => $this->input->post('retur_jual_id', 'true'),
    //         'barang_id'                => $this->input->post('barang_id', 'true'),
    //         'retur_jual_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
    //         'unit_id'                  => $this->input->post('unit_id', 'true'),
    //         'retur_jual_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
    //         'retur_jual_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
    //         'retur_jual_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
    //         'retur_jual_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
    //         'retur_jual_detail_update' => date('Y-m-d H:i:s'),
    //     );

    //     $this->db->insert('ok_retur_jual_detail', $data);

    //     // Update Stok
    //     $barang_id     = $this->input->post('barang_id', 'true');
    //     $unit_id       = $this->input->post('unit_id', 'true');
    //     $qty           = intval(str_replace(",", "", $this->input->post('qty', 'true')));
    //     $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
    //     $Multiple      = $dataUnit->unit_multiple;
    //     $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
    //     $unit_id_utama = $dataUnitUtama->unit_id;
    //     $StokUtama     = $dataUnitUtama->unit_qty;
    //     if ($unit_id == $unit_id_utama) {
    //         $Stok = ($StokUtama + $qty);
    //     } else {
    //         $Stok = (($qty * $Multiple) + $StokUtama);
    //     }

    //     // Update Stok Utama
    //     $dataStok = array(
    //         'unit_qty'    => $Stok,
    //         'unit_update' => date('Y-m-d H:i:s'),
    //     );

    //     $this->db->where('unit_id', $unit_id_utama);
    //     $this->db->update('ok_unit', $dataStok);

    //     // Konversi ke Unit Lain selain Utama
    //     $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
    //     foreach ($listUnit as $u) {
    //         $dataStokUnit = array(
    //             'unit_qty'    => ($Stok / $u->unit_multiple),
    //             'unit_update' => date('Y-m-d H:i:s'),
    //         );

    //         $this->db->where('unit_id', $u->unit_id);
    //         $this->db->update('ok_unit', $dataStokUnit);
    //     }
    // }

    // public function update_data_detail()
    // {
    //     $retur_jual_detail_id = $this->input->post('retur_jual_detail_id', 'true');
    //     $barang_id            = $this->input->post('barang_id', 'true');
    //     $unit_id              = $this->input->post('unit_id', 'true');
    //     $qty                  = intval(str_replace(",", "", $this->input->post('qty', 'true')));
    //     $qty_lama             = $this->input->post('qty_lama', 'true');
    //     $data                 = array(
    //         'retur_jual_detail_qty'    => intval(str_replace(",", "", $this->input->post('qty', 'true'))),
    //         'unit_id'                  => $this->input->post('unit_id', 'true'),
    //         'retur_jual_detail_harga'  => intval(str_replace(",", "", $this->input->post('harga', 'true'))),
    //         'retur_jual_detail_ppn'    => str_replace(",", "", $this->input->post('ppn', 'true')),
    //         'retur_jual_detail_ppn_rp' => $this->input->post('ppn_rupiah', 'true'),
    //         'retur_jual_detail_total'  => intval(str_replace(",", "", $this->input->post('total', 'true'))),
    //         'retur_jual_detail_update' => date('Y-m-d H:i:s'),
    //     );

    //     $this->db->where('retur_jual_detail_id', $retur_jual_detail_id);
    //     $this->db->update('ok_retur_jual_detail', $data);

    //     // Update Stok
    //     $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
    //     $Multiple      = $dataUnit->unit_multiple;
    //     $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
    //     $unit_id_utama = $dataUnitUtama->unit_id;
    //     $StokUtama     = $dataUnitUtama->unit_qty;
    //     if ($unit_id == $unit_id_utama) {
    //         $Stok = (($StokUtama - $qty_lama) + $qty); // Stok Utama - Qty Lama + Qty Baru
    //     } else {
    //         $MultipleLama = ($qty_lama * $Multiple);
    //         $Stok         = (($StokUtama - $MultipleLama) + ($qty * $Multiple));
    //     }

    //     // Update Stok Utama
    //     $dataStok = array(
    //         'unit_qty'    => $Stok,
    //         'unit_update' => date('Y-m-d H:i:s'),
    //     );

    //     $this->db->where('unit_id', $unit_id_utama);
    //     $this->db->update('ok_unit', $dataStok);

    //     // Konversi ke Unit Lain selain Utama
    //     $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
    //     foreach ($listUnit as $u) {
    //         $dataStokUnit = array(
    //             'unit_qty'    => ($Stok / $u->unit_multiple),
    //             'unit_update' => date('Y-m-d H:i:s'),
    //         );

    //         $this->db->where('unit_id', $u->unit_id);
    //         $this->db->update('ok_unit', $dataStokUnit);
    //     }
    // }

    // public function delete_data_detail($id)
    // {
    //     // Hitung Stok dulu
    //     $dataLama  = $this->db->get_where('ok_retur_jual_detail', array('retur_jual_detail_id' => $id))->row();
    //     $barang_id = $dataLama->barang_id;
    //     $unit_id   = $dataLama->unit_id;
    //     $qty       = $dataLama->retur_jual_detail_qty;

    //     // Update Stok
    //     $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
    //     $Multiple      = $dataUnit->unit_multiple;
    //     $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
    //     $unit_id_utama = $dataUnitUtama->unit_id;
    //     $StokUtama     = $dataUnitUtama->unit_qty;
    //     if ($unit_id == $unit_id_utama) {
    //         $Stok = ($StokUtama - $qty); // Stok Utama - Qty
    //     } else {
    //         $MultipleLama = ($qty * $Multiple);
    //         $Stok         = ($StokUtama - $MultipleLama);
    //     }

    //     // Update Stok Utama
    //     $dataStok = array(
    //         'unit_qty'    => $Stok,
    //         'unit_update' => date('Y-m-d H:i:s'),
    //     );

    //     $this->db->where('unit_id', $unit_id_utama);
    //     $this->db->update('ok_unit', $dataStok);

    //     // Konversi ke Unit Lain selain Utama
    //     $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
    //     foreach ($listUnit as $u) {
    //         $dataStokUnit = array(
    //             'unit_qty'    => ($Stok / $u->unit_multiple),
    //             'unit_update' => date('Y-m-d H:i:s'),
    //         );

    //         $this->db->where('unit_id', $u->unit_id);
    //         $this->db->update('ok_unit', $dataStokUnit);
    //     }

    //     // Hapus Data
    //     $this->db->where('retur_jual_detail_id', $id);
    //     $this->db->delete('ok_retur_jual_detail');
    // }

    public function update_data_retur_jual()
    {
        $retur_jual_id = $this->input->post('retur_jual_id', 'true');
        $data          = array(
            'retur_jual_no_faktur' => trim(stripHTMLtags($this->input->post('no_faktur', 'true'))),
            'retur_jual_tanggal'   => date('Y-m-d', strtotime($this->input->post('tanggal', 'true'))),
            'pelanggan_id'         => $this->input->post('pelanggan_id', 'true'),
            'retur_jual_total'     => intval(str_replace(",", "", $this->input->post('totalretur_jual', 'true'))),
            'user_username'        => $username,
            'retur_jual_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('retur_jual_id', $retur_jual_id);
        $this->db->update('ok_retur_jual', $data);
    }

    public function insert_data_pelanggan()
    {
        $data = array(
            'pelanggan_kode'   => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'pelanggan_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'pelanggan_alamat' => strtoupper(trim(stripHTMLtags($this->input->post('alamat_pelanggan', 'true')))),
            'pelanggan_kota'   => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'pelanggan_telp'   => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'pelanggan_disc'   => $this->input->post('disc_plg', 'true'),
            'pelanggan_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_pelanggan', $data);
    }

    public function delete_data_retur_jual($id)
    {
        $listDetail = $this->db->order_by('retur_jual_detail_id', 'asc')->get_where('ok_retur_jual_detail', array('retur_jual_id' => $id))->result();
        foreach ($listDetail as $d) {
            // Hitung Stok dulu
            $retur_jual_detail_id = $d->retur_jual_detail_id;
            $dataLama             = $this->db->get_where('ok_retur_jual_detail', array('retur_jual_detail_id' => $retur_jual_detail_id))->row();
            $barang_id            = $dataLama->barang_id;
            $unit_id              = $dataLama->unit_id;
            $qty                  = $dataLama->retur_jual_detail_qty;

            // Update Stok
            $dataUnit      = $this->db->get_where('ok_unit', array('unit_id' => $unit_id))->row();
            $Multiple      = $dataUnit->unit_multiple;
            $dataUnitUtama = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->row();
            $unit_id_utama = $dataUnitUtama->unit_id;
            $StokUtama     = $dataUnitUtama->unit_qty;
            if ($unit_id == $unit_id_utama) {
                $Stok = ($StokUtama - $qty); // Stok Utama - Qty
            } else {
                $MultipleLama = ($qty * $Multiple);
                $Stok         = ($StokUtama - $MultipleLama);
            }

            // Update Stok Utama
            $dataStok = array(
                'unit_qty'    => $Stok,
                'unit_update' => date('Y-m-d H:i:s'),
            );

            $this->db->where('unit_id', $unit_id_utama);
            $this->db->update('ok_unit', $dataStok);

            // Konversi ke Unit Lain selain Utama
            $listUnit = $this->db->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_id !=' => $unit_id_utama))->result();
            foreach ($listUnit as $u) {
                $dataStokUnit = array(
                    'unit_qty'    => ($Stok / $u->unit_multiple),
                    'unit_update' => date('Y-m-d H:i:s'),
                );

                $this->db->where('unit_id', $u->unit_id);
                $this->db->update('ok_unit', $dataStokUnit);
            }
        }

        // Hapus Data
        $this->db->where('retur_jual_id', $id);
        $this->db->delete('ok_retur_jual');
    }
}
/* Location: ./application/model/admin/Returjual_m.php */

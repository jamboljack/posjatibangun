<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Bukti Pembayaran <?=$detail->piutang_no_faktur;?></title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 2px;
    }

    th {
        height: 25px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:20px
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" colspan="4" align="right" valign="top"><h1><u>PEMBAYARAN PIUTANG</u></h1></td>
        </tr>
        <tr>
            <td width="50%" align="left" rowspan="5" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
            <td width="15%" valign="top">Tanggal</td>
            <td width="2%" valign="top">:</td>
            <td width="33%" valign="top"><?=date('d-m-Y', strtotime($detail->piutang_tanggal));?></td>
        </tr>
        <tr>
            <td valign="top">No. Faktur</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->piutang_no_faktur);?></td>
        </tr>
        <tr>
            <td valign="top">Nama Pelanggan</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->pelanggan_nama);?></td>
        </tr>
        <tr>
            <td valign="top">Alamat</td>
            <td valign="top">:</td>
            <td valign="top"><?=trim($detail->pelanggan_alamat.' '.$detail->pelanggan_kota.'<br>'.$detail->pelanggan_telp);?></td>
        </tr>
    </table>
    <br>
    <table width="100%" align="center">
        <tr>
            <th width="3%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th width="37%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO. FAKTUR PENJUALAN</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TANGGAL</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TOTAL</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">DIBAYAR</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">SISA</th>
        </tr>
        <?php 
        $no        = 1;
        $totalsisa = 0;
        foreach($listDetail as $r) {
        ?>
        <tr>
            <td align="center" style="border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
            <td style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->penjualan_no_faktur;?></td>
            <td align="center" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=date('d-m-Y', strtotime($r->penjualan_tanggal));?></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->piutang_detail_total, 0, '', ',');?></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->piutang_detail_bayar, 0, '', ',');?></td>
            <td align="right" style="border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->piutang_detail_sisa, 0, '', ',');?></td>
        </tr>
        <?php 
            $totalsisa = ($totalsisa+$r->piutang_detail_sisa);
            $no++; 
        } 
        ?>
        <tr>
            <td colspan="6"><br></td>
        </tr>
        <tr>
            <td colspan="4" align="right"><b>Total :</b></td>
            <td align="right"><b><?=number_format($detail->piutang_total,0,'',',');?></b></td>
            <td align="right"><b><?=number_format($totalsisa,0,'',',');?></b></td>
        </tr>
        <tr>
            <td colspan="6"><br></td>
        </tr>
        <tr>
            <td colspan="6"><b>Terbilang : <?=Terbilang($detail->piutang_total);?> Rupiah</b></td>
        </tr>
		<tr>
            <td colspan="6" align="center"><br></td>
        </tr>
		<tr>
            <td colspan="2" align="center">Penerima<br><br><br><br>( <?=$detail->user_name;?> )</td>
			<td colspan="4"></td>
        </tr>
    </table>
</div>
</body>
</html>
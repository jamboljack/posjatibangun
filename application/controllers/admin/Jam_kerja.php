<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jam_kerja extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/jam_kerja_m');
    }

    public function index()
    {
        $this->template->display('admin/master/jam_kerja_v');
    }

    public function data_list()
    {
        $List = $this->jam_kerja_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row          = array();
            $jam_kerja_id = $r->jam_kerja_id;
            $row[]        = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $jam_kerja_id . "'" . ')"><i class="icon-pencil"></i></a>
                             <a onclick="hapusData(' . $jam_kerja_id . ')" title="Delete Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->jam_kerja_hari;
            $row[]  = $r->jam_kerja_masuk;
            $row[]  = $r->jam_kerja_pulang;
            $row[]  = $r->jam_kerja_libur;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->jam_kerja_m->count_all(),
            "recordsFiltered" => $this->jam_kerja_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function nama_exists($nama)
    {
        $this->db->where('jam_kerja_hari', $nama);
        $query = $this->db->get('ok_jam_kerja');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_nama_exists()
    {
        if (array_key_exists('nama', $_POST)) {
            if ($this->nama_exists($this->input->post('nama', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->jam_kerja_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->jam_kerja_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->jam_kerja_m->update_data();
    }

    public function deletedata($id)
    {
        $this->jam_kerja_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Jam_kerja.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Proses extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function prosesunit()
    {
        $listBarang = $this->db->order_by('barang_kode', 'asc')->get('ok_barang')->result();
        foreach ($listBarang as $r) {
            $barang_id   = $r->barang_id;
            $barang_kode = $r->barang_kode;
            // Data Unit Head
            $dataUnit     = $this->db->get_where('ok_unit', array('barang_id' => $barang_id))->row();
            $unit_id_head = $dataUnit->unit_id;
            $stok_utama   = $dataUnit->unit_qty;
            $unit_nama    = $dataUnit->unit_nama;
            // Data Barang Desktop
            $dataBarang = $this->db->get_where('desk_barang', array('kd_brg' => $barang_kode))->row();
            $unit_besar = $dataBarang->kd_sat1;
            if ($unit_nama != $unit_besar) {
                $stok = ($dataBarang->jml_sat1 * $stok_utama);
                $data = array(
                    'barang_id'     => $barang_id,
                    'unit_nama'     => $dataBarang->kd_sat1,
                    'unit_multiple' => $dataBarang->jml_sat1,
                    'unit_id_head'  => $unit_id_head,
                    'unit_ppn'      => $dataBarang->ppn,
                    'unit_qty'      => $stok,
                    'unit_hrg_beli' => $dataBarang->hrg_sat1,
                    'unit_hrg_jual' => $dataBarang->kms_jlf,
                    'unit_set'      => 0,
                    'unit_update'   => date('Y-m-d H:i:s'),
                );

                $this->db->insert('ok_unit', $data);
            }
        }
    }

    public function prosesdetailbeli()
    {
        $listBeli = $this->db->order_by('pembelian_no_faktur', 'asc')->get('v_pembelian')->result();
        foreach ($listBeli as $r) {
            $pembelian_id = $r->pembelian_id;
            $no_faktur    = $r->pembelian_no_faktur;
            $tanggal      = $r->pembelian_tanggal;
            $kode_agen    = $r->suplier_kode;
            // echo $no_faktur.' '.$tanggal.' '.$kode_agen.'<br>';
            $listDetail   = $this->db->get_where('desk_beli_detail', array('no_bukti' => $no_faktur, 'tanggal' => $tanggal, 'kd_agen' => $kode_agen))->result();
            foreach ($listDetail as $d) {
                $barang_kode = $d->kd_brg;
                $satuan_lama = $d->kd_satuan;
                $dataBarang  = $this->db->get_where('ok_barang', array('barang_kode' => $barang_kode))->row();
                $barang_id   = $dataBarang->barang_id;
                $dataUnit    = $this->db->order_by('unit_id', 'asc')->limit(1)->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_nama' => $satuan_lama))->row();
                if (count($dataUnit) > 0) {
                    $unit_id = $dataUnit->unit_id;
                    $data    = array(
                        'pembelian_id'                 => $pembelian_id,
                        'barang_id'                    => $barang_id,
                        'pembelian_detail_qty'         => $d->qty,
                        'unit_id'                      => $unit_id,
                        'pembelian_detail_harga'       => $d->hrgsat,
                        'pembelian_detail_disc1'       => $d->dis1,
                        'pembelian_detail_disc2'       => $d->dis2,
                        'pembelian_detail_disc3'       => $d->dis3,
                        'pembelian_detail_disc4'       => $d->dis4,
                        'pembelian_detail_ppn'         => $d->ppn,
                        'pembelian_detail_ppn_rp'      => $d->ppn_rp,
                        'pembelian_detail_harga_pokok' => $d->hrgpbl,
                        'pembelian_detail_total'       => $d->debet,
                        'pembelian_detail_update'      => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_pembelian_detail', $data);
                }
            }
        }
    }

    public function prosesdetailjual()
    {
        $listJual = $this->db->order_by('penjualan_no_faktur', 'asc')->get_where('ok_penjualan', array('penjualan_id >=' => 40001, 'penjualan_id <=' => 50000))->result();
        foreach ($listJual as $r) {
            $penjualan_id = $r->penjualan_id;
            $no_faktur    = trim($r->penjualan_no_faktur);
            $listDetail   = $this->db->get_where('desk_jual_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $barang_kode = trim($d->kd_brg);
                $satuan_lama = trim($d->kd_satuan);
                $dataBarang  = $this->db->get_where('ok_barang', array('barang_kode' => $barang_kode))->row();
                if (count($dataBarang) > 0) {
                    $barang_id = $dataBarang->barang_id;
                    $dataUnit  = $this->db->order_by('unit_id', 'asc')->limit(1)->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_nama' => $satuan_lama))->row();
                    if (count($dataUnit) > 0) {
                        $unit_id = $dataUnit->unit_id;
                        $data    = array(
                            'penjualan_id'                 => $penjualan_id,
                            'barang_id'                    => $barang_id,
                            'penjualan_detail_qty'         => $d->qty,
                            'unit_id'                      => $unit_id,
                            'penjualan_detail_harga_pokok' => $d->hrg_pokok,
                            'penjualan_detail_harga'       => $d->hrgsat,
                            'penjualan_detail_disc'        => $d->discitem,
                            'penjualan_detail_disc_rp'     => (($d->discitem * $d->hrgsat) / 100),
                            'penjualan_detail_total'       => $d->debet,
                            'penjualan_detail_update'      => date('Y-m-d H:i:s'),
                        );

                        $this->db->insert('ok_penjualan_detail', $data);
                    }
                }
            }
        }
    }

    public function prosescekdetail()
    {
        $listJual = $this->db->order_by('penjualan_no_faktur', 'asc')->get_where('ok_penjualan', array('penjualan_id >=' => 30001, 'penjualan_id <=' => 40000))->result();
        foreach ($listJual as $r) {
            $penjualan_id = $r->penjualan_id;
            $no_faktur    = trim($r->penjualan_no_faktur);
            $listDetail   = $this->db->get_where('desk_jual_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $barang_kode = trim($d->kd_brg);
                $dataBarang  = $this->db->get_where('ok_barang', array('barang_kode' => $barang_kode))->row();
                if (count($dataBarang) == 0) {
                    echo $barang_kode . '<br>';
                }
            }
        }
    }

    public function prosesdetailhutang()
    {
        $listJual = $this->db->order_by('hutang_id', 'asc')->get('ok_hutang')->result();
        foreach ($listJual as $r) {
            $hutang_id  = $r->hutang_id;
            $no_faktur  = trim($r->hutang_no_faktur);
            $listDetail = $this->db->get_where('desk_hutang_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $no_bukti = trim($d->nom_fkt);
                $dataBeli = $this->db->get_where('ok_pembelian', array('pembelian_no_faktur' => $no_bukti))->row();
                if (count($dataBeli) > 0) {
                    $pembelian_id = $dataBeli->pembelian_id;
                    $data         = array(
                        'hutang_id'            => $hutang_id,
                        'pembelian_id'         => $pembelian_id,
                        'hutang_detail_total'  => $d->jml_pht,
                        'hutang_detail_bayar'  => $d->jml_byr,
                        'hutang_detail_sisa'   => $d->sis_pht,
                        'hutang_detail_update' => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_hutang_detail', $data);
                } else {
                    echo $no_bukti . '<br>';
                }
            }
        }
    }

    public function prosesdetailpiutang()
    {
        $listJual = $this->db->order_by('piutang_id', 'asc')->get('ok_piutang')->result();
        foreach ($listJual as $r) {
            $piutang_id = $r->piutang_id;
            $no_faktur  = trim($r->piutang_no_faktur);
            $listDetail = $this->db->get_where('desk_piutang_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $no_bukti = trim($d->nom_fkt);
                $dataBeli = $this->db->get_where('ok_penjualan', array('penjualan_no_faktur' => $no_bukti))->row();
                if (count($dataBeli) > 0) {
                    $penjualan_id = $dataBeli->penjualan_id;
                    $data         = array(
                        'piutang_id'            => $piutang_id,
                        'penjualan_id'          => $penjualan_id,
                        'piutang_detail_total'  => $d->jml_pht,
                        'piutang_detail_bayar'  => $d->jml_byr,
                        'piutang_detail_sisa'   => $d->sis_pht,
                        'piutang_detail_update' => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_piutang_detail', $data);
                }
            }
        }
    }

    // Proses Detail Retur Beli
    public function prosesdetailreturbeli()
    {
        $listBeli = $this->db->order_by('retur_beli_no_faktur', 'asc')->get('ok_retur_beli')->result();
        foreach ($listBeli as $r) {
            $retur_beli_id = $r->retur_beli_id;
            $no_faktur     = $r->retur_beli_no_faktur;
            $listDetail    = $this->db->get_where('desk_returbeli_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $barang_kode = $d->kd_brg;
                $satuan_lama = $d->kd_satuan;
                $dataBarang  = $this->db->get_where('ok_barang', array('barang_kode' => $barang_kode))->row();
                $barang_id   = $dataBarang->barang_id;
                $dataUnit    = $this->db->order_by('unit_id', 'asc')->limit(1)->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_nama' => $satuan_lama))->row();
                if (count($dataUnit) > 0) {
                    $unit_id = $dataUnit->unit_id;
                    $data    = array(
                        'retur_beli_id'                 => $retur_beli_id,
                        'barang_id'                     => $barang_id,
                        'retur_beli_detail_qty'         => $d->qty,
                        'unit_id'                       => $unit_id,
                        'retur_beli_detail_harga'       => $d->hrgsat,
                        'retur_beli_detail_disc1'       => $d->dis1,
                        'retur_beli_detail_disc2'       => $d->dis2,
                        'retur_beli_detail_disc3'       => $d->dis3,
                        'retur_beli_detail_disc4'       => $d->dis4,
                        'retur_beli_detail_ppn'         => $d->ppn,
                        'retur_beli_detail_ppn_rp'      => $d->ppn_rp,
                        'retur_beli_detail_harga_pokok' => $d->hrgpbl,
                        'retur_beli_detail_total'       => $d->debet,
                        'retur_beli_detail_update'      => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('ok_retur_beli_detail', $data);
                }
            }
        }
    }

    // Retur Jual
    public function prosesdetailreturjual()
    {
        $listJual = $this->db->order_by('retur_jual_no_faktur', 'asc')->get('ok_retur_jual')->result();
        foreach ($listJual as $r) {
            $retur_jual_id = $r->retur_jual_id;
            $no_faktur     = $r->retur_jual_no_faktur;
            $listDetail    = $this->db->get_where('desk_returjual_detail', array('no_bukti' => $no_faktur))->result();
            foreach ($listDetail as $d) {
                $barang_kode = $d->kd_brg;
                $satuan_lama = $d->kd_satuan;
                $dataBarang  = $this->db->get_where('ok_barang', array('barang_kode' => $barang_kode))->row();
                if (count($dataBarang) > 0) {
                    $barang_id = $dataBarang->barang_id;
                    $dataUnit  = $this->db->order_by('unit_id', 'asc')->limit(1)->get_where('ok_unit', array('barang_id' => $barang_id, 'unit_nama' => $satuan_lama))->row();
                    if (count($dataUnit) > 0) {
                        $unit_id = $dataUnit->unit_id;
                        $data    = array(
                            'retur_jual_id'                 => $retur_jual_id,
                            'barang_id'                     => $barang_id,
                            'retur_jual_detail_qty'         => $d->qty,
                            'unit_id'                       => $unit_id,
                            'retur_jual_detail_harga_pokok' => $d->hrg_pokok,
                            'retur_jual_detail_harga'       => $d->hrgsat,
                            'retur_jual_detail_disc'        => $d->discitem,
                            'retur_jual_detail_disc_rp'     => (($d->discitem * $d->hrgsat) / 100),
                            'retur_jual_detail_total'       => $d->debet,
                            'retur_jual_detail_update'      => date('Y-m-d H:i:s'),
                        );

                        $this->db->insert('ok_retur_jual_detail', $data);
                    }
                } else {
                    echo $barang_kode . '<br>';
                }
            }
        }
    }
}
/* Location: ./application/controller/Proses.php */

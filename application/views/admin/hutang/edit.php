<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Pembayaran Hutang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/hutang');?>">Pembayaran Hutang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Detail Pembayaran Hutang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-edit"></i> Form Data Suplier
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">No. Faktur</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="no_faktur" value="<?=$detail->hutang_no_faktur;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Tanggal/Hari</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="tanggal" value="<?=date('d-m-Y', strtotime($detail->hutang_tanggal));?>" readonly>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="hari" value="<?=getDay($detail->hutang_tanggal);?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Nama Suplier</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="nama_suplier" id="nama_suplier" autocomplete="off" value="<?=$detail->suplier_nama;?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="alamat" id="alamat" autocomplete="off" value="<?=$detail->suplier_alamat;?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Total Pembayaran
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">User :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$detail->user_username;?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalinvoice">Rp. <?=number_format($detail->hutang_total,0,'',',');?></div>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pembayaran Hutang 
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th>No. Faktur</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="15%">Jumlah Hutang</th>
                                    <th width="15%">Jumlah Bayar</th>
                                    <th width="15%">Sisa</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-danger" href="<?=site_url('admin/hutang/printfaktur/'.$detail->hutang_id);?>" target="_blank"><i class="fa fa-print"></i> Print Bukti</a>
                <a href="<?=site_url('admin/hutang');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Kembali</a>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript">
var table;
$(document).ready(function() {
    var hutang_id = '<?=$detail->hutang_id;?>';
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "destoy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/hutang/data_detail_list/')?>"+hutang_id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5],
                "orderable": false,
            },
            {
                "targets": [ 0, 2 ],
                "className": "text-center",
            },
            {
                "targets": [ 3, 4, 5 ],
                "className": "text-right",
            }
        ],
    });    
});
</script>
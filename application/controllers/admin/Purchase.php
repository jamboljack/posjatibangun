<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purchase extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/purchase_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $data['listTipe']    = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/purchase/view', $data);
    }

    public function data_list()
    {
        $List = $this->purchase_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row         = array();
            $purchase_id = $r->purchase_id;
            $linkedit    = site_url('admin/purchase/editdata/' . $purchase_id);
            $linkprint   = site_url('admin/purchase/printfaktur/' . $purchase_id);
            $row[]       = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                            <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>
                            <a onclick="hapusData(' . $purchase_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->purchase_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->purchase_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = number_format($r->purchase_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_all(),
            "recordsFiltered" => $this->purchase_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        // Hapus Temp by Username dan Tanggal
        $username = $this->session->userdata('username');
        $this->db->where('user_username', $username);
        $this->db->delete('ok_purchase_temp');

        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $this->template->display('admin/purchase/add', $data);
    }

    private function kode_exists($kode)
    {
        $this->db->where('suplier_kode', $kode);
        $query = $this->db->get('ok_suplier');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function kodebarang_exists($kode_add)
    {
        $this->db->where('barang_kode', $kode_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kodebarang_exists()
    {
        if (array_key_exists('kode_add', $_POST)) {
            if ($this->kodebarang_exists($this->input->post('kode_add', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function namabarang_exists($nama_add)
    {
        $this->db->where('barang_nama', $nama_add);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_namabarang_exists()
    {
        if (array_key_exists('nama_add', $_POST)) {
            if ($this->namabarang_exists(stripHTMLtags($this->input->post('nama_add', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function data_temp_list()
    {
        $List = $this->purchase_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row              = array();
            $purchase_temp_id = $r->purchase_temp_id;
            $row[]            = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $purchase_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                                 <a onclick="hapusData(' . $purchase_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = number_format($r->purchase_temp_qty, 0, '', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->purchase_temp_harga, 0, '', ',');
            $row[]  = number_format($r->purchase_temp_disc1, 2, '.', '');
            $row[]  = number_format($r->purchase_temp_disc2, 2, '.', '');
            $row[]  = number_format($r->purchase_temp_disc3, 2, '.', '');
            $row[]  = number_format($r->purchase_temp_disc4, 2, '.', '');
            $row[]  = number_format($r->purchase_temp_ppn, 2, '.', '');
            $row[]  = number_format($r->purchase_temp_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_tmp_all(),
            "recordsFiltered" => $this->purchase_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_suplier_list()
    {
        $List = $this->purchase_m->get_suplier_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row        = array();
            $suplier_id = $r->suplier_id;
            $row[]      = '<a href="javascript:void(0)" title="Pilih" onclick="pilihsuplier(' . "'" . $suplier_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]      = $no;
            $row[]      = $r->suplier_kode;
            $row[]      = $r->suplier_nama;
            $row[]      = $r->suplier_alamat;
            $row[]      = $r->suplier_kota;
            $row[]      = $r->suplier_telp;
            $data[]     = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_suplier_all(),
            "recordsFiltered" => $this->purchase_m->count_suplier_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_barang_list()
    {
        $List = $this->purchase_m->get_barang_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $row[]     = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $barang_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]     = $no;
            $row[]     = $r->barang_kode;
            $row[]     = $r->barang_nama;
            $row[]     = $r->barang_merk;
            $row[]     = number_format($r->unit_qty, 2, '.', ',');
            $row[]     = $r->unit_nama;
            $row[]     = number_format($r->unit_hrg_beli, 0, '', ',');
            $data[]    = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_barang_all(),
            "recordsFiltered" => $this->purchase_m->count_barang_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_suplier($id)
    {
        $data = $this->db->get_where('ok_suplier', array('suplier_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_suplier_by_kode($kode_suplier)
    {
        $data = $this->db->get_where('ok_suplier', array('suplier_kode' => trim(strtoupper($kode_suplier))))->row();
        echo json_encode($data);
    }

    public function get_data_barang($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_barang_by_kode($kode_barang)
    {
        $data = $this->db->get_where('v_barang', array('barang_kode' => trim(strtoupper($kode_barang))))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_purchase', array('purchase_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $this->purchase_m->insert_data_item();
    }

    public function updateitem()
    {
        $this->purchase_m->update_data_item();
    }

    public function deleteitem($id)
    {
        $this->purchase_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('purchase_temp_total', 'total')->get_where('ok_purchase_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function data_unit_list($barang_id)
    {
        $List = $this->purchase_m->get_unit_datatables($barang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row     = array();
            $unit_id = $r->unit_id;
            $row[]   = '<a href="javascript:void(0)" title="Pilih" onclick="pilihunit(' . "'" . $unit_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]   = $r->unit_nama;
            $row[]   = number_format($r->unit_qty, 0, '', ',');
            $row[]   = number_format($r->unit_hrg_beli, 0, '', ',');
            $row[]   = number_format($r->unit_disc1, 2, '.', '');
            $row[]   = number_format($r->unit_disc2, 2, '.', '');
            $row[]   = number_format($r->unit_disc3, 2, '.', '');
            $row[]   = number_format($r->unit_disc4, 2, '.', '');
            $row[]   = number_format($r->unit_ppn, 2, '.', '');
            $row[]   = number_format($r->unit_hrg_pokok, 0, '', ',');
            $data[]  = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_unit_all($barang_id),
            "recordsFiltered" => $this->purchase_m->count_unit_filtered($barang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_unit($id)
    {
        $data = $this->db->get_where('v_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedataunit()
    {
        $this->purchase_m->update_data_unit();
    }

    private function faktur_exists($no_faktur)
    {
        $this->db->where('purchase_no_faktur', $no_faktur);
        $query = $this->db->get('ok_purchase');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_faktur_exists()
    {
        if (array_key_exists('no_faktur', $_POST)) {
            if ($this->faktur_exists($this->input->post('no_faktur', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->purchase_m->insert_data_purchase();
    }

    public function savedatabarang()
    {
        $this->purchase_m->insert_data_barang();
    }

    public function editdata($purchase_id)
    {
        $data['detail'] = $this->db->get_where('v_purchase', array('purchase_id' => $purchase_id))->row();
        $this->template->display('admin/purchase/edit', $data);
    }

    public function data_detail_list($purchase_id)
    {
        $List = $this->purchase_m->get_detail_datatables($purchase_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row                = array();
            $purchase_detail_id = $r->purchase_detail_id;
            $row[]              = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $purchase_detail_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $purchase_detail_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = number_format($r->purchase_detail_qty, 0, '', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->purchase_detail_harga, 0, '', ',');
            $row[]  = number_format($r->purchase_detail_disc1, 2, '.', '');
            $row[]  = number_format($r->purchase_detail_disc2, 2, '.', '');
            $row[]  = number_format($r->purchase_detail_disc3, 2, '.', '');
            $row[]  = number_format($r->purchase_detail_disc4, 2, '.', '');
            $row[]  = number_format($r->purchase_detail_ppn, 2, '.', '');
            $row[]  = number_format($r->purchase_detail_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->purchase_m->count_detail_all($purchase_id),
            "recordsFiltered" => $this->purchase_m->count_detail_filtered($purchase_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($purchase_id)
    {
        $data = $this->db->select_sum('purchase_detail_total', 'total')->get_where('ok_purchase_detail', array('purchase_id' => $purchase_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->purchase_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_purchase_detail', array('purchase_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->purchase_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->purchase_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->purchase_m->update_data_purchase();
    }

    public function savedatasuplier()
    {
        $this->purchase_m->insert_data_suplier();
    }

    public function deletedata($id)
    {
        $this->purchase_m->delete_data_purchase($id);
    }

    public function printfaktur($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_purchase', array('purchase_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_purchase_detail', array('purchase_id' => $id))->result();
        $this->load->view('admin/purchase/printfaktur_v', $data);
    }
}
/* Location: ./application/controller/admin/Purchase.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_beli_periode extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/lap_beli_periode_m');
    }

    public function index()
    {
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $data['listTipe']    = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/reportbeli/reportbeliperiode_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_beli_periode_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->pembelian_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]  = number_format($r->pembelian_ppn, 0, '', ',');
            $row[]  = number_format($r->pembelian_ppn_rp, 0, '', ',');
            $row[]  = number_format($r->pembelian_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_beli_periode_m->count_all(),
            "recordsFiltered" => $this->lap_beli_periode_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printperiode($dari = 'all', $sampai = 'all', $suplier = 'all', $tipe = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($dari != 'all' && $sampai != 'all' && $suplier == 'all' && $tipe == 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('pembelian_tanggal >=' => $tgl_dari, 'pembelian_tanggal <=' => $tgl_sampai))->result();
        } elseif ($dari == 'all' && $sampai == 'all' && $suplier != 'all' && $tipe == 'all') {
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('suplier_id' => $suplier))->result();
        } elseif ($dari == 'all' && $sampai == 'all' && $suplier == 'all' && $tipe != 'all') {
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('tipe_bayar_id' => $tipe))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $suplier != 'all' && $tipe == 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('pembelian_tanggal >=' => $tgl_dari, 'pembelian_tanggal <=' => $tgl_sampai, 'suplier_id' => $suplier))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $suplier == 'all' && $tipe != 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('pembelian_tanggal >=' => $tgl_dari, 'pembelian_tanggal <=' => $tgl_sampai, 'tipe_bayar_id' => $tipe))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $suplier != 'all' && $tipe != 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get_where('v_pembelian', array('pembelian_tanggal >=' => $tgl_dari, 'pembelian_tanggal <=' => $tgl_sampai, 'suplier_id' => $suplier, 'tipe_bayar_id' => $tipe))->result();
        } else {
            $data['listData'] = $this->db->order_by('pembelian_tanggal', 'asc')->get('v_pembelian')->result();
        }

        $this->load->view('admin/reportbeli/printperiode_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_beli_periode.php */

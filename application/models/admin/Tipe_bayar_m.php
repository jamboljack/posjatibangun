<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tipe_bayar_m extends CI_Model
{
    public $table         = 'ok_tipe_bayar';
    public $column_order  = array(null, null, 'tipe_bayar_nama', 'tipe_bayar_set', 'tipe_bayar_bank');
    public $column_search = array('tipe_bayar_nama');
    public $order         = array('tipe_bayar_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'tipe_bayar_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('name', 'true')))),
            'tipe_bayar_set'    => $this->input->post('lstSet', 'true'),
            'tipe_bayar_bank'   => $this->input->post('lstBank', 'true'),
            'tipe_bayar_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_tipe_bayar', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ok_tipe_bayar');
        $this->db->where('tipe_bayar_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $tipe_bayar_id = $this->input->post('id', 'true');
        $data          = array(
            'tipe_bayar_nama'   => strtoupper(trim(stripHTMLtags($this->input->post('name', 'true')))),
            'tipe_bayar_set'    => $this->input->post('lstSet', 'true'),
            'tipe_bayar_bank'   => $this->input->post('lstBank', 'true'),
            'tipe_bayar_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('tipe_bayar_id', $tipe_bayar_id);
        $this->db->update('ok_tipe_bayar', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('tipe_bayar_id', $id);
        $this->db->delete('ok_tipe_bayar');
    }
}
/* Location: ./application/models/admin/Tipe_bayar_m.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_stok_opname extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/lap_stok_opname_m');
    }

    public function index()
    {
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $data['listRak']      = $this->db->order_by('rak_nama', 'asc')->get('ok_rak')->result();
        $this->template->display('admin/reportbarang/reportstokopname_v', $data);
    }

    public function customNumberFormat($value)
    {
        if (intval($value) == $value) {
            return number_format($value, 0, '', '.');
        } else {
            return number_format($value, 2, ',', '.');
        }
    }

    public function data_list()
    {
        $List = $this->lap_stok_opname_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->so_rekap_tanggal));
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = $r->kategori_nama;
            $row[]  = $r->rak_nama;
            $row[]  = $this->customNumberFormat($r->so_rekap_qty);
            $row[]  = $this->customNumberFormat($r->so_rekap_real);
            $row[]  = ($r->so_rekap_selisih > 0 ? '+' . $this->customNumberFormat($r->so_rekap_selisih) : $this->customNumberFormat($r->so_rekap_selisih));
            $row[]  = $r->unit_nama;
            $row[]  = $this->customNumberFormat($r->harga);
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_stok_opname_m->count_all(),
            "recordsFiltered" => $this->lap_stok_opname_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printdata($tanggal, $kategori = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($kategori != 'all') {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get_where('ok_kategori', array('kategori_id' => $kategori))->result();
        } else {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        }

        $data['tanggal'] = $tanggal;
        $this->load->view('admin/reportbarang/printstokopname_v', $data);
    }

    public function printdetail($tanggal, $kategori = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($kategori != 'all') {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get_where('ok_kategori', array('kategori_id' => $kategori))->result();
        } else {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        }

        $data['tanggal'] = $tanggal;
        $this->load->view('admin/reportbarang/printstokopnamedetail_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_stok_opname.php */

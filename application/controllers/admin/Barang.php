<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/barang_m');
    }

    public function index()
    {
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $data['listSuplier']  = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $data['listRak']      = $this->db->order_by('rak_nama', 'asc')->get('ok_rak')->result();
        $this->template->display('admin/barang/view', $data);
    }

    public function data_list()
    {
        $List = $this->barang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $barang_id = $r->barang_id;
            $linkunit  = site_url('admin/barang/listunit/' . $barang_id);
            $row[]     = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $barang_id . "'" . ')"><i class="icon-pencil"></i></a>
                          <a href="' . $linkunit . '" title="Daftar Unit & Harga"><i class="icon-screen-tablet"></i></a>
                          <a onclick="hapusData(' . $barang_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[] = $no;
            if ($r->unit_qty <= $r->barang_min_stok) {
                $kode     = '<span class="label label-danger">' . $r->barang_kode . '</span>';
                $nama     = '<span class="label label-danger">' . $r->barang_nama . '</span>';
                $kategori = '<span class="label label-danger">' . $r->kategori_nama . '</span>';
                $merk     = '<span class="label label-danger">' . $r->barang_merk . '</span>';
                $qty      = '<span class="label label-danger">' . number_format($r->unit_qty, 2, '.', ',') . '</span>';
            } else {
                $kode     = $r->barang_kode;
                $nama     = $r->barang_nama;
                $kategori = $r->kategori_nama;
                $merk     = $r->barang_merk;
                $qty      = number_format($r->unit_qty, 2, '.', ',');
            }
            $row[]  = $kode;
            $row[]  = $nama;
            $row[]  = $kategori;
            $row[]  = $merk;
            $row[]  = $qty;
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->unit_hrg_pokok, 0, '', ',');
            $row[]  = ($r->barang_tgl_beli != '' ? date('d-m-Y', strtotime($r->barang_tgl_beli)) : '-');
            $row[]  = $r->suplier_nama;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->barang_m->count_all(),
            "recordsFiltered" => $this->barang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function kode_exists($kode)
    {
        $this->db->where('barang_kode', $kode);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    private function nama_exists($nama)
    {
        $this->db->where('barang_nama', $nama);
        $query = $this->db->get('ok_barang');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_nama_exists()
    {
        if (array_key_exists('nama', $_POST)) {
            if ($this->nama_exists(stripHTMLtags($this->input->post('nama', 'true'))) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        if (!empty($_FILES['foto']['name'])) {
            $jam                     = time();
            $config['file_name']     = 'Barang_' . $jam . '.jpg';
            $config['upload_path']   = './img/barang/';
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['overwrite']     = true;
            $config['max_size']      = 0;
            $this->load->library('upload');
            $this->upload->initialize($config);
            // Resize
            $configThumb                   = array();
            $configThumb['image_library']  = 'gd2';
            $configThumb['source_image']   = '';
            $configThumb['maintain_ratio'] = true;
            $configThumb['overwrite']      = true;
            // $configThumb['width']          = 150;
            // $configThumb['height']         = 200;
            $this->load->library('image_lib');
            if (!$this->upload->do_upload('foto')) {
                $response['status'] = 'error';
            } else {
                $upload                      = $this->upload->do_upload('foto');
                $upload_data                 = $this->upload->data();
                $name_array[]                = $upload_data['file_name'];
                $configThumb['source_image'] = $upload_data['full_path'];
                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();

                $this->barang_m->insert_data();
                $response['status'] = 'success';
            }
        } else {
            $this->barang_m->insert_data();
            $response['status'] = 'success';
        }

        echo json_encode($response);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        if (!empty($_FILES['foto']['name'])) {
            $jam                     = time();
            $config['file_name']     = 'Barang_' . $jam . '.jpg';
            $config['upload_path']   = './img/barang/';
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['overwrite']     = true;
            $config['max_size']      = 0;
            $this->load->library('upload');
            $this->upload->initialize($config);
            // Resize
            $configThumb                   = array();
            $configThumb['image_library']  = 'gd2';
            $configThumb['source_image']   = '';
            $configThumb['maintain_ratio'] = true;
            $configThumb['overwrite']      = true;
            // $configThumb['width']          = 150;
            // $configThumb['height']         = 200;
            $this->load->library('image_lib');
            if (!$this->upload->do_upload('foto')) {
                $response['status'] = 'error';
            } else {
                $upload                      = $this->upload->do_upload('foto');
                $upload_data                 = $this->upload->data();
                $name_array[]                = $upload_data['file_name'];
                $configThumb['source_image'] = $upload_data['full_path'];
                $this->image_lib->initialize($configThumb);
                $this->image_lib->resize();

                $this->barang_m->update_data();
                $response['status'] = 'success';
            }
        } else {
            $this->barang_m->update_data();
            $response['status'] = 'success';
        }

        echo json_encode($response);
    }

    public function deletedata($id)
    {
        $this->barang_m->delete_data($id);
    }

    public function listunit($barang_id)
    {
        $data['detail']   = $this->db->get_where('v_barang', array('barang_id' => $barang_id))->row();
        $data['listUnit'] = $this->db->order_by('unit_nama', 'asc')->get_where('v_unit', array('barang_id' => $barang_id, 'unit_set' => 1))->result();
        $this->template->display('admin/barang/unitlist_v', $data);
    }

    public function check_harga($barang_id)
    {
        $data = $this->db->get_where('v_barang', array('barang_id' => $barang_id))->row();
        echo json_encode($data);
    }

    public function pilih_unit($unit_id)
    {
        $query = $this->db->order_by('unit_nama', 'asc')->get_where('ok_unit', array('unit_id !=' => $unit_id))->result();
        $data  = "<option value=''> - Pilih Unit - </option>";
        foreach ($query as $value) {
            $data .= "<option value='" . $value->unit_id . "'>" . strtoupper($value->unit_nama) . "</option>";
        }
        echo $data;
    }

    public function pilih_unit_head_edit($unit_id_head)
    {
        $query = $this->db->order_by('unit_nama', 'asc')->get('ok_unit')->result();
        $data  = "<option value=''> - Pilih Unit Atasan - </option>";
        foreach ($query as $value) {
            if ($value->unit_id == $unit_id_head) {
                $data .= "<option value='" . $value->unit_id . "' selected>" . strtoupper($value->unit_nama) . "</option>";
            } else {
                $data .= "<option value='" . $value->unit_id . "'>" . strtoupper($value->unit_nama) . "</option>";
            }
        }
        echo $data;
    }

    public function pilih_unit_edit($unit_id, $unit_id_head)
    {
        $query = $this->db->order_by('unit_nama', 'asc')->get_where('ok_unit', array('unit_id !=' => $unit_id_head))->result();
        $data  = "<option value=''> - Pilih Unit - </option>";
        foreach ($query as $value) {
            if ($value->unit_id == $unit_id) {
                $data .= "<option value='" . $value->unit_id . "' selected>" . strtoupper($value->unit_nama) . "</option>";
            } else {
                $data .= "<option value='" . $value->unit_id . "'>" . strtoupper($value->unit_nama) . "</option>";
            }
        }
        echo $data;
    }

    public function data_unit_list($barang_id)
    {
        $List = $this->barang_m->get_unit_datatables($barang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $unit_id   = $r->unit_id;
            $barang_id = $r->barang_id;
            $link      = site_url('admin/unit/logdata/' . $unit_id);
            if ($r->unit_set == 0) {
                $setting = '<a onclick="setUtama(' . $unit_id . ',' . $barang_id . ')" title="Set Utama"><i class="icon-check"></i></a>';
            } else {
                $setting = '';
            }
            $row[] = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $unit_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a title="Log History Harga" href="javascript:void(0)" onclick="log_data(' . "'" . $unit_id . "'" . ')"><i class="fa fa-history"></i></a> ' . $setting;
            $row[]  = $no;
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->unit_multiple, 2, '.', ',');
            $row[]  = $r->unit_head;
            $row[]  = number_format($r->unit_qty, 2, '.', ',');
            $row[]  = number_format($r->unit_hrg_beli, 0, '', ',');
            $row[]  = number_format($r->unit_disc1, 2, '.', ',');
            $row[]  = number_format($r->unit_disc2, 2, '.', ',');
            $row[]  = number_format($r->unit_disc3, 2, '.', ',');
            $row[]  = number_format($r->unit_disc4, 2, '.', ',');
            $row[]  = number_format($r->unit_ppn, 2, '.', ',');
            $row[]  = number_format($r->unit_hrg_pokok, 0, '', ',');
            $row[]  = number_format($r->unit_hrg_jual, 0, '', ',');
            $row[]  = number_format($r->unit_jml_jual, 0, '', ',');
            $row[]  = ($r->unit_set == 1 ? '<span class="label label-success">UTAMA</span>' : '');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->barang_m->count_unit_all($barang_id),
            "recordsFiltered" => $this->barang_m->count_unit_filtered($barang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function savedataunit()
    {
        $this->barang_m->insert_data_unit();
    }

    public function get_data_unit($id)
    {
        $data = $this->db->get_where('v_unit', array('unit_id' => $id))->row();
        echo json_encode($data);
    }

    public function updatedataunit()
    {
        $this->barang_m->update_data_unit();
    }

    public function data_unit_log_list($unit_id)
    {
        $List = $this->barang_m->get_unit_log_datatables($unit_id);
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->log_date));
            $row[]  = number_format($r->log_harga_beli_lama, 0, '.', ',');
            $row[]  = number_format($r->log_harga_beli_baru, 0, '', ',');
            $row[]  = number_format($r->log_harga_beli_persen, 2, '.', ',');
            $row[]  = number_format($r->log_harga_pokok_lama, 0, '.', ',');
            $row[]  = number_format($r->log_harga_pokok_baru, 0, '', ',');
            $row[]  = number_format($r->log_harga_jual_lama, 0, '.', ',');
            $row[]  = number_format($r->log_harga_jual_baru, 0, '', ',');
            $row[]  = number_format($r->log_harga_jual_persen, 2, '.', ',');
            $row[]  = $r->user_username;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->barang_m->count_unit_log_all($unit_id),
            "recordsFiltered" => $this->barang_m->count_unit_log_filtered($unit_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function setutama($id, $barang_id)
    {
        $checkData = $this->db->get_where('v_unit', array('unit_set' => 1, 'barang_id' => $barang_id))->row();
        if (count($checkData) > 0) {
            $unit_id = $checkData->unit_id;
            // Update ID jadi Utama
            $data = array(
                'unit_set'     => 1,
                'unit_id_head' => '',
            );

            $this->db->where('unit_id', $id);
            $this->db->update('ok_unit', $data);

            // Update Unit Barang
            $dataBarang = array(
                'unit_id' => $id,
            );

            $this->db->where('barang_id', $barang_id);
            $this->db->update('ok_barang', $dataBarang);

            // Update Data Lama jadi tidak Utama
            $dataLama = array(
                'unit_set' => 0,
            );

            $this->db->where('unit_id', $unit_id);
            $this->db->update('ok_unit', $dataLama);

            // Update Atasan Unit Lain
            $dataHead = array(
                'unit_id_head' => $id,
            );

            $this->db->where('unit_id !=', $id);
            $this->db->where('barang_id', $barang_id);
            $this->db->update('ok_unit', $dataHead);
        }
    }
}
/* Location: ./application/controller/admin/Barang.php */

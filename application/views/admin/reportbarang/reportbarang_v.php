<link href="<?=base_url();?>backend/js/sweetalert2.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>backend/js/sweetalert2.min.js"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Daftar Stok Barang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Report</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Laporan Barang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Daftar Stok Barang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Stok Barang
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData" class="btn btn-warning btn-xs">
                                <i class="fa fa-search"></i> Filter Data
                            </a>
                            <a onclick="printData()" class="btn btn-danger btn-xs">
                                <i class="icon-printer"></i> Print
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="10%">Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th width="15%">Merk</th>
                                    <th width="15%">Kategori</th>
                                    <th width="5%">Stok</th>
                                    <th width="5%">Min. Stok</th>
                                    <th width="10%">Unit</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/lap_stok_barang/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstKategori = $('#lstKategori').val();
                data.lstSuplier  = $('#lstSuplier').val();
                data.lstRak      = $('#lstRak').val();
                data.merk        = $('#merk').val();
                data.lstStok     = $('#lstStok').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0 ],
                "orderable": false,
            },
            {
                "targets": [ 0 ],
                "className": "text-center",
            },
            {
                "targets": [ 5, 6 ],
                "className": "text-right",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

function printData() {
    var lstKategori = $('#lstKategori').val();
    if (lstKategori === '') {
        var kategori = 'all';
    } else {
        var kategori = lstKategori;
    }
    var lstSuplier = $('#lstSuplier').val();
    if (lstSuplier === '') {
        var suplier = 'all';
    } else {
        var suplier = lstSuplier;
    }
    var lstRak = $('#lstRak').val();
    if (lstRak === '') {
        var rak = 'all';
    } else {
        var rak = lstRak;
    }
    var mrk = $('#merk').val();
    if (mrk === '') {
        var merk = 'all';
    } else {
        var merk = mrk;
    }
    var lstStok = $('#lstStok').val();
    if (lstStok === '') {
        var stok = 'all';
    } else {
        var stok = lstStok;
    }

    var url = "<?=site_url('admin/lap_stok_barang/printdata/');?>"+kategori+'/'+suplier+'/'+merk+'/'+stok+'/'+rak;
    window.open(url, "_blank");
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Kategori</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstKategori" id="lstKategori">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listKategori as $r) {?>
                            <option value="<?=$r->kategori_id;?>"><?=$r->kategori_nama;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Suplier</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstSuplier" id="lstSuplier">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listSuplier as $r) {?>
                            <option value="<?=$r->suplier_id;?>"><?=$r->suplier_nama;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Rak</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstRak" id="lstRak">
                            <option value="">- SEMUA DATA -</option>
                            <?php foreach ($listRak as $r) {?>
                            <option value="<?=$r->rak_id;?>"><?=$r->rak_nama;?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Merk</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Input Merk" name="merk" id="merk" autocomplete="off">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Stok</label>
                    <div class="col-md-9">
                        <select class="form-control" name="lstStok" id="lstStok">
                            <option value="">- SEMUA DATA -</option>
                            <option value="1">< Min. Stok</option>
                            <option value="2">> Min. Stok</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
            </div>
            </form>
        </div>
    </div>
</div>

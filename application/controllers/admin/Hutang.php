<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/hutang_m');
    }

    public function index()
    {
        $username = $this->session->userdata('username');
        $listTemp = $this->db->get_where('ok_hutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $pembelian_id  = $r->pembelian_id;
            $pembayaran    = $r->hutang_temp_bayar;
            $dataPembelian = $this->db->get_where('ok_pembelian', array('pembelian_id' => $pembelian_id))->row();
            $sisahutang    = $dataPembelian->pembelian_sisa_hutang;
            $dibayar       = $dataPembelian->pembelian_dibayar;
            $dataUpdate    = array(
                'pembelian_dibayar'     => ($dibayar - $pembayaran),
                'pembelian_sisa_hutang' => ($sisahutang + $pembayaran),
                'pembelian_update'      => date('Y-m-d H:i:s'),
            );

            $this->db->where('pembelian_id', $pembelian_id);
            $this->db->update('ok_pembelian', $dataUpdate);
        }

        // Hapus Temp by Username dan Tanggal
        $this->db->where('user_username', $username);
        $this->db->delete('ok_hutang_temp');

        $data['meta']        = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listSuplier'] = $this->db->order_by('suplier_nama', 'asc')->get('ok_suplier')->result();
        $this->template->display('admin/hutang/view', $data);
    }

    public function data_list()
    {
        $List = $this->hutang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row       = array();
            $hutang_id = $r->hutang_id;
            $linkedit  = site_url('admin/hutang/editdata/' . $hutang_id);
            $linkprint = site_url('admin/hutang/printfaktur/' . $hutang_id);
            $row[]     = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                          <a onclick="printNota(' . $hutang_id . ')" title="Print Nota"><i class="icon-screen-tablet"></i></a>
                          <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>';
            $row[]  = $no;
            $row[]  = $r->hutang_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->hutang_tanggal));
            $row[]  = $r->suplier_nama;
            $row[]  = $r->suplier_alamat;
            $row[]  = number_format($r->hutang_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->hutang_m->count_all(),
            "recordsFiltered" => $this->hutang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        $username = $this->session->userdata('username');
        $listTemp = $this->db->get_where('ok_hutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $pembelian_id  = $r->pembelian_id;
            $pembayaran    = $r->hutang_temp_bayar;
            $dataPembelian = $this->db->get_where('ok_pembelian', array('pembelian_id' => $pembelian_id))->row();
            $sisahutang    = $dataPembelian->pembelian_sisa_hutang;
            $dibayar       = $dataPembelian->pembelian_dibayar;
            $dataUpdate    = array(
                'pembelian_dibayar'     => ($dibayar - $pembayaran),
                'pembelian_sisa_hutang' => ($sisahutang + $pembayaran),
                'pembelian_update'      => date('Y-m-d H:i:s'),
            );

            $this->db->where('pembelian_id', $pembelian_id);
            $this->db->update('ok_pembelian', $dataUpdate);
        }

        // Hapus Temp by Username dan Tanggal
        $this->db->where('user_username', $username);
        $this->db->delete('ok_hutang_temp');

        $data['meta']     = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listTipe'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/hutang/add', $data);
    }

    public function data_temp_list()
    {
        $List = $this->hutang_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row            = array();
            $hutang_temp_id = $r->hutang_temp_id;
            $row[]          = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $hutang_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $hutang_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->pembelian_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = number_format($r->hutang_temp_total, 0, '', ',');
            $row[]  = number_format($r->hutang_temp_bayar, 0, '', ',');
            $row[]  = number_format($r->hutang_temp_sisa, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->hutang_m->count_tmp_all(),
            "recordsFiltered" => $this->hutang_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_hutang_suplier_list()
    {
        $List = $this->hutang_m->get_suplier_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row        = array();
            $suplier_id = $r->suplier_id;
            $row[]      = '<a href="javascript:void(0)" title="Pilih" onclick="pilihsuplier(' . "'" . $suplier_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]      = $no;
            $row[]      = $r->suplier_nama;
            $row[]      = $r->suplier_alamat;
            $row[]      = $r->suplier_kota;
            $row[]      = number_format($r->totalhutang, 0, '', ',');
            $data[]     = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->hutang_m->count_suplier_all(),
            "recordsFiltered" => $this->hutang_m->count_suplier_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    // Pembelian
    public function data_pembelian_list($id)
    {
        $List = $this->hutang_m->get_pembelian_datatables($id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pembelian_id = $r->pembelian_id;
            $row[]        = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $pembelian_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]        = $no;
            $row[]        = $r->pembelian_no_faktur;
            $row[]        = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]        = number_format($r->pembelian_total, 0, '', ',');
            $row[]        = number_format($r->pembelian_dibayar, 0, '', ',');
            $row[]        = number_format($r->pembelian_sisa_hutang, 0, '', ',');
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->hutang_m->count_pembelian_all($id),
            "recordsFiltered" => $this->hutang_m->count_pembelian_filtered($id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_suplier($id)
    {
        $data = $this->db->get_where('ok_suplier', array('suplier_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_pembelian($id)
    {
        $data = $this->db->get_where('v_pembelian', array('pembelian_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_hutang', array('hutang_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $this->hutang_m->insert_data_item();
    }

    public function updateitem()
    {
        $this->hutang_m->update_data_item();
    }

    public function deleteitem($id)
    {
        $this->hutang_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('hutang_temp_bayar', 'total')->get_where('ok_hutang_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function savedata()
    {
        $this->hutang_m->insert_data_hutang();
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_hutang', array('hutang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_list_item($id)
    {
        $data = $this->db->get_where('v_hutang_detail', array('hutang_id' => $id))->result();
        echo json_encode($data);
    }

    public function editdata($hutang_id)
    {
        $data['detail'] = $this->db->get_where('v_hutang', array('hutang_id' => $hutang_id))->row();
        $this->template->display('admin/hutang/edit', $data);
    }

    public function data_detail_list($hutang_id)
    {
        $List = $this->hutang_m->get_detail_datatables($hutang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->pembelian_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->pembelian_tanggal));
            $row[]  = number_format($r->hutang_detail_total, 0, '', ',');
            $row[]  = number_format($r->hutang_detail_bayar, 0, '', ',');
            $row[]  = number_format($r->hutang_detail_sisa, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->hutang_m->count_detail_all($hutang_id),
            "recordsFiltered" => $this->hutang_m->count_detail_filtered($hutang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($hutang_id)
    {
        $data = $this->db->select_sum('hutang_detail_total', 'total')->get_where('ok_hutang_detail', array('hutang_id' => $hutang_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->hutang_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_hutang_detail', array('hutang_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->hutang_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->hutang_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->hutang_m->update_data_hutang();
    }

    public function savedatasuplier()
    {
        $this->hutang_m->insert_data_suplier();
    }

    public function deletedata($id)
    {
        $this->hutang_m->delete_data_hutang($id);
    }

    public function printfaktur($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_hutang', array('hutang_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_hutang_detail', array('hutang_id' => $id))->result();
        $this->load->view('admin/hutang/printfaktur_v', $data);
    }
}
/* Location: ./application/controller/admin/Hutang.php */

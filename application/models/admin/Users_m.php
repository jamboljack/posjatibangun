<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Users_m extends CI_Model
{
    public $table         = 'ok_users';
    public $column_order  = array(null, null, 'user_username', 'user_name', 'user_email', 'user_level', 'user_status');
    public $column_search = array('user_username', 'user_name', 'user_email', 'user_level', 'user_status');
    public $order         = array('user_name' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('lstLevel', 'true')) {
            $this->db->where('user_level', $this->input->post('lstLevel', 'true'));
        }
        if ($this->input->post('lstStatus', 'true')) {
            $this->db->where('user_status', $this->input->post('lstStatus', 'true'));
        }

        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function insert_data()
    {
        $data = array(
            'user_username'    => trim(stripHTMLtags($this->input->post('username', 'true'))),
            'user_password'    => sha1(trim(stripHTMLtags($this->input->post('password', 'true')))),
            'user_name'        => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
            'user_email'       => trim(stripHTMLtags($this->input->post('email', 'true'))),
            'user_level'       => trim($this->input->post('lstLevel', 'true')),
            'user_date_create' => date('Y-m-d H:i:s'),
            'user_date_update' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_users', $data);
    }

    public function select_by_id($user_username)
    {
        $this->db->select('*');
        $this->db->from('ok_users');
        $this->db->where('user_username', $user_username);

        return $this->db->get();
    }

    public function update_data()
    {
        $user_username = $this->input->post('id', 'true');
        $password      = trim($this->input->post('password', 'true'));

        if (!empty($password)) {
            $data = array(
                'user_password'    => sha1(trim(stripHTMLtags($this->input->post('password', 'true')))),
                'user_name'        => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
                'user_email'       => trim(stripHTMLtags($this->input->post('email', 'true'))),
                'user_level'       => trim($this->input->post('lstLevel', 'true')),
                'user_status'      => $this->input->post('lstStatus', 'true'),
                'user_date_update' => date('Y-m-d H:i:s'),
            );
        } else {
            $data = array(
                'user_name'        => strtoupper(stripHTMLtags($this->input->post('name', 'true'))),
                'user_email'       => trim(stripHTMLtags($this->input->post('email', 'true'))),
                'user_level'       => trim($this->input->post('lstLevel', 'true')),
                'user_status'      => $this->input->post('lstStatus', 'true'),
                'user_date_update' => date('Y-m-d H:i:s'),
            );
        }

        $this->db->where('user_username', $user_username);
        $this->db->update('ok_users', $data);
    }

    public function update_data_akses()
    {
        $akses_id = $this->input->post('id', 'true');
        $data     = array(
            'c01'          => trim(stripHTMLtags($this->input->post('chk01', 'true'))),
            'c02'          => trim(stripHTMLtags($this->input->post('chk02', 'true'))),
            'c03'          => trim(stripHTMLtags($this->input->post('chk03', 'true'))),
            'c04'          => trim(stripHTMLtags($this->input->post('chk04', 'true'))),
            'c05'          => trim(stripHTMLtags($this->input->post('chk05', 'true'))),
            'c06'          => trim(stripHTMLtags($this->input->post('chk06', 'true'))),
            'c11'          => trim(stripHTMLtags($this->input->post('chk11', 'true'))),
            'c12'          => trim(stripHTMLtags($this->input->post('chk12', 'true'))),
            'c13'          => trim(stripHTMLtags($this->input->post('chk13', 'true'))),
            'c21'          => trim(stripHTMLtags($this->input->post('chk21', 'true'))),
            'c22'          => trim(stripHTMLtags($this->input->post('chk22', 'true'))),
            'c23'          => trim(stripHTMLtags($this->input->post('chk23', 'true'))),
            'c24'          => trim(stripHTMLtags($this->input->post('chk24', 'true'))),
            'c25'          => trim(stripHTMLtags($this->input->post('chk25', 'true'))),
            'c26'          => trim(stripHTMLtags($this->input->post('chk26', 'true'))),
            'c27'          => trim(stripHTMLtags($this->input->post('chk27', 'true'))),
            'c31'          => trim(stripHTMLtags($this->input->post('chk31', 'true'))),
            'c32'          => trim(stripHTMLtags($this->input->post('chk32', 'true'))),
            'c41'          => trim(stripHTMLtags($this->input->post('chk41', 'true'))),
            'c42'          => trim(stripHTMLtags($this->input->post('chk42', 'true'))),
            'c43'          => trim(stripHTMLtags($this->input->post('chk43', 'true'))),
            'c44'          => trim(stripHTMLtags($this->input->post('chk44', 'true'))),
            'c45'          => trim(stripHTMLtags($this->input->post('chk45', 'true'))),
            'c46'          => trim(stripHTMLtags($this->input->post('chk46', 'true'))),
            'c51'          => trim(stripHTMLtags($this->input->post('chk51', 'true'))),
            'c52'          => trim(stripHTMLtags($this->input->post('chk52', 'true'))),
            'c61'          => trim(stripHTMLtags($this->input->post('chk61', 'true'))),
            'c62'          => trim(stripHTMLtags($this->input->post('chk62', 'true'))),
            'c63'          => trim(stripHTMLtags($this->input->post('chk63', 'true'))),
            'c64'          => trim(stripHTMLtags($this->input->post('chk64', 'true'))),
            'c65'          => trim(stripHTMLtags($this->input->post('chk65', 'true'))),
            'c66'          => trim(stripHTMLtags($this->input->post('chk66', 'true'))),
            'c67'          => trim(stripHTMLtags($this->input->post('chk67', 'true'))),
            'c68'          => trim(stripHTMLtags($this->input->post('chk68', 'true'))),
            'c69'          => trim(stripHTMLtags($this->input->post('chk69', 'true'))),
            'c71'          => trim(stripHTMLtags($this->input->post('chk71', 'true'))),
            'c72'          => trim(stripHTMLtags($this->input->post('chk72', 'true'))),
            'c73'          => trim(stripHTMLtags($this->input->post('chk73', 'true'))),
            'c81'          => trim(stripHTMLtags($this->input->post('chk81', 'true'))),
            'c82'          => trim(stripHTMLtags($this->input->post('chk82', 'true'))),
            'c83'          => trim(stripHTMLtags($this->input->post('chk83', 'true'))),
            'c84'          => trim(stripHTMLtags($this->input->post('chk84', 'true'))),
            'c85'          => trim(stripHTMLtags($this->input->post('chk85', 'true'))),
            'c91'          => trim(stripHTMLtags($this->input->post('chk91', 'true'))),
            'c92'          => trim(stripHTMLtags($this->input->post('chk92', 'true'))),
            'c93'          => trim(stripHTMLtags($this->input->post('chk93', 'true'))),
            'c94'          => trim(stripHTMLtags($this->input->post('chk94', 'true'))),
            'c95'          => trim(stripHTMLtags($this->input->post('chk95', 'true'))),
            'c101'         => trim(stripHTMLtags($this->input->post('chk101', 'true'))),
            'c102'         => trim(stripHTMLtags($this->input->post('chk102', 'true'))),
            'c20'          => trim(stripHTMLtags($this->input->post('chk20', 'true'))),
            'akses_update' => date('Y-m-d H:i:s'),
        );

        $this->db->where('akses_id', $akses_id);
        $this->db->update('ok_akses', $data);

        // Cek Menu Utama
        $dataAkses = $this->db->get_where('ok_akses', array('akses_id' => $akses_id))->row();
        // Master Umum
        $c01 = $dataAkses->c01;
        $c02 = $dataAkses->c02;
        $c03 = $dataAkses->c03;
        $c04 = $dataAkses->c04;
        $c05 = $dataAkses->c05;
        if ($c01 == 0 && $c02 == 0 && $c03 == 0 && $c04 == 0 & $c05 == 0) {
            $dataUmum = array(
                'c0' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataUmum);
        } else {
            $dataUmum = array(
                'c0' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataUmum);
        }
        // Master Barang
        $c11 = $dataAkses->c11;
        $c12 = $dataAkses->c12;
        $c13 = $dataAkses->c13;
        if ($c11 == 0 && $c12 == 0 && $c13 == 0) {
            $dataBarang = array(
                'c1' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataBarang);
        } else {
            $dataBarang = array(
                'c1' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataBarang);
        }
        // Transaksi
        $c21 = $dataAkses->c21;
        $c22 = $dataAkses->c22;
        $c23 = $dataAkses->c23;
        $c24 = $dataAkses->c24;
        $c25 = $dataAkses->c25;
        $c26 = $dataAkses->c26;
        $c27 = $dataAkses->c27;
        if ($c21 == 0 && $c22 == 0 && $c23 == 0 && $c24 == 0 && $c25 == 0 && $c26 == 0 && $c27 == 0) {
            $dataTransaksi = array(
                'c2' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataTransaksi);
        } else {
            $dataTransaksi = array(
                'c2' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataTransaksi);
        }
        // Users
        $c31 = $dataAkses->c31;
        $c32 = $dataAkses->c32;
        if ($c31 == 0 && $c32 == 0) {
            $dataUsers = array(
                'c3' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataUsers);
        } else {
            $dataUsers = array(
                'c3' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataUsers);
        }
        // Lap. Pembelian
        $c41 = $dataAkses->c41;
        $c42 = $dataAkses->c42;
        $c43 = $dataAkses->c43;
        $c44 = $dataAkses->c44;
        $c45 = $dataAkses->c45;
        $c46 = $dataAkses->c46;
        if ($c41 == 0 && $c42 == 0 && $c43 == 0 && $c44 == 0 && $c45 == 0 && $c46 == 0) {
            $dataLapBeli = array(
                'c4' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapBeli);
        } else {
            $dataLapBeli = array(
                'c4' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapBeli);
        }
        // Lap. Retur Pembelian
        $c51 = $dataAkses->c51;
        $c52 = $dataAkses->c52;
        if ($c51 == 0 && $c52 == 0) {
            $dataLapReturBeli = array(
                'c5' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapReturBeli);
        } else {
            $dataLapReturBeli = array(
                'c5' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapReturBeli);
        }
        // Lap. Penjualan
        $c61 = $dataAkses->c61;
        $c62 = $dataAkses->c62;
        $c63 = $dataAkses->c63;
        $c64 = $dataAkses->c64;
        $c65 = $dataAkses->c65;
        $c66 = $dataAkses->c66;
        $c67 = $dataAkses->c67;
        $c68 = $dataAkses->c68;
        $c69 = $dataAkses->c69;
        if ($c61 == 0 && $c62 == 0 && $c63 == 0 && $c64 == 0 && $c65 == 0 && $c66 == 0 && $c67 == 0 && $c68 == 0 && $c69 == 0) {
            $dataLapPenjualan = array(
                'c6' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapPenjualan);
        } else {
            $dataLapPenjualan = array(
                'c6' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapPenjualan);
        }
        // Lap. Retur Penjualan
        $c71 = $dataAkses->c71;
        $c72 = $dataAkses->c72;
        $c73 = $dataAkses->c73;
        if ($c71 == 0 && $c72 == 0 && $c73 == 0) {
            $dataLapReturPenjualan = array(
                'c7' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapReturPenjualan);
        } else {
            $dataLapReturPenjualan = array(
                'c7' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapReturPenjualan);
        }
        // Lap. Hutang
        $c81 = $dataAkses->c81;
        $c82 = $dataAkses->c82;
        $c83 = $dataAkses->c83;
        $c84 = $dataAkses->c84;
        $c85 = $dataAkses->c85;
        if ($c81 == 0 && $c82 == 0 && $c83 == 0 && $c84 == 0 && $c85 == 0) {
            $dataLapHutang = array(
                'c8' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapHutang);
        } else {
            $dataLapHutang = array(
                'c8' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapHutang);
        }
        // Lap. Piutang
        $c91 = $dataAkses->c91;
        $c92 = $dataAkses->c92;
        $c93 = $dataAkses->c93;
        $c94 = $dataAkses->c94;
        $c95 = $dataAkses->c95;
        if ($c91 == 0 && $c92 == 0 && $c93 == 0 && $c94 == 0 && $c95 == 0) {
            $dataLapPiutang = array(
                'c9' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapPiutang);
        } else {
            $dataLapPiutang = array(
                'c9' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapPiutang);
        }
        // Lap. Barang
        $c101 = $dataAkses->c101;
        $c102 = $dataAkses->c102;
        if ($c101 == 0 && c102 == 0) {
            $dataLapBarang = array(
                'c10' => 0,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapBarang);
        } else {
            $dataLapBarang = array(
                'c10' => 1,
            );

            $this->db->where('akses_id', $akses_id);
            $this->db->update('ok_akses', $dataLapBarang);
        }
    }
}
/* Location: ./application/model/admin/Users_m.php */

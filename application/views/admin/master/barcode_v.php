<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Cetak Barcode</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Cetak Barcode</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Barang
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData">
                                <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Filter Data</button>
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th width="15%">Kode</th>
                                    <th>Nama Barang</th>
                                    <th width="10%">Stok</th>
                                    <th width="15%">Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Cetak Barcode
                        </div>
                        <div class="actions">
                            <a class="btn btn-danger btn-xs" onclick="resetData()" href="javascript:;">
                                <i class="fa fa-refresh"></i> Reset Data
                            </a>
                            <div class="btn-group">
                                <button type="button" class="btn btn-fit-height btn-success dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"><i class="icon-printer"></i> Print <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="javascript:;" onclick="printData()"><i class="fa fa-barcode"></i> Barcode</a></li>
                                    <li><a href="javascript:;" onclick="printDataNon()"><i class="fa fa-barcode"></i> Barcode Tanpa Harga</a></li>
                                    <li><a href="javascript:;" onclick="printDataRak()"><i class="fa fa-book"></i> Rak</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableDataBarcode">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th width="15%">Kode</th>
                                    <th>Nama Barang</th>
                                    <th width="15%">Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.number').maskMoney({thousands:',', precision:0});
});

function reload_table() {
    table.ajax.reload(null,false);
    tableBarcode.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/barcode/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstKategoriFilter = $('#lstKategoriFilter').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 5 ],
                "className": "text-right",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

var tableBarcode;
$(document).ready(function() {
    tableBarcode = $('#tableDataBarcode').DataTable({
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [20, 50, 75, 100, -1],
                [20, 50, 75, 100, "All"]
        ],
        "pageLength": 20,
        "ajax": {
            "url": "<?=site_url('admin/barcode/data_barcode_list')?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 4 ],
                "className": "text-right",
            }
        ],
    });
});

function hapusData(barcode_id) {
    var id = barcode_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Data ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/barcode/deletedata')?>/"+id,
            type: "POST",
            success: function(data) {
                swal({
                    title:"Sukses",
                    text: "Hapus Data Sukses",
                    showConfirmButton: false,
                    type: "success",
                    timer: 2000
                });
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Data Gagal');
            }
        });
    });
}

function addData(id) {
    $('#formAdd')[0].reset();
    $.ajax({
        url : "<?=site_url('admin/barcode/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.barang_id);
            $('#kode').val(data.barang_kode);
            $('#nama').val(data.barang_nama);
            $('#formModalAdd').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formAdd');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formAdd").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            jumlah: { required: true }
        },
        messages: {
            jumlah: { required :'Jumlah required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formAdd").serialize();
            $.ajax({
                url: '<?=site_url('admin/barcode/savedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    $('#formModalAdd').modal('hide');
                    reload_table();
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Proses Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalAdd').modal('hide');
                }
            });
        }
    });
});

function resetData() {
    swal({
        title: 'Anda Yakin ?',
        text: 'Data ini akan di Reset !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/barcode/deletedataall')?>",
            type: "POST",
            success: function(data) {
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Data Gagal');
            }
        });
    });
}

function printData() {
    var url = "<?=site_url('admin/barcode/printbarcode');?>";
    window.open(url, "_blank");
}

function printDataRak() {
    var url = "<?=site_url('admin/barcode/printrak');?>";
    window.open(url, "_blank");
}

function printDataNon() {
    var url = "<?=site_url('admin/barcode/printnonharga');?>";
    window.open(url, "_blank");
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Kategori</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstKategoriFilter" id="lstKategoriFilter">
                                <option value="">- SEMUA DATA -</option>
                                <?php foreach ($listKategori as $r) { ?>
                                <option value="<?=$r->kategori_id;?>"><?=$r->kategori_nama;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formAdd" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Form Tambah Cetak Barcode</h4>
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Kode Barang</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="kode" id="kode" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-4 control-label">Nama Barang</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="nama" id="nama" autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Jumlah</label>
                        <div class="col-md-4">
                            <input type="text" class="form-control number" name="jumlah" id="jumlah" placeholder="0" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Tambahkan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_hutang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_hutang_m');
    }

    public function index()
    {
        $this->template->display('admin/reporthutang/reporthutang_v');
    }

    public function data_list()
    {
        $List = $this->lap_hutang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->suplier_nama;
            $row[]  = $r->suplier_alamat . ' - ' . $r->suplier_kota;
            $row[]  = $r->suplier_telp;
            $row[]  = number_format($r->total, 0, '', ',');
            $row[]  = number_format($r->totalbayar, 0, '', ',');
            $row[]  = number_format($r->totalhutang, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_hutang_m->count_all(),
            "recordsFiltered" => $this->lap_hutang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printhutang()
    {
        $data['header']   = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['listData'] = $this->db->order_by('suplier_nama', 'asc')->get('v_hutang_suplier')->result();
        $this->load->view('admin/reporthutang/printhutang_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_hutang.php */

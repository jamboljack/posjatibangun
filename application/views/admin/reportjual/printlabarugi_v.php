<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Laporan Laba/Rugi</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:16px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<?php 
if ($this->uri->segment(4) != 'all' && $this->uri->segment(5) != 'all') {
    $periode = 'PERIODE : '.$this->uri->segment(4).' s/d '.$this->uri->segment(5);
} else {
    $periode = '';
}

$pelanggan_id = $this->uri->segment(6);
if ($pelanggan_id != 'all') {
    $dataPelanggan = $this->db->get_where('ok_pelanggan', array('pelanggan_id' => $pelanggan_id))->row();
    $pelanggan     = 'PELANGGAN : '.$dataPelanggan->pelanggan_nama;
} else {
    $pelanggan     = 'SEMUA PELANGGAN';
}
?>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="font-size: 15px; font-weight: bold;"><u>LAPORAN LABA/RUGI</u></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?=$periode.'<br>'.$pelanggan;?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <th width="15%" style="border-left: 0.5px solid black; border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO. FAKTUR</th>
            <th width="30%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NAMA BARANG</th>
            <th width="15%" colspan="2" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">JUMLAH</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">HPP</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">OMSET</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TOTAL L/R</th>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">L/R (%)</th>
        </tr>
        <?php 
        $dari           = date('Y-m-d', strtotime($this->uri->segment(4)));
        $sampai         = date('Y-m-d', strtotime($this->uri->segment(5)));
        $totalhppfoot   = 0;
        $totalomsetfoot = 0;
        $totallr        = 0;
        $totalpersenfoot= 0;
        for($i=$dari;$i<=$sampai;$i++) {
            $tanggal  = $i;
            if ($pelanggan_id != 'all') {
                $listData = $this->db->order_by('penjualan_no_faktur', 'asc')->get_where('v_labarugi', array('penjualan_tanggal' => $i, 'pelanggan_id' => $pelanggan_id))->result();
            } else {
                $listData = $this->db->order_by('penjualan_no_faktur', 'asc')->get_where('v_labarugi', array('penjualan_tanggal' => $i))->result();
            }

            if (count($listData) > 0) {
        ?>
        <tr>
            <th colspan="9" style="border-left: 0.5px solid black; border-right: 0.5px solid black;">Tanggal : <?=date('d-m-Y', strtotime($i));?></th>
        </tr>
        <?php
            $no_faktur    = '';
            $totalpokok   = 0;
            $totalomset   = 0;
            $totalselisih = 0;
            $totalpersen  = 0;
            foreach($listData as $r) { 
                $persen      = @(($r->omset - $r->harga_pokok)/$r->harga_pokok)*100;
                if ($r->penjualan_no_faktur==$no_faktur) {
                    $faktur  = '';
                } else {
                    $faktur  = $r->penjualan_no_faktur;
                }
        ?>
        <tr>
            <td style="border-left: 0.5px solid black;  border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$faktur;?></td>
            <td style="border-top: 0.5px solid black; border-right: 0.5px solid black; border-bottom: 0.5px solid black;"><?=$r->barang_nama;?></td>
            <td align="right" width="8" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black;"><?=number_format($r->penjualan_detail_qty,2,'.',',');?></td>
            <td align="right" width="7" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->unit_nama;?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->harga_pokok,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->omset,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($r->selisih,0,'',',');?></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($persen,2,'.',',');?> %</td>
        </tr>
        <?php
                $no_faktur      = $r->penjualan_no_faktur;
                $totalpokok     = ($totalpokok+$r->harga_pokok);
                $totalomset     = ($totalomset+$r->omset);
                $totalselisih   = ($totalselisih+$r->selisih);
                $totalhppfoot   = ($totalhppfoot+$r->harga_pokok);
                $totalomsetfoot = ($totalomsetfoot+$r->omset);
                $totallr        = ($totallr+$r->selisih);
            }
            
            $totalpersen    = (($totalomset - $totalpokok)/$totalpokok)*100;
        ?>
        <tr>
            <td colspan="4" align="center" style="border-left: 0.5px solid black;  border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b>SUB TOTAL : <?=date('d-m-Y', strtotime($i));?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalpokok,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalomset,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalselisih,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalpersen,2,'.',',');?> %</b></td>
        </tr>
        <?php
            }
        }

        $totalpersenfoot = @(($totalomsetfoot - $totalhppfoot)/$totalhppfoot)*100;
        ?>
        <tr>
            <td colspan="4" align="center" style="border-left: 0.5px solid black;  border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b>TOTAL</b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalhppfoot,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalomsetfoot,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totallr,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalpersenfoot,2,'.',',');?> %</b></td>
        </tr>
    </table>
</div>
</body>
</html>
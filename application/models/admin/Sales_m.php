<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales_m extends CI_Model
{
    public $table         = 'ok_sales';
    public $column_order  = array(null, null, 'sales_tgl_kerja', 'sales_kode', 'sales_nama', 'sales_alamat', 'sales_telp', 'sales_wilayah');
    public $column_search = array('sales_kode', 'sales_nama', 'sales_alamat', 'sales_telp', 'sales_wilayah');
    public $order         = array('sales_nama' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->table);

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // public function getkodeurut()
    // {
    //     $this->db->select('sales_kode as kode', false);
    //     $this->db->limit(1);
    //     $this->db->order_by('sales_kode', 'desc');
    //     $query = $this->db->get('ok_sales');
    //     if ($query->num_rows() != 0) {
    //         $data = $query->row();
    //         $kode = intval($data->kode) + 1;
    //     } else {
    //         $kode = 1;
    //     }
    //     $kodebarang = str_pad($kode, 5, "0", STR_PAD_LEFT);
    //     return $kodebarang;
    // }

    public function insert_data()
    {
        // $kode = $this->getkodeurut();
        $data = array(
            'sales_kode'      => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
            'sales_tgl_kerja' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'sales_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'sales_alamat'    => strtoupper(trim(stripHTMLtags($this->input->post('alamat', 'true')))),
            'sales_kota'      => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'sales_telp'      => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'sales_wilayah'   => strtoupper(trim(stripHTMLtags($this->input->post('wilayah', 'true')))),
            'sales_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_sales', $data);
    }

    public function select_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('ok_sales');
        $this->db->where('sales_id', $id);

        return $this->db->get();
    }

    public function update_data()
    {
        $sales_id = $this->input->post('id', 'true');
        $data     = array(
            'sales_tgl_kerja' => date('Y-m-d', strtotime($this->input->post('tanggal'))),
            'sales_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
            'sales_alamat'    => strtoupper(trim(stripHTMLtags($this->input->post('alamat', 'true')))),
            'sales_kota'      => strtoupper(trim(stripHTMLtags($this->input->post('kota', 'true')))),
            'sales_telp'      => strtoupper(trim(stripHTMLtags($this->input->post('telp', 'true')))),
            'sales_wilayah'   => strtoupper(trim(stripHTMLtags($this->input->post('wilayah', 'true')))),
            'sales_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('sales_id', $sales_id);
        $this->db->update('ok_sales', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('sales_id', $id);
        $this->db->delete('ok_sales');
    }
}
/* Location: ./application/models/admin/Sales_m.php */

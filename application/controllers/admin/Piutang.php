<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/piutang_m');
    }

    public function index()
    {
        $username = $this->session->userdata('username');
        // Update Pembayaran Hutang
        $listTemp = $this->db->get_where('ok_piutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $penjualan_id  = $r->penjualan_id;
            $pembayaran    = $r->piutang_temp_bayar;
            $dataPembelian = $this->db->get_where('ok_penjualan', array('penjualan_id' => $penjualan_id))->row();
            $sisapiutang   = $dataPembelian->penjualan_sisa_piutang;
            $dibayar       = $dataPembelian->penjualan_dibayar;
            $dataUpdate    = array(
                'penjualan_dibayar'      => ($dibayar - $pembayaran),
                'penjualan_sisa_piutang' => ($sisapiutang + $pembayaran),
                'penjualan_update'       => date('Y-m-d H:i:s'),
            );

            $this->db->where('penjualan_id', $penjualan_id);
            $this->db->update('ok_penjualan', $dataUpdate);
        }

        // Hapus Temp
        $this->db->where('user_username', $username);
        $this->db->delete('ok_piutang_temp');

        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $data['listTipeBayar'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/piutang/view', $data);
    }

    public function data_list()
    {
        $List = $this->piutang_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row        = array();
            $piutang_id = $r->piutang_id;
            $linkedit   = site_url('admin/piutang/editdata/' . $piutang_id);
            $linkprint  = site_url('admin/piutang/printfaktur/' . $piutang_id);
            $row[]      = '<a href="' . $linkedit . '" title="Edit Data"><i class="icon-pencil"></i></a>
                          <a onclick="printNota(' . $piutang_id . ')" title="Print Nota"><i class="icon-screen-tablet"></i></a>
                          <a href="' . $linkprint . '" title="Print Faktur" target="_blank"><i class="icon-printer"></i></a>';
            $row[]  = $no;
            $row[]  = $r->piutang_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->piutang_tanggal));
            $row[]  = $r->pelanggan_nama;
            $row[]  = $r->pelanggan_alamat;
            $row[]  = number_format($r->piutang_total, 0, '', ',');
            $row[]  = $r->tipe_bayar_nama;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->piutang_m->count_all(),
            "recordsFiltered" => $this->piutang_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function adddata()
    {
        $username = $this->session->userdata('username');
        // Update Pembayaran Hutang
        $listTemp = $this->db->get_where('ok_piutang_temp', array('user_username' => $username))->result();
        foreach ($listTemp as $r) {
            $penjualan_id  = $r->penjualan_id;
            $pembayaran    = $r->piutang_temp_bayar;
            $dataPembelian = $this->db->get_where('ok_penjualan', array('penjualan_id' => $penjualan_id))->row();
            $sisapiutang   = $dataPembelian->penjualan_sisa_piutang;
            $dibayar       = $dataPembelian->penjualan_dibayar;
            $dataUpdate    = array(
                'penjualan_dibayar'      => ($dibayar - $pembayaran),
                'penjualan_sisa_piutang' => ($sisapiutang + $pembayaran),
                'penjualan_update'       => date('Y-m-d H:i:s'),
            );

            $this->db->where('penjualan_id', $penjualan_id);
            $this->db->update('ok_penjualan', $dataUpdate);
        }

        // Hapus Temp
        $this->db->where('user_username', $username);
        $this->db->delete('ok_piutang_temp');

        $data['meta']          = $this->db->get_where('ok_meta', array('meta_id' => 1))->row();
        $data['listTipeBayar'] = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/piutang/add', $data);
    }

    public function data_temp_list()
    {
        $List = $this->piutang_m->get_tmp_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row             = array();
            $piutang_temp_id = $r->piutang_temp_id;
            $row[]           = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $piutang_temp_id . "'" . ')"><i class="icon-pencil"></i></a>
                            <a onclick="hapusData(' . $piutang_temp_id . ')" title="Hapus Data"><i class="icon-close"></i></a>';
            $row[]  = $no;
            $row[]  = $r->penjualan_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]  = number_format($r->piutang_temp_total, 0, '', ',');
            $row[]  = number_format($r->piutang_temp_bayar, 0, '', ',');
            $row[]  = number_format($r->piutang_temp_sisa, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->piutang_m->count_tmp_all(),
            "recordsFiltered" => $this->piutang_m->count_tmp_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function data_piutang_pelanggan_list()
    {
        $List = $this->piutang_m->get_pelanggan_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pelanggan_id = $r->pelanggan_id;
            $row[]        = '<a href="javascript:void(0)" title="Pilih" onclick="pilihpelanggan(' . "'" . $pelanggan_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]        = $no;
            $row[]        = $r->pelanggan_nama;
            $row[]        = $r->pelanggan_alamat;
            $row[]        = $r->pelanggan_kota;
            $row[]        = number_format($r->totalpiutang, 0, '', ',');
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->piutang_m->count_pelanggan_all(),
            "recordsFiltered" => $this->piutang_m->count_pelanggan_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    // Pembelian
    public function data_penjualan_list($id)
    {
        $List = $this->piutang_m->get_penjualan_datatables($id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row          = array();
            $penjualan_id = $r->penjualan_id;
            $row[]        = '<a href="javascript:void(0)" title="Pilih" onclick="pilihdata(' . "'" . $penjualan_id . "'" . ')"><i class="icon-check"></i></a>';
            $row[]        = $no;
            $row[]        = $r->penjualan_no_faktur;
            $row[]        = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]        = number_format($r->penjualan_total, 0, '', ',');
            $row[]        = number_format($r->penjualan_dibayar, 0, '', ',');
            $row[]        = number_format($r->penjualan_sisa_piutang, 0, '', ',');
            $data[]       = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->piutang_m->count_penjualan_all($id),
            "recordsFiltered" => $this->piutang_m->count_penjualan_filtered($id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_pelanggan($id)
    {
        $data = $this->db->get_where('ok_pelanggan', array('pelanggan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_penjualan($id)
    {
        $data = $this->db->get_where('v_penjualan', array('penjualan_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data_item($id)
    {
        $data = $this->db->get_where('v_tmp_piutang', array('piutang_temp_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_data($id)
    {
        $data = $this->db->get_where('v_piutang', array('piutang_id' => $id))->row();
        echo json_encode($data);
    }

    public function get_list_item($id)
    {
        $data = $this->db->get_where('v_piutang_detail', array('piutang_id' => $id))->result();
        echo json_encode($data);
    }

    public function saveitem()
    {
        $this->piutang_m->insert_data_item();
    }

    public function updateitem()
    {
        $this->piutang_m->update_data_item();
    }

    public function deleteitem($id)
    {
        $this->piutang_m->delete_data_item($id);
    }

    public function get_data_total_temp()
    {
        $username = $this->session->userdata('username');
        $data     = $this->db->select_sum('piutang_temp_bayar', 'total')->get_where('ok_piutang_temp', array('user_username' => $username))->row();
        echo json_encode($data);
    }

    public function savedata()
    {
        $this->piutang_m->insert_data_piutang();
    }

    public function editdata($piutang_id)
    {
        $data['detail'] = $this->db->get_where('v_piutang', array('piutang_id' => $piutang_id))->row();
        $this->template->display('admin/piutang/edit', $data);
    }

    public function data_detail_list($piutang_id)
    {
        $List = $this->piutang_m->get_detail_datatables($piutang_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->penjualan_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]  = number_format($r->piutang_detail_total, 0, '', ',');
            $row[]  = number_format($r->piutang_detail_bayar, 0, '', ',');
            $row[]  = number_format($r->piutang_detail_sisa, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->piutang_m->count_detail_all($piutang_id),
            "recordsFiltered" => $this->piutang_m->count_detail_filtered($piutang_id),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function get_data_total_detail($piutang_id)
    {
        $data = $this->db->select_sum('piutang_detail_total', 'total')->get_where('ok_piutang_detail', array('piutang_id' => $piutang_id))->row();
        echo json_encode($data);
    }

    public function saveitemdetail()
    {
        $this->piutang_m->insert_data_detail();
    }

    public function get_data_detail($id)
    {
        $data = $this->db->get_where('v_piutang_detail', array('piutang_detail_id' => $id))->row();
        echo json_encode($data);
    }

    public function updateitemdetail()
    {
        $this->piutang_m->update_data_detail();
    }

    public function deleteitemdetail($id)
    {
        $this->piutang_m->delete_data_detail($id);
    }

    public function updatedata()
    {
        $this->piutang_m->update_data_piutang();
    }

    public function savedatapelanggan()
    {
        $this->piutang_m->insert_data_pelanggan();
    }

    public function deletedata($id)
    {
        $this->piutang_m->delete_data_piutang($id);
    }

    public function printfaktur($id)
    {
        $data['header']     = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $data['detail']     = $this->db->get_where('v_piutang', array('piutang_id' => $id))->row();
        $data['listDetail'] = $this->db->get_where('v_piutang_detail', array('piutang_id' => $id))->result();
        $this->load->view('admin/piutang/printfaktur_v', $data);
    }
}
/* Location: ./application/controller/admin/Piutang.php */

<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Jam Kerja</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Config</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Jam Kerja</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Jam Kerja
                        </div>
                        <div class="actions">
                            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#formModalAdd">
                                <i class="fa fa-plus-circle"></i> Tambah
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th>Hari</th>
                                    <th width="10%">Jam Masuk</th>
                                    <th width="10%">Jam Pulang</th>
                                    <th width="5%">Libur</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/jam_kerja/data_list');?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3, 4, 5 ],
                "className": "text-center",
            }
        ],
    });
});

$(document).ready(function() {
    var form        = $('#formInput');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formInput").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama: { required: true, 
                remote: {
                    url: "<?=site_url('admin/jam_kerja/register_nama_exists');?>",
                    type: "post",
                    data: {
                        nama: function() {
                            return $("#nama").val();
                        }
                    }
                } 
            },
            lstLibur: { required: true }
        },
        messages: {            
            nama: { required :'Hari Jam Kerja required', remote:'Hari Jam Kerja sudah Ada' },
            lstLibur: { required :'Status Libur required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formInput").serialize();
            $.ajax({
                url: '<?=site_url('admin/jam_kerja/savedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal({
                        title:"Sukses",
                        text: "Simpan Data Sukses",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                    reload_table();
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Simpan Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalAdd').modal('hide');
                    resetformInput();
                }
            });
        }
    });
});

function resetformInput() {
    $("#nama").val('');
    $("#masuk").val('');
    $("#pulang").val('');
    $("#lstLibur").val('');

    var MValid = $("#formInput");
    MValid.validate().resetForm();
    MValid.find(".has-error").removeClass("has-error");
    MValid.removeAttr('aria-describedby');
    MValid.removeAttr('aria-invalid');
}

function edit_data(id) {
    $('#formEdit')[0].reset();
    $.ajax({
        url : "<?=site_url('admin/jam_kerja/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.jam_kerja_id);
            $('#jam_kerja_hari').val(data.jam_kerja_hari);
            $('#jam_kerja_masuk').val(data.jam_kerja_masuk);
            $('#jam_kerja_pulang').val(data.jam_kerja_pulang);
            $('#jam_kerja_libur').val(data.jam_kerja_libur);
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formEdit');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formEdit").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama: { required: true },
            lstLibur: { required: true }
        },
        messages: {            
            nama: { required :'Hari Jam Kerja required' },
            lstLibur: { required :'Status Libur required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formEdit").serialize();
            $.ajax({
                url: '<?=site_url('admin/jam_kerja/updatedata');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    swal({
                        title:"Sukses",
                        text: "Update Data Sukses",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "success"
                    });
                    $('#formModalEdit').modal('hide');
                    reload_table();
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Update Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalEdit').modal('hide');
                }
            });
        }
    });
});

function hapusData(jam_kerja_id) {
    var id = jam_kerja_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Data ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/jam_kerja/deletedata')?>/"+id,
            type: "POST",
            success: function(data) {
                swal({
                    title:"Sukses",
                    text: "Hapus Data Sukses",
                    showConfirmButton: false,
                    type: "success",
                    timer: 2000
                });
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Data Gagal');
            }
        });
    });
}
</script>

<div class="modal" id="formModalAdd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formInput" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-plus-circle"></i> Form Tambah Jam Kerja</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Hari Jam Kerja</label>
                        <input type="text" class="form-control" placeholder="Input Hari Jam Kerja" name="nama" id="nama" autocomplete="off">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Jam Masuk</label>
                                <input type="text" class="form-control" style="text-align:center;" placeholder="00:00" name="masuk" id="masuk" autocomplete="off">
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Jam Pulang</label>
                                <input type="text" class="form-control" style="text-align:center;" placeholder="00:00" name="pulang" id="pulang" autocomplete="off">
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Libur</label>
                                <select class="form-control" name="lstLibur" id="lstLibur">
                                    <option value="">- Status Libur -</option>
                                    <option value="Y">YA</option>
                                    <option value="T">TIDAK</option>
                                </select>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Simpan</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formEdit" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-edit"></i> Form Edit Jam Kerja</h4>
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Hari Jam Kerja</label>
                        <input type="text" class="form-control" placeholder="Input Hari Jam Kerja" name="nama" id="jam_kerja_hari" autocomplete="off">
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Jam Masuk</label>
                                <input type="text" class="form-control" style="text-align:center;" placeholder="00:00" name="masuk" id="jam_kerja_masuk" autocomplete="off">
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Jam Pulang</label>
                                <input type="text" class="form-control" style="text-align:center;" placeholder="00:00" name="pulang" id="jam_kerja_pulang" autocomplete="off">
                            </div>  
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label">Libur</label>
                                <select class="form-control" name="lstLibur" id="jam_kerja_libur">
                                    <option value="">- Status Libur -</option>
                                    <option value="Y">YA</option>
                                    <option value="T">TIDAK</option>
                                </select>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Update</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<link href="<?=base_url();?>backend/js/sweetalert2.css" rel="stylesheet" type="text/css" />
<script src="<?=base_url();?>backend/js/sweetalert2.min.js"></script>
<script src="<?=base_url('backend/js/jquery.maskMoney.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Pembayaran Piutang</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Transaksi</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="<?=site_url('admin/piutang');?>">Pembayaran Piutang</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Tambah Pembayaran Piutang</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Form Data Pelanggan 
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formInput" name="formInput">
                                <input type="hidden" name="pelanggan_id" id="pelanggan_id">

                                    <div class="form-body">
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Tanggal/Hari</label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?=date('d-m-Y');?>" readonly>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="tanggal" id="tanggal" value="<?=getDay(date('Y-m-d'));?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Nama Pelanggan</label>
                                            <div class="col-md-9">
                                                <div class="input-group" style="text-align:left">
                                                    <div class="input-icon right">
                                                    <i class="fa"></i>
                                                        <input type="text" class="form-control" name="nama_pelanggan" id="nama_pelanggan" autocomplete="off" placeholder="Cari Nama Pelanggan" autofocus>
                                                    </div>
                                                    <span class="input-group-btn">
                                                        <a id="btn-pelanggan" class="btn green" title="Cari Data Pelanggan"><i class="fa fa-search"></i></a>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input">
                                            <label class="col-md-3 control-label">Alamat</label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="alamat" id="alamat" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tipe Bayar</label>
                                            <div class="col-md-9">
                                                <div class="input-icon right">
                                                    <i class="fa"></i>
                                                    <select class="form-control" name="lstTipeBayar" id="lstTipeBayar">
                                                        <option value="">- Pilih Tipe Bayar -</option>
                                                        <?php foreach ($listTipeBayar as $r) { ?>
                                                        <option value="<?=$r->tipe_bayar_id;?>"><?=$r->tipe_bayar_nama;?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet box blue-madison">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-plus-circle"></i> Total Pembayaran
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form" class="form-horizontal form" method="post" id="formTotal" name="formTotal">
                                <input type="hidden" name="totaltransaksi" id="totaltransaksi">
                                    <div class="form-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group form-md-line-input">
                                                    <label class="col-md-4 control-label">User :</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="form-control" name="kasir" value="<?=$this->session->userdata('nama');?>" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-12 value" id="totalpembayaran"></div>
                                        </div>
                                    </div>
                                    <br><br>
                                </form>
                            </div>
                        </div>
                    </div> 
                </div>   
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pembayaran Piutang 
                        </div>
                    </div>

                    <div class="portlet-body form">
                        <form method="post" id="formBeli" role="form" name="formBeli" class="form">
                        <input type="hidden" name="penjualan_id" id="penjualan_id">
                        <input type="hidden" name="piutang_temp_id" id="piutang_temp_id">
                        <input type="hidden" name="bayar_piutang_lama" id="bayar_piutang_lama">

                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>No. Faktur</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="no_faktur" id="no_faktur" placeholder="Cari No. Faktur" autocomplete="off">
                                                <span class="input-group-addon">
                                                    <a data-toggle="modal" data-target="#formDataPiutang" title="Cari Data Penjualan">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Tanggal</label>
                                            <input type="text" class="form-control number" name="tanggalbeli" id="tanggalbeli" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Jumlah Piutang</label>
                                            <input type="text" class="form-control" placeholder="0" name="jumlahpiutang" id="jumlahpiutang" readonly>
                                        </div>
                                    </div>                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Jumlah Bayar</label>
                                            <div class="input-icon right">
                                                <i class="fa"></i>
                                                <input type="text" class="form-control number" name="bayarpiutang" id="bayarpiutang" autocomplete="off" placeholder="0" onkeyup="hitungSisa()" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Sisa</label>
                                            <input type="text" class="form-control" name="sisa" id="sisa" placeholder="0" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions" align="center">
                                <button class="btn btn-primary" id="btn_item" name="btn_item" disabled><i class="fa fa-floppy-o"></i> Simpan Pembayaran</button>
                                <a onclick="resetForm()" class="btn btn-danger" id="btn_reset" name="btn_reset" disabled><i class="fa fa-refresh"></i> Reset</a>
                                <button type="button" class="btn btn-primary" id="btn_simpan" name="btn_simpan" disabled><i class="fa fa-floppy-o"></i> Simpan Transaksi</button>
                                <a href="<?=site_url('admin/piutang');?>" type="button" class="btn btn-warning"><i class="fa fa-times"></i> Batal</a>
                            </div>
                        </form>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="5%"></th>
                                    <th width="5%">No</th>
                                    <th>No. Faktur</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="15%">Jumlah Piutang</th>
                                    <th width="15%">Jumlah Bayar</th>
                                    <th width="15%">Sisa</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?=base_url();?>backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#formCariPelanggan').on('shown.bs.modal', function () {
       var table = $('#tableDataPelanggan').DataTable();
       table.columns.adjust();
    });

    $('#formDataPiutang').on('shown.bs.modal', function () {
       var table = $('#tableDataPiutang').DataTable();
       table.columns.adjust();
    });
});

var statusinput;
statusinput = 'Tambah';

$("#btn-pelanggan").click(function() {
    $('#formCariPelanggan').modal('show');
});

$(document).ready(function() {
    $('.number').maskMoney({thousands:',', precision:0});
    $('.digit').maskMoney({thousands:'', precision:2});
    hitungTotal();
    dataPelanggan();
    var id = '99999999';
    dataPiutang(id);
    document.getElementById("btn_simpan").style.pointerEvents = "none";
});

function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "paging": false,
        "info": false,
        "searching": false,
        "destoy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/piutang/data_temp_list')?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [ 0, 1, 2, 3, 4, 5, 6 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 5, 6 ],
                "className": "text-right",
            }
        ],
    });    
});

function dataPelanggan() {
    var tablePelanggan;
    tablePelanggan = $('#tableDataPelanggan').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/piutang/data_piutang_pelanggan_list'); ?>",
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1 ],
                "className": "text-center",
            },
            {
                "targets": [ 5 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihpelanggan(id) {
    $.ajax({
        url : "<?=site_url('admin/piutang/get_data_pelanggan/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#pelanggan_id').val(data.pelanggan_id);
            $('#nama_pelanggan').val(data.pelanggan_nama);
            $('#alamat').val(data.pelanggan_alamat);
            $("#btn_simpan").attr("disabled", false);
            document.getElementById("btn_simpan").style.pointerEvents = "auto";
            $('#formCariPelanggan').modal('hide');
            dataPiutang(id);
            $('#no_faktur').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function dataPiutang(id) {
    var tablePiutang;
    tablePiutang = $('#tableDataPiutang').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "ajax": {
            "url": "<?=site_url('admin/piutang/data_penjualan_list/'); ?>"+id,
            "type": "POST"
        },
        "columnDefs": [
            {
                "targets": [0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3 ],
                "className": "text-center",
            },
            {
                "targets": [ 4, 5, 6 ],
                "className": "text-right",
            }
        ],
    });
}

function pilihdata(id) {
    $.ajax({
        url : "<?=site_url('admin/piutang/get_data_penjualan/'); ?>" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale          = 'en';
            var options         = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter       = new Intl.NumberFormat(locale, options);
            $('#penjualan_id').val(data.penjualan_id);
            $('#no_faktur').val(data.penjualan_no_faktur);
            $('#tanggalbeli').val((data.penjualan_tanggal).split('-').reverse().join('-'));
            $('#jumlahpiutang').val(formatter.format(data.penjualan_sisa_piutang));
            $('#sisa').val(formatter.format(data.penjualan_sisa_piutang));
            document.formBeli.bayarpiutang.disabled=false;
            document.formBeli.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
            $('#formDataPiutang').modal('hide');
            $('#bayarpiutang').focus();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hitungSisa() {
    var locale       = 'en';
    var options      = {minimumFractionDigits: 0, maximumFractionDigits: 0};
    var formatter    = new Intl.NumberFormat(locale, options);
    var myForm       = document.formBeli;
    var JumlahPiutang = myForm.jumlahpiutang.value;
    JumlahPiutang     = JumlahPiutang.replace(/[,]/g, ''); // Ini String
    JumlahPiutang     = parseInt(JumlahPiutang); // Ini Integer
    var Bayar        = myForm.bayarpiutang.value;
    Bayar            = Bayar.replace(/[,]/g, ''); // Ini String
    Bayar            = parseInt(Bayar); // Ini Integer
    if (Bayar > 0) {
        if (Bayar > JumlahPiutang) {
            NilaiBayar = JumlahPiutang;
        } else {
            NilaiBayar = Bayar;
        }
        Sisa        = (JumlahPiutang-Bayar);
    } else {
        NilaiBayar = Bayar;
        Sisa       = JumlahPiutang;
    }

    myForm.bayarpiutang.value = formatter.format(NilaiBayar);
    
    if (Sisa > 0) {
        myForm.sisa.value = formatter.format(Sisa);
    } else {
        myForm.sisa.value = 0;
    }
}

function resetForm() {
    statusinput = 'Tambah';
    $('#penjualan_id').val('');
    $('#no_faktur').val('');
    $('#tanggalbeli').val('');
    $('#jumlahpiutang').val('');
    $('#bayarpiutang').val('');
    $('#sisa').val('');
    document.formBeli.bayarpiutang.disabled=true;
    $("#btn_item").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    var id = document.getElementById('pelanggan_id').value;
    dataPiutang(id);
    $('#no_faktur').focus();
    var MValid = $("#formBeli");
    MValid.validate().resetForm();
}

$(document).ready(function() {
    var form    = $('form');
    var error   = $('.alert-danger', form);
    var success = $('.alert-success', form);

    $("form").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_pelanggan: { required: true },
            lstTipeBayar: { required: true },
            no_faktur: { required: true },
            bayarpiutang: { required: true }
        },
        messages: {
            nama_pelanggan: { required :'Nama Pelanggan required' },
            lstTipeBayar: { required :'Tipe Bayar required' },
            no_faktur: { required :'No. Faktur required' },
            bayarpiutang: { required :'Jumlah Pembayaran required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        errorPlacement: function (error, element) {
            var icon = $(element).parent('.input-icon').children('i');
            icon.removeClass('fa-check').addClass("fa-warning");
            icon.attr("data-original-title", error.text()).tooltip({'container': 'body'});
        },
        highlight: function (element) {
            $(element)
            .closest('.form-group').removeClass("has-success").addClass('has-error');
        },
        unhighlight: function (element) {
        },
        success: function (label, element) {
            var icon = $(element).parent('.input-icon').children('i');
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            icon.removeClass("fa-warning").addClass("fa-check");
        },
        submitHandler: function(form) {
            if (statusinput == 'Tambah') {
                dataString = $(".form").serialize();
                $.ajax({
                    url: '<?=site_url('admin/piutang/saveitem');?>',
                    type: "POST",
                    data: dataString,
                    success: function(data) {
                        resetForm();
                        reload_table();
                        hitungTotal();
                    },
                    error: function() {
                        swal({
                            title:"Error",
                            text: "Simpan Item Gagal",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                    }
                });
            } else {
                dataString = $(".form").serialize();
                $.ajax({
                    url: '<?=site_url('admin/piutang/updateitem');?>',
                    type: "POST",
                    data: dataString,
                    success: function(data) {
                        resetForm();
                        reload_table();
                        hitungTotal();
                    },
                    error: function() {
                        swal({
                            title:"Error",
                            text: "Update Item Gagal",
                            timer: 2000,
                            showConfirmButton: false,
                            type: "error"
                        });
                    }
                });
            }
        }
    });
});

function hitungTotal() {
    $.ajax({
        url : "<?=site_url('admin/piutang/get_data_total_temp');?>",
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var Total;
            var TotalPiutang;
            Total     = 0;
            TotalPiutang = 0;
            if (data == null) {
                Total        = 0;
                TotalPiutang  = 0;
            } else {
                var locale      = 'en';
                var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                var formatter   = new Intl.NumberFormat(locale, options);
                TotalPiutang     = data.total;
                Total           = formatter.format(TotalPiutang);
            }

            $('#totaltransaksi').val(TotalPiutang);
            $('#totalpembayaran').text('Rp. '+Total);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get Total');
        }
    });
}

function edit_data(id) {
    statusinput = 'Edit';
    $.ajax({
        url : "<?=site_url('admin/piutang/get_data_item/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            var locale      = 'en';
            var options     = {minimumFractionDigits: 0, maximumFractionDigits: 0};
            var formatter   = new Intl.NumberFormat(locale, options);
            var options1    = {minimumFractionDigits: 2, maximumFractionDigits: 2};
            var formatter1  = new Intl.NumberFormat(locale, options1);
            $('#piutang_temp_id').val(data.piutang_temp_id);
            $('#penjualan_id').val(data.penjualan_id);
            $('#no_faktur').val(data.penjualan_no_faktur);
            document.formBeli.no_faktur.disabled=true;
            $('#tanggalbeli').val((data.penjualan_tanggal).split('-').reverse().join('-'));
            $('#jumlahpiutang').val(formatter.format(data.piutang_temp_total));
            $('#bayarpiutang').val(formatter.format(data.piutang_temp_bayar));
            $('#bayar_piutang_lama').val(data.piutang_temp_bayar);
            $('#sisa').val(formatter.format(data.piutang_temp_sisa));
            document.formBeli.bayarpiutang.disabled=false;
            document.formBeli.btn_item.disabled=false;
            $("#btn_reset").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

function hapusData(piutang_temp_id) {
    var id = piutang_temp_id;
    swal({
        title: 'Anda Yakin ?',
        text: 'Item ini akan di Hapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya',
        cancelButtonText: 'Batal',
        closeOnConfirm: true
    }, function(isConfirm) {
        if (!isConfirm) return;
        $.ajax({
            url : "<?=site_url('admin/piutang/deleteitem')?>/"+id,
            type: "POST",
            success: function(data) {
                resetForm();
                reload_table();
                hitungTotal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Hapus Item Gagal');
            }
        });
    });
}

function resetFormPiutang() {
    statusinput = 'Tambah';
    $('#pelanggan_id').val('');
    $('#nama_pelanggan').val('');
    $('#alamat').val('');
    $('#lstTipeBayar').val('');
    $('#penjualan_id').val('');
    $('#no_faktur').val('');
    $('#tanggalbeli').val('');
    $('#jumlahpiutang').val('');
    $('#bayarpiutang').val('');
    $('#sisa').val('');
    document.formBeli.bayarpiutang.disabled=true;
    document.formBeli.btn_item.disabled=false;
    $("#btn_simpan").attr("disabled", true);
    $("#btn_reset").attr("disabled", true);
    document.getElementById("btn_simpan").style.pointerEvents = "none";
    dataPelanggan();
    hitungTotal();
    var id = '99999999';
    dataPiutang(id);

    var MValid = $("form");
    MValid.validate().resetForm();
    MValid.find(".has-success, .has-warning, .fa-warning, .fa-check").removeClass("has-success has-warning fa-warning fa-check");
    MValid.find("i.fa[data-original-title]").removeAttr('data-original-title');
    document.getElementById("nama_pelanggan").focus();
}

$(document).ready(function() {
    var form        = $('form');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("form").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_pelanggan: { required: true }
        },
        messages: {
            nama_pelanggan: {
                required :'Nama Pelanggan required'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        }
    });

    $("#btn_simpan").click(function() {
        if($('form').valid()) {
            simpanTransaksi();
        }
    });
});

var statusprinter = '<?=$meta->meta_print_status;?>';
var printer       = new Recta('<?=$meta->meta_print_key;?>', '<?=$meta->meta_print_port;?>');
function simpanTransaksi() {
    dataString = $(".form").serialize();
    $.ajax({
        url: '<?=site_url('admin/piutang/savedata');?>',
        type: "POST",
        data: dataString,
        dataType: 'JSON',
        success: function(data) {
            if (data.id != '') {
                swal({
                    title:"Sukses",
                    text: "Simpan Pembayaran Piutang Sukses",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "success"
                });
                
                if (statusprinter == 2) {
                    var piutang_id = data.id;
                    $.ajax({
                        url: '<?=site_url('admin/piutang/get_data/');?>'+piutang_id,
                        type: "POST",
                        dataType: 'JSON',
                        success: function(datap1) {
                            var locale        = 'en';
                            var options       = {minimumFractionDigits: 0, maximumFractionDigits: 0};
                            var formatter     = new Intl.NumberFormat(locale, options);
                            var options1      = {minimumFractionDigits: 2, maximumFractionDigits: 2};
                            var formatter1    = new Intl.NumberFormat(locale, options1);
                            var NoOrder       = datap1.piutang_no_faktur;
                            var Tanggal       = datap1.piutang_tanggal;
                            var NamaPelanggan = datap1.pelanggan_nama;
                            var Kasir         = datap1.user_username;
                            Header(NoOrder, Tanggal, NamaPelanggan, Kasir);
                            $.ajax({
                                url: '<?=site_url('admin/piutang/get_list_item/');?>'+piutang_id,
                                type: "POST",
                                dataType: 'JSON',
                                success: function(dataitem1) {
                                    if (dataitem1 != null) {
                                        var x1 = dataitem1.length;
                                        for(var i = 0; i < x1; i++) {
                                            var NoFaktur   = dataitem1[i].penjualan_no_faktur;
                                            var TotalItem  = formatter.format(dataitem1[i].piutang_detail_total);
                                            var DibayarItem= formatter.format(dataitem1[i].piutang_detail_bayar);
                                            var Sisa       = formatter.format(dataitem1[i].piutang_detail_sisa);
                                            ListItem(NoFaktur, TotalItem, DibayarItem, Sisa);
                                        }

                                        var TipeBayar  = datap1.tipe_bayar_nama;
                                        var Total      = formatter.format(datap1.piutang_total);
                                        Footer(Total, TipeBayar);
                                        FooterEnd();
                                    }
                                }
                            });
                        }
                    });
                }
            } else {
                swal({
                    title:"Info",
                    text: "Data Penjualan Tidak Ada",
                    timer: 2000,
                    showConfirmButton: false,
                    type: "info"
                });
            }
            reload_table();
            resetFormPiutang();
        },
        error: function() {
            swal({
                title:"Error",
                text: "Simpan Pembayaran Piutang Gagal",
                timer: 2000,
                showConfirmButton: false,
                type: "error"
            });
            reload_table();
            resetFormPiutang();
        }
    });
    return false;
}

function Header(NoOrder, Tanggal, NamaPelanggan, Kasir) {
    var LimitChar = 36;
    if(NoOrder.length <= LimitChar) {
        txtNoOrder = NoOrder;
    } else {
        txtNoOrder = NoOrder.substring(0, LimitChar);
    }

    if(NamaPelanggan.length <= LimitChar) {
        txtNamaPelanggan = NamaPelanggan;
    } else {
        txtNamaPelanggan = NamaPelanggan.substring(0, LimitChar);
    }

    printer.open().then(function () {
      printer.align('center')
        .bold(true)
        .text('TOKO JATI BANGUN')
        .text('Jl. AKBP Agil Kusumadya No.110A, Jatiwetan')
        .text('KUDUS')
        .text('(0291)2911225')
        .bold(false)
        .text('------------------------------------------------')
        .print()
    })

    printer.open().then(function () {
      printer.align('left')
        .text('Bayar PTG : '+txtNoOrder)
        .text('Tanggal   : '+Tanggal.split("-").reverse().join("-"))
        .text('Pelanggan : '+txtNamaPelanggan)
        .text('Kasir     : '+Kasir)
        .text('------------------------------------------------')
        .text('NO. FAKTUR           TOTAL    DIBAYAR       SISA')
        .print()
    })
}

function ListItem(NoFaktur, TotalItem, DibayarItem, Sisa) {
    var limitNoFaktur = 15;
    var limitTotal    = 10;
    var limitDibayar  = 10;
    var limitSisa     = 10;
    var txtNoFaktur   = '';
    var txtTotal      = 0;
    var txtDibayar    = 0;
    var txtSisa       = 0;

    if(NoFaktur.length <= limitNoFaktur) {
        txtNoFaktur = NoFaktur.padEnd(limitNoFaktur, ' ')
    } else {
        txtNoFaktur = NoFaktur.substring(0, limitNoFaktur);
    }

    if (TotalItem.length <= limitTotal) {
        txtTotal = TotalItem.padStart(limitTotal, ' ')
    } else {
        txtTotal = TotalItem.substring(0, limitTotal);
    }

    if (DibayarItem.length <= limitDibayar) {
        txtDibayar = DibayarItem.padStart(limitDibayar, ' ')
    } else {
        txtDibayar = DibayarItem.substring(0, limitDibayar);
    }

    if (Sisa.length <= limitSisa) {
        txtSisa = Sisa.padStart(limitSisa, ' ')
    } else {
        txtSisa = Sisa.substring(0, limitSisa);
    }

    printer.open().then(function () {
      printer.align('left')
        .text(txtNoFaktur+" "+txtTotal+" "+txtDibayar+" "+txtSisa)
        .print()
    })
}

function Footer(Total, TipeBayar) {
    var limitNominal    = 13;
    var txtTotal        = 0;
    var txtTipeBayar    = '';

    if (Total.length <= limitNominal) {
        txtTotal = Total.padStart(limitNominal, ' ')
    } else {
        txtTotal = Total.substring(0, limitNominal);
    }

    if(TipeBayar.length <= limitNominal) {
        txtTipeBayar = TipeBayar.padStart(limitNominal, ' ')
    } else {
        txtTipeBayar = TipeBayar.substring(0, limitNominal);
    }

    printer.open().then(function () {
        printer.align('left')
        .text("                           TOTAL : "+txtTotal)
        .text("                      TIPE BAYAR : "+txtTipeBayar)
        .print()
    })
}

function FooterEnd() {
    printer.open().then(function () {
        printer.align('left')
        .text('')
        .feed(3)
        .cut()
        .print()
    })
}
</script>

<div class="modal fade bs-modal-lg" id="formCariPelanggan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-users"></i> Daftar Pelanggan</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataPelanggan">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="15%">Nama Pelanggan</th>
                            <th width="55%">Alamat</th>
                            <th width="15%">Kota</th>
                            <th width="10%">Total Piutang</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-modal-lg" id="formDataPiutang" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-shopping-cart"></i> Daftar Penjualan</h4>
            </div>

            <div class="modal-body">
                <table class="table table-striped table-hover" id="tableDataPiutang">
                    <thead>
                        <tr>
                            <th width="5%"></th>
                            <th width="5%">No</th>
                            <th width="50%">No. Faktur</th>
                            <th width="10%">Tanggal</th>
                            <th width="10%">Total</th>
                            <th width="10%">Dibayar</th>
                            <th width="10%">Sisa</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
$meta = $this->menu_m->select_meta()->row();
?>
<div class="login-container lightmode">
    <div class="login-box animated fadeInDown">
        <div class="login-logo"></div>
        <div class="login-body">
            <div class="login-title"><strong>Log In</strong> ke Aplikasi</div>
            <form  class="form-horizontal" method="post" name="formLogin" id="formLogin">
            <div class="form-group">
                <div class="col-md-12">
                    <input type="text" class="form-control" name="username" id="username" placeholder="Username Anda" autocomplete="off" required autofocus>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <input type="password" class="form-control" name="password" placeholder="Password Anda" autocomplete="off"/>
                </div>
            </div>
            <div class="alert alert-danger text-center" role="alert" id="msgHide"></div>
            <div class="form-group">
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6">
                    <button class="btn btn-danger btn-block"><i class="fa fa-sign-in"></i> Login</button>
                </div>
            </div>
            </form>
        </div>
        <div class="login-footer">
            <div class="pull-left">
                &copy; <?=date('Y');?> <?=$meta->meta_name;?>
            </div>
        </div>
    </div>
</div>

<script src="<?=base_url('backend/assets/global/plugins/jquery.min.js');?>" type="text/javascript"></script>
<script src="<?=base_url('backend/assets/global/plugins/jquery-migrate.min.js');?>" type="text/javascript"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[\w.@]+$/i.test(value);
}, "Hanya Huruf, Angka dan Garis Bawah");

$(document).ready(function() {
    $("#msgHide").hide();
}); 

$(document).ready(function() {
    var form        = $('#formLogin');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formLogin").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            username: { required: true, alphanumeric: true,
                remote: {
                    url: "<?=site_url('login/check_login_exists');?>",
                    type: "post",
                    data: {
                        username: function() {
                            return $("#username").val();
                        }
                    }
                } 
            },
            password: { required: true }
        },
        messages: {
            username: {
                required :'Username harus diisi', remote: 'Username Anda tidak terdaftar'
            },
            password: {
                required :'Password harus diisi'
            }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            dataString = $("#formLogin").serialize();
            $.ajax({
                url: '<?=site_url('login/validasi');?>',
                type: "POST",
                data: dataString,
                dataType: "JSON",
                success: function(data) {
                    if (data.status === 'success') {
                        location.reload();
                    } else if(data.status=== 'salah') {
                        $('#msgHide').show();
                        $('#msgHide').html('<div class="alert-text">'+data.msg+'</div>');
                    } else {
                        $('#msgHide').show();
                        $('#msgHide').html('<div class="alert-text">'+data.msg+'</div>');
                    }
                },
                error: function() {
                    $('#msgHide').show();
                    $('#msgHide').html('<div class="alert-text">Error Proses Login</div>');
                }
            });
        }
    });
});

</script>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<link rel="shortcut icon" href="<?=base_url('img/logo-icon.png');?>">
<title>Print Transaksi Kasir</title>
<style type="text/css">
    table {
        width: 100%;
        border-collapse: collapse;
    }
    
    tr, td {
        padding: 3px;
    }

    th {
        height: 20px;
        background-color: #eff3f8;
    }
    body{
        font-family: "Franklin Gothic Medium";
        font-size:12px;
    }
    h1{
        font-size:16px;
        font-weight: bold;
    }
    .page {
        width: 21cm;
        min-height: 29.7cm;
        padding: 0cm;
        margin: 0.1cm auto;
        border: 0.3px #D3D3D3 none;
        border-radius: 2px;
        background: white;
    }

    @media print{
        #comments_controls,
        #print-link{
            display:none;
        }
    }
</style>
</head>
<body>
<a href="#Print">
<img src="<?=base_url('img/print.png');?>" height="24" width="24" title="Print" id="print-link" onClick="window.print();return false;" />
</a>
<?php 
$periode = 'PERIODE : '.$this->uri->segment(4).' s/d '.$this->uri->segment(5);
?>
<div class="page">
    <table width="100%" align="center" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="top"><?='<b>'.$header->contact_name.'</b><br>'.$header->contact_address.'<br>No. Telp : '.$header->contact_phone.'<br>Email : '.$header->contact_email;?></td>
        </tr>
        <tr>
            <td align="center" valign="top"><hr style="height:2px; border-top:2px solid black; border-bottom:1px solid black;"></td>
        </tr>
        <tr>
            <td align="center" valign="top" style="font-size: 15px; font-weight: bold;"><u>TRANSAKSI KASIR</u></td>
        </tr>
        <tr>
            <td align="center" valign="top"><?=$periode;?></td>
        </tr>
    </table>
    <br>
    <table width="100%">
        <tr>
            <th width="5%" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">NO</th>
            <th style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">KASIR</th>
            <?php foreach($listTipe as $r) { ?>
            <th width="10%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->tipe_bayar_nama;?></th>
            <?php } ?>
            <th width="13%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">ONGKOS KIRIM</th>
            <th width="15%" style="border-top: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">SALDO AKHIR</th>
        </tr>
        <?php 
        $tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(4)));
        $tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(5)));
        $kasir      = $this->uri->segment(6);
        $total      = 0;
        $totalbayar = 0;
        $totalongkos= 0;
        $totaljual  = 0;
        while (strtotime($tgl_dari) <= strtotime($tgl_sampai)) {
        ?>
        <tr>
            <th colspan="<?=4+count($listTipe);?>" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;">TANGGAL : <?=date('d-m-Y', strtotime($tgl_dari));?></th>
        </tr>
        <?php
            $no         = 1;
            if ($kasir != 'all') {
                $listData = $this->db->order_by('user_name', 'asc')->get_where('ok_users', array('user_username' => $kasir))->result();
            } else {
                $listData = $this->db->order_by('user_name', 'asc')->get('ok_users')->result();
            }
            foreach($listData as $r) { 
                $user_username = $r->user_username;
                $dataPenjualan = $this->db->select_sum('penjualan_total', 'total')->select_sum('penjualan_ongkos', 'ongkos')->get_where('v_penjualan', array('user_username' => $user_username, 'penjualan_tanggal' => $tgl_dari))->row();
                $totaljual = ($dataPenjualan->total+$dataPenjualan->ongkos);
            ?>
            <tr>
                <td align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$no;?></td>
                <td style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=$r->user_username;?></td>
                <?php 
                foreach($listTipe as $t) { 
                    $tipe_bayar_id = $t->tipe_bayar_id;
                    $dataNominal   = $this->db->select_sum('penjualan_total', 'total')->get_where('v_penjualan', array('user_username' => $user_username, 'tipe_bayar_id' => $tipe_bayar_id, 'penjualan_tanggal' => $tgl_dari))->row();
                ?>
                <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($dataNominal->total,0,'',',');?></td>
                <?php
                } 
                ?>
                <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($dataPenjualan->ongkos,0,'',',');?></td>
                <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><?=number_format($totaljual,0,'',',');?></td>
            </tr>
            <?php 
                $total       = ($total+$dataPenjualan->total);
                $totalongkos = ($totalongkos+$dataPenjualan->ongkos);
                $no++;
            } 

            $tgl_dari = date("Y-m-d", strtotime("+1 day", strtotime($tgl_dari)));
        }
        ?>
        <tr>
            <td colspan="2" align="center" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b>TOTAL</b></td>
            <?php 
            $tgl_dari   = date('Y-m-d', strtotime($this->uri->segment(4)));
            $tgl_sampai = date('Y-m-d', strtotime($this->uri->segment(5)));
            foreach($listTipe as $t) { 
                $tipe_bayar_id = $t->tipe_bayar_id;
                $dataNominal   = $this->db->select_sum('penjualan_total', 'total')->get_where('v_penjualan', array('tipe_bayar_id' => $tipe_bayar_id, 'penjualan_tanggal >=' => $tgl_dari, 'penjualan_tanggal <=' => $tgl_sampai))->row();
            ?>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($dataNominal->total,0,'',',');?></b></td>
            <?php } ?>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($totalongkos,0,'',',');?></b></td>
            <td align="right" style="border-top: 0.5px solid black; border-left: 0.5px solid black; border-bottom: 0.5px solid black; border-right: 0.5px solid black;"><b><?=number_format($total+$totalongkos,0,'',',');?></b></td>
        </tr>
    </table>
</div>
</body>
</html>
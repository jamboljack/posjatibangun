<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelanggan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/pelanggan_m');
    }

    public function index()
    {
        $this->template->display('admin/master/pelanggan_v');
    }

    public function data_list()
    {
        $List = $this->pelanggan_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row          = array();
            $pelanggan_id = $r->pelanggan_id;
            $row[]        = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $pelanggan_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a onclick="hapusData(' . $pelanggan_id . ')" title="Delete Data">
                            <i class="icon-close"></i>
                        </a>';
            $row[]  = $no;
            $row[]  = $r->pelanggan_kode;
            $row[]  = $r->pelanggan_nama;
            $row[]  = $r->pelanggan_alamat . ' - ' . $r->pelanggan_kota;
            $row[]  = number_format($r->pelanggan_disc, 2, '.', ',');
            $row[]  = $r->pelanggan_telp;
            $row[]  = number_format($r->pelanggan_termin, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->pelanggan_m->count_all(),
            "recordsFiltered" => $this->pelanggan_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function kode_exists($kode)
    {
        $this->db->where('pelanggan_kode', $kode);
        $query = $this->db->get('ok_pelanggan');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->pelanggan_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->pelanggan_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->pelanggan_m->update_data();
    }

    public function deletedata($id)
    {
        $this->pelanggan_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Pelanggan.php */

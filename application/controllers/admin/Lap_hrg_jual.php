<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_hrg_jual extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/lap_hrg_jual_m');
    }

    public function index()
    {
        $data['listKategori'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        $this->template->display('admin/reportjual/reportjualharga_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_hrg_jual_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->barang_kode;
            $row[]  = $r->barang_nama;
            $row[]  = $r->kategori_nama;
            $row[]  = number_format($r->unit_qty, 2, '.', ',');
            $row[]  = $r->unit_nama;
            $row[]  = number_format($r->unit_hrg_jual, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_hrg_jual_m->count_all(),
            "recordsFiltered" => $this->lap_hrg_jual_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printharga($kategori = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($kategori != 'all') {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get_where('ok_kategori', array('kategori_id' => $kategori))->result();
        } else {
            $data['listData'] = $this->db->order_by('kategori_nama', 'asc')->get('ok_kategori')->result();
        }

        $this->load->view('admin/reportjual/printharga_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_hrg_jual.php */

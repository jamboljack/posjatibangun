<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_retur_jual_periode extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_retur_jual_periode_m');
    }

    public function index()
    {
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $data['listTipe']      = $this->db->order_by('tipe_bayar_nama', 'asc')->get('ok_tipe_bayar')->result();
        $this->template->display('admin/reportreturjual/reportreturjualperiode_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_retur_jual_periode_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->retur_jual_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->retur_jual_tanggal));
            $row[]  = $r->pelanggan_nama;
            $row[]  = ($r->tipe_bayar_set == 'D' ? '<span class="label label-success">' . $r->tipe_bayar_nama . '</span>' : '<span class="label label-danger">' . $r->tipe_bayar_nama . '</span>');
            $row[]  = number_format($r->retur_jual_ppn, 2, '.', ',');
            $row[]  = number_format($r->retur_jual_diskon, 0, '', ',');
            $row[]  = number_format($r->retur_jual_total, 0, '', ',');
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_retur_jual_periode_m->count_all(),
            "recordsFiltered" => $this->lap_retur_jual_periode_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printperiode($dari = 'all', $sampai = 'all', $pelanggan = 'all', $tipe = 'all')
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($dari != 'all' && $sampai != 'all' && $pelanggan == 'all' && $tipe == 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('retur_jual_tanggal >=' => $tgl_dari, 'retur_jual_tanggal <=' => $tgl_sampai))->result();
        } elseif ($dari == 'all' && $sampai == 'all' && $pelanggan != 'all' && $tipe == 'all') {
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('pelanggan_id' => $pelanggan))->result();
        } elseif ($dari == 'all' && $sampai == 'all' && $pelanggan == 'all' && $tipe != 'all') {
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('tipe_bayar_id' => $tipe))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $pelanggan != 'all' && $tipe == 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('retur_jual_tanggal >=' => $tgl_dari, 'retur_jual_tanggal <=' => $tgl_sampai, 'pelanggan_id' => $pelanggan))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $pelanggan == 'all' && $tipe != 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('retur_jual_tanggal >=' => $tgl_dari, 'retur_jual_tanggal <=' => $tgl_sampai, 'tipe_bayar_id' => $tipe))->result();
        } elseif ($dari != 'all' && $sampai != 'all' && $pelanggan != 'all' && $tipe != 'all') {
            $tgl_dari         = date('Y-m-d', strtotime($dari));
            $tgl_sampai       = date('Y-m-d', strtotime($sampai));
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get_where('v_retur_jual', array('retur_jual_tanggal >=' => $tgl_dari, 'retur_jual_tanggal <=' => $tgl_sampai, 'pelanggan_id' => $pelanggan, 'tipe_bayar_id' => $tipe))->result();
        } else {
            $data['listData'] = $this->db->order_by('retur_jual_tanggal', 'asc')->get('v_retur_jual')->result();
        }

        $this->load->view('admin/reportreturjual/printperiode_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_retur_jual_periode.php */

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_jual_rekap_pelanggan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
    }

    public function index()
    {
        $this->template->display('admin/reportjual/reportjualrekappelanggan_v');
    }

    public function printdata($dari, $sampai)
    {
        $data['header']   = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        $tgl_dari         = date('Y-m-d', strtotime($dari));
        $tgl_sampai       = date('Y-m-d', strtotime($sampai));
        $sql              = "call total_pelanggan('$tgl_dari','$tgl_sampai')";
        $data['listData'] = $this->db->query($sql)->result();
        $this->load->view('admin/reportjual/printrekappelanggan_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_jual_rekap_pelanggan.php */

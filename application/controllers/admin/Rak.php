<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rak extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_user();
        $this->load->library('template');
        $this->load->model('admin/rak_m');
    }

    public function index()
    {
        $this->template->display('admin/master/rak_v');
    }

    public function data_list()
    {
        $List = $this->rak_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row    = array();
            $rak_id = $r->rak_id;
            $row[]  = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $rak_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a onclick="hapusData(' . $rak_id . ')" title="Delete Data">
                            <i class="icon-close"></i>
                        </a>';
            $row[]  = $no;
            $row[]  = $r->rak_nama;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->rak_m->count_all(),
            "recordsFiltered" => $this->rak_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function savedata()
    {
        $this->rak_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->rak_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->rak_m->update_data();
    }

    public function deletedata($id)
    {
        $this->rak_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Rak.php */

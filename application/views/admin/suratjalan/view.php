<link href="<?=base_url('backend/js/sweetalert2.css');?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('backend/js/sweetalert2.min.js');?>"></script>

<div class="page-content-wrapper">
    <div class="page-content">
        <h3 class="page-title">Surat Jalan</h3>
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <i class="fa fa-home"></i>
                    <a href="<?=site_url('admin/home');?>">Dashboard</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Menu Pengiriman</a>
                    <i class="fa fa-angle-right"></i>
                </li>
                <li>
                    <a href="#">Surat Jalan</a>
                </li>
            </ul>
            <div class="page-toolbar">
                <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height blue-madison">
                    <i class="icon-calendar">&nbsp; </i><span class="uppercase visible-lg-inline-block"><?=tgl_indo(date('Y-m-d'));?></span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue-madison">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-list"></i> Daftar Pengiriman
                        </div>
                        <div class="actions">
                            <a data-toggle="modal" data-target="#filterData">
                                <button type="button" class="btn btn-warning btn-xs"><i class="fa fa-search"></i> Filter Data</button>
                            </a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-hover" id="tableData">
                            <thead>
                                <tr>
                                    <th width="3%"></th>
                                    <th width="5%">No</th>
                                    <th width="8%">No. Faktur</th>
                                    <th width="10%">Tanggal</th>
                                    <th>Nama Pelanggan</th>
                                    <th width="8%">Tipe Bayar</th>
                                    <th width="8%">Bruto</th>
                                    <th width="5%">Diskon</th>
                                    <th width="8%">Total</th>
                                    <th width="10%">Kasir</th>
                                    <th width="5%">Print</th>
                                    <th width="5%">Kirim</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js');?>"></script>
<script type="text/javascript" src="<?=base_url('backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
function reload_table() {
    table.ajax.reload(null,false);
}

var table;
$(document).ready(function() {
    table = $('#tableData').DataTable({
        "destroy": true,
        "responsive": true,
        "processing": false,
        "serverSide": true,
        "order": [],
        "lengthMenu": [
                [10, 20, 50, 75, 100, -1],
                [10, 20, 50, 75, 100, "All"]
        ],
        "pageLength": 10,
        "ajax": {
            "url": "<?=site_url('admin/suratjalan/data_list')?>",
            "type": "POST",
            "data": function(data) {
                data.lstPelanggan = $('#lstPelanggan').val();
                data.tgl_dari     = $('#tgl_dari').val();
                data.tgl_sampai   = $('#tgl_sampai').val();
                data.lstTipeBayar = $('#lstTipeBayar').val();
                data.lstKirim     = $('#lstKirim').val();
            }
        },
        "columnDefs": [
            {
                "targets": [ 0, 1 ],
                "orderable": false,
            },
            {
                "targets": [ 0, 1, 3, 5, 10, 11],
                "className": "text-center",
            },
            {
                "targets": [ 6, 7, 8 ],
                "className": "text-right",
            }
        ],
    });

    $('#btn-filter').click(function() {
        reload_table();
        $('#filterData').modal('hide');
    });

    $('#btn-reset').click(function() {
        $('#form-filter')[0].reset();
        reload_table();
        $('#filterData').modal('hide');
    });
});

function printSurat(penjualan_id) {
    var id = penjualan_id;
    $.ajax({
        url : "<?=site_url('admin/suratjalan/get_data/');?>"+id,
        type: "GET",
        dataType: "JSON",
        success: function(data) {
            $('#id').val(data.penjualan_id);
            $('#nama_pelanggan').val(data.penjualan_nama);
            $('#alamat_pelanggan').val(data.penjualan_alamat);
            $('#formModalEdit').modal('show');
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
        }
    });
}

$(document).ready(function() {
    var form        = $('#formEdit');
    var error       = $('.alert-danger', form);
    var success     = $('.alert-success', form);

    $("#formEdit").validate({
        errorElement: 'span',
        errorClass: 'help-block help-block-error',
        focusInvalid: false,
        ignore: "",
        rules: {
            nama_pelanggan: { required: true },
            alamat_pelanggan: { required: true }
        },
        messages: {
            nama_pelanggan: { required :'Nama Pelanggan required' },
            alamat_pelanggan: { required :'Alamat Pelanggan required' }
        },
        invalidHandler: function (event, validator) {
            success.hide();
            error.show();
            Metronic.scrollTo(error, -200);
        },
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        success: function (label) {
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function(form) {
            var id     = $('#id').val();
            dataString = $("#formEdit").serialize();
            $.ajax({
                url: '<?=site_url('admin/suratjalan/updatedatapelanggan');?>',
                type: "POST",
                data: dataString,
                success: function(data) {
                    reload_table();
                    $('#formModalEdit').modal('hide');
                    var url = "<?=site_url('admin/suratjalan/printsuratjalan/');?>"+id;
                    window.open(url, "_blank");
                },
                error: function() {
                    swal({
                        title:"Error",
                        text: "Update Data Gagal",
                        timer: 2000,
                        showConfirmButton: false,
                        type: "error"
                    });
                    $('#formModalEdit').modal('hide');
                }
            });
        }
    });
});

function setKirim(ex) {
    var id = $(ex).attr('id');
    if ($(ex).is(':checked')) {
        var isi = 'S';
    }else{
        var isi = 'B';
    }
    $.ajax({
        url : "<?=site_url('admin/suratjalan/updatestatus/');?>"+id+'/'+isi,
        type: "GET",
        success: function(data) {
            swal({
                title:"Sukses",
                text: "Pengiriman Barang Sukses",
                showConfirmButton: false,
                type: "success",
                timer: 2000
            });
        },
        error: function() {
            swal({
                title:"Gagal",
                text: "Pengiriman Barang Gagal",
                showConfirmButton: false,
                type: "error",
                timer: 2000
            });
        }
    });
}
</script>

<div class="modal" id="filterData" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="form-filter" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-search"></i> Filter Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Periode</label>
                        <div class="col-md-9">
                            <div class="input-group">
                                <input type="text" class="form-control date-picker" style="text-align: center;" name="tgl_dari" id="tgl_dari" placeholder="Dari Tanggal" data-date-format="dd-mm-yyyy" value="01-<?=date('m');?>-<?=date('Y');?>" autocomplete="off">
                                <span class="input-group-addon"><b>s/d</b></span>
                                <input type="text" class="form-control date-picker" style="text-align: center;" name="tgl_sampai" id="tgl_sampai" placeholder="Sampai Tanggal" data-date-format="dd-mm-yyyy" value="<?=date('d-m-Y');?>" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Pelanggan</label>
                        <div class="col-md-9">
                            <select class="form-control" name="lstPelanggan" id="lstPelanggan">
                                <option value="">- SEMUA DATA -</option>
                                <?php foreach ($listPelanggan as $r) {?>
                                <option value="<?=$r->pelanggan_id;?>"><?=$r->pelanggan_nama;?></option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" id="btn-filter"><i class="fa fa-search"></i> Filter</button>
                    <button type="button" class="btn btn-default" id="btn-reset"><i class="fa fa-refresh"></i> Reset</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal" id="formModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" method="post" id="formEdit" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="fa fa-truck"></i> Form Input Surat Jalan</h4>
                    <input type="hidden" name="id" id="id">
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-4 control-label">Nama Pelanggan</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" placeholder="Input Nama Pelanggan" name="nama_pelanggan" id="nama_pelanggan" autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Alamat Pelanggan</label>
                        <div class="col-md-8">
                            <textarea class="form-control" placeholder="Input Alamat Pelanggan" name="alamat_pelanggan" id="alamat_pelanggan" autocomplete="off" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Update</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Utility extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('download');
        $this->load->helper('file');
        $this->load->library('template');
    }

    public function index()
    {
        redirect(base_url());
    }

    public function backup()
    {
        $this->load->dbutil();
        $prefs = array(
            'format'   => 'zip',
            'filename' => 'db_jatibangun-' . date('Ymd') . '.sql',
        );

        $backup  = &$this->dbutil->backup($prefs);
        $db_name = 'backup-db-' . date("Y-m-d-H-i-s") . '.zip'; // file name
        $save    = 'download/' . $db_name;
        write_file($save, $backup);
        force_download($db_name, $backup);
    }
}
/* Location: ./application/controller/Utility.php */

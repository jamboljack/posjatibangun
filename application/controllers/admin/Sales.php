<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/sales_m');
    }

    public function index()
    {
        $this->template->display('admin/master/sales_v');
    }

    public function data_list()
    {
        $List = $this->sales_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];

        foreach ($List as $r) {
            $no++;
            $row      = array();
            $sales_id = $r->sales_id;
            $row[]    = '<a title="Edit Data" href="javascript:void(0)" onclick="edit_data(' . "'" . $sales_id . "'" . ')"><i class="icon-pencil"></i></a>
                        <a onclick="hapusData(' . $sales_id . ')" title="Delete Data">
                            <i class="icon-close"></i>
                        </a>';
            $row[]  = $no;
            $row[]  = date('d-m-Y', strtotime($r->sales_tgl_kerja));
            $row[]  = $r->sales_kode;
            $row[]  = $r->sales_nama;
            $row[]  = $r->sales_alamat . ' - ' . $r->sales_kota;
            $row[]  = $r->sales_telp;
            $row[]  = $r->sales_wilayah;
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->sales_m->count_all(),
            "recordsFiltered" => $this->sales_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    private function kode_exists($kode)
    {
        $this->db->where('sales_kode', $kode);
        $query = $this->db->get('ok_sales');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function register_kode_exists()
    {
        if (array_key_exists('kode', $_POST)) {
            if ($this->kode_exists($this->input->post('kode', 'true')) == true) {
                echo json_encode(false);
            } else {
                echo json_encode(true);
            }
        }
    }

    public function savedata()
    {
        $this->sales_m->insert_data();
    }

    public function get_data($id)
    {
        $data = $this->sales_m->select_by_id($id)->row();
        echo json_encode($data);
    }

    public function updatedata()
    {
        $this->sales_m->update_data();
    }

    public function deletedata($id)
    {
        $this->sales_m->delete_data($id);
    }
}
/* Location: ./application/controller/admin/Sales.php */

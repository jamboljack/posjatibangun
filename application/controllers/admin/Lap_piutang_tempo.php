<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lap_piutang_tempo extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_auth_admin();
        $this->load->library('template');
        $this->load->model('admin/lap_piutang_tempo_m');
    }

    public function index()
    {
        $data['listPelanggan'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        $this->template->display('admin/reportpiutang/reportpiutangtempo_v', $data);
    }

    public function data_list()
    {
        $List = $this->lap_piutang_tempo_m->get_datatables();
        $data = array();
        $no   = $_POST['start'];
        foreach ($List as $r) {
            $no++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $r->pelanggan_nama;
            $row[]  = $r->penjualan_no_faktur;
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tanggal));
            $row[]  = number_format($r->penjualan_total, 0, '', ',');
            $row[]  = number_format($r->penjualan_dibayar, 2, '.', ',');
            $row[]  = number_format($r->penjualan_sisa_piutang, 0, '', ',');
            $row[]  = date('d-m-Y', strtotime($r->penjualan_tgl_tempo));
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->lap_piutang_tempo_m->count_all(),
            "recordsFiltered" => $this->lap_piutang_tempo_m->count_filtered(),
            "data"            => $data,
        );

        echo json_encode($output);
    }

    public function printdata($pelanggan = 'all', $tgl_dari, $tgl_sampai)
    {
        $data['header'] = $this->db->get_where('ok_contact', array('contact_id' => 1))->row();
        if ($pelanggan != 'all') {
            $data['listData'] = $this->db->order_by('pelanggan_nama', 'asc')->get_where('ok_pelanggan', array('pelanggan_id' => $pelanggan))->result();
        } else {
            $data['listData'] = $this->db->order_by('pelanggan_nama', 'asc')->get('ok_pelanggan')->result();
        }

        $this->load->view('admin/reportpiutang/printtempo_v', $data);
    }
}
/* Location: ./application/controller/admin/Lap_piutang_tempo.php */

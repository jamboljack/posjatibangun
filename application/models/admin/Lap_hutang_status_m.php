<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Lap_hutang_status_m extends CI_Model
{
    public $table        = 'v_pembelian';
    public $column_order = array(null, 'suplier_nama', 'pembelian_no_faktur',
        'pembelian_tanggal', 'pembelian_total', 'pembelian_dibayar', 'pembelian_sisa_hutang', null);
    public $column_search = array('suplier_nama', 'pembelian_no_faktur',
        'pembelian_tanggal', 'pembelian_total', 'pembelian_dibayar', 'pembelian_sisa_hutang');
    public $order = array('pembelian_tanggal' => 'asc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        if ($this->input->post('lstStatus', 'true')) {
            $status = $this->input->post('lstStatus', 'true');
            if ($status == 1) {
                $this->db->where('pembelian_sisa_hutang !=', 0);
            } else {
                $this->db->where('pembelian_sisa_hutang', 0);
            }
        }
        
        $this->db->from($this->table);
        $this->db->where('tipe_bayar_set', 'K');

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        $this->db->where('tipe_bayar_set', 'K');
        return $this->db->count_all_results();
    }
}
/* Location: ./application/model/admin/Lap_hutang_status_m.php */

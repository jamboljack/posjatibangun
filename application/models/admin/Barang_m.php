<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang_m extends CI_Model
{
    public $table        = 'v_barang';
    public $column_order = array(null, null, 'barang_kode', 'barang_nama', 'kategori_nama', 'barang_merk', 'unit_qty',
        'unit_nama', 'unit_hrg_pokok', 'barang_tgl_beli', 'suplier_nama');
    public $column_search = array('barang_kode', 'barang_nama', 'kategori_nama', 'barang_merk');
    public $order         = array('barang_kode' => 'asc');

    public $table1        = 'v_unit';
    public $column_order1 = array(null, null, 'unit_nama', 'unit_multiple', 'unit_head', 'unit_qty', 'unit_hrg_beli', 'unit_disc1', 'unit_disc1',
        'unit_disc3', 'unit_disc4', 'unit_ppn', 'unit_hrg_pokok', 'unit_hrg_jual', 'unit_jml_jual', 'unit_set');
    public $column_search1 = array('unit_nama');
    public $order1         = array('unit_set' => 'desc');

    public $table2        = 'ok_unit_log';
    public $column_order2 = array(null, 'log_date', 'log_harga_beli_lama', 'log_harga_beli_baru',
        'log_harga_beli_persen', 'log_harga_pokok_lama', 'log_harga_pokok_baru', 'log_harga_jual_lama', 'log_harga_jual_baru', 'user_username');
    public $column_search2 = array('log_date', 'log_harga_beli_lama', 'log_harga_beli_baru',
        'log_harga_beli_persen', 'log_harga_jual_lama', 'log_harga_jual_baru', 'user_username');
    public $order2 = array('log_id' => 'desc');

    public function __construct()
    {
        parent::__construct();
    }

    private function _get_datatables_query()
    {
        if ($this->input->post('lstKategoriFilter', 'true')) {
            $this->db->where('kategori_id', $this->input->post('lstKategoriFilter', 'true'));
        }
        if ($this->input->post('lstSuplier', 'true')) {
            $this->db->where('suplier_id', $this->input->post('lstSuplier', 'true'));
        }
        if ($this->input->post('lstRak', 'true')) {
            $this->db->where('rak_id', $this->input->post('lstRak', 'true'));
        }

        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // public function getkodeurut()
    // {
    //     $this->db->select('barang_kode as kode', false);
    //     $this->db->limit(1);
    //     $this->db->order_by('barang_kode', 'desc');
    //     $query = $this->db->get('ok_barang');
    //     if ($query->num_rows() != 0) {
    //         $data = $query->row();
    //         $kode = intval($data->kode) + 1;
    //     } else {
    //         $kode = 1;
    //     }
    //     $kodebarang = str_pad($kode, 5, "0", STR_PAD_LEFT);
    //     return $kodebarang;
    // }

    public function insert_data()
    {
        if (!empty($_FILES['foto']['name'])) {
            $data = array(
                'barang_kode'     => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
                'barang_nama'     => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_seo'      => seo_title(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_merk'     => strtoupper(trim(stripHTMLtags($this->input->post('merk', 'true')))),
                'kategori_id'     => trim($this->input->post('lstKategori', 'true')),
                'rak_id'          => trim($this->input->post('lstRak', 'true')),
                'barang_min_stok' => str_replace(",", "", $this->input->post('minstok', 'true')),
                'barang_foto'     => $this->upload->file_name,
                'barang_update'   => date('Y-m-d H:i:s'),
            );
        } else {
            $data = array(
                'barang_kode'     => strtoupper(trim(stripHTMLtags($this->input->post('kode', 'true')))),
                'barang_nama'     => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_seo'      => seo_title(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_merk'     => strtoupper(trim(stripHTMLtags($this->input->post('merk', 'true')))),
                'kategori_id'     => trim($this->input->post('lstKategori', 'true')),
                'rak_id'          => trim($this->input->post('lstRak', 'true')),
                'barang_min_stok' => str_replace(",", "", $this->input->post('minstok', 'true')),
                'barang_update'   => date('Y-m-d H:i:s'),
            );
        }

        $this->db->insert('ok_barang', $data);
        $barang_id = $this->db->insert_id();

        // Insert ke Table Price Stock
        $dataStock = array(
            'barang_id'      => $barang_id,
            'unit_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('unit', 'true')))),
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('harga_beli', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('disc1', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('disc2', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('disc3', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('disc4', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('ppn', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('harga_jual', 'true'))),
            'unit_set'       => 1,
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit', $dataStock);
        $unit_id = $this->db->insert_id();

        // Update Satuan Barang
        $dataUnit = array(
            'unit_id' => $unit_id,
        );

        $this->db->where('barang_id', $barang_id);
        $this->db->update('ok_barang', $dataUnit);

        // Insert ke Log Price
        $dataLog = array(
            'unit_id'               => $unit_id,
            'log_date'              => date('Y-m-d'),
            'log_harga_beli_lama'   => 0,
            'log_harga_beli_baru'   => intval(str_replace(",", "", $this->input->post('harga_beli', 'true'))),
            'log_harga_pokok_lama'  => 0,
            'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
            'log_harga_beli_persen' => 0,
            'log_harga_jual_lama'   => 0,
            'log_harga_jual_baru'   => intval(str_replace(",", "", $this->input->post('harga_jual', 'true'))),
            'log_harga_jual_persen' => 0,
            'user_username'         => $this->session->userdata('username'),
            'log_update'            => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit_log', $dataLog);
    }

    public function update_data()
    {
        $barang_id = $this->input->post('id', 'true');
        if (!empty($_FILES['foto']['name'])) {
            $data = array(
                'barang_nama'     => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_seo'      => seo_title(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'kategori_id'     => trim($this->input->post('lstKategori', 'true')),
                'rak_id'          => trim($this->input->post('lstRak', 'true')),
                'barang_merk'     => strtoupper(trim(stripHTMLtags($this->input->post('merk', 'true')))),
                'barang_min_stok' => str_replace(",", "", $this->input->post('minstok', 'true')),
                'barang_foto'     => $this->upload->file_name,
                'barang_update'   => date('Y-m-d H:i:s'),
            );
        } else {
            $data = array(
                'barang_nama'     => strtoupper(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'barang_seo'      => seo_title(trim(stripHTMLtags($this->input->post('nama', 'true')))),
                'kategori_id'     => trim($this->input->post('lstKategori', 'true')),
                'rak_id'          => trim($this->input->post('lstRak', 'true')),
                'barang_merk'     => strtoupper(trim(stripHTMLtags($this->input->post('merk', 'true')))),
                'barang_min_stok' => str_replace(",", "", $this->input->post('minstok', 'true')),
                'barang_update'   => date('Y-m-d H:i:s'),
            );
        }

        $this->db->where('barang_id', $barang_id);
        $this->db->update('ok_barang', $data);
    }

    public function delete_data($id)
    {
        $this->db->where('barang_id', $id);
        $this->db->delete('ok_barang');
    }

    private function _get_unit_datatables_query($barang_id)
    {
        $this->db->from($this->table1);
        $this->db->where('barang_id', $barang_id);

        $i = 0;
        foreach ($this->column_search1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search1) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order1[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order1)) {
            $order = $this->order1;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_datatables($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_filtered($barang_id)
    {
        $this->_get_unit_datatables_query($barang_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_all($barang_id)
    {
        $this->db->from($this->table1);
        $this->db->where('barang_id', $barang_id);

        return $this->db->count_all_results();
    }

    public function insert_data_unit()
    {
        // Insert ke Table Price
        $dataPrice = array(
            'barang_id'      => $this->input->post('barang_id', 'true'),
            'unit_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('unit', 'true')))),
            'unit_multiple'  => str_replace(",", "", $this->input->post('multiple', 'true')),
            'unit_id_head'   => $this->input->post('lstUnitHead', 'true'),
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('harga_beli', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('disc1', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('disc2', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('disc3', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('disc4', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('ppn', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('harga_jual', 'true'))),
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit', $dataPrice);
        $unit_id = $this->db->insert_id();

        // Insert ke Log Price
        $dataLog = array(
            'unit_id'               => $unit_id,
            'log_date'              => date('Y-m-d'),
            'log_harga_beli_lama'   => 0,
            'log_harga_beli_baru'   => intval(str_replace(",", "", $this->input->post('harga_beli', 'true'))),
            'log_harga_pokok_lama'  => 0,
            'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
            'log_harga_beli_persen' => 0,
            'log_harga_jual_lama'   => 0,
            'log_harga_jual_baru'   => intval(str_replace(",", "", $this->input->post('harga_jual', 'true'))),
            'log_harga_jual_persen' => 0,
            'user_username'         => $this->session->userdata('username'),
            'log_update'            => date('Y-m-d H:i:s'),
        );

        $this->db->insert('ok_unit_log', $dataLog);
    }

    public function update_data_unit()
    {
        $unit_id = $this->input->post('id', 'true');
        $data    = array(
            'unit_nama'      => strtoupper(trim(stripHTMLtags($this->input->post('unit', 'true')))),
            'unit_multiple'  => str_replace(",", "", $this->input->post('multiple', 'true')),
            'unit_hrg_beli'  => intval(str_replace(",", "", $this->input->post('harga_beli', 'true'))),
            'unit_disc1'     => str_replace(",", "", $this->input->post('disc1', 'true')),
            'unit_disc2'     => str_replace(",", "", $this->input->post('disc2', 'true')),
            'unit_disc3'     => str_replace(",", "", $this->input->post('disc3', 'true')),
            'unit_disc4'     => str_replace(",", "", $this->input->post('disc4', 'true')),
            'unit_ppn'       => str_replace(",", "", $this->input->post('ppn', 'true')),
            'unit_hrg_pokok' => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
            'unit_hrg_jual'  => intval(str_replace(",", "", $this->input->post('harga_jual', 'true'))),
            'unit_update'    => date('Y-m-d H:i:s'),
        );

        $this->db->where('unit_id', $unit_id);
        $this->db->update('ok_unit', $data);

        // Log harga Beli
        $unit_buy_old = $this->input->post('unit_hrg_beli_old', 'true');
        $unit_buy_new = intval(str_replace(",", "", $this->input->post('harga_beli', 'true')));
        // Log harga jual
        $unit_sell_old = $this->input->post('unit_hrg_jual_old', 'true');
        $unit_sell_new = intval(str_replace(",", "", $this->input->post('harga_jual', 'true')));

        if ($unit_buy_old != $unit_buy_new || $unit_sell_old != $unit_sell_new) {
            $margin_buy   = ($unit_buy_new - $unit_buy_old);
            $percent_buy  = (($margin_buy / $unit_buy_old) * 100);
            $margin_sell  = ($unit_sell_new - $unit_sell_old);
            $margin       = @($margin_sell / $unit_sell_old);
            $percent_sell = ($margin * 100);
            $dataLog      = array(
                'unit_id'               => $unit_id,
                'log_date'              => date('Y-m-d'),
                'log_harga_beli_lama'   => $unit_buy_old,
                'log_harga_beli_baru'   => $unit_buy_new,
                'log_harga_beli_persen' => $percent_buy,
                'log_harga_pokok_lama'  => $this->input->post('unit_hrg_pokok_old', 'true'),
                'log_harga_pokok_baru'  => intval(str_replace(",", "", $this->input->post('harga_pokok', 'true'))),
                'log_harga_jual_lama'   => $unit_sell_old,
                'log_harga_jual_baru'   => $unit_sell_new,
                'log_harga_jual_persen' => $percent_sell,
                'user_username'         => $this->session->userdata('username'),
                'log_update'            => date('Y-m-d H:i:s'),
            );

            $this->db->insert('ok_unit_log', $dataLog);
        }
    }

    private function _get_unit_log_datatables_query($unit_id)
    {
        $this->db->from($this->table2);
        $this->db->where('unit_id', $unit_id);

        $i = 0;
        foreach ($this->column_search2 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search2) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order2)) {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_unit_log_datatables($unit_id)
    {
        $this->_get_unit_log_datatables_query($unit_id);
        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function count_unit_log_filtered($unit_id)
    {
        $this->_get_unit_log_datatables_query($unit_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_unit_log_all($unit_id)
    {
        $this->db->from($this->table2);
        $this->db->where('unit_id', $unit_id);

        return $this->db->count_all_results();
    }
}
/* Location: ./application/models/admin/Barang_m.php */
